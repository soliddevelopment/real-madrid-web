<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NoSpaces implements Rule
{
    public function __construct()
    {
        //
    }
    
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        return preg_match('/^\S*$/u', $value);
    }
    
    public function message()
    {
        return 'No deben haber espacios en el campo :attribute';
    }
}
