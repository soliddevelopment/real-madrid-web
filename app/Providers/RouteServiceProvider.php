<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     * In addition, it is set as the URL generator's root namespace.
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        $this->mapGeneral();
        $this->mapApi();
        $this->mapAdminFijas();
        $this->mapAdminCustom();
        $this->mapPublico();
    }

    protected function mapGeneral()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /*protected function mapPublico()
    {
        Route::middleware('web')
            ->namespace($this->namespace.'\Publico')
            ->group(base_path('routes/publico/custom.php'));
    }*/

    protected function mapPublico()
    {
        $archivos = [
            'routes/publico/auth.php',
            'routes/publico/custom.php',
        ];

        Route::group([
            'middleware' => 'publico',
            'namespace' => $this->namespace . '\Publico',
        ], function ($router) use ($archivos) {
            foreach ($archivos as $item) {
                require base_path($item);
            }
        });
    }

    protected function mapApi()
    {
        $archivos = [
            'routes/api/fijas/misc.php',
            'routes/api/custom/misc.php',
        ];

        Route::group([
            'prefix' => 'api',
            'middleware' => 'api',
            'namespace' => $this->namespace.'\Api',
        ], function ($router) use($archivos) {
            foreach($archivos as $item){
                require base_path($item);
            }
        });

        /*Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace.'\Api')
             ->group(base_path('routes/api/fijas.php'));*/
    }

    /*protected function mapApiCustom()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace.'\Api')
            ->group(base_path('routes/api/custom.php'));
    }*/

    protected function mapAdminFijas()
    {
        $archivos = [
            'routes/admin/fijas/misc.php',
            'routes/admin/fijas/media.php',
            'routes/admin/fijas/usuarios.php',
            'routes/admin/fijas/configs.php',
            'routes/admin/fijas/secciones.php',
            'routes/admin/fijas/posts.php',
            'routes/admin/fijas/categorias.php',
            'routes/admin/fijas/extras.php',
            'routes/admin/fijas/contactos.php',
            'routes/admin/fijas/contenidos.php',
            'routes/admin/fijas/galleries.php',
        ];
        Route::group([
            'middleware' => 'admin',
            'namespace' => $this->namespace.'\Admin',
        ], function ($router) use($archivos) {
            foreach($archivos as $item){
                require base_path($item);
            }
        });
    }

    protected function mapAdminCustom()
    {
        Route::middleware('admin')
            ->namespace($this->namespace.'\Admin')
            ->group(base_path('routes/admin/custom/misc.php'));
    }

}
