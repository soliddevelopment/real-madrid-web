<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

use Validator;
use Auth;
use Route;

use App\Models\Fijos\User;
use App\Models\Fijos\Config;
use App\Models\Fijos\Seccion;
use App\Models\Fijos\Extra;
use App\Models\Fijos\Gallery;
use App\Models\Fijos\Post;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);
    
        Validator::extend(
            'recaptcha',
            'App\\Validators\\ReCaptcha@validate'
        );

        // CODIGO INTERMEDIO PARA TODOS LOS QUE VAYAN A USAR EL LAYOUT ADMIN.MAIN

        app('view')->composer('*', function ($view) {

            $route = app('request')->route();

            if($route != null){

                $configs = Config::all();
                $route_name = Route::currentRouteName();

                $userId = Auth::id();
                if($userId){
                    $usuario = User::find($userId);
                } else {
                    $user_id = null;
                    $usuario = null;
                }

                if($usuario){
                    $seccionesActivas = Seccion::getSeccionesPricipales();
                } else {
                    $seccionesActivas = [];
                }

                $contenidosGlobalesArray = ['urlcomofunciona'];
                $contenidosGlobales = Extra::getContenidosFromArray($contenidosGlobalesArray);

                $action = $route->getAction();
                $controller = class_basename($action['controller']);
                list($controller, $action) = explode('@', $controller);
                $compactController = compact('controller', 'action');

                // CUSTOM:

                $view->with('controller', $compactController['controller']);
                $view->with('usuario', $usuario ? $usuario : null);
                $view->with('route_name', $route_name ? $route_name : null);
                $view->with('configs', $configs);
                $view->with('seccionesActivas', $seccionesActivas);
                $view->with('contenidosGlobales', $contenidosGlobales);

            }


        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
