<?php

namespace App\Providers;
use App\Policies\CandidatoPolicy;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Models\Fijos\User;
use App\Models\Custom\Empresa;
use App\Models\Custom\Oferta;
use App\Models\Custom\Candidato;

use App\Policies\UserPolicy;
use App\Policies\EmpresaPolicy;
use App\Policies\OfertaPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Empresa::class => EmpresaPolicy::class,
        Oferta::class => OfertaPolicy::class,
        Candidato::class => CandidatoPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
    }
}
