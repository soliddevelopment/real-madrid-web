<?php

namespace App\Models\Fijos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DB;
use Validator;

use App\Http\Traits\Crud;
use App\Models\Base;
use App\Mail\EmailContacto;
//use App\Mail\EmailGenerico;
//use App\Models\Fijos\Config;

class Contacto extends Base
{
    use Crud;

    protected $table = 'contactos';

    public static function store(Request $request){
        $validator = Validator::make($request->all(), [
            // 'slug' => 'required', Rule::unique('secciones')->ignore($id), new NoSpaces,
            'nombre' => 'required|string|max:255',
            'email' => 'required|email',
            'g-recaptcha-response'=>'required|recaptcha'
        ], [
            'g-recaptcha-response.required' => 'El captcha check falló'
        ]);

        $elem = new Contacto;

        if ($validator->fails()) {
            return self::errorResponse('Error en los datos enviados', 'VALIDATOR', null, $validator->errors());

        } else {

            $elem->nombre = $request->input('nombre');
//            $elem->apellido = $request->input('apellido');
//            $request->input('sexo') == null ? $elem->sexo = 0 : $elem->sexo = $request->input('sexo');
//            $elem->cedula = $request->input('cedula');
            $elem->empresa = $request->input('empresa');
//            $elem->puesto = $request->input('puesto');
            $elem->telefono = $request->input('telefono');
            $elem->email = $request->input('email');
//            $elem->direccion = $request->input('direccion');
            $elem->mensaje = $request->input('mensaje');
//            $elem->campo1 = $request->input('campo1');


            $source = null;
            if($request->input('source') == 'fb' || $request->input('source') == 'google'){
                $source = $request->input('source');
            }
            $elem->source = $source;

            $elem->save();

            if($configEmailContacto = Config::where('slug', 'email-contacto')->first()) {
                $emailContacto = $configEmailContacto->getVal();
                Mail::to($emailContacto)->send(new EmailContacto($elem));

                /*Mail::to($emailContacto)->send(new EmailGenerico(
                    'Nuevo mensaje',
                    'Nuevo mensaje de contacto',
                    'Recibiste un nuevo mensaje de contacto en tu sitio web.',
                    asset('/p/img/logo-email.png'),
                    route('admin.contactos.detalle', $elem->id),
                    'Ir a mensaje',
                    'noreply@solid.com.sv',
                    'Solid'
                ));*/
            }
            return self::successResponse($elem, $validator);
        }
    }
}
