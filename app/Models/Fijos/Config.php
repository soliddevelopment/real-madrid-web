<?php

namespace App\Models\Fijos;
use DB;

use App\Models\Base;

class Config extends Base
{
    protected $table = 'configs';
    protected $tienePermisos = true;
    
    public function getVal()
    {
        switch($this->tipo){
            case 'IMG':
                $val = $this->valor_img;
                break;
            case 'STR':
                $val = $this->valor_str;
                break;
            case 'INT':
                $val = $this->valor_int;
                break;
            case 'BOOL':
                $val = $this->valor_bool;
                break;
        }
        return $val;
    }
    
}
