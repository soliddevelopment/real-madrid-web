<?php

namespace App\Models\Fijos;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

use App\Models\Base;

class Categoria extends Base
{
    use SoftDeletes;

    protected $table = 'categorias';

    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    /*public function subcategorias(){
        return $this->hasMany('App\Models\Subcategoria');
    }*/
}
