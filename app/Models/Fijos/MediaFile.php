<?php

namespace App\Models\Fijos;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Image;
use Storage;
use Validator;
use File;
use Carbon\Carbon;

use App\Http\Traits\Crud;
use App\Models\Base;

class MediaFile extends Base
{
    use SoftDeletes, Crud;

    protected $table = 'media_files';
    public static $cols = ['media_files.id', 'media_files.folder_id', 'media_files.mime_type', 'media_files.nombre', 'media_files.filename'];

    public function post(){
        return $this->belongsTo('App\Models\Fijos\Post', 'img_id');
    }

    // Para obtener lista programaticamente
    public static function getListaFiles($items=null, $q=null, $mimetypes=null, $order=null, $folder_actual=null){
        $cols = ['id', 'folder_id', 'mime_type', 'nombre', 'filename', 'created_by', 'created_at'];

        if($items == null) $items = 25;

        $files = MediaFile::whereNull('deleted_at');
        //$files = DB::table('media_files')->whereNull('deleted_at');

        if( $q && strlen($q) > 0 ){
            $files = $files->where('nombre', 'like', "%{$q}%");
        }

        if( $mimetypes ){
            $mimeTypesArray = explode(',', $mimetypes);
            $files = $files->whereIn('mime_type', $mimeTypesArray);
        }

        if($folder_actual == null){
            $files = $files->whereNull('folder_id');
        } else {
            $files = $files->where('folder_id', $folder_actual);
        }

        //DB::table('media_files')->whereIn('id', $archivos_arr)->delete();

        if( $order ){
            switch($order){
                case 'fecha-desc':
                    $files = $files->orderBy('id', 'desc');
                    break;
                case 'fecha-asc':
                    $files = $files->orderBy('id', 'asc');
                    break;
                case 'nombre':
                    $files = $files->orderBy('nombre', 'asc');
                    break;
            }
        }
        $files = $files->paginate($items);
        foreach($files as $file){
            $file = MediaFile::datosAdicionales($file);
        }
        return $files;
    }

    // Se le manda el request
    public static function getLista($request){
        return self::getListaFiles($request->input('items'), $request->input('q'), $request->input('mimetypes'), $request->input('order'), $request->input('folder_actual'));
    }

    public static function datosAdicionales($mediafile=null){
        if($mediafile !== null){
            $mediafile->getSetFileRoute();
            $mediafile->selected = false;
            return $mediafile;
        }
    }

    public function getSetFileRoute() {
        $this->route = asset('storage/mediamanager/'.$this->filename);
    }

    public function getExtension() {
        $mimeTypeArr = explode('/', $this->mime_type);
        $ext = strtolower($mimeTypeArr[1]);
        if($ext === 'jpeg') $ext = 'jpg';
        return $ext;
    }

    public static function postUpload($request, $returnJson=false){
        $validator = Validator::make($request->all(), [
            //'file' => 'required|mimes:jpeg,jpg,bmp,png,gif',
            //'file' => 'required|image',
            'file' => 'required',
        ]);
        if ($validator->fails()) {
            return self::errorResponse('Error de validación', 'VALIDATOR', null, $validator->errors(), null);
        }
        $mediafile = new MediaFile;
        return MediaFile::upload($request, $mediafile, $returnJson);
    }

    public static function saveFile($file, $filename=null) {

        $original_ext = $file->guessExtension();

        if($filename === null || strlen($filename) === 0){
            $original_name = $file->getClientOriginalName();
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $original_name);
            $original_ext = $file->guessExtension();
            $filename = 'media-'.time().'-'.clean($withoutExt).'.'.$original_ext;
        }
        
        $newpath = Storage::putFileAs(
            'public'.'/'.'mediamanager'.'/', $file, $filename
        );

        // $newpath = Storage::disk('local')->put('public'.'/'.'mediamanager'.'/'.$filename, $img, 'public');

        return [
            'filename' => $filename,
            'route' => asset('storage/mediamanager/'.$filename),
            'newpath' => $newpath
        ];
    }

    // GUARDA UNA IMAGEN EN EL DISCO
    public static function saveImage($file, $filename=null, $maxW=1920, $maxH=1920) {

        $original_ext = $file->guessExtension();

        if($filename === null || strlen($filename) === 0){
            $original_name = $file->getClientOriginalName();
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $original_name);
            $original_ext = $file->guessExtension();
            $filename = 'media-'.time().'-'.clean($withoutExt).'.'.$original_ext;
        }

        if($original_ext == 'gif'){
            $newpath = Storage::putFileAs(
                'public'.'/'.'mediamanager'.'/', $file, $filename
            );

        } else {

            $maxWidth = $maxW;
            $maxHeight = $maxH;

            $img = Image::make($file->getRealPath());

            if($img->width() > $maxWidth){
                $img->resize($maxWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }

            if($img->height() > $maxHeight){
                $img->resize(null, $maxHeight, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }

            $img->stream(); // <-- Key point
            $newpath = Storage::disk('local')->put('public'.'/'.'mediamanager'.'/'.$filename, $img, 'public');
        }

        return [
            'filename' => $filename,
            'route' => asset('storage/mediamanager/'.$filename),
            'newpath' => $newpath
        ];
    }

    public static function upload($request, $mediafile, $returnJson=true){
        if($request->hasFile('file')){
            $usr = $request->user();
            $file = $request->file('file');
            $ext = $file->guessExtension();
    
            if(substr($file->getMimeType(), 0, 5) == 'image') {
                $newFile = MediaFile::saveImage($file);
            } else {
                $newFile = MediaFile::saveFile($file);
            }
            
            $filename = $newFile['filename'];
            
            $folder_id = $request->input('folder_id');
            if($folder_id && $folder_id > 0) $mediafile->folder_id = $folder_id;
            
            if($ext == 'doc' || $ext == 'docx'){
                $mime_type = 'application/msword';
            } else {
                $mime_type = $file->getMimeType();
            }

            $mediafile->nombre = $file->getClientOriginalName();
            $mediafile->mime_type = $mime_type;
            $mediafile->filename = $filename;
            $mediafile->created_by = $usr->id;
            $mediafile->save();
            $mediafile = MediaFile::datosAdicionales($mediafile);

            if($returnJson){
                return response()->json([
                    'success' => true,
                    'error' => null,
                    'file' => $mediafile
                ], 200);
            } else {
                return [
                    'success' => true,
                    'error' => null,
                    'file' => $mediafile
                ];
            }

        } else {
            if($returnJson){
                return response()->json([
                    'success' => false,
                    'error' => 'No se recibió archivo',
                ], 400);
            } else {
                return [
                    'success' => false,
                    'error' => 'No se recibió archivo',
                ];
            }

        }

    }

    public static function borrar($request){
        $archivos_str = $request->input('archivos');
        $archivos_arr = explode(',', $archivos_str);
        DB::table('media_files')
            ->whereIn('id', $archivos_arr)
            ->update(['deleted_at' => Carbon::now()]);
        /*if(Storage::disk('public')->has($img->img)){
            Storage::disk('public')->delete($img->img);
        }*/
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

    public static function getImgSrc($img_id=null, $w=null, $h=null, $forceJpg=false){
        $imgRoute = route('mediaimg', [ 'id'=>$img_id, 'w'=>$w, 'h'=>$h ]);
        return $imgRoute;
    }

    public static function getFileResponse($id){
        if($id != null && $file = MediaFile::withTrashed()->find($id)){
            $src = 'storage/mediamanager/'.$file->filename;
            $fileResponse = response()->file($src);
            return $fileResponse;
        }
    }

    // Metodo que me regresa la imagen para mi funcion mediafile()
    public static function getImgResponse($img_id, $w=null, $h=null, $forceJpg=true){
        if($img_id && $img = MediaFile::withTrashed()->find($img_id)){
            if($forceJpg) $ext = 'jpg'; else $ext = $img->getExtension();
            $src = 'storage/mediamanager/'.$img->filename;
            
        } else {
            $src = 'a/img/no-disponible.png';
            //$img = Image::make($src);
            $ext = 'jpg';
        }

        if($ext == 'gif'){
            // Si es GIF, no lo resize
            $imgResponse = response()->file($src);
            
        } else { // No es GIF

            $finalImg = Image::make($src);
            $originalWidth = $finalImg->width();
            $originalHeight = $finalImg->height();

            // width pedido es menor o igual al original, hay que usarlo
            //$imgResponse = response()->file($src);
            if($w >= $h || ($w && !$h)){
                if(($w && $w > $originalWidth) ){
                    $newW = $originalWidth;
                    if($h){
                        $newH = ($originalWidth * $h) / $w;
                    } else {
                        $newH = $originalHeight;
                    }
                    $w = round($newW, 0);
                    $h = round($newH, 0);
                }
            }

            if($h > $w || ($h && !$w)){
                if(($h && $h > $originalHeight)){
                    $newH = $originalHeight;
                    if($h){
                        $newW = ($originalHeight * $w) / $h;
                    } else {
                        //$newW = ($h * $originalWidth) / $originalHeight;
                        $newW = $originalWidth;
                    }
                    $w = round($newW, 0);
                    $h = round($newH, 0);
                }
            }
            

            if($w != null && $h != null && $w > 0 && $h > 0){
                // Se especificaron todas las medidas
                $finalImg = $finalImg->fit($w, $h);

            } else {
                if($w != null && $w != 0 && ($h == null || $h == 0)){
                    // Fue especificada width y height no
                    $finalImg = $finalImg->resize($w, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else if($h != null && $h != 0 && ($w == null || $w == 0)){
                    // Fue especificada height y width no
                    $finalImg = $finalImg->resize(null, $h, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                } else {
                    $finalImg = Image::make($src);
                }
            }
            $imgResponse = $finalImg->response($ext);
        }

        /*} else {
            $src = 'a/img/no-disponible.png';
            if($w != null && $h != null && $w > 0 && $h > 0){
                $finalImg = Image::make($src)->fit($w, $h);
            } else {
                $finalImg = Image::make($src);
            }
            $imgResponse = $finalImg->response();
        }*/

        return $imgResponse;
    }

    public static function edit($request){
        if($request->hasFile('img')) {

            $usr = $request->user();
            $file = $request->file('img');
            $ext = $file->extension();
            if($ext === 'jpeg') $ext = 'jpg';

            if($request->input('id') !== null){
                $old_mediafile = MediaFile::find($request->input('id'));
            } else $old_mediafile = null;

            $filename = 'media-'.time().'-'.generateRandomString(10).'.'.$ext;
            //$filename = 'media-edit-'.time().'-'.clean($file->getClientOriginalName());

            $newFile = MediaFile::saveImage($file, $filename);
            $filename = $newFile['filename'];

            $mediafile = new MediaFile;

            $mediafile->nombre = $old_mediafile !== null ? 'Copia de '.$old_mediafile->nombre : 'Copia';
            $mediafile->mime_type = $file->getMimeType();
            $mediafile->filename = $filename;
            $mediafile->created_by = $usr->id;
            $mediafile->save();
            $mediafile = MediaFile::datosAdicionales($mediafile);

            return response()->json([
                'success' => true,
                'error' => null,
                'file' => $mediafile
            ]);

        } else {
            return response()->json([
                'success' => false,
                'error' => 'no se envio archivo img'
            ]);
        }
    }

    public static function mover($request){
        if($request->input('folder_id')){
            $folder = MediaFolder::find($request->input('folder_id'));
            $folder_id = $folder->id;
        } else {
            $folder = null;
            $folder_id = null;
        }

        $archivos_str = $request->input('archivos');
        $archivos_arr = explode(',', $archivos_str);

        DB::table('media_files')
            ->whereIn('id', $archivos_arr)
            ->update(['folder_id' => $folder_id]);

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

}
