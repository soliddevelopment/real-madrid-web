<?php

namespace App\Models\Fijos;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;

use App\Models\Base;
use App\Http\Traits\Crud;

use Validator;
use DB;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes, Crud;

    protected $fillable = [
        'nombre', 'apellido', 'usuario', 'email', 'password',
    ];
    protected static $cols = [
        'users.id', 'users.role_id', 'users.usuario', 'users.email', 'users.nombre', 'users.apellido', 'users.img_id', 'users.sexo', 'users.email_verified_at', 'users.created_at',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        // return $this->belongsToMany('App\Models\Fijos\UserRole', 'user_userrole', 'user_id', 'userrole_id');
        return $this->belongsTo('App\Models\Fijos\UserRole', 'role_id');
    }

    public function cuentas(){
        return $this->belongsToMany('App\Models\Custom\Cuenta', 'cuentas_users', 'user_id', 'cuenta_id');
    }

    public function empresa(){
        return $this->hasOne('App\Models\Custom\Empresa', 'user_id');
    }

    public function candidato(){
        return $this->hasOne('App\Models\Custom\Candidato', 'user_id');
    }

    public function hasRole($roleSlug){
        if($this->role !== null && $this->role->slug === $roleSlug){
            return true;
        } else return false;
        // if($this->roles()->where('slug', $role)->first()){ return true; } else { return false; }
    }

    public function hasAnyRole($roles) {
        if(is_array($roles)) {
            foreach($roles as $role){ if($this->hasRole($role)) return true; }
        } else if($this->hasRole($roles)) return true;
        return false;
    }

    // Override. Sirve para hacer login con usuario, ademas de email, en passport
    public function findForPassport($username) {
        return $this->where('usuario', $username)->first();
    }

    // Retorna info del usuario que esta haciendo el request + un array con sus roles
    public static function getUserInfo($user_id)
    {
        $cols = self::$cols;
        $cols[] = DB::raw('userroles.slug AS role');
//        $user = DB::table('users')->select($cols)->leftJoin('userroles', 'userroles.id', 'users.role_id')
        $user = User::leftJoin('userroles', 'userroles.id', 'users.role_id')
            ->where('users.id', $user_id)->where('active', 1)
            ->with([
                'medico' => function($q){
                    $q->with([
                        'cuenta' => function($q){
                            $q->select('id', 'nombre');
                        },
                        'especialidades' => function($q){
                            $q->select('id', 'nombre');
                        },
                        'idiomas'
                    ]);
                    // $q->select('user_id', 'cuenta_id', 'tipo_codigo_id', 'codigo', 'active');
                },
                'cuentas' => function($q){
                    $q->select('id','nombre');
                    $q->with([
                        'medicos' => function($q){
                            $q->select('user_id', 'cuenta_id');
                        }
                    ]);
                }
            ])
            ->first($cols);

        /*$user->medico = DB::table('medicos')
            ->where('user_id', $user->id)
            ->first();

        $user->cuenta = DB::table('cuentas')
            ->join('cuentas_users', 'cuentas.id', 'cuentas_users.cuenta_id')
            ->where('cuentas_users.user_id', $user->id)
            ->first();*/

        return $user;
    }

    // FUNCION NORMAL DE OBJETO
    // Lista de usuarios cambia segun los roles que ese usuario tenga
    public function getUsers(){
        // Si el usuario tiene role superadmin, puede ver todo
        // si tiene role admin, puede ver a todos menos a superadmins
        // si no tiene role admin, no puede ver nada

        $cols = self::$cols;
        $cols[] = DB::raw('userroles.slug AS role');

        if($this->hasRole('superadmin')){
            //$usuarios = User::all()->flatten();
            $usuarios = User::leftJoin('userroles', 'userroles.id', 'users.role_id')
                ->get($cols);

        } else if ($this->hasRole('admin')) {
            $usuarios = User::leftJoin('userroles', 'userroles.id', 'users.role_id')
                ->where('userroles.slug', '!=', 'superadmin')
                ->orWhereNull('userroles.slug')
                ->get($cols);
        } else {
            $usuarios = [];
        }
        foreach($usuarios as $item){
            if($item->medico !== null) $item->es_medico = true; else $item->es_medico = false;
        }
        return $usuarios;
    }

    // Me dice si un usuario tiene permiso para editar a otro en base a sus roles
    // superadmins pueden editar a admins y a otros superadmins
    // admins pueden editar a otros q no sean admins (supervisores, basic y nada incluidos)
    public function hasPermissionOnUser($userEdit, $tipo='edit') {
        if($this->id === $userEdit->id) {
            return true;
        }
        if($this->hasRole('superadmin')){
            return true;
        } else if ($this->hasRole('admin')){
            if( ! $userEdit->hasRole('admin')){
                return true;
            } else {
                return false;
            }
        } else {

            return false;
        }
    }

    public static function editar($request, $id=null){
        $array_validations = [
            'nombre' => 'required|string|max:255',
            'apellido' => 'required|string|max:255',
            'email' => [
                'required', 'string', 'email',
                Rule::unique('users')->ignore($id),
            ],
            /*'usuario' => [
                'sometimes', 'string', 'regex:"^[a-zA-Z\d-_]+$"',
                Rule::unique('users')->ignore($id),
            ],*/
            'password' => 'sometimes|required|string|min:6|confirmed|regex:"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$"',
            //'tipo' => 'required|string|max:255',
        ];

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0){
            $edit = true;
        } else {
            $edit = false;
            $password_validations = array(
                'password' => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required|min:6'
            );
            array_push($array_validations, $password_validations);
        }

        $validator = Validator::make($request->all(), $array_validations);
        if ($validator->fails()) {
            return User::errorResponse('Errores de validación', 'VALIDATOR', null, $validator->errors());
        }

        $usr = $request->user();

        $isSuperadmin = $usr->hasRole('superadmin');
        $isAdmin = $usr->hasRole('admin');

        if(!$edit){
            $usuarioEdit = new User();
            if($isSuperadmin || $isAdmin){
                $hasPermission = true;
            } else {
                $hasPermission = false;
            }
        } else {
            $usuarioEdit = User::find($id);
            if($usuarioEdit === null) {
                return User::errorResponse('No se encontró al usuario', 'USER', null);
            } else {
                //$hasPermission = $user_posting->hasPermissionOnUser($usuarioEdit, 'edit');
                if($usr->can('update', $usuarioEdit)){
                    $hasPermission = true;
                } else {
                    $hasPermission = false;
                }
            }
        }

        if($hasPermission){
            $usuarioEdit->nombre = $request->input('nombre');
            $usuarioEdit->apellido = $request->input('apellido');
            $usuarioEdit->email = $request->input('email');
            $usuarioEdit->usuario = $request->input('usuario');
            $usuarioEdit->sexo = $request->input('sexo');

            // CUSTOM
            /*if($request->input('tipo')=='CANDIDATO' || $request->input('tipo')=='EMPRESA'){
                $tipo = $request->input('tipo');
            } else {
                $tipo = 'CANDIDATO';
            }*/
            //$usuarioEdit->tipo = $tipo;

            if($request->input('role') !== null && $role = UserRole::find($request->input('role'))){
                $usuarioEdit->role_id = $role->id;
            } else if($request->input('role') == 0){
                $usuarioEdit->role_id = null;
            }

            if($request->input('img_id') !== null && $mediafile = MediaFile::find($request->input('img_id'))){
                $usuarioEdit->img_id = $mediafile->id;
            } else {
                $usuarioEdit->img_id = null;
            }

            if($request->input('telefono') !== null && strlen($request->input('telefono'))>0) {
                $usuarioEdit->telefono = $request->input('telefono');
            }

            if($request->input('sexo') !== null && ($request->input('sexo') == 0 || $request->input('sexo') == 1)){
                $usuarioEdit->sexo = $request->input('sexo');
            } else {
                $usuarioEdit->sexo = 1;
            }

            if(!$edit) $usuarioEdit->password = bcrypt($request->input('password'));

            $usuarioEdit->save();
            return self::successResponse($usuarioEdit);

        } else {
            return self::errorResponse('No tiene permisos de editar a ese usuario', 'PERMISOS', null);
        }

    }

    public static function borrar($request, $id=null){
        $item = self::find($id);
        if($item){
            $item->delete();
            return self::successResponse();
        } else {
            return self::errorResponse('No se encontró ese usuario', 'USER');
        }
    }


}
