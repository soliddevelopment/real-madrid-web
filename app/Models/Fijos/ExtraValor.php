<?php

namespace App\Models\Fijos;
use DB;

use App\Models\Base;

class ExtraValor extends Base
{

    protected $table = 'extras_valores';


    public function tipo(){
        return $this->belongsTo('App\Models\Fijos\ExtraTipo', 'tipo_id');
    }

    public function extra(){
        return $this->belongsTo('App\Models\Fijos\Extra', 'extra_id');
    }

    public function post(){
        return $this->belongsTo('App\Models\Fijos\Post', 'post_id');
    }
    /*public function posts(){
        return $this->hasMany('App\Models\Post');
    }*/

    /*public function subcategorias(){
        return $this->hasMany('App\Models\Subcategoria');
    }*/
}
