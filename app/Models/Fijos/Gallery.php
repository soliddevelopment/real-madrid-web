<?php

namespace App\Models\Fijos;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;

use DB;
use Validator;
use Carbon\Carbon;

use App\Http\Traits\Crud;
use App\Rules\NoSpaces;
use App\Models\Base;

class Gallery extends Base
{
    use Crud, SoftDeletes;

    protected $table = 'galleries';

    public function mediafiles() {
        return $this->belongsToMany('App\Models\Fijos\MediaFile', 'gallery_mediafile', 'gallery_id', 'mediafile_id');
    }

    public function post(){
        return $this->belongsTo('App\Models\Fijos\Post', 'post_id');
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++

    public static function getBySlug($slug=null){
        $withArray = [
            'mediafiles'=> function($q){
                $q->select('media_files.id', 'media_files.mime_type', 'media_files.nombre', 'media_files.filename');
            },
        ];
        $elem = self::with($withArray)->whereNull('post_id')->whereNull('ent_id')->where('slug', $slug)->first();
        return $elem;
    }

    public static function store($request, $id=null) {

        $validator = Validator::make($request->all(), [
//            'slug' => 'required', Rule::unique('galleries')->ignore($id), new NoSpaces,
            'slug' => 'required',
            'titulo' => 'required|string|max:255',
        ]);

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) {
            $edit = true;
            $elem = self::find($id);
        } else {
            $edit = false;
            $elem = new self;
        }

        if ($validator->fails()) {
            return [
                'success' => false,
                'model' => null,
                'error' => 'Revisar los campos por favor',
                'error_code' => 'VALIDATOR',
                'validator' => $validator
            ];

        } else {

            $post = null;

            if($request->input('post_id') && $post = Post::find($request->input('post_id'))){
                // viene de boton que crea la galeria si un post no la tiene
                $elem->post_id = $post->id;
            }

            if($request->input('cover') != null) $elem->cover = $request->input('cover');
            if(!$edit) $elem->slug = $request->input('slug');
            $elem->titulo = $request->input('titulo');
            $elem->resumen = $request->input('resumen');

            $slugsConflictivos = self::where('slug', '=', $elem->slug)->get();
            if($slugsConflictivos != null && count($slugsConflictivos)>0){
                return self::errorResponse('Ese slug ya existe para ese elemento.','SLUG');
            }

            $elem->save();

            if($post){
                // viene de boton que crea la galeria si un post no la tiene
                $extraValor = new ExtraValor();
                $extraValor->post_id = $post->id;
                $extraValor->extra_id = $request->input('extra_id');
                $extraValor->tipo_id = $request->input('extra_tipo_id');
                $extraValor->valor_txt_large = $elem->id;
                $extraValor->save();
            }

            return self::successResponse($elem);
        }

    }

    public static function galeriaPost($request) {

        $validator = Validator::make($request->all(), [
            'gallery_str' => 'required|string',
            'gallery_id' => 'required',
        ]);

        $id = $request->input('gallery_id');

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0 && $gallery = self::find($id)) {
            $edit = true;
        } else {
            return self::errorResponse('Esa galería no existe', 'ID');
        }

        if ($validator->fails()) {
            return [
                'success' => false,
                'model' => null,
                'error' => 'Errores de validación',
                'error_code' => 'VALIDATOR',
                'validator' => $validator
            ];

        } else {
            $str = $request->input('gallery_str');
            $strArray = explode(',', $str);
            foreach($strArray as $i){
                if(is_numeric($i) && $i > 0){
                    $gallery->mediafiles()->sync($i, [
                        // 'autor_id' => $autorId
                    ]);
                }
            }
            return self::successResponse($gallery);
        }

    }

    public static function newGalleryFromPost($post, $extra){
        $gallery = new self();
        $gallery->titulo = $post->titulo.' galería';
        $gallery->slug = $post->slug.'-gallery';
        $gallery->post_id = $post->id;
        $gallery->save();

        $extraValor = new ExtraValor();
        $extraValor->post_id = $post->id;
        $extraValor->extra_id = $extra->id;
        $extraValor->tipo_id = $extra->tipo->id;
        $extraValor->valor_txt_large = $gallery->id;
        $extraValor->save();
    }

}
