<?php

namespace App\Models\Fijos;

use Illuminate\Database\Eloquent\Model;
use App\Models\Base;

class UserRole extends Base
{
    public $timestamps = false;
    public $table = 'userroles';

    public function users(){
        return $this->belongsToMany('App\Models\Fijos\User', 'user_userrole', 'user_id', 'userrole_id');
    }

}
