<?php

namespace App\Models\Fijos;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;

use DB;
use Validator;
use Session;

use App\Http\Traits\Crud;
use App\Rules\NoSpaces;
use App\Models\Base;
use App\Models\Fijos\MediaFile;

class Post extends Base
{
    use SoftDeletes, Crud;

    protected $table = 'posts';

    public function seccion(){
        return $this->belongsTo('App\Models\Fijos\Seccion', 'seccion_id');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Fijos\Categoria', 'categoria_id');
    }

    public function galleries(){
        return $this->hasMany('App\Models\Fijos\Gallery', 'post_id');
    }

    public function extrasValores(){
        return $this->hasMany('App\Models\Fijos\ExtraValor', 'post_id');
    }

    public function mediaFile(){
        return $this->hasOne('App\Models\Fijos\MediaFile', 'img_id');
    }
    
    public function relatedPosts(){
        return $this->belongsToMany('App\Models\Fijos\Post', 'posts_relations', 'main_post_id', 'related_id');
    }

    public function getSetExtras($arr=[]){
        $extras = $this->seccion->extras;
        foreach($extras as $extra){
            $columna = $extra->slug;
            $continue = true;
            if($arr !== null && is_array($arr) && count($arr)>0){
                if(!in_array($columna, $arr)){
                    $continue = false;
                }
            }
            
            if($continue){
                $extraValor = ExtraValor::where('post_id', $this->id)->where('extra_id', $extra->id);
                /*if($arr !== null && is_array($arr) && count($arr)>0){
                    $extraValor = $extraValor->whereIn('slug', $arr);
                }*/
                $extraValor = $extraValor->first();
    
                if($extraValor != null){
                    $this->$columna = $extraValor->valor_txt_large;
        
                    if($extra->tipo->slug == 'gallery'){
                        // aca debo traer el gallery y ponerlo como la propiedad
                        $withArray = [
                            'mediafiles'=> function($q){
//                            $q->select('media_files.id', 'media_files.mime_type', 'media_files.nombre', 'media_files.filename');
                                $q->select(MediaFile::$cols);
                                $q->orderBy('gallery_mediafile.order');
                            },
                        ];
                        $galeria = Gallery::with($withArray)->where('id', $this->$columna)->first();
                        if($galeria){
                            //$this->$columna = $galeria->mediafiles;
                            $this->$columna = $galeria;
                        } else {
                            $this->$columna = null;
                        }
                    }
        
                } else {
                    $this->$columna = null;
                }
            } else {
                $this->$columna = null;
            }
            
        }
    }

    public static function findBySlug($slug='', $varios=false, $cols=[]){
        if(!is_numeric($slug) && strlen($slug)>0){
            $post = Post::where('slug', $slug);
//            if($cols!=null && count($cols)>0){
//                if(!$varios) $post = $post->first($cols); else $post = $post->get($cols);
//            } else {
            if(!$varios) $post = $post->first(); else $post = $post->get();
//            }

            if($post != null){
                $post->getSetExtras();
            }
            return $post;

        }
    }

    public function findOtros(){
        $posts = Post::findForSeccion([$this->seccion->slug], 9, false, false, [], $this->categoria_id);
        $postsFinales = [];
        foreach($posts as $item){
            if($item->id != $this->id){
                $postsFinales[] = $item;
            }
        }
        return $postsFinales;
    }

    public static function findForSeccion($seccionesArray, $camposDinamicos=[], $normalCols=[], $limit=null,
                                          $soloDestacados=false, $incluirInactivos=false, $categoriaSlug=null,
                                          $parentPostId=null, $orderBy=0){

        if($normalCols == null || count($normalCols)==0){
            // $cols = ['posts.*'];
            $cols = ['posts.id', 'posts.categoria_id', 'posts.titulo', 'posts.slug', 'posts.active', 'posts.destacado'];
        } else {
            $cols = [];
            foreach($normalCols as $c){
                $cols[] = 'posts.'.$c;
            }
        }
        array_merge($cols, [
            'secciones.slug AS seccion_slug', 'categorias.titulo AS categoria_titulo', 'categorias.slug AS categoria_slug'
        ]);

        if($parentPostId){
            $parentPost = self::find($parentPostId);
        } else {
            $parentPost = null;
        }

        $posts = Post::whereIn('secciones.slug', $seccionesArray)
            ->select($cols)
            ->join('secciones', 'secciones.id', '=', 'posts.seccion_id')
            ->leftJoin('categorias', 'categorias.id', '=', 'posts.categoria_id')
            ->whereNull('posts.deleted_at');

        if($categoriaSlug != null && !is_numeric($categoriaSlug) && strlen($categoriaSlug) > 0){
            $posts = $posts->where('categorias.slug', '=', $categoriaSlug);
        }
        if($soloDestacados){
            $posts = $posts->where('posts.destacado', 1);
        }
        if($parentPost){
            $posts = $posts->where('post_id', '=', $parentPost->id);
        }
        if(!$incluirInactivos){
            $posts = $posts->where('posts.active', 1);
        }
        if(!empty($seccion) && $seccion !== null && $seccion->sortable === 1){
            $posts = $posts->orderBy('posts.id', 'desc');
        }
        if($orderBy > 0){
            switch($orderBy){
                case 1:
                    $posts = $posts->orderBy('posts.titulo', 'asc');
                    break;
            }
        }

        $posts = $posts->with([
            'extrasValores' => function($q){
                $q->select(['extra_id', 'tipo_id', 'post_id', 'valor_txt_large']);
                $q->with([
                    'extra'=> function($q){
                        $q->select('extras.id', 'extras.slug', 'extras.titulo', 'extras.order');
                    },
                    'tipo' => function($q){
                        $q->select('extras_tipos.id', 'extras_tipos.slug', 'extras_tipos.titulo', 'extras_tipos.regex');
                    }
                ]);
            }
        ]);

        $orderNormally = true;
        if(count($seccionesArray)==1){
            $seccion = Seccion::findBySlug($seccionesArray[0]);
            if($seccion != null && $seccion->sortable == 1){
                $posts = $posts->orderBy('order', 'asc');
                $orderNormally = false;
            }
        }
        if($orderNormally) $posts = $posts->orderBy('id', 'desc');

        if($limit && is_numeric($limit) && $limit > 0){
            $posts = $posts->paginate($limit);
        } else {
            $posts = $posts->get();
        }

        $posts = self::prepararCamposExtra($posts, $camposDinamicos);

        return $posts;
    }

    public static function prepararCamposExtra($posts, $camposArr=[]){
        foreach($posts as $item){

            foreach($item->extrasValores as $valor){
                if($valor->extra != null){
                    $columna = $valor->extra->slug;
                    if($valor->tipo->slug == 'gallery'){
                        // tengo que agregar los mediafiles y no el id d la galeria
                        if(in_array($columna, $camposArr)){
                            if($valor->valor_txt_large != null && strlen($valor->valor_txt_large)>0){
                                $gallery_id = $valor->valor_txt_large;
                                $withArray = [
                                    'mediafiles'=> function($q){
                                        //$q->select('media_files.id', 'media_files.mime_type', 'media_files.nombre', 'media_files.filename');
                                        $q->select(MediaFile::$cols);
                                        $q->orderBy('gallery_mediafile.order');
                                    },
                                ];
                                if($gallery = Gallery::with($withArray)->where('id', $gallery_id)->first()){
                                    // $gallery->mediafiles->sortBy('order');
                                    $item->$columna = $gallery;
                                }
                            }
                        }

                    } else {
                        if(in_array($columna, $camposArr)){
                            if($valor->valor_txt_large != null && strlen($valor->valor_txt_large)>0){
                                $item->$columna = $valor->valor_txt_large;
                            }
                        }
                    }
                }
            }
            unset($item->extrasValores);
            unset($item->extras_valores);
        }
        return $posts;
    }

    public static function store($request, $id=null) {

        $validator = Validator::make($request->all(), [
            //'slug' => 'required', Rule::unique('secciones')->ignore($id), new NoSpaces,
            'seccion_id' => 'required',
            'titulo' => 'required|string|max:255',
        ]);

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) {
            $edit = true;
            $post = Post::find($id);
            $seccion = $post->seccion;
        } else {
            $edit = false;
            $post = new Post;
            $post->active = 1;
            $seccion = Seccion::find($request->input('seccion_id'));
        }

        if($seccion != null){

            if($request->input('parent_id') != null) {
                $parentPost = self::find($request->input('parent_id'));
            } else {
                $parentPost = null;
            }

            if($seccion->slug_manual == 1){
                // VERIFICO SI NO HAY CONFLICTO CON SLUGS
                $slugsConflictivos = Post::where('slug', $request->input('slug'))
                    ->where('seccion_id', '=', $seccion->id);
                if($edit){
                    $slugsConflictivos = $slugsConflictivos->where('id', '!=', $post->id);
                }
                $slugsConflictivos = $slugsConflictivos->get();

                //dd($slugsConflictivos);

                if(($slugsConflictivos != null && count($slugsConflictivos)>0) || strlen($request->input('slug')) == 0 || $request->input('slug') == null){
                    if(!$edit){
                        $redirect_route = redirect()->route('admin.secciones.posts.create', $seccion->slug)->withErrors([])->withInput();
                    } else {
                        $redirect_route = redirect()->route('admin.secciones.posts.edit', ['seccionSlug'=> $seccion->slug, 'id'=>$id])->withErrors([])->withInput();
                    }
                    if(strlen($request->input('slug')) == 0 || $request->input('slug') == null){
                        $txt = 'Slug no válido';
                    } else {
                        $txt = 'Ese slug ya existe';
                    }
                    return self::errorResponse($txt, 'SLUG', null, null, $redirect_route);
                }
                $slug = preg_replace('/\s+/', '', $request->input('slug'));

            } else {
                $slug = self::strToSlug(trim($request->input('titulo')));
            }

            // PROBLEMAS DE VALIDACION
            if ($validator->fails()) {
                if($edit){
                    $redirect_route = redirect()->route('admin.secciones.posts.edit', ['seccionSlug'=>$seccion->slug, 'id'=>$post->id])->withErrors($validator)->withInput();
                } else {
                    $redirect_route = redirect()->route('admin.secciones.posts.create', ['seccionSlug'=>$seccion->slug])->withErrors($validator)->withInput();
                }
                return self::errorResponse('Hay errores en los campos requeridos.', 'VALIDATOR', null, $validator->errors(), $redirect_route);

            } else {
                // TOD BIEN APARENTEMENTE

                if($parentPost) {
                    $post->post_id = $parentPost->id;
                }

                $post->seccion_id = $seccion->id;
                $post->categoria_id = $request->input('categoria_id');
                if($request->input('img_id') !== null) {
                    $post->img_id = $request->input('img_id');
                }
                
                $post->slug = $slug;
                $post->titulo = $request->input('titulo');
                $post->tags = $request->input('tags');
                // $post->resumen = $request->input('resumen');
                $post->save();
    
                if($request->input('related') !== null && is_array($request->input('related'))) {
                    $post->relatedPosts()->detach();
                    $relatedArr = $request->input('related');
                    foreach($relatedArr as $relatedId){
                        $post->relatedPosts()->attach($relatedId);
                    }
                    $post->img_id = $request->input('img_id');
                }


                // Aca itero en todos los extras de post de esta seccion
                $extras = $seccion->extras;
                if($extras !== null){
                    foreach($extras as $extra){
                        $input = $request->input($extra->slug);

                        if($extra->tipo->slug == 'img' && $input == null){
                            // es imagen y viene null, no debe reemplzarse por uno vacio
                        } else if($extra->tipo->slug == 'gallery'){
                            // es gallery y se esta creando un post, crear galeria para el post
                            if(!$edit){ // esta creando
                                Gallery::newGalleryFromPost($post, $extra);
                            } else {
                                // no hacer nada
                            }

                        } else {

                            $extraValor = ExtraValor::where('post_id', $post->id)
                                ->where('extra_id', $extra->id)
                                ->first();
                            if($extraValor == null){
                                $extraValor = new ExtraValor();
                                $extraValor->tipo_id = $extra->tipo_id; // este dato se duplica para facilidad de las consultas
                                $extraValor->post_id = $post->id;
                                $extraValor->extra_id = $extra->id;
                            }
                            $extraValor->valor_txt_large = $input == null ? '' : $input;
                            $extraValor->save();
                        }

                    }
                }
    
    
                if($id !== null){
                    if($parentPost){
                        //$redirect_route = redirect()->route('admin.secciones.posts.index', ['slug'=>$seccion->slug, 'parentSlug'=>$parentPost->seccion->slug, 'parentPostId'=>$parentPost->id]);
                        $redirect_route = redirect()->route('admin.secciones.posts.index', ['slug'=>$seccion->slug, 'parentPostId'=>$parentPost->id]);
                    } else {
                        $redirect_route = redirect()->route('admin.secciones.posts.index', $seccion->slug);
                    }
                } else {
                    // <a href="{{ route('admin.secciones.posts.create', ['slug'=>$seccion->slug, 'parentPostId'=>$parentPost->id]) }}" class="btn btn-primary">+ Nuevo item</a>
                    if($parentPost){
                        $redirect_route = redirect()->route('admin.secciones.posts.create', ['slug'=>$seccion->slug, 'parentPostId'=>$parentPost->id]);
                    } else {
                        $redirect_route = redirect()->route('admin.secciones.posts.create', $seccion->slug);
                    }
                }

                return self::successResponse($post, null, $redirect_route);
            }
        } else {
            Session::flash('error', 'No existe esa sección');
            return redirect()->route('admin.secciones.posts.index', $seccion->slug);
        }

    }

}
