<?php

namespace App\Models\Fijos;
use Illuminate\Http\Request;

use App\Http\Traits\Crud;
use App\Models\Base;

class GalleryMediaFile extends Base
{
    use Crud;
    protected $table = 'gallery_mediafile';

}
