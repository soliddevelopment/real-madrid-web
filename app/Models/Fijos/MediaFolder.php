<?php

namespace App\Models\Fijos;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;

use DB;
use Image;
use Storage;
use Validator;
use File;
use Carbon\Carbon;

use App\Http\Traits\Crud;
use App\Models\Base;

class MediaFolder extends Base
{
    use SoftDeletes, Crud;

    protected $table = 'media_folders';
/*
    public function post(){
        return $this->belongsTo('App\Models\Fijos\Post', 'img_id');
    }*/


    public static function getLista($request){
        $folders = self::orderBy('nombre');
        if($request->input('folder_actual')){
            $folders = $folders->where('folder_id', $request->input('folder_actual'));
        } else {
            $folders = $folders->whereNull('folder_id');
        }
        $folders = $folders->get();
        return $folders;
    }

    public static function store($request){

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
        ]);

        if($request->input('id') && is_numeric($request->input('id')) && $elem = self::find($request->input('id'))){
            $edit = true;
        } else {
            $edit = false;
            $elem = new self;
        }

        if ($validator->fails()) {
            return self::errorResponse('Errores de validación', 'VALIDATOR', $validator->errors());
        } else {

            $elem->nombre = $request->input('nombre');

            if(!$edit){ // solo si esta creando uno nuevo permito cambiar el folder_id por ahora
                $folder_id = null;
                if($request->input('folder_actual') && $folder = self::find($request->input('folder_actual'))){
                    $folder_id = $folder->id;
                }
                $elem->folder_id = $folder_id;
            }

            $elem->save();

            return self::successResponse($elem);
        }
    }

    public static function borrar($request){
        if($request->input('id') && is_numeric($request->input('id'))){
            $folder = self::find($request->input('id'));
            $folder->delete();
        }
        return self::successResponse();
    }

}
