<?php
namespace App\Models\Fijos;
use Illuminate\Validation\Rule;
use Session;
use Validator;
use DB;

use App\Http\Traits\Crud;
use App\Rules\NoSpaces;
use App\Models\Base;

use App\Models\Fijos\Post;
use App\Models\Fijos\ExtraValor;

class Extra extends Base
{
    use Crud;
    protected $table = 'extras';

    public function tipo(){
        return $this->belongsTo('App\Models\Fijos\ExtraTipo', 'tipo_id');
    }

    public function seccion(){
        return $this->belongsTo('App\Models\Fijos\Seccion', 'seccion_id');
    }

    public function valorObj(){
        return $this->hasOne('App\Models\Fijos\ExtraValor', 'extra_id');
    }
    
    public static function createNew($titulo, $slug, $modo='ITEM', $tipo_slug=null, $seccion_id=null, $required=false){
        $extra = new self;
        if($exists = self::where('slug', '=', $slug)->where('modo','=',$modo)->where('seccion_id', $seccion_id)->first()){
            return self::errorResponse('No se pudo crear por el slug', 'SLUG');
        }
        
        $tipo = ExtraTipo::where('slug', '=', $tipo_slug)->first();
        if(!$tipo) return self::errorResponse('No se pudo crear extra porque no existe ese tipo', 'EXTRA_TIPO');
        
        $extra->modo = $modo;
        $extra->tipo_id = $tipo->id;
        $extra->seccion_id = $seccion_id;
        $extra->titulo = $titulo;
        $extra->slug = $slug;
        $extra->required = $required;
        $extra->save();
        return $extra;
    }

    public static function getContenidosFromArray($arr=[], $simplificarArray=true) {
        $cols = [
            'extras.id', 'extras.seccion_id', 'extras.tipo_id', 'extras.slug', 'extras.titulo', 'extras.width', 'extras.height',
            'extras_tipos.slug AS tipo_slug', 'extras_valores.valor_txt_large AS valor'
        ];
        $extras = self::whereIn('extras.slug', $arr);
        $extras = $extras->leftJoin('extras_tipos', 'extras_tipos.id', '=', 'extras.tipo_id');
        $extras = $extras->leftJoin('extras_valores', 'extras_valores.extra_id', 'extras.id');
        $extras = $extras->get($cols);
        if($simplificarArray){
            $extras_finales = [];
            // foreach($extras as $item){ $extras_finales[$item->slug] = $item->valor; }
            foreach($arr as $item){
                $extras_finales[$item] = '';
                foreach($extras as $ex){
                    if($ex->slug == $item){
                        $extras_finales[$item] = $ex->valor;
                    }
                }
            }
            return $extras_finales;
        }
        return $extras;
    }

    public static function getAsContenido($extra_id=null){
        if($extra = self::find($extra_id)){
            $extra = self::getForSeccion($extra->seccion_id, null, $extra_id, 'CONTENIDO');
            return $extra;
        }
        return null;
    }

    public static function getForPost($post_id=null, $modo='ITEM'){
        if($post = Post::find($post_id)){
            $extras = self::getForSeccion($post->seccion_id, $post->id, null, $modo);
            return $extras;
        }
        return null;
    }

    public static function getForSeccion($seccion_id=null, $post_id=null, $extra_id=null, $modo='ITEM'){
//        $extras = self::where('seccion_id', $seccion_id);
        $cols = [
            'extras.id', 'extras.seccion_id', 'extras.tipo_id', 'extras.slug', 'extras.titulo', 'extras.order', 'extras.width', 'extras.height',
            'extras.required',
            'extras_tipos.slug AS tipo_slug', 'extras_tipos.titulo AS tipo_titulo', 'extras_tipos.regex AS tipo_regex', 'extras_tipos.display_mode AS tipo_display_mode'
        ];

        $extras = DB::table('extras');
        $extras = $extras->leftJoin('extras_tipos', 'extras_tipos.id', '=', 'extras.tipo_id');

        if($seccion_id != null) $extras = $extras->where('extras.seccion_id', $seccion_id);

        if($post_id != null){
            $extras = $extras->leftJoin('extras_valores', function($join) use ($post_id){
                $join->on('extras_valores.extra_id', '=', 'extras.id')
                    ->where('extras_valores.post_id', '=', $post_id);
            });
            $cols[] = 'extras_valores.valor_txt_large AS valor';
            $cols[] = 'extras_valores.id AS valor_id';
        }

        if($extra_id != null){
            $extras = $extras->leftJoin('extras_valores', 'extras_valores.extra_id', 'extras.id');
            $cols[] = 'extras_valores.valor_txt_large AS valor';
            $cols[] = 'extras_valores.id AS valor_id';
            $extras = $extras->where('extras.id', $extra_id);
        }

        $extras = $extras->where('modo', $modo);
        $extras = $extras->select($cols);
        $extras = $extras->orderBy('order');

        if($extra_id != null){
            $extras = $extras->first();
            if($extras && (!$extras->valor || empty($extras->valor))){
                $extras->valor = null;
            }
        } else {
            $extras = $extras->get();
            if($post_id == null){
                foreach($extras as $e){
                    $e->valor = null;
                }
            }
        }

        return $extras;
    }

    public static function store($request, $id=null, $modo='ITEM') {
        $validator = Validator::make($request->all(), [
            'tipo_slug' => 'required',
            'titulo' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png',
             // 'seccion_id' => 'required|integer|min:1',
        ]);

        $seccion = Seccion::find($request->input('seccion_id'));

        if($id!==null){
            $edit = true;
            $extra = Extra::where('id', $id)
                ->where('modo', $modo)
                ->first();
        } else {
            $edit = false;
            $extra = new Extra;
            $extra->modo = $modo;
        }

        if ($validator->fails()) {
            if($edit){
                if($modo == 'ITEM'){
                    $redirect_route = redirect('admin.secciones.extras.edit', [ 'seccion_id'=>$seccion->id, 'id' => $extra->id ])->withErrors($validator)->withInput();
                } else {
                    $redirect_route = redirect('admin.secciones.contenidos.edit', [ 'seccion_id'=> $seccion != null ? $seccion->id: '', 'id' => $extra->id ])->withErrors($validator)->withInput();
                }

            } else {
                if($modo == 'ITEM'){
                    $redirect_route = redirect()->route('admin.secciones.extras.create', $seccion->id)->withErrors($validator)->withInput();
                } else {
                    $redirect_route = redirect()->route('admin.secciones.contenidos.create', $seccion != null ? $seccion->id: '')->withErrors($validator)->withInput();
                }
            }
            return self::errorResponse('Hubo errores en los campos', 'VALIDATOR', null, $validator, $redirect_route);

        } else {

            // Testeo si slug ya existe
            if($seccion != null) {
                $slug_existe = Extra::where('seccion_id', $seccion->id)
                    ->where('modo', $modo)
                    ->where('slug', $request->input('slug'))
                    ->first();
            } else {
                $slug_existe = Extra::where('modo', $modo)
                    ->where('slug', $request->input('slug'))
                    ->first();
            }

            if($slug_existe !== null){ // slug ya existe
                if($edit && $slug_existe->id == $id){
                    // Existe pero tod bien porq es el mismo id, esta editando
                } else {
                    if($edit){
                        if($modo == 'ITEM'){
                            $redirect_route = redirect('admin.secciones.extras.edit', [ 'seccion_id'=>$seccion->id, 'id' => $extra->id ])->withErrors($validator)->withInput();
                        } else {
                            $redirect_route = redirect('admin.secciones.contenidos.edit', [ 'seccion_id'=> $seccion != null ? $seccion->id : null, 'id' => $extra->id ])->withErrors($validator)->withInput();
                        }
                    } else {
                        if($modo == 'ITEM'){
                            $redirect_route = redirect()->route('admin.secciones.extras.create', $seccion->id)->withErrors($validator)->withInput();
                        } else {
                            $redirect_route = redirect()->route('admin.secciones.contenidos.create', $seccion != null ? $seccion->id : null)->withErrors($validator)->withInput();
                        }
                    }
                    return self::errorResponse('El slug ya existe', 'SLUG', null, $validator, $redirect_route);
                }
            }

            $extra->seccion_id = $seccion != null ? $seccion->id : null;
            $extra->slug = $request->input('slug');
            $extra->titulo = $request->input('titulo');

            if($tipoExtra = ExtraTipo::where('slug', $request->input('tipo_slug'))->first()){
                $extra->tipo_id = $tipoExtra->id;
            } else {
                return self::errorResponse('No existe ese tipo de extra', 'TIPO', $extra, null);
            }

            if($request->input('width') != null && is_numeric($request->input('width')) && $request->input('width') > 0){
                $extra->width = $request->input('width');
            } else {
                $extra->width = null;
            }

            if($request->input('height') != null && is_numeric($request->input('height')) && $request->input('height') > 0){
                $extra->height = $request->input('height');
            } else {
                $extra->height = null;
            }

            $extra->save();

            if($edit){
                if($modo == 'ITEM'){
                    $redirect_route = redirect()->route('admin.secciones.extras.index', ['seccionSlug' => $seccion->slug]);
                } else {
                    $redirect_route = redirect()->route('admin.secciones.contenidos.index', ['seccionSlug' => $seccion != null ? $seccion->slug : null]);
                }

            } else {
                if($modo == 'ITEM'){
                    $redirect_route = redirect()->route('admin.secciones.extras.create', ['seccion_id' => $seccion->id]);
                } else {
                    $redirect_route = redirect()->route('admin.secciones.contenidos.create');
                }
            }

            return self::successResponse($extra, null, $redirect_route);
        }
    }


    /*$cita->with([
    'medico'=>function($q){
        $q->select(['user_id', 'cuenta_id', 'tipo_codigo_id', 'codigo', 'active']);
    },
    'clinica'=>function($q){
        $q->with([
            'departamento' => function($q){
                $q->select(['id', 'nombre', 'codigo', 'active']);
            }
        ]);
        $q->select(['id', 'medico_id', 'departamento_id', 'nombre', 'direccion', 'lat', 'lng', 'active']);
    },
    'paciente',
    ]);*/

}
