<?php

namespace App\Models\Fijos;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

use Session;
use DB;
use Validator;

use App\Http\Traits\Crud;

use App\Rules\NoSpaces;
use App\Models\Base;
use App\Models\Fijos\Categoria;
use App\Models\Fijos\Extra;

class Seccion extends Base
{
    use SoftDeletes, Crud;

    protected $table = 'secciones';

    public function posts(){
        return $this->hasMany('App\Models\Fijos\Post', 'seccion_id');
    }

    public function extras(){
        return $this->hasMany('App\Models\Fijos\Extra', 'seccion_id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Fijos\Seccion', 'seccion_id');
    }

    public function children(){
        return $this->hasMany('App\Models\Fijos\Seccion', 'seccion_id');
    }

    public function categorias(){
        return $this->hasMany('App\Models\Fijos\Categoria', 'seccion_id');
    }

    public static function createNew($titulo, $slug, $ops=[], $parent_id=null){
        return self::createNewBlog($titulo, $slug, $ops, $parent_id);
    }
    
    public static function createNewForm($titulo, $slug, $ops=[]){
        $seccion = new self;
        if($exists = self::where('slug', '=', $slug)->first()){
            return self::errorResponse('No se pudo crear por el slug', 'SLUG');
        }
    
        $seccion->tipo = 'FORM';
        $seccion->titulo = $titulo;
        $seccion->slug = $slug;
        $seccion->asignarBooleans($ops);
        $seccion->save();
        return $seccion;
        
    }
    
    public static function createNewBlog($titulo, $slug, $ops=[], $parent_id=null){
        $seccion = new self;
        if($exists = self::where('slug', '=', $slug)->first()){
            return self::errorResponse('No se pudo crear por el slug', 'SLUG');
        }
        $seccion->tipo = 'BLOG';
        $seccion->titulo = $titulo;
        $seccion->slug = $slug;
        $seccion->asignarBooleans($ops);

        if($parent_id && $parent_id > 0){
            $seccion->seccion_id = $parent_id;
        }

        $seccion->save();
        return $seccion;
    }
    
    public function asignarBooleans($ops){
        if(in_array('has_children', $ops)){
            $this->has_children = 1;
        } else {
            $this->has_children = 0;
        }
    
        if(in_array('has_categorias', $ops)){
            $this->has_categorias = 1;
        } else {
            $this->has_categorias = 0;
        }
    
        if(in_array('categorias_administrables', $ops)){
            $this->categorias_administrables = 1;
        } else {
            $this->categorias_administrables = 0;
        }
    
        if(in_array('sortable', $ops)){
            $this->sortable = 1;
        } else {
            $this->sortable = 0;
        }
    
        if(in_array('adjuntos', $ops)){
            $this->adjuntos = 1;
        } else {
            $this->adjuntos = 0;
        }
    
        if(in_array('slug_manual', $ops)){
            $this->slug_manual = 1;
        } else {
            $this->slug_manual = 0;
        }
    
        if(in_array('destacados', $ops)){
            $this->destacados = 1;
        } else {
            $this->destacados = 0;
        }
    }

    public function newExtra($titulo, $slug, $modo, $tipo_slug, $required=false){
        return Extra::createNew($titulo, $slug, $modo, $tipo_slug, $this->id, $required);
    }
    
    public function newCampo($titulo, $slug, $tipo_slug, $required=false){
        return Extra::createNew($titulo, $slug, 'CAMPO', $tipo_slug, $this->id, $required);
    }

    public function getCategorias($onlyActive=true, $orderBy='alphabetic'){
        $cats = Categoria::all()->where('seccion_id', $this->id);
        if($onlyActive) $cats = $cats->where('active', 1);
        if($orderBy == 'alphabetic') {
            $cats = $cats->sortBy('titulo');
        } else {
            $cats = $cats->sortBy('created_at');
        }
        return $cats;
    }

    public function getExtras(){
        $arrModosPermitidos = ['ITEM', 'CAMPO'];
        $extras = Extra::all()->where('seccion_id', $this->id);
        $extras = $extras->whereIn('modo', $arrModosPermitidos);
        $extras = $extras->sortBy('order');
        return $extras;
    }

    public function getContenidos(){
        $extras = Extra::all()->where('seccion_id', $this->id);
        $extras = $extras->where('modo', 'CONTENIDO');
        $extras = $extras->sortBy('order');
        return $extras;
    }

    /*public function contenidos(){
        return $this->hasMany('App\Models\Contenido');
    }*/

    public static function findBySlug($slug='', $varios=false){
        if(!is_numeric($slug) && strlen($slug)>0){
            $seccion = Seccion::where('slug', $slug);
            if(!$varios) $seccion = $seccion->first(); else $seccion = $seccion->get();
            return $seccion;
        }
    }

    public function getCrumbsList($parentPostId=null){
        $arrFinal = Seccion::getParentCrumb($this->id, $parentPostId);
        $arrFinal = array_reverse($arrFinal);
        return $arrFinal;
    }

    public static function getParentCrumb($idSeccion, $parentPostId, $arr=[]){
        $seccionActual = Seccion::find($idSeccion);
        $postParentActual = Post::find($parentPostId);

        $postParentActualSlug = null;
        if($postParentActual !== null) {
            $postParentActualSlug = $postParentActual->slug;
        }

        if($seccionActual->seccion_id == null || $postParentActual == null){
            // no tiene parent
            $route = route('admin.secciones.posts.index', $seccionActual->slug);
        } else {
            // tiene parent
            $seccionParent = Seccion::find($seccionActual->seccion_id);
            $route = route('admin.secciones.posts.index', ['seccion_slug' => $seccionActual->slug, 'parentPostId' => $postParentActual->id]);
        }

        $arr[] = [
            'route' => $route,
            'title' => $seccionActual->titulo,
        ];

        // Aca termina la recursividad
        if($seccionActual->seccion_id == null){
            return $arr;
        }

        if($postParentActual != null) {
            // tiene post parent
            $parent_id = $postParentActual->post_id;
        } else {
            // no tiene post parent, mando null
            $parent_id = null;
        }

        return Seccion::getParentCrumb($seccionActual->seccion_id, $parent_id, $arr);
    }

    public static function editar($request, $id=null) {
        $validator = Validator::make($request->all(), [
            'slug' => 'required', Rule::unique('secciones')->ignore($id), new NoSpaces,
            'titulo' => 'required|string|max:255',
        ]);

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) {
            $edit = true;
            $seccion = Seccion::find($id);
            $route = 'admin.secciones.index';
        } else {
            $edit = false;
            $seccion = new Seccion;
            $seccion->active = 1;
            $route = 'admin.secciones.create';
        }

        if($request->input('seccion_id') !== null && is_numeric($request->input('seccion_id')) && $request->input('seccion_id') > 0){
            $seccion->seccion_id = $request->input('seccion_id');
        } else {
            $seccion->seccion_id = null;
        }

        // Testeo si slug ya existe
        $slug_existe = Seccion::findBySlug($request->input('slug'));

        if($slug_existe !== null){ // slug ya existe

            if($edit && $slug_existe->id == $seccion->id){
                // Existe el slug, pero es el mismo id asi que todo bien porque esta editando solamente

            } else if($seccion->seccion_id !== null) {
                // Hay un grupo parent que se quiere guardar ahorita
                // Como voy a guardar algunos slugs repetidos, debo verificar que los siblings inmediatos
                // no tengan el mismo slug, pero los siblings de otros grupos no importa
                $siblings = Seccion::where('slug', $request->input('slug'));
                $siblings = $siblings->where('seccion_id', $seccion->seccion_id);
                $siblings = $siblings->get();

                if(count($siblings) > 0){
                    // esta repetido con otro child
                    $error = \Illuminate\Validation\ValidationException::withMessages([
                        'slug' => ['Ya existe ese slug (child)']
                    ]); throw $error;
                } else {
                    // tod bien exite pero es child no repetido
                }

            } else {
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'slug' => ['Ya existe ese slug']
                ]); throw $error;
            }
        }

        if ($validator->fails()) {
            if($edit){
                return redirect('admin.secciones.edit', $seccion->id)->withErrors($validator)->withInput();
            } else {
                return redirect('admin.secciones.create')->withErrors($validator)->withInput();
            }
        } else {
            $seccion->slug = $request->input('slug');
            $seccion->titulo = $request->input('titulo');

            if( $request->input('has_children') !== null){
                $seccion->has_children = $request->input('has_children');
            } else {
                $seccion->has_children = 0;
            }

            if( $request->input('has_imgs') !== null){
                $seccion->has_imgs = $request->input('has_imgs');
            } else {
                $seccion->has_imgs = 0;
            }

            if( $request->input('has_categorias') !== null){
                $seccion->has_categorias = $request->input('has_categorias');
            } else {
                $seccion->has_categorias = 0;
            }

            if( $request->input('categorias_administrables') !== null){
                $seccion->categorias_administrables = $request->input('categorias_administrables');
            } else {
                $seccion->categorias_administrables = 0;
            }

            if( $request->input('sortable') !== null){
                $seccion->sortable = $request->input('sortable');
            } else {
                $seccion->sortable = 0;
            }

            if( $request->input('adjuntos') !== null){
                $seccion->adjuntos = $request->input('adjuntos');
            } else {
                $seccion->adjuntos = 0;
            }
    
            if( $request->input('has_related') !== null){
                $seccion->has_related = $request->input('has_related');
            } else {
                $seccion->has_related = 0;
            }

            $seccion->save();

            Session::flash('success', 'La sección fue guardada con éxito');
            return redirect()->route($route, $seccion->id);
        }
    }

    public static function getSeccionesPricipales(){
        $secciones = self::where('active', 1)
            ->whereNull('seccion_id')
            ->orderBy('titulo', 'asc')
            ->get();
        return $secciones;
    }
    
    public function newFormEntry(Request $request){
    
    }

}
