<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{

    public static function findBySlug($slug, $varios=false){
        $elem = self::where('slug', '=', $slug);
        if($varios){
            $elem = $elem->get();
        } else {
            $elem = $elem->first();
        }
        return $elem;
    }

    public static function slugExists($slug) {
        $slugExists = self::where('slug', '=', $slug)->first();
        if($slugExists) return true;
        return false;
    }

    public static function strToSlug($str) {
        $cleanStr = clean($str);
        if(self::slugExists($cleanStr)){
            return self::strToSlug($cleanStr.'-1');
        } else {
            return strtolower($cleanStr);
        }
    }
}
