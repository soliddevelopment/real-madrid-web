<?php

namespace App\Models\Custom;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;
use Validator;
use Carbon\Carbon;

use App\Http\Traits\Crud;
use App\Models\Base;

class Area extends Base
{
    use Crud, SoftDeletes;

    protected $table = 'areas';


}
