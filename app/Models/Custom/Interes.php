<?php

namespace App\Models\Custom;
use Illuminate\Http\Request;

use DB;
use Validator;
use Carbon\Carbon;

use App\Http\Traits\Crud;
use App\Models\Base;

use App\Mail\EmailGenerico;
use App\Models\Fijos\Config;

use App\Models\Fijos\Post;

class Interes extends Base
{
    use Crud;

    protected $table = 'intereses';


    public function servicio(){
        return $this->belongsTo('App\Models\Fijos\Post', 'servicio_id');
    }

    public static function store(Request $request){
        $validator = Validator::make($request->all(), [
            // 'slug' => 'required', Rule::unique('secciones')->ignore($id), new NoSpaces,
            'nombre' => 'required|string|max:255',
            'email' => 'required|email',
        ]);

        $elem = new self;

        if ($validator->fails()) {
            return self::errorResponse('Error en los datos enviados', 'VALIDATOR');

        } else {

            $elem->servicio_id = $request->input('servicio_id');
            $elem->nombre = $request->input('nombre');
            $elem->apellido = $request->input('apellido');
//            $elem->fecha_nacimiento = $request->input('fecha_nacimiento');
            $elem->telefono = $request->input('telefono');
            $elem->email = $request->input('email');
            $elem->save();

            if($configEmailContacto = Config::where('slug', 'email-unete')->first()) {
                $emailContacto = $configEmailContacto->getVal();
                Mail::to($emailContacto)->send(new EmailGenerico(
                    'Nuevo mensaje',
                    'Nueva persona interesada en unirse',
                    'Esta persona está interesada en formar parte del equipo de 0MN1.',
                    asset('/p/img/logo-email.png'),
                    route('admin.contactos.detalle', $elem->id),
                    'Ir a mensaje',
                    'app@omni.cr',
                    '0MN1'
                ));
            }
            return self::successResponse($elem, $validator);
        }
    }


}
