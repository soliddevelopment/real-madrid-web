<?php

namespace App\Policies;
use Illuminate\Auth\Access\HandlesAuthorization;

use App\Models\Fijos\User;

class UserPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function view(User $user, User $userEdit){
        if($user->hasRole('superadmin') || $user->hasRole('admin')){
            return true; // todos los admins y superadmins se pueden ver entre si
        }
        // si no es admin ni superadmin, puede verse solamente si es el mismo
        $sameId = $user->id == $userEdit->id;
        return $sameId;
    }

    public function update(User $user, User $userEdit){
        if($user->hasRole('superadmin') || $user->id == $userEdit->id){
            return true; // si es superadmin o si el user id es el mismo
        }

        if($user->hasRole('admin')){
            if($user->hasRole('superadmin') || $user->hasRole('admin')){
                return false; // es un admin intentando editar a otro admin o superadmin
            } else {
                return true; // es un admin editando a otro
            }
        }

        // TODO: aqui va a hacerse la logica futura de usuarios colaboradores

        return false;

    }

    public function delete(User $user, User $userEdit){
        if($user->hasRole('superadmin')){
            return true;
        }

        if($user->hasRole('admin')){
            if($user->hasRole('superadmin') || $user->hasRole('admin')){
                return false; // es un admin intentando editar a otro admin o superadmin
            } else {
                return true; // es un admin editando a otro
            }
        }

        $sameId = $user->id == $userEdit->id;
        return $sameId;
    }
}
