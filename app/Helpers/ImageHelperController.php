<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

class ImageHelperController extends \App\Http\Controllers\Controller
{
    
    /*public function getImg($img, $w=200, $h=200)
    {
        return Image::make(public_path("/storage/".$img))->resize($w, $h)->response('jpg');
    }*/
    public function getImg()
    {
        $file = Input::get('img', null);
        $w = Input::get('w', false);
        $h = Input::get('h', false);
        
        $img = Image::make(public_path($file));
    
        if($w !== null && $h !== null){
        } else {
            $img->resize($w, $h, function ($constraint) use ($w, $h) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }
        
        return $img;
    }
}
