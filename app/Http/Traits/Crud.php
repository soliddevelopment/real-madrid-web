<?php

namespace App\Http\Traits;
// use Illuminate\Database\Eloquent\Model;

trait Crud
{
    public static function successResponse($model=null, $validator=null, $redirect_route=null, $custom_message=null){
        return [
            'success' => true,
            'data' => $model,
            'error' => null,
            'error_code' => null,
            'validator' => $validator,
            'redirect_route' => $redirect_route,
            'custom_message' => $custom_message,
        ];
    }

    public static function errorResponse($error='', $errorCode='', $model=null, $validatorErrors=[], $redirect_route=null){
        return [
            'success' => false,
            'data' => $model,
            'error' => $error,
            'error_code' => $errorCode,
            'validator' => $validatorErrors,
            'redirect_route' => $redirect_route
        ];
    }
}
