<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class CheckAdminRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null){
            // return response('No tiene permisos para entrar aquí', 401);
            // return response('No tiene permisos para entrar aquí 1 ', 401);

            // return response('No tiene permisos para entrar aquí', 401);
    
        }
        
        $actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null;
        
        if($request->user()->hasAnyRole($roles) || $roles === null){
            return $next($request);
        }
        
        $roles = DB::table('userroles')
            ->leftJoin('user_userrole', 'user_userrole.userrole_id', '=', 'userroles.id')
            ->select('userroles.*', 'user_userrole.user_id AS user_id')
            ->where('user_userrole.user_id', '=', $request->user()->id)
            ->get();
        
        if(count($roles)>0){
            return redirect()->route('admin.home.home');
        } else {
            return redirect()->route('publico.home.home');
        }
    
    
    }
    
}
