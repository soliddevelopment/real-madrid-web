<?php

namespace App\Http\Middleware;

use App\Models\Fijos\Config;
use Closure;
use Route;

class CheckForComingSoonMode
{
    public function handle($request, Closure $next)
    {
        $configConstruccion = Config::where('slug', '=', 'sitio-en-construccion')->first();
        $route = Route::getRoutes()->match($request);
        $currentroute = $route->getName();
        if($currentroute !== 'publico.home.proximamente' && $configConstruccion->valor_bool == 1){
            return redirect()->route('publico.home.proximamente');
        }
        return $next($request);
    }
}
