<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class CheckApiRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null){
            return response('No tiene permisos suficientes para realizar esta accion', 401);
            // return redirect()->route('admin.home.home');
            // return response('No tiene permisos para entrar aquí 1 ', 401);
        }

        $actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null;

        if($request->user()->hasAnyRole($roles) || !$roles){
            return $next($request);
        }

        $roles = DB::table('userroles')
            ->leftJoin('user_userrole', 'user_userrole.userrole_id', '=', 'userroles.id')
            ->select('userroles.*', 'user_userrole.user_id AS user_id')
            ->where('user_userrole.user_id', '=', $request->user()->id)
            ->get();

        if(count($roles)>0){
            return response('No tiene permisos suficientes para realizar esta accion', 403);
        } else {
            // return redirect()->route('publico.home.home');
            return response('No tiene permisos suficientes para realizar esta accion', 403);
        }

        // return response('No tiene permisos para entrar aquí 2', 401);

    }

}
