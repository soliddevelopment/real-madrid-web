<?php

namespace App\Http\Middleware;

use Closure;

class UnderConstruction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = $request->route()->getName();
        if ($routeName == 'publico.construccion') {
            return $next($request);
        } else if($request->query('isTester') || $request->hasCookie('isTester')){
            $response = $next($request);
            return $response->withCookie(cookie()->forever('isTester', 1));
        }
        return redirect()->route('publico.construccion');
    }
}
