<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckPublicoRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null){
            return redirect()->route('publico.auth.login');
        }
        return $next($request);

        /*$actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null;
        if($request->user()->hasAnyRole($roles) || !$roles){
            return $next($request);
        }*/

        // return response('No tiene permisos para entrar aquí 2', 401);
        // return redirect()->route('publico.auth.register');

    }

}
