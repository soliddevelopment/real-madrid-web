<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Session;
use Validator;
use View;
use App\Models\Fijos\Seccion;
use App\Models\Fijos\Post;

class SeccionesController extends BaseController
{

    public function index()
    {
        $secciones = Seccion::orderBy('titulo', 'asc')->get();
        $datosView = [
            'seccion_nombre' => 'Secciones',
            'seccion_titulo' => 'Secciones',
            'txt_nuevo' => 'Nueva',
            'crumbs' => [
                [
                    'title' => 'Secciones',
                    'route' => route('admin.secciones.index')
                ]
            ]
        ];
        $view = View::make('admin.secciones.index', [
            'secciones' => $secciones,
            'datosView' => $datosView
        ]);
        return $view;
    }

    public function edit($id=null)
    {
        $secciones = Seccion::where('active', 1)->where('has_children', 1)->orderBy('created_at', 'desc')->get();
        if($id!=null){
            $seccion = Seccion::find($id);
            $title = 'Editar sección: '.$seccion->titulo;
        } else {
            $seccion = null;
            $title = 'Nueva sección';
        }
        $datosView = [
            'seccion_nombre' => $title,
            'seccion_titulo' => $title,
            'txt_nuevo' => 'Nueva',
            'crumbs' => [
                [
                    'title' => 'Secciones',
                    'route' => route('admin.secciones.index')
                ],
                [
                    'title' => $title,
                    'route' => null
                ],
            ]
        ];
        $view = View::make('admin.secciones.edit', [
            'title' => $title,
            'seccion' => $seccion,
            'secciones' => $secciones,
            'datosView' => $datosView
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        return Seccion::editar($request, $id);
    }

    public function destroy($id)
    {
        $seccion = Seccion::find($id);
        $seccion->delete();
        Session::flash('success', 'La sección fue borrada');
        return redirect()->route('admin.secciones.index');
    }

    public function st(Request $request, $id)
    {
        $seccion = Seccion::find($id);
        $seccion->active = $request->input('val');
        $seccion->save();

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

}
