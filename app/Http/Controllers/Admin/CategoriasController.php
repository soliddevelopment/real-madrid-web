<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;

use View;
use Image;
use Validator;
use Session;

use App\Models\Fijos\Categoria;
use App\Models\Fijos\Seccion;
use App\Models\Fijos\Post;

class CategoriasController extends BaseController
{

    public function index(Request $request, $seccionSlug=null)
    {
        $usr = $request->user();
        $seccion = Seccion::where('slug', $seccionSlug)->first();
        $categorias = $seccion->getCategorias(false);

        $datosView = [
            'seccion_titulo' => 'Categorías de '.$seccion->titulo,
            'crumbs' => [
                [
                    'title' => $seccion->titulo,
                    'route' => route('admin.secciones.posts.index', $seccion->slug)
                ],
                [
                    'title' => 'Categorías',
                    // 'route' => route('admin.secciones.index')
                    'route' => null
                ]
            ]
        ];

        /*$parent = Post::find($request->input('parent_id'));
        if($parent === null) $parent_id = null; else $parent_id = $parent->id;
        $crumbs = $seccion->getCrumbsList($parent_id);*/

        if($usr->hasRole('superadmin') || ($seccion->has_categorias === 1 && $seccion->categorias_administrables === 1)){

            $view = View::make('admin.secciones.categorias.index', [
//                'parent' => $parent,
                'datosView' => $datosView,
                'categorias' => $categorias,
                'seccion' => $seccion,
            ]);
            return $view;
        } else {
            Session::flash('error', 'Las categorías de esa sección no son administrables');
            return redirect()->route('admin.home.home');
        }
    }

    public function edit(Request $request, $seccion_id=null, $id=null)
    {
        if($id!=null){
            $edit = true;
            $categoria = Categoria::find($id);
            $seccion = Seccion::find($categoria->seccion_id);
            $title = 'Editar categoría de '.$seccion->titulo;
        } else {
            $edit = false;
            $categoria = null;
            $seccion = Seccion::find($seccion_id);
            $title = 'Nueva categoría de '.$seccion->titulo;
        }

        /*$parent = Post::find($request->input('parent_id'));
        if($parent === null) $parent_id = null; else $parent_id = $parent->id;
        $crumbs = $seccion->getCrumbsList($parent_id);*/

        $usr = $request->user();

        if($usr->hasRole('superadmin') || ($seccion->has_categorias === 1 && $seccion->categorias_administrables === 1)){

            $datosView = [
                'seccion_titulo' => 'Editar',
                'crumbs' => [
                    [
                        'title' => $seccion->titulo,
                        'route' => route('admin.secciones.posts.index', $seccion->slug)
                    ],
                    [
                        'title' => 'Categorías',
                        'route' => route('admin.secciones.categorias.index', $seccion->slug)
                    ],
                    [
                        'title' => $edit ? 'Editar' : 'Nueva',
                        'route' => null
                    ]
                ]
            ];

            $view = View::make('admin.secciones.categorias.edit', [
//                'parent' => $parent,
                'title' => $title,
                'seccion' => $seccion,
                'datosView' => $datosView,
                'categoria' => $categoria,
            ]);
            return $view;

        } else {
            Session::flash('error', 'Las categorías de esa sección no son administrables');
            return redirect()->route('admin.home.home');
        }
    }

    public function store(Request $request, $id=null)
    {
        $validator = Validator::make($request->all(), [
            // 'slug' => 'required', Rule::unique('grupos')->ignore($id), new NoSpaces,
            'titulo' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'img' => 'max:3000|mimes:jpeg,jpg,bmp,png',
            'seccion_id' => 'required|integer|min:1',
        ]);

        $seccion = Seccion::find($request->input('seccion_id'));

        if($id!==null){
            $edit = true;
            $categoria = Categoria::find($id);
        } else {
            $edit = false;
            $categoria = new Categoria;
        }

        if ($validator->fails()) {
            if($edit){
                return redirect('admin.secciones.categorias.edit', [ 'seccion_id'=>$seccion->id, 'id' => $categoria->id ])->withErrors($validator)->withInput();
            } else {
                return redirect('admin.secciones.categorias.create', $seccion->id)->withErrors($validator)->withInput();
            }
        } else {

            // Testeo si slug ya existe
            $slug_existe = Categoria::where('seccion_id', $seccion->id)->where('slug', $request->input('slug'))->first();
            if($slug_existe !== null){ // slug ya existe
                if($edit && $slug_existe->id == $id){
                    // Existe pero todo bien porq es el mismo id
                } else {
                    $error = \Illuminate\Validation\ValidationException::withMessages([
                        'slug' => ['Ya existe ese slug']
                    ]); throw $error;
                }
            }

            $categoria->seccion_id = $seccion->id;
            $categoria->slug = $request->slug;
            $categoria->titulo = $request->titulo;

            /*if($request->hasFile('img')){
                $file = $request->file('img');
                $imgpath = 'cat-'.time().'-'.$file->getClientOriginalName();
                // $img = Image::make($file)->resize(320, 240)->save($imgpath);
                $img = Image::make($file)->save('storage/'.$imgpath);
                $categoria->img = $imgpath;
            }*/

            $categoria->save();

            Session::flash('success', 'La categoría fue guardada con éxito.');

            if($edit){
                return redirect()->route('admin.secciones.categorias.index', ['seccionSlug' => $seccion->slug]);
//                return redirect()->route('admin.secciones.categorias.edit', ['seccion_id' => $seccion->id, 'id' => $categoria->id]);
            } else {
                return redirect()->route('admin.secciones.categorias.create', ['seccion_id' => $seccion->id]);
            }
        }

    }

    public function destroy($id)
    {
        $categoria = Categoria::find($id);
        $seccion = Seccion::find($categoria->seccion_id);
        $categoria->delete();
        Session::flash('success', 'La categoría fue borrada!');
        return redirect()->route('admin.secciones.categorias.index', ['seccionSlug'=>$seccion->slug]);
    }

    public function st(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        $categoria->active = $request->input('val');
        $categoria->save();
        return response()->json([ 'success' => true, 'error' => null ]);
    }
}
