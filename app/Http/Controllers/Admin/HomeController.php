<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;

use App\Models\User;
use App\Models\Grupo;
use App\Models\Fijos\Config;

class HomeController extends BaseController
{
    public function index()
    {
        $configCorreo = Config::where('slug', '=', 'email-contacto')->first();
        $view = View::make('admin.home.home', [
            'configCorreo' => $configCorreo,
        ]);
        return $view;
    }
}
