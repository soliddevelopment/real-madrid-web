<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;

use View;
use Image;
use Validator;
use Session;

use App\Models\Fijos\Contacto;

class ContactosController extends BaseController
{

    public function index(Request $request)
    {
        $usr = $request->user();
        $contactos = Contacto::orderBy('id', 'desc')->get();
        $datosView = [
            'seccion_titulo' => 'Contactos',
            'crumbs' => [
                [
                    'title' => 'Contactos',
                    'route' => null
                ]
            ]
        ];
        $view = View::make('admin.contactos.index', [
            'datosView' => $datosView,
            'contactos' => $contactos,
        ]);
        return $view;
    }

    public function edit(Request $request, $id=null)
    {
        if($id!=null){
            $edit = true;
            $contacto = Contacto::find($id);
            $title = 'Editar contacto';
        } else {
            $edit = false;
            $contacto = null;
            $title = 'Nuevo contacto';
        }

        $usr = $request->user();

        $datosView = [
            'seccion_titulo' => 'Editar',
            'crumbs' => [
                [
                    'title' => 'Contactos',
                    'route' => route('admin.contactos.index')
                ],
                [
                    'title' => $edit ? 'Editar' : 'Nuevo',
                    'route' => null
                ]
            ]
        ];

        $view = View::make('admin.contactos.edit', [
            'title' => $title,
            'datosView' => $datosView,
            'contacto' => $contacto,
        ]);
        return $view;

    }

    public function detalle(Request $request, $id=null)
    {
        $contacto = Contacto::find($id);
        $usr = $request->user();
        $datosView = [
            'seccion_titulo' => 'Detalle contacto #'.$contacto->id,
            'crumbs' => [
                [
                    'title' => 'Contactos',
                    'route' => route('admin.contactos.index')
                ],
                [
                    'title' => 'Detalle contacto',
                    'route' => null
                ]
            ]
        ];
        $view = View::make('admin.contactos.detalle', [
            'datosView' => $datosView,
            'contacto' => $contacto,
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        $response = Contacto::store($request);
        if($response['success']){
            Session::flash('success', 'El contacto fue guardado con éxito.');
            if($id !== null){
                return redirect()->route('admin.contactos.index');
            } else {
                return redirect()->route('admin.contactos.create');
            }
        } else {
            Session::flash('error', 'No se pudo guardar el contacto.');
            if($id === null){
                return redirect()->route('admin.contactos.create')->withErrors($response['validator'])->withInput();
            } else {
                return redirect()->route('admin.contactos.edit', $id)->withErrors($response['validator'])->withInput();
            }
        }
    }

    public function destroy($id)
    {
        $contacto = Contacto::find($id);
        $contacto->delete();
        Session::flash('success', 'El contacto fue borrado!');
        return redirect()->route('admin.contactos.index');
    }

}
