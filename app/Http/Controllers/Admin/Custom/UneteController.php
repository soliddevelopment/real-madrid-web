<?php

namespace App\Http\Controllers\Admin\Custom;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseController;

use View;
use Image;
use Validator;
use Session;

use App\Models\Custom\Interes;

class UneteController extends BaseController
{

    public function index(Request $request)
    {
        $contactos = Interes::orderBy('id', 'desc')->get();
        $datosView = [
            'seccion_titulo' => 'Contactos interesados',
            'crumbs' => [
                [
                    'title' => 'Contactos',
                    'route' => null
                ]
            ]
        ];
        $view = View::make('admin.custom.intereses.index', [
            'datosView' => $datosView,
            'contactos' => $contactos,
        ]);
        return $view;
    }

    public function destroy($id)
    {
        $contacto = Interes::find($id);
        $contacto->delete();
        Session::flash('success', 'El item fue borrado!');
        return redirect()->route('admin.intereses.index');
    }

}
