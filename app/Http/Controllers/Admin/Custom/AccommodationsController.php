<?php
namespace App\Http\Controllers\Admin\Custom;
use App\Models\Fijos\Gallery;
use Illuminate\Http\Request;

use Auth;
use View;
use Session;

use App\Http\Controllers\Admin\BaseController;
use App\Models\Custom\Accommodation;
use App\Models\Custom\Amenity;

class AccommodationsController extends BaseController
{
    public function index()
    {
        $elementos = Accommodation::orderBy('order', 'asc')->get();
        $datosView = [
            'seccion_titulo' => 'Accommodations',
            'txt_nuevo' => 'Nuevo',
            'crumbs' => [
                [
                    'title' => 'Accommodations',
                    'route' => route('admin.custom.accommodations.index')
                ]
            ]
        ];
        $view = View::make('admin.custom.accommodations.index', [
            'elementos' => $elementos,
            'datosView' => $datosView
        ]);
        return $view;
    }

    public function edit($id=null)
    {
        if($id!=null){
            $elem = Accommodation::find($id);
            $title = 'Editar';
            $amenities = $elem->amenities;
        } else {
            $elem = null;
            $title = 'Nuevo';
            $amenities = [];
        }
        $datosView = [
            'seccion_nombre' => $title,
            'seccion_titulo' => $title,
            'txt_nuevo' => 'Nuevo',
            'crumbs' => [
                [
                    'title' => 'Accommodations',
                    'route' => route('admin.custom.accommodations.index')
                ],
                [
                    'title' => $title,
                    'route' => null
                ],
            ]
        ];

        $galleries = Gallery::all();

        $view = View::make('admin.custom.accommodations.edit', [
            'elem' => $elem,
            'galleries' => $galleries,
            'amenities' => $amenities,
            'datosView' => $datosView,
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        $response = Accommodation::store($request, $id);
        if($response['success']){
            Session::flash('success', 'El item fue guardado con éxito');
            return redirect()->route('admin.custom.accommodations.index');
        } else {
            /*if($response['error_code'] === 'EMPRESA'){
                $response['validator']->getMessageBag()->add('empresa', 'No existe esa empresa');
            }*/

            if($id === null){
                return redirect()->route('admin.custom.accommodations.create')->withErrors($response['validator'])->withInput();
            } else {
                return redirect()->route('admin.custom.accommodations.edit', $id)->withErrors($response['validator'])->withInput();
            }
        }
    }

    public function destroy($id)
    {
        $elem = Accommodation::find($id);
        $elem->delete();
        Session::flash('success', 'El ítem fue borrado');
        return redirect()->route('admin.custom.accommodations.index');
    }

    public function st(Request $request, $id)
    {
        $elem = Accommodation::find($id);
        $elem->active = $request->input('val');
        $elem->save();
        return response()->json([ 'success' => true, 'error' => null ]);
    }

    public function sort(Request $request){
        $elemsArr = $request->input('elem');
        $x=1;
        foreach($elemsArr as $item){
            $obj = Accommodation::find($item);
            if($obj !== null){
                $obj->order = $x;
                $obj->save();
                $x++;
            }
        }
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

}
