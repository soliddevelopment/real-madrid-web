<?php
namespace App\Http\Controllers\Admin;
use App\Models\Fijos\MediaFile;
use Illuminate\Http\Request;

use Auth;
use View;
use Session;

use App\Models\Fijos\Gallery;
use App\Models\Fijos\GalleryMediaFile;
use App\Models\Fijos\Post;

class GalleriesController extends BaseController
{
    public function index()
    {
        $elementos = Gallery::orderBy('id', 'desc')
            ->whereNull('post_id')
            ->whereNull('ent_id')
            ->get();
        $datosView = [
            'seccion_titulo' => 'Galerías',
            'txt_nuevo' => 'Nuevo',
            'crumbs' => [
                [
                    'title' => 'Galerías',
                    'route' => route('admin.galleries.index')
                ]
            ]
        ];
        $view = View::make('admin.galleries.index', [
            'elementos' => $elementos,
            'datosView' => $datosView
        ]);
        return $view;
    }

    public function edit($id=null, Request $request)
    {
        if($id!=null){
            $elem = Gallery::find($id);
            $title = 'Editar';
        } else {
            $elem = null;
            $title = 'Nuevo';
        }
        $datosView = [
            'seccion_nombre' => $title,
            'seccion_titulo' => $title,
            'txt_nuevo' => 'Nueva',
            'crumbs' => [
                [
                    'title' => 'Galerías',
                    'route' => route('admin.galleries.index')
                ],
                [
                    'title' => $title,
                    'route' => null
                ],
            ]
        ];

        /*$post = Post::find($request->input('post_id'));
        if($post == null){
            Session::flash('error', 'Ocurrió un error, vuelve a intentarlo.');
            return redirect()->route('admin.home.home');
        }*/

        $view = View::make('admin.galleries.edit', [
            'elem' => $elem,
            'datosView' => $datosView,
        ]);
        return $view;
    }

    public function detail($id=null) {
        if($id!=null && $gallery = Gallery::find($id)){
            /*$imgs = $gallery->mediafiles;
            $imgs = $imgs->sortBy('order');*/
            $cols = [
                'media_files.id', 'media_files.mime_type', 'media_files.nombre', 'media_files.filename',
                'gallery_mediafile.order'
            ];

            $imgs = MediaFile::select($cols)
                ->join('gallery_mediafile', 'gallery_mediafile.mediafile_id', 'media_files.id')
                ->where('gallery_mediafile.gallery_id', $gallery->id)
                ->orderBy('gallery_mediafile.order', 'asc')
                ->get();

            foreach($imgs as $img){
                //$img = MediaFile::datosAdicionales($img);
                $img->route = asset('storage/mediamanager/'.$img->filename);
            }

            $title = 'Administrar galería';
        } else {
            return redirect()->route('admin.galleries.index');
        }

        if($gallery->post != null){
            $crumbsArray = [
                [
                    'title' => $gallery->post->seccion->titulo,
                    'route' => route('admin.secciones.posts.index', $gallery->post->seccion->slug)
                ],
                [
                    'title' => $gallery->post->titulo,
                    'route' => route('admin.secciones.posts.edit', ['id'=>$gallery->post->id, 'seccionSlug'=>$gallery->post->seccion->slug])
                ]
            ];
        } else {
            $crumbsArray = [
                [
                    'title' => 'Galerías',
                    'route' => route('admin.galleries.index')
                ]
            ];
        }

//        dd($gallery->post);

        $crumbsArray[] = [
            'title' => $title,
            'route' => null
        ];

        $datosView = [
            'seccion_nombre' => $title,
            'seccion_titulo' => $title,
            'txt_nuevo' => 'Nueva',
            'crumbs' => $crumbsArray
        ];

        $view = View::make('admin.galleries.detail', [
            'gallery' => $gallery,
            'imgs' => $imgs,
            'datosView' => $datosView,
        ]);
        return $view;
    }

    public function store(Request $request, $id=null)
    {
        $response = Gallery::store($request, $id);

        if($response['success']){
            Session::flash('success', $id ? 'La galería fue guardada con éxito' : 'Se creó una galería con éxito');
            return redirect()->route('admin.galleries.detail', $response['data']->id);
//            return redirect()->route('admin.galleries.index');

        } else {
            Session::flash('error', $response['error']);

            /*if($response['error_code'] === 'SLUG'){
                $response['validator']->getMessageBag()->add('slug', $response['error']);
            }*/

            if($id === null){
                return redirect()->route('admin.galleries.create')->withErrors($response['validator'])->withInput();
            } else {
                return redirect()->route('admin.galleries.edit', $id)->withErrors($response['validator'])->withInput();
            }

        }
    }

    public function destroy($id)
    {
        $elem = Gallery::find($id);
        $elem->delete();
        Session::flash('success', 'El ítem fue borrado');
        return redirect()->route('admin.galleries.index');
    }

    public function st(Request $request, $id)
    {
        $elem = Gallery::find($id);
        $elem->active = $request->input('val');
        $elem->save();
        return response()->json([ 'success' => true, 'error' => null ]);
    }

    public function galeriaPost(Request $request)
    {
        $response = Gallery::galeriaPost($request);
        if($response['success']){
            Session::flash('success', 'Las imágenes fueron agregadas a la galería.');
            return response()->json($response);
        } else {
            return response()->json($response);
            /*if($response['error_code'] === 'EMPRESA'){
                $response['validator']->getMessageBag()->add('empresa', 'No existe esa empresa');
            }*/
        }
    }

    public function sort(Request $request, $id=null){
        $elemsArr = $request->input('elem');
        $x=1;
        foreach($elemsArr as $item){
            $obj = GalleryMediaFile::where('gallery_id', $id)->where('mediafile_id', $item)->first();
            if($obj !== null){
                $obj->order = $x;
                $obj->save();
                $x++;
            }
        }
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

    public function deleteElem(Request $request, $id=null){
        if($gallery = Gallery::find($id)){
            $gallery->mediafiles()->detach($request->input('img_id'));
            return response()->json([
                'success' => true,
                'error' => null
            ]);
        } else {
            return response()->json([
                'success' => false,
                'error' => 'No se encontro esa galeria id #'.$id
            ]);
        }
    }

}
