<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;

use Session;
use DB;
use View;

use App\Models\Fijos\User;
use App\Models\Fijos\UserRole;

class UsersController extends BaseController
{
    public function index()
    {
        $user = Auth::user();
        $datosView = [
            'seccion_nombre' => 'Usuarios',
            'seccion_titulo' => 'Usuarios',
            'txt_nuevo' => 'Nuevo',
            'crumbs' => [
                [
                    'title' => 'Usuarios',
                    'route' => route('admin.users.index')
                ]
            ]
        ];
        $view = View::make('admin.users.index', [
             'datosView' => $datosView,
        ]);
        return $view;
    }

    public function edit(Request $request, $id=null)
    {
        if($id!=null){
            $userEdit = User::find($id);
            $title = $userEdit->usuario;
        } else {
            $userEdit = null;
            $title = 'Nuevo usuario';
        }

        $usr = $request->user();

        if($usr->hasRole('superadmin') || $usr->hasRole('admin')){
            $roles = UserRole::all()->where('slug', '!=', 'superadmin');
        } else {
            $roles = UserRole::all()
                ->where('slug', '!=', 'superadmin');
        }

        if($usr->can('update', $userEdit)){
            $canMakeChanges = true;
        } else {
            $canMakeChanges = false;
        }

        $datosView = [
            'seccion_nombre' => $title,
            'seccion_titulo' => $title,
            'txt_nuevo' => 'Nuevo',
            'crumbs' => [
                [
                    'title' => 'Usuarios',
                    'route' => route('admin.users.index')
                ],
                [
                    'title' => $title,
                    'route' => null
                ]
            ]
        ];

        $view = View::make('admin.users.edit', [
            'canMakeChanges' => $canMakeChanges,
            'user' => $userEdit,
            'roles' => $roles,
            'title' => $title,
            'datosView' => $datosView,
        ]);
        return $view;
    }
    
    public function store(Request $request, $id=null)
    {
        $response = User::editar($request, $id);
        if($response['success']){
            Session::flash('success', 'El usuario fue guardado con éxito.');
            return redirect()->route('admin.users.index');
        } else {
            Session::flash('error', $response['error']);
            if ($id) {
                return redirect()->route('admin.users.edit', $id)->withErrors($response['validator'])->withInput();
            } else {
                return redirect()->route('admin.users.create')->withErrors($response['validator'])->withInput();
            }
        }
    }

    public function updatePassword(Request $request, $id)
    {
        // validate data
        $this->validate($request, [
            'password' => 'string|min:6|confirmed',
            'password_confirmation' => 'min:6'
        ]);

        $user = Auth::user(); // el admin loggeado
        $usuarioEditado = User::find($id); // el usuario siendo editado

        $continuar = $this->permisosEditarUsuario($user, $usuarioEditado);

        if($continuar){
            $usuarioEditado->password = bcrypt($request->input('password'));
            $usuarioEditado->save();
            Session::flash('success', 'La contraseña del usuario se modificó exitosamente');
            return redirect()->route('admin.users.edit', $usuarioEditado->id);

        } else {
            // Un Admin esta tratando de editar a un Global
            $roles = Role::all();
            $err = ['No tiene permisos para editar a este usuario'];
            return redirect()->route('admin.users.index')->withErrors($err);
        }

    }

    public function permisosEditarUsuario($user, $usuarioEditado, $edit=true){
        $continuar = false;
        if($user->id == $usuarioEditado->id){
            $continuar = true;
        } else if($user->hasRole('superadmin')){
            $continuar = true;
        } else if( $user->hasRole('Admin') ){
            if($usuarioEditado->hasRole('superadmin') || $usuarioEditado->hasRole('admin')){ // admin tratando d editar a otro admin o a global
                $continuar = false;
            } else $continuar = true;

        } else if($user->hasRole('ClienteAdmin')){
            if(!$usuarioEditado->hasRole('superadmin') && !$usuarioEditado->hasRole('admin')){
                // verificar que usuario editado pertenece a misma cuenta q clienteadmin
                if($edit){
                    foreach($user->cuentas as $cu){
                        foreach($cu->users as $us){
                            if($us->id === $usuarioEditado->id){
                                $continuar = true;
                                break;
                            }
                        }
                    }
                } else {
                    // el usuario editado no va a pertenecer a niunguna cuenta todavia
                    $continuar = true;
                }
            } else {
                $continuar = false;
            }
        } else {
            $continuar = false;
        }
        return $continuar;
    }

    public function destroy($id)
    {
        /*$usuario = User::find($id);
        $usuario->roles()->detach();
        $usuario->delete();*/

        $usuarioEditado = User::find($id);
        $usuarioEditado->st2 = 0;
        $usuarioEditado->save(); // soft delete
        Session::flash('success', 'El usuario fue borrado!');
        return redirect()->route('admin.users.index');
    }

    public function st(Request $request, $id)
    {
        // Guardo info del usuario
        $usuarioEditado = User::find($id);
        $usuarioEditado->st = $request->input('val');
        $usuarioEditado->save();

        // HACER SOFT DELETE MEJOR
        // NO DEJAR BORRAR SI EL USUARIO TIENE ROLES

        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }



    public function listaAjax(Request $request)
    {
        $usr = $request->user();
        $columnas = [
            'users.id', 'users.nombre', 'users.apellido', 'users.usuario', 'users.email', 'users.telefono', 'users.created_at', DB::raw('userroles.nombre AS role'),
        ];
        // $usuarios = User::where('active', 1)->leftJoin('userroles', 'users.role_id', 'userroles.id');
        $usuarios = DB::table('users')
            ->where('users.active', 1)
            ->whereNull('users.deleted_at')
            ->leftJoin('userroles', 'users.role_id', 'userroles.id');
        if(!$usr->hasRole('superadmin')){ $usuarios = $usuarios->where('userroles.slug', '!=', 'superadmin'); }
        $usuarios = $usuarios->get($columnas);
        return Datatables::of($usuarios)->make();
    }

    public function importar(Request $request)
    {
        $usr = $request->user();
        $roles = Role::all()->where('nombre', '!=', 'superadmin');
        $view = View::make('admin.users.importar.edit', [
            'roles' => $roles
        ]);
        return $view;
    }

    public function importarPost(Request $request)
    {
        if($request->hasFile('file')){
            $file = $request->file('file');
            $role_id = $request->input('role');
            $role_selected = Role::find($role_id);
            Excel::import(new UsersImport($role_selected), request()->file('file'));
            Session::flash('success', 'El usuario fue guardado con éxito.');
            return redirect()->route('admin.users.index');

        } else {
            Session::flash('error', 'Debe subir un archivo de excel');
            return redirect()->route('admin.users.index');
        }

    }
}
