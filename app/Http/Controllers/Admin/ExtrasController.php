<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;

use View;
use Image;
use Validator;
use Session;

use App\Models\Fijos\Extra;
use App\Models\Fijos\ExtraTipo;
use App\Models\Fijos\ExtraValor;
use App\Models\Fijos\Seccion;
use App\Models\Fijos\Post;

class ExtrasController extends BaseController
{

    public function index(Request $request, $seccionSlug=null)
    {
        $usr = $request->user();
        $seccion = Seccion::where('slug', $seccionSlug)->first();
        $extras = $seccion->getExtras();

        $datosView = [
            'seccion_titulo' => 'Extras de '.$seccion->titulo,
            'crumbs' => [
                [
                    'title' => $seccion->titulo,
                    'route' => route('admin.secciones.posts.index', $seccion->slug)
                ],
                [
                    'title' => 'Extras',
                    'route' => null
                ]
            ]
        ];

        if($usr->hasRole('superadmin')){
            $view = View::make('admin.secciones.extras.index', [
                'datosView' => $datosView,
                'extras' => $extras,
                'seccion' => $seccion,
                'modo' => 'ITEM',
                'edit_route_name' => 'admin.secciones.extras.edit',
                'destroy_route_name' => 'admin.secciones.extras.destroy',
            ]);
            return $view;
        } else {
            Session::flash('error', 'No se pudo realizar esa acción');
            return redirect()->route('admin.home.home');
        }
    }

    public function edit(Request $request, $seccion_id=null, $id=null)
    {
        if($id!=null){
            $edit = true;
            $extra = Extra::find($id);
            $seccion = Seccion::find($extra->seccion_id);
            $title = 'Editar extra de '.$seccion->titulo;
        } else {
            $edit = false;
            $extra = null;
            $seccion = Seccion::find($seccion_id);
            $title = 'Nueva extra de '.$seccion->titulo;
        }

        $usr = $request->user();
        $tipos = ExtraTipo::all();

        if($usr->hasRole('superadmin')){

            $datosView = [
                'seccion_titulo' => 'Editar',
                'crumbs' => [
                    [
                        'title' => $seccion->titulo,
                        'route' => route('admin.secciones.posts.index', $seccion->slug)
                    ],
                    [
                        'title' => 'Extras',
                        'route' => route('admin.secciones.extras.index', $seccion->slug)
                    ],
                    [
                        'title' => $edit ? 'Editar' : 'Nueva',
                        'route' => null
                    ]
                ]
            ];

            $view = View::make('admin.secciones.extras.edit', [
                'tipos' => $tipos,
                'title' => $title,
                'seccion' => $seccion,
                'datosView' => $datosView,
                'extra' => $extra,
                'modo' => 'ITEM',
                'new_route_name' => 'admin.secciones.extras.store',
                'edit_route_name' => 'admin.secciones.extras.update',
            ]);
            return $view;

        } else {
            Session::flash('error', 'Las extras de esa sección no son administrables');
            return redirect()->route('admin.home.home');
        }
    }

    public function store(Request $request, $id=null)
    {
        $response = Extra::store($request, $id, 'ITEM');
        if($response['success']){
            Session::flash('success', 'El ítem fue guardado con éxito.');
            return $response['redirect_route'];
        } else {
            if($response['error_code'] == 'SLUG'){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'slug' => ['Ya existe ese slug']
                ]); throw $error;
            } else if($response['error_code'] == 'VALIDATOR') {
                Session::flash('error', 'Por favor revise los campos requeridos.');
                return $response['redirect_route'];
            } else if($response['error_code'] == 'TIPO') {
                Session::flash('error', 'No existe ese tipo');
                return $response['redirect_route'];
            }
        }
    }

    public function destroy($id)
    {
        $extra = Extra::find($id);
        $seccion = Seccion::find($extra->seccion_id);
        $extra->delete();
        Session::flash('success', 'La extra fue borrada!');
        return redirect()->route('admin.secciones.extras.index', ['seccionSlug'=>$seccion->slug]);
    }

    public function st(Request $request, $id)
    {
        $extra = Extra::find($id);
        $extra->active = $request->input('val');
        $extra->save();
        return response()->json([ 'success' => true, 'error' => null ]);
    }

    public function sort(Request $request){
        $elemsArr = $request->input('elem');
        $x=1;
        foreach($elemsArr as $item){
            $obj = Extra::find($item);
            if($obj !== null){
                $obj->order = $x;
                $obj->save();
                $x++;
            }
        }
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

}
