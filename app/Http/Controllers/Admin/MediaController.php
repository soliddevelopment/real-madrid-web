<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Storage;

use App\Models\Fijos\User;
use App\Models\Fijos\MediaFile;
use App\Models\Fijos\MediaFolder;

class MediaController extends BaseController
{
    
    public function index()
    {
        return view('admin.media.index');
    }
    
    public function lista(Request $request)
    {
        $imgs = MediaFile::getLista($request);
        return $imgs;
    }

    public function postUpload(Request $request){
        return MediaFile::postUpload($request);
    }

    public function summernotePost(Request $request){
        $response = MediaFile::postUpload($request);
        if($response['success']){
            return mediaimg($response['file']->id, null, null, false);
        } else {
            return null;
        }
    }
    
    public function borrar(Request $request){
        return MediaFile::borrar($request);
    }
    
    public function edit(Request $request, $id=null){
        return MediaFile::edit($request, $id);
    }

    public function mover(Request $request){
        return MediaFile::mover($request);
    }

    public function listaFolders(Request $request){
        return MediaFolder::getLista($request);
    }
    public function crearFolder(Request $request){
        return MediaFolder::store($request);
    }
    public function borrarFolder(Request $request){
        return MediaFolder::borrar($request);
    }

}
