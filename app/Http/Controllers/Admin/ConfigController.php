<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use View;
use Session;
use Image;

use App\Models\Fijos\Config;
use App\Models\Fijos\MediaFile;

class ConfigController extends BaseController
{
    public function index(Request $request){
        $usr = $request->user();
        if($usr->hasRole('superadmin')){
            $configuraciones = Config::all();
        } else {
            $configuraciones = Config::all()->where('only_superadmin', '=', 0);
        }
        
        $datosView = [
            'seccion_nombre' => 'Configuración',
            'seccion_titulo' => 'Configuración',
            'txt_nuevo' => 'Nueva',
            'crumbs' => [
                [
                    'title' => 'Configuración',
                    'route' => route('admin.config.index')
                ]
            ]
        ];

        $view = View::make('admin.config.index', [
            'configuraciones' => $configuraciones,
            'datosView' => $datosView
        ]);
        return $view;
    }

    public function edit(Request $request, $id=null)
    {
        if($id!=null){
            $config = Config::find($id);
            $title = $config->titulo;
        } else {
            $config = null;
            $title = 'Nuevo item';
        }
    
        $datosView = [
            'seccion_nombre' => $title,
            'seccion_titulo' => $title,
            'txt_nuevo' => 'Nueva',
            'crumbs' => [
                [
                    'title' => 'Configuración',
                    'route' => route('admin.config.index')
                ],
                [
                    'title' => $title,
                    'route' => null
                ]
            ]
        ];
        
        $view = View::make('admin.config.edit', [
            'title' => $title,
            'config' => $config,
            'datosView' => $datosView
        ]);
        return $view;
    }


    public function store(Request $request, $id=null)
    {
        $this->validate($request, [
            'titulo' => 'required|string|max:255',
            'slug' => [
                'required', 'string', 'max:255',
                Rule::unique('configs')->ignore($id),
            ],
            'tipo' => 'required|string|max:255'
        ]);

        if(isset($id) && $id!=null && is_numeric($id) && $id > 0) {
            $edit = true;
            $config = Config::find($id);
        } else {
            $edit = false;
            $config = new Config;
        }

        $usr = $request->user();

        if($usr->hasRole('superadmin')){
            $config->only_superadmin = $request->input('only_superadmin');
            $config->tipo = $request->input('tipo');
            $config->slug = $request->input('slug');
            $config->titulo = $request->input('titulo');
            $config->descripcion = $request->input('descripcion');
        }

        $proceed = false;
        if($edit && $config->only_global == 0){
            $proceed = true;
        }

        if($usr->hasRole('superadmin') || ( !$usr->hasRole('superadmin') && $edit && $config->only_superadmin == 0 )){
            $config->valor_str = $request->input('valor_str');
            $config->valor_int = $request->input('valor_int');
            $config->valor_bool = $request->input('valor_bool');
            
            $valor_img = $request->input('valor_img');
            if(is_numeric($valor_img) && $valor_img > 0){
                $mediaImg = MediaFile::find($valor_img);
                if($mediaImg !== null){
                    $config->valor_img = $mediaImg->id;
                }
            }

            if($request->hasFile('valor_img')){
                /*$file = $request->file('valor_img');
                $oldfilename = $file->getClientOriginalName();
                $newfilename = str_replace(' ', '', $oldfilename);
                $imgpath = time().'-'.$newfilename;
                // $img = Image::make($file)->save('storage/'.$imgpath);
                $config->valor_img = $imgpath;
                
                Storage::disk('public')->put('file.txt', 'Contents');*/
            }
        }

        $config->save();

        Session::flash('success', 'La configuración fue guardada con éxito.');
        return redirect()->route('admin.config.index');
        dd($request->all());
    }
    
    public function destroy($id)
    {
        $config = Config::find($id);
        $config->delete();
        Session::flash('success', 'La configuración fue borrada!');
        return redirect()->route('admin.config.index');
    }
    
    public function st(Request $request, $id)
    {
        $config = Config::find($id);
        $config->valor_bool = $request->input('val');
        $config->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }
}
