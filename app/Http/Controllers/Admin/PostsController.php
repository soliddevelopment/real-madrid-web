<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;

use Nexmo\Account\Price;
use Session;
use Validator;
use View;
use DB;

use App\Models\Fijos\Seccion;
use App\Models\Fijos\Post;
use App\Models\Fijos\Categoria;
use App\Models\Fijos\Extra;
use App\Models\Fijos\Gallery;


class PostsController extends BaseController
{

    public function index(Request $request, $seccionSlug=null){
        if($seccionSlug !== null && strlen($seccionSlug)>0 && $seccion = Seccion::where('slug', $seccionSlug)->first()){

            if($seccion->seccion_id && $seccionParent = Seccion::find($seccion->seccion_id)){
                // tiene parent
            } else {
                // no tiene parent
                $seccionParent = null;
            }

            $parentPost = Post::find($request->input('parentPostId'));

            // Busqueda de cateogrias
            $categorias = $seccion->getCategorias();
            $selected_cat = Categoria::where('slug', $request->query('cat'))->first();

            // Busqueda de posts
            $posts = Post::where('seccion_id', $seccion->id);
            if($selected_cat) $posts = $posts->where('categoria_id', $selected_cat->id);
            if($seccion->sortable == 1){
                $posts = $posts->orderBy('order', 'asc');
            } else {
                $posts = $posts->orderBy('id', 'desc');
            }
            if($parentPost) $posts = $posts->where('post_id', $parentPost->id);
            $posts = $posts->get();
    
            $thumb = false;
            $extraImagenExists = $seccion->extras()->where('slug', '=', 'imagen')->first();
            if($extraImagenExists){
                $thumb = true;
                foreach($posts as $item){
                    $item->getSetExtras(['imagen', 'galeria']);
                    if($item->imagen){
                        $item->finalImg = mediaimg($item->imagen, 30, 30);
                    } else {
                        $item->finalImg = mediaimg(null, 30, 30);
                    }
                }
            }
    
            $galeria = false;
            $extraGaleriaExists = $seccion->extras()->where('slug', '=', 'galeria')->first();
            if($extraGaleriaExists){
                $galeria = true;
            }

            // Seteo crumbs dependiendo si tien parent o no
            if($seccionParent){
                if($parentPost){
                    $crumbsArray = $seccion->getCrumbsList($parentPost->id);
                } else {
                    // tiene seccion parent, pero no viene el id del post parent
                    Session::flash('error', 'Hubo un error de navegación, vuelve a intentarlo.');
                    return redirect()->route('admin.secciones.posts.index', $seccionParent->slug);
                }
            } else {
                $crumbsArray = [
                    [
                        'title' => ucfirst($seccion->titulo),
                        // 'route' => route('admin.secciones.index')
                        'route' => null
                    ]
                ];
            }
            $datosView = [
                'seccion_titulo' => ucfirst($seccion->titulo),
                'crumbs' => $crumbsArray
            ];

            $view = View::make('admin.secciones.posts.index', [
                'thumb' => $thumb,
                'galeria' => $galeria,
                'categorias' => $categorias,
                'selected_cat' => $selected_cat,
                'seccion' => $seccion,
                'datosView' => $datosView,
                'posts' => $posts,
                'parentPost' => $parentPost,
            ]);
            return $view;
        } else {
            Session::flash('error', 'Esa sección no existe...');
            return redirect()->route('admin.home.home');
        }

    }

    public function edit(Request $request, $seccionSlug=null, $id=null)
    {
        if($seccionSlug !== null && strlen($seccionSlug)>0 && $seccion = Seccion::where('slug', $seccionSlug)->first()){

            if($request->input('parentPostId')) {
                $parentPost = Post::find($request->input('parentPostId'));
            } else {
                $parentPost = null;
            }

            if($id!=null){
                $post = Post::find($id);
                $seccion = $post->seccion;
                $title = 'Editar ítem: '.$seccion->titulo;
                $extras = Extra::getForPost($post->id);
            } else {
                $post = null;
                $title = 'Nuevo ítem';
                $extras = Extra::getForSeccion($seccion->id);

                if($seccion->seccion_id){
                    if(!$parentPost){ // tiene parent pero no viene el post
                        Session::flash('error', 'Hubo un error de navegación, vuelve a intentarlo.');
                        return redirect()->route('admin.secciones.posts.index', $seccion->slug);
                    }
                }
            }

            $categorias = $seccion->getCategorias();
            
            if($seccion->has_related){
                $otrosPosts = Post::where('seccion_id', $seccion->id);
                if($post) $otrosPosts = $otrosPosts->where('id', '!=', $post->id);
                $otrosPosts = $otrosPosts->get();
                // aca debo ver para cada uno si tienen o no relacion actual al post
                foreach($otrosPosts as $otro){
                    if($post){
                        $isRelated = DB::table('posts_relations')
                            ->where('main_post_id', '=', $post->id)
                            ->where('related_id', '=', $otro->id)
                            ->get();
                        if($isRelated && count($isRelated)>0){
                            $otro->checked = true;
                        } else {
                            $otro->checked = false;
                        }
                    } else {
                        $otro->checked = false;
                    }
                }
            } else {
                $otrosPosts = [];
            }

            // galerias que ya estan adicionadas a este tipo d posts
            $galleriesExtras = [];
            foreach($extras as $ex){
                if($ex->tipo_slug == 'gallery'){
                    $galleriesExtras[] = $ex;
                }
            }

            // lista de todas las galerias para que pueda seleccionarlas en el control
            $galleries = Gallery::whereNull('post_id')->get();

            // Seteo crumbs dependiendo si tien parent o no
            if($parentPost){
                $crumbsArray = $seccion->getCrumbsList($parentPost->id);
            } else {
                $crumbsArray = [
                    [
                        'title' => $seccion->titulo,
                        'route' => route('admin.secciones.posts.index', $seccion->slug)
                    ]
                ];
            }

            $crumbsArray[] = [
                'title' => $title,
                'route' => null
            ];

            $datosView = [
                'seccion_nombre' => $title,
                'seccion_titulo' => $title,
                'txt_nuevo' => 'Nueva',
                'crumbs' => $crumbsArray
            ];

            $view = View::make('admin.secciones.posts.edit', [
                'title' => $title,
                'categorias' => $categorias,
                'otrosPosts' => $otrosPosts,
                'post' => $post,
                'extras' => $extras,
                'galleries' => $galleries,
                'galleriesExtras' => $galleriesExtras,
                'seccion' => $seccion,
                'datosView' => $datosView,
                'parentPost' => $parentPost,
            ]);
            return $view;
        } else {
            Session::flash('error', 'Ese ítem no existe...');
            return redirect()->route('admin.home.home');
        }
    }
    
    public function detalle(Request $request, $seccionSlug=null, $id=null)
    {
        if($seccionSlug !== null && strlen($seccionSlug)>0 && $seccion = Seccion::where('slug', $seccionSlug)->first()){
            
            if($id!=null){
                $post = Post::find($id);
                $seccion = $post->seccion;
                $title = $post->titulo;
                
                if($seccion->tipo == 'FORM'){
                    $modo = 'CAMPO';
                } else $modo = 'ITEM';
                $extras = Extra::getForPost($post->id, $modo);
            }
            
            $crumbsArray = [
                [
                    'title' => $seccion->titulo,
                    'route' => route('admin.secciones.posts.index', $seccion->slug)
                ],
                [
                    'title' => $title,
                    'route' => null
                ]
            ];
            
            $datosView = [
                'seccion_nombre' => $title,
                'seccion_titulo' => $title,
                'txt_nuevo' => 'Nueva',
                'crumbs' => $crumbsArray
            ];
            
            $view = View::make('admin.secciones.posts.detalle', [
                'title' => $title,
                'post' => $post,
                'extras' => $extras,
                'seccion' => $seccion,
                'datosView' => $datosView,
            ]);
            return $view;
        } else {
            Session::flash('error', 'Ese ítem no existe...');
            return redirect()->route('admin.home.home');
        }
    }

    public function store(Request $request, $id=null)
    {
        $response = Post::store($request, $id);
        if($response['success']){
            Session::flash('success', 'El ítem fue guardado con éxito.');
            return $response['redirect_route'];

        } else {

            if($response['error_code'] == 'SLUG'){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'slug' => [$response['error']]
                ]); throw $error;

            } else if($response['error_code'] == 'VALIDATOR') {
                Session::flash('error', 'Por favor revise los campos requeridos.');
                return $response['redirect_route'];

            } else {

            }

        }

    }

    public function destroy($id)
    {
        $item = Post::find($id);
        if($item !== null) {
            $seccionSlug = $item->seccion->slug;
            $item->delete();
            Session::flash('success', 'El ítem fue borrado');
            return redirect()->route('admin.secciones.posts.index', $seccionSlug);
        }
    }

    public function st(Request $request, $id)
    {
        $item = Post::find($id);
        $item->active = $request->input('val');
        $item->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

    public function destacado(Request $request, $id)
    {
        $item = Post::find($id);
        $item->destacado = $request->input('val');
        $item->save();
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

    public function sort(Request $request, $seccion_id=null){
        $elemsArr = $request->input('elem');
        $x=1;
        foreach($elemsArr as $item){
            $obj = Post::find($item);
            if($obj !== null){
                $obj->order = $x;
                $obj->save();
                $x++;
            }
        }
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }

}
