<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;

use View;
use Image;
use Validator;
use Session;

use App\Models\Fijos\Extra;
use App\Models\Fijos\ExtraTipo;
use App\Models\Fijos\ExtraValor;
use App\Models\Fijos\Seccion;
use App\Models\Fijos\Post;

class ContenidosController extends BaseController
{

    public function index(Request $request, $seccionSlug=null)
    {
        $usr = $request->user();

        if($seccionSlug != null) {
            $seccion = Seccion::where('slug', $seccionSlug)->first();
            $contenidos = $seccion->getContenidos();
        } else {
            $seccion = null;
            $contenidos = Extra::all()->where('modo', 'CONTENIDO')
                ->sortBy('order');
        }

        $datosView = [
            'seccion_titulo' => $seccion != null ? 'Contenidos de '.$seccion->titulo : 'Contenidos generales',
            'crumbs' => []
        ];
        if($seccion != null){
            $datosView['crumbs'][] = [
                'title' => $seccion->titulo,
                'route' => route('admin.secciones.posts.index', $seccion->slug)
            ];
        }
        $datosView['crumbs'][] = [
            'title' => 'Contenidos',
            'route' => null
        ];

        $view = View::make('admin.secciones.extras.index', [
            'datosView' => $datosView,
            'extras' => $contenidos,
            'seccion' => $seccion,
            'modo' => 'CONTENIDO',
            'edit_route_name' => 'admin.secciones.contenidos.edit',
            'destroy_route_name' => 'admin.secciones.contenidos.destroy',
        ]);
        return $view;
    }

    public function edit(Request $request, $id=null)
    {
        if($id!=null){
            $edit = true;
            $extra = Extra::find($id);
            $seccion = Seccion::find($extra->seccion_id);
            $title = $seccion != null ? 'Editar contenido de '.$seccion->titulo : 'Editar contenido';
        } else {
            $edit = false;
            $extra = null;
            // $seccion = Seccion::find($seccion_id);
            $seccion = null;
            // $title = $seccion != null ? 'Nuevo contenido de '.$seccion->titulo: 'Nuevo contenido';
            $title = 'Nuevo contenido';
        }

        $usr = $request->user();
        $tipos = ExtraTipo::all();

        $secciones = Seccion::where('active', 1)->orderBy('titulo', 'asc')->get();

        if($usr->hasRole('superadmin')){

            $datosView = [
                'seccion_titulo' => 'Editar',
                'crumbs' => []
            ];

            if($seccion != null) {
                $datosView['crumbs'][] = [
                    'title' => $seccion->titulo,
                    'route' => route('admin.secciones.posts.index', $seccion->slug)
                ];
                $datosView['crumbs'][] = [
                    'title' => 'Contenidos',
                    'route' => route('admin.secciones.contenidos.index', $seccion->slug)
                ];

            } else {
                $datosView['crumbs'][] = [
                    'title' => 'Contenidos',
                    'route' => route('admin.secciones.contenidos.index')
                ];
            }

            $datosView['crumbs'][] = [
                'title' => $edit ? 'Editar' : 'Nuevo',
                'route' => null
            ];

            $view = View::make('admin.secciones.extras.edit', [
                'tipos' => $tipos,
                'title' => $title,
                'seccion' => $seccion,
                'datosView' => $datosView,
                'extra' => $extra,
                'campoDinamico' => $extra,
                'modo' => 'CONTENIDO',
                'new_route_name' => 'admin.secciones.contenidos.store',
                'edit_route_name' => 'admin.secciones.contenidos.update',
                'secciones' => $secciones
            ]);
            return $view;

        } else {
            Session::flash('error', 'Los contenidos de esa sección no son administrables');
            return redirect()->route('admin.home.home');
        }
    }

    public function editarvalor(Request $request, $id=null)
    {
        if($id!=null){
            $edit = true;
            $extra = Extra::getAsContenido($id);
            $seccion = Seccion::find($extra->seccion_id);
        } else {
            Session::flash('error', 'No se encontró el contenido');
            return redirect()->route('admin.secciones.contenidos.index');
        }

        $usr = $request->user();

        $datosView = [
            'seccion_titulo' => $seccion != null ? 'Editar contenido de '.$seccion->titulo : 'Editar contenido',
            'crumbs' => [
                [
                    'title' => 'Contenidos',
                    'route' => route('admin.secciones.contenidos.index')
                ],
                [
                    'title' => 'Editar valor',
                    'route' => null
                ]
            ]
        ];

        $view = View::make('admin.secciones.extras.editarvalor', [
            'seccion' => $seccion,
            'datosView' => $datosView,
            'extra' => $extra,
            'campoDinamico' => $extra,
        ]);
        return $view;
    }

    public function storevalor(Request $request, $id=null){
        $extra = Extra::find($id);
        $input = $request->input($extra->slug);
        if($input !== null){
            $extraValor = ExtraValor::where('extra_id', $extra->id)->first();
            if($extraValor == null){
                $extraValor = new ExtraValor();
                $extraValor->post_id = null;
                $extraValor->extra_id = $extra->id;
                $extraValor->tipo_id = $extra->tipo_id; // este dato se duplica para facilidad de las consultas
            }
            $extraValor->valor_txt_large = $input;
            $extraValor->save();

            Session::flash('success', 'El contenido fue guardado con éxito');
            return redirect()->route('admin.secciones.contenidos.index');

        } else {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                $extra->slug => ['Revisar el contenido']
            ]); throw $error;
        }
    }

    public function store(Request $request, $id=null)
    {
        $response = Extra::store($request, $id, 'CONTENIDO');
        if($response['success']){
            Session::flash('success', 'El ítem fue guardado con éxito.');
            return $response['redirect_route'];
        } else {
            if($response['error_code'] == 'SLUG'){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'slug' => ['Ya existe ese slug']
                ]); throw $error;
            } else if($response['error_code'] == 'VALIDATOR') {
                Session::flash('error', 'Por favor revise los campos requeridos.');
                return $response['redirect_route'];
            } else if($response['error_code'] == 'TIPO') {
                Session::flash('error', 'No existe ese tipo');
                return $response['redirect_route'];
            }
        }
    }

    public function destroy($id)
    {
        $extra = Extra::find($id);
        $seccion = Seccion::find($extra->seccion_id);
        $extra_id = $extra->id;
        $extra->delete();

        ExtraValor::where('extra_id', $extra_id)->delete();

        Session::flash('success', 'El contenido fue borrado!');

        if($seccion != null){
            $redirect = redirect()->route('admin.secciones.contenidos.index', ['seccionSlug'=>$seccion->slug]);
        } else {
            $redirect = redirect()->route('admin.secciones.contenidos.index');
        }
        return $redirect;
    }

    public function sort(Request $request){
        $elemsArr = $request->input('elem');
        $x=1;
        foreach($elemsArr as $item){
            $obj = Extra::find($item);
            if($obj !== null){
                $obj->order = $x;
                $obj->save();
                $x++;
            }
        }
        return response()->json([
            'success' => true,
            'error' => null
        ]);
    }



}
