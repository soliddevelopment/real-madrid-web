<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use Response;

class PdfViewerController extends Controller
{

    public function index($filename)
    {
        // $filename = 'assets/others/129778.pdf';
        $path = storage_path().DIRECTORY_SEPARATOR.$filename;
        // dd($path);

        return Response::make($path, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; '.$filename,
        ]);
    }
}
