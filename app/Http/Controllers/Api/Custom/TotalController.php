<?php

namespace App\Http\Controllers\Api\Custom;

use Vinkla\Instagram\Instagram;
use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;

use App\Models\Fijos\Post;
use App\Models\Fijos\Gallery;


class TotalController extends BaseController
{

    public function getDatos(Request $request)
    {
        // HOME BANNER Y PROMOCIONES
        $genericSlides = Post::findForSeccion(['home-banner'], ['descripcion', 'imagen']);
        $promociones = Post::findForSeccion(['promociones'], ['descripcion', 'imagen', 'validez', 'mostrarenbanner']);
        $promocionesSlides = [];

        foreach($genericSlides as $item){
            $item->validez = null;
            $item->tipo = 'generic';
        }
        foreach($promociones as $item){
            if($item->mostrarenbanner == 1 || $item->mostrarenbanner == '1'){
                $promocionesSlides[] = $item;
                $item->tipo = 'promo';
            }
        }
        $homeSliderSlides = $genericSlides->merge($promocionesSlides);

        // CARROS
        $carros = Post::findForSeccion(['carros'], ['ano', 'slogan', 'descripcion', 'imagenthumb', 'galeriaprincipal']);

        // GALERIA FOOTER
        $galeriaPrincipal = Gallery::getBySlug('galeria-principal');

        // TIPS
        $tips = Post::findForSeccion(['tips'], ['descripcion', 'imagen']);

        return response([
            'carros' => $carros,
            'galeriaPrincipal' => $galeriaPrincipal,
            'homeSliderSlides' => $homeSliderSlides,
            'promociones' => $promociones,
            'tips' => $tips,
        ], 200);
//         return response()->json($elems, 200);
    }

    /*public function getDetalle(Request $request, $slug=null)
    {
        $withArray = [
            'galeria' => function($q){
                $q->select(['galleries.id', 'galleries.slug', 'galleries.titulo', 'galleries.cover', 'galleries.resumen', 'galleries.active']);
                $q->with([
                    'mediafiles'=> function($q){
                        $q->select('media_files.id', 'media_files.mime_type', 'media_files.nombre', 'media_files.filename');
                    },
                ]);
            },
            'packages' => function($q){
                $q->select(['packages.id', 'packages.accommodation_id', 'packages.slug', 'packages.nombre', 'packages.bajada', 'packages.active', 'packages.order']);
                $q->with([
                    'prices'=> function($q){
                        $q->select(['packageprices.id', 'packageprices.package_id', 'packageprices.nights_txt', 'packageprices.value', 'packageprices.order']);
                    },
                ]);
            },
            'amenities' => function($q){
                $q->select(['id', 'accommodation_id', 'nombre', 'active', 'order']);
            }
        ];
        $elem = Accommodation::with($withArray)->where('slug', $slug)->where('active', 1)->first();
        $elem->amenities = $elem->amenities->where('active', 1)->sortBy('order');

        $elem->galeria = $elem->galeria;
        $elem->galeria->mediafiles = $elem->galeria->mediafiles;

        $elem->packages = $elem->packages;
        foreach($elem->packages as $package){
            $colsAmenities = ['amenities.id', 'amenities.package_id', 'amenities.nombre', 'amenities.active', 'amenities.order'];
            $package->amenities = Amenity::where('package_id', '=', $package->id)
                ->where('active', 1)
                ->where('active', 1)
                ->orderby('order', 'asc')
                ->get($colsAmenities);
            $package->prices = $package->prices;
        }

        return response()->json($elem, 200);
    }*/

}
