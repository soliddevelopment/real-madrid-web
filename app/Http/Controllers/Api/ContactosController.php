<?php

namespace App\Http\Controllers\Api;
use Vinkla\Instagram\Instagram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use View;
use Validator;

use App\Models\Fijos\Contacto;


class ContactosController extends BaseController
{

    public function contactoPost(Request $request){

        $response = Contacto::store($request);
        if($response['success']){
            return response()->json([
                'success' => true,
                'model' => $response['data'],
                'error' => null,
                'error_code' => null,
                'validator' => $response['validator']
            ], 200);

        } else {
            return response()->json([
                'success' => false,
                'model' => null,
                'error' => 'Errores de validación',
                'error_code' => 'VALIDATOR',
                'validator' => $response['validator']
            ], 400);
        }

    }

}
