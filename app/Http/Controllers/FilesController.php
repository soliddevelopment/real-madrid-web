<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use Response;

use App\Models\Fijos\MediaFile;

class FilesController extends Controller
{

    public function getFile(Request $request, $file_name)
    {
        // $file_name = $request->input('file_name');
        $path = storage_path().'/'.'app/'.$file_name;
        if (file_exists($path)) {
            return Response::download($path);
        } else {
            return 'Archivo no existe';
        }
    }

    public function mediaFile(Request $request, $id){
        // return MediaFile::getFileResponse($request->input('id'));
        if($mediafile = MediaFile::find($id)){
            $path = storage_path().'/'.'app/public/mediamanager/'.$mediafile->filename;
            if (file_exists($path)) {
                return Response::download($path);
            } else {
                return 'Archivo no existe';
            }
        }
    }

}
