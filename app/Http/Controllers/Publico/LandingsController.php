<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Session;
use View;
use Auth;
use Validator;

//use App\Models\Fijos\User;
use App\Models\Fijos\Post;
use App\Models\Fijos\Extra;


class LandingsController extends BaseController
{
    public function general(Request $request){
        $trabajos = Post::findForSeccion(['trabajos'], ['descripcion', 'galeriatrabajo', 'bajada', 'imagenlista'], [], null, true);
        // $contenidos = Extra::getContenidosFromArray(['cantidadeventos']);
        $meta = [
            'title' => 'Solid | Diseño y desarrollo de software, páginas web y apps',
            'description' => 'Desarrollamos software a la medida, sitios web y apps de calidad que ayudan a nuestros clientes resolver problemas, volverse más eficientes y cumplir sus metas.',
        ];

        $view = View::make('publico.landings.general', [
            'trabajos' => $trabajos,
            'meta' => $meta,
            'footerSimple' => true
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }

}
