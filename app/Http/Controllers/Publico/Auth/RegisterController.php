<?php

namespace App\Http\Controllers\Publico\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;

use App\Models\Fijos\User;
use View;

class RegisterController extends Controller
{
    use RegistersUsers;

//    protected $redirectTo = '/';

    public $registeredId = null;
    public $registeredTipo = null;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function redirectTo(){
//        return '/empresa/crear';
        if($this->registeredTipo == 'EMPRESA'){
            return route('publico.empresa.perfil.create', ['from'=>'registro']);
        } else {
            return route('publico.candidato.perfil.create', ['from'=>'registro']);
        }
    }

    public function showRegistrationForm(Request $request)
    {
        if($request->input('tipo') !== null){
            $tipo = strtoupper($request->input('tipo'));
            if($tipo == 'CANDIDATO' || $tipo == 'EMPRESA'){} else {
                $tipo = 'CANDIDATO';
            }
        } else $tipo = 'CANDIDATO';

        $view = View::make('publico.auth.register', [
            'hideHeaderFooter' => true,
            'tipo' => $tipo,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()) {
            return redirect()->route('publico.auth.register', ['registrarme'=>true])->withErrors($validator)->withInput();
        }

        event(new Registered($user = $this->create($request->all())));
        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
//            'usuario' => ['required', 'string', 'max:255', 'unique:users', 'regex:"^[a-zA-Z\d-_]+$"'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'tipo' => ['required', 'string'],
        ]);
    }

    protected function create(array $data)
    {
        if($data['tipo'] == 'CANDIDATO' || $data['tipo'] == 'EMPRESA'){
            $tipo = $data['tipo'];
        } else {
            $tipo = 'CANDIDATO';
        }

        $newUser = new User();
        $newUser->nombre = $data['nombre'];
        $newUser->apellido = $data['apellido'];
        $newUser->email = $data['email'];
        $newUser->tipo = $tipo;
        $newUser->password = Hash::make($data['password']);
        $newUser->save();

        return $newUser;

    }

    protected function registered(Request $request, $user)
    {
        $this->registeredTipo = $user->tipo;
        $this->registeredId = $user->id;
    }
}
