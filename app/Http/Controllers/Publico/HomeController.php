<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Session;
use View;
use Auth;
use Validator;

//use App\Models\Fijos\User;
use App\Models\Fijos\Post;
use App\Models\Fijos\Extra;
use App\Models\Fijos\Gallery;


class HomeController extends BaseController
{
    public function index(Request $request){
//        $homeSlides = Post::findForSeccion(['home-banner'], ['imagen', 'descripcion', 'subida']);
        //$productosDestacados = Post::findForSeccion(['productos'], ['imagen'], [], null, true);
        //$contenidos = Extra::getContenidosFromArray(['cantidadeventos']);
        
        $meta = [
            'title' => 'Fundación Real Madrid | Clinic de fútbol en Costa Rica',
            'description' => 'Clinic de fútbol de la Fundación Real Madrid en Costa Rica. Entrenos como los más grandes bajo la metodología de entrenamiento del Real Madrid CF.',
        ];
        $view = View::make('publico.home.home', [
            //'productosDestacados' => $productosDestacados,
            'meta' => $meta,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function clinics(Request $request){
        //$hitos = Post::findForSeccion(['timeline'], ['bajada']);
        $meta = [
            'title' => 'Fundación Real Madrid | Clinic de fútbol en Costa Rica',
            'description' => 'Clinic de fútbol de la Fundación Real Madrid en Costa Rica. Entrenos como los más grandes bajo la metodología de entrenamiento del Real Madrid CF.',
        ];
        $view = View::make('publico.home.clinics', [
            'meta' => $meta,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function challenge(Request $request){
        //$hitos = Post::findForSeccion(['timeline'], ['bajada']);
        $meta = [
            'title' => 'Fundación Real Madrid | Clinic de fútbol en Costa Rica',
            'description' => 'Clinic de fútbol de la Fundación Real Madrid en Costa Rica. Entrenos como los más grandes bajo la metodología de entrenamiento del Real Madrid CF.',
        ];
        $view = View::make('publico.home.challenge', [
            'meta' => $meta,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function realspirit(Request $request){
        //$hitos = Post::findForSeccion(['timeline'], ['bajada']);
        $fotos = Post::findForSeccion(['fotos'], ['imagen']);
        $meta = [
            'title' => 'Fundación Real Madrid | Clinic de fútbol en Costa Rica',
            'description' => 'Clinic de fútbol de la Fundación Real Madrid en Costa Rica. Entrenos como los más grandes bajo la metodología de entrenamiento del Real Madrid CF.',
        ];
        $view = View::make('publico.home.realspirit', [
            'meta' => $meta,
            'fotos' => $fotos,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function worldwide(Request $request){
        //$hitos = Post::findForSeccion(['timeline'], ['bajada']);
        $meta = [
            'title' => 'Fundación Real Madrid | Clinic de fútbol en Costa Rica',
            'description' => 'Clinic de fútbol de la Fundación Real Madrid en Costa Rica. Entrenos como los más grandes bajo la metodología de entrenamiento del Real Madrid CF.',
        ];
        $view = View::make('publico.home.worldwide', [
            'meta' => $meta,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    public function gracias(Request $request){
        //$trabajos = Post::findForSeccion(['trabajos'], ['descripcion', 'galeriatrabajo', 'bajada', 'imagenlista']);
        $meta = [
            'title' => 'Fundación Real Madrid | Clinic de fútbol en Costa Rica',
            'description' => 'Clinic de fútbol de la Fundación Real Madrid en Costa Rica. Entrenos como los más grandes bajo la metodología de entrenamiento del Real Madrid CF.',
        ];
        $view = View::make('publico.home.gracias', [
            //'trabajos' => $trabajos,
            'meta' => $meta,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
}
