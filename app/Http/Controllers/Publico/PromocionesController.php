<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Session;
use View;
use Auth;
use Validator;

//use App\Models\Fijos\User;
use App\Models\Fijos\Post;
use App\Models\Fijos\Extra;
use App\Models\Fijos\Gallery;
use App\Models\Fijos\Seccion;
use App\Models\Fijos\Categoria;

class PromocionesController extends BaseController
{
    public function index(Request $request){
        $seccion = Seccion::findBySlug('productos');
        $categorias = Categoria::where('seccion_id', '=', $seccion->id)->get();
        
        if($request->input('cat') && strlen($request->input('cat'))>0 && $selectedCat = Categoria::findBySlug($request->input('cat'))){
            $productos = Post::findForSeccion(['productos'], ['bajada', 'imagen'], null, null, null, null, $selectedCat->slug, null, 1);
        } else {
            $selectedCat = null;
            $productos = Post::findForSeccion(['productos'], ['bajada', 'imagen'], null, null, null, null, null, null, 1);
        }
        
        $meta = [
            'title' => 'Fundación Real Madrid | Clinic de fútbol en Costa Rica',
            'description' => '',
        ];
        $view = View::make('publico.productos.index', [
            'selectedCat' => $selectedCat,
            'categorias' => $categorias,
            'productos' => $productos,
            'meta' => $meta,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }
    
    
    public function detalle(Request $request, $slug=null){
        if($producto = Post::findBySlug($slug)){
            $meta = [
                'title' => 'Laboratorio DB | '.$producto->titulo.' - DB',
                'description' => $producto->titulo.' '.$producto->bajada,
            ];
            $view = View::make('publico.productos.detalle', [
                'producto' => $producto,
                'meta' => $meta,
            ]);
            if ($request->ajax()) return $view->renderSections()['content']; else return $view;
        } else {
            Session::flash('error', '¡Oops! No se encontró ese producto');
            return redirect()->route('publico.productos.index');
        }
    }
    
}
