<?php

namespace App\Http\Controllers\Publico;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Session;
use View;
use Validator;

use App\Models\Fijos\Contacto;
use App\Models\Fijos\Seccion;
use App\Mail\EmailGenerico;
use App\Models\Fijos\Config;
use App\Models\Fijos\Post;
use App\Models\Fijos\User;
use App\Models\Fijos\Extra;

class ContactoController extends BaseController
{

    /*public function index(Request $request){
        $noticias = Post::findForSeccion(['noticias'], 25);
        $view = View::make('publico.home.home', [
//            'noticias' => $noticias,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }*/

    /*public function index(Request $request){
        $view = View::make('publico.home.contacto', [
            // 'areas' => $areas,
        ]);
        if ($request->ajax()) return $view->renderSections()['content']; else return $view;
    }*/

    public function contactoPost(Request $request){
        $response = Contacto::store($request);
        if($response['success']){
            return response()->json([
                'success' => true,
                'model' => $response['data'],
                'error' => null,
                'error_code' => null,
                'validator' => $response['validator']
            ], 200);

        } else {
            return response()->json([
                'success' => false,
                'model' => null,
                'error' => 'Errores de validación',
                'error_code' => 'VALIDATOR',
                'validator' => $response['validator']
            ], 400);
        }
    }

    public function contactoNuevo(Request $request){
        $response = Contacto::store($request);
        if($response['success']){
            Session::flash('success', '¡Gracias por contactarnos! Nos pondremos en contacto en seguida.');
            //return redirect()->back();
            return redirect()->route('publico.home.gracias');
        } else {
            Session::flash('error', $response['error']);
            return Redirect::to(URL::previous() . "#footer")->withErrors($response['validator'])->withInput();
            //return redirect()->back()->withErrors($response['validator'])->withInput();
            //return redirect()->route('publico.home.gracias')->withErrors($response['validator'])->withInput();
        }
    }
    
    
    public function nuevaInscripcion(Request $request){
        $seccionForm = Seccion::findBySlug('inscripcion');
        $datosExtra = [
            'seccion_id' => $seccionForm->id,
            'titulo' => 'Inscripción de '.$request->input('nombre').' '.$request->input('apellido')
        ];
        $request = $request->merge($datosExtra);
        $response = Post::store($request);
        $post = $response['data'];
        $extras = Extra::getForPost($post->id, 'CAMPO');
        
        if($response['success']){
    
            $toArray = [
                [ 'name' => 'Valery', 'email' => 'valery@brandy.la' ],
                [ 'name' => 'Clinic', 'email' => 'clinic@frmclinicscostarica.com' ],
            ];
            
            $body = '';
            $body.= '<ul>';
            foreach($extras as $extra) {
                $body .= '<li>';
                $body .= '<strong>'.$extra->titulo.'</strong>: ';
                $body .= '<span>'.$extra->valor.'</span>';
                $body .= '</li>';
            }
            $body .= '<ul>';
    
            Mail::to($toArray)->send(new EmailGenerico(
                'Nueva inscripción',
                'Nueva inscripción',
                $body,
                asset('/p/img/logo-peq.png'),
                route('admin.secciones.posts.detalle', ['seccionSlug'=> $seccionForm->slug, 'id' => $response['data']->id]),
                'Ir a inscripción'
            ));
    
            Session::flash('success', 'El ítem fue guardado con éxito.');
            return redirect()->route('publico.home.gracias');
        } else {
            return Redirect::to(URL::previous() . "#footer")->withErrors($response['validator'])->withInput();
            /*if($response['error_code'] == 'SLUG'){
                $error = \Illuminate\Validation\ValidationException::withMessages([
                    'slug' => [$response['error']]
                ]); throw $error;
            
            } else if($response['error_code'] == 'VALIDATOR') {
                Session::flash('error', 'Por favor revise los campos requeridos.');
                return $response['redirect_route'];
            
            } else {
            
            }*/
        
        }
    }

}
