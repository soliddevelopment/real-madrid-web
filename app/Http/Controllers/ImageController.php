<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

use App\Models\Fijos\MediaFile;

class ImageController extends Controller
{

    public function mediaImg(Request $request){
        $w = $request->input('w');
        $h = $request->input('h');
        $id = $request->input('id');
        $forceJpg = $request->input('forcejpg');
        if($forceJpg == 'true' || $forceJpg == true) $forceJpg = true; else $forceJpg = false;
        $imgResponse = MediaFile::getImgResponse($id, $w, $h, $forceJpg);
        return $imgResponse;
    }

    /*public function thumb()
    {
        $img = Input::get('img', null);
        $w = Input::get('w', false);
        $h = Input::get('h', false);

        if($w != null && $h != null && $w > 0 && $h > 0){
            $imgResponse = Image::make(public_path($img))->fit($w, $h)->response('jpg');
        } else {
            $imgResponse = Image::make(public_path($img))->response('jpg');
        }
        return $imgResponse;
        // return Image::make(public_path($img))->resize($w, $h)->response('jpg');
    }*/

    /*public function editarImagen(Request $request)
    {
        if($request->input('id') !== null){
            if($request->input('campo') !== null && strlen($request->input('campo'))>0){
                $campo = $request->input('campo');
            } else {
                $campo = 'img';
            }

            $elem = null;
            switch($request->input('tipo')){
                case 0:
                    $elem = Post::find($request->input('id'));
                    break;
                case 1:
                    $elem = Contenido::find($request->input('id'));
                    $campo = 'valor_img';
                    break;
            }

            if($request->hasFile('img')) {
                $file = $request->file('img');
                $imgpath = time() . '-' . $file->getClientOriginalName();
                $img = Image::make($file)->save('storage/' . $imgpath);
                $elem->$campo = $imgpath;
                $elem->save();

                return response()->json([
                    'success' => true,
                    'error' => null,
                    'img' => asset('/storage/'.$elem->$campo)
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'error' => 'no se envio archivo img'
                ]);
            }

        } else {
            return response()->json([
                'success' => false,
                'error' => 'no se envio id'
            ]);
        }
    }*/
}
