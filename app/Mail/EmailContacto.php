<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Fijos\Config;
use App\Models\Fijos\Contacto;

class EmailContacto extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(Contacto $contacto)
    {
        $this->contacto = $contacto;

        $configLogo = Config::where('slug', 'contacto-logo')->first();
        if($configLogo) {
            $logo = mediaimg($configLogo->getVal(), 230);
            $this->logo = $logo;
        } else {
            $this->logo = '';
        }
    
        $this->fromEmail = config("MAIL_FROM_ADDRESS", "noreply@solid.com.sv");
        $this->fromName = config("MAIL_FROM_NAME", "Solid");

        $this->subject = 'Nuevo mensaje de '.$this->contacto->nombre;
        $this->titulo = 'Nuevo mensaje de contacto';
        $this->body = 'Recibiste un nuevo mensaje de contacto en tu sitio web.';
        $this->btn_route = route('admin.contactos.detalle', $this->contacto->id);
        $this->btn_txt = 'Ir a mensaje';
    }

    public function build()
    {
        return $this
            ->from($this->fromEmail, $this->fromName)
            ->subject($this->subject)
            ->markdown('publico.emails.contacto', [
                'contacto' => $this->contacto,
                'logo' => $this->logo,
                'titulo' => $this->titulo,
                'body' => $this->body,
                'btn_route' => $this->btn_route,
                'btn_txt' => $this->btn_txt,
            ]);
    }
}
