<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Fijos\Post;
use App\Models\Fijos\Extra;

class EmailGenerico extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($subject, $titulo, $body, $logo, $btn_route=null, $btn_txt=null, $from_email=null, $from_name=null )
    {
        $this->subject = $subject;
        $this->titulo = $titulo;
        $this->body = $body;
        $this->btn_route = $btn_route;
        $this->btn_txt = $btn_txt;
        $this->logo = $logo;
    
        if($from_email){
            $this->fromEmail = $from_email;
        } else {
            $this->fromEmail = config('misc.MAIL_FROM_ADDRESS');
        }
    
        if($from_email){
            $this->fromName = $from_name;
        } else {
            $this->fromName = config('misc.MAIL_FROM_NAME');
        }
        
    }
    
    public function build()
    {
        return $this
            ->from($this->fromEmail, $this->fromName)
            ->subject($this->subject)
            ->markdown('publico.emails.generico', [
                'logo' => $this->logo,
                'titulo' => $this->titulo,
                'body' => $this->body,
                'btn_route' => $this->btn_route,
                'btn_txt' => $this->btn_txt,
            ]);
    }
}
