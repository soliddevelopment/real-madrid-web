<?php

return [
    
    'GOOGLE_RECAPTCHA_KEY' => env('GOOGLE_RECAPTCHA_KEY', null),
    'GOOGLE_RECAPTCHA_SECRET' => env('GOOGLE_RECAPTCHA_SECRET', null),
    
    'MAIL_FROM_ADDRESS' => env('MAIL_FROM_ADDRESS', null),
    'MAIL_FROM_NAME' => env('MAIL_FROM_NAME', null),
    
    'BUSINESS_EMAIL' => env('BUSINESS_EMAIL', null),
];
