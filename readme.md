# Instalar dependencias globales

Si nunca habia compilado proyectos Laravel, es necesario instalar las dependencias globales para correr proyectos de este tipo:
- Instalar server apache 
    - https://laragon.org/ (opción recomendada)
- Instalar composer (para manejar dependencias PHP) 
    - https://getcomposer.org/

----

# Primera vez corriendo proyecto

Si esta es la primera vez que corre el proyecto, es necesario realizar los siguientes pasos.

- Crear base de datos para proyecto
- Instalar dependencias PHP del proyecto:
    - ``composer install``
- Preparar archivo **.env**:
    - Si no existe archivo **.env**, crearlo y copiar contenido de **.env.example**
    - Llenar información necesaria en ese archivo 
        - Informacion base de datos: DB_DATABASE, DB_USERNAME, DB_PASSWORD
        - Cambiar APP_NAME al nombre del proyecto
        - Cambiar APP_URL al que corresponde 
- Generar llave de proyecto:
    - ``php artisan key:generate``
- Borrar cache de Laravel:
    - ``php artisan config:cache`` 
- Preparar carpeta de storage:
    - ``php artisan storage:link``
- Ejecutar migraciones y seeds:
    - ``php artisan migrate --seed`` 
- Instalar dependencias javascript:
    - ``npm install``

## Siguientes veces

- Hacer pull del repositorio
- Ejecutar estos comandos por si hubo modificaciones:
    - ``php artisan migrate``
    - ``composer install``
    - ``npm install``
- Compilar proyecto Laravel:
    - ``php artisan serve``
- Compilar CSS y JS:
    - ``npm run dev`` o:
    - ``npm run watch-poll``

----

## Otra información

### Assets CSS Y JS
- El CSS y librerías se encuentra en /resources/assets/publico/css/[nombre]
- Los assets CSS y JS se incluyen en el archivo **/webpack.min.js** ubicado en el root
- Si desea agregar un archivo, deberá agregarlo dentro de la carpeta con su nombre 
*/resources/assets/publico/css/[nombre]/*, y luego agregarlo en **/webpack.min.js**. Hay que agregarlo como un elemento más junto a los demás de su misma carpeta. 

### Views
Si desea agregar una nueva view, hacer lo siguiente:
- Crear ruta GET en */routes/publico/custom.php*
- Crear acción (con el nombre que se especificó en la ruta) en controller (si habrán varias views, crear un controller específico)
- Crear carpeta con view en */resources/views/publico/*


