
<div class="row">
    <div class="col-md-6">
        <div class="campos-normales">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('nombre', 'Nombre:') }}
                        {{ Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('apellido', 'Apellido:') }}
                        {{ Form::text('apellido', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('slug', 'Slug:') }}
                        {{ Form::text('slug', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('sexo', 'Sexo:') }}
                        {{ Form::text('sexo', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('categoria_id', 'Categoría:') }}
                <select name="categoria_id" id="categoria_id" class="form-control">
                    <option value="0">- Ninguna -</option>
                    @foreach($categorias as $item)
                        <option value="{{ $item->id }}" {{ $post != null && $post->categoria_id == $item->id ? 'selected':'' }} >
                            {{ $item->titulo }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('img', 'Imagen:') }}
                        <media-manager-btn
                                v-bind:select-mode="true"
                                custom-class="btn btn-primary btn-sm" input-name="img_id"
                                :preview="true" :img="true" default-img="{{ $post !== null && $post->img_id !== null ? mediaimg($post->img_id, 100, 100) : '' }}"
                                title="Seleccionar imagen" txt='Seleccionar imagen'
                        ></media-manager-btn>
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('resumen', 'Resumen:') }}
                        {{ Form::textarea('resumen', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows'=>2]) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('tags', 'Tags:') }}
                        {{ Form::textarea('tags', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows'=>2]) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="campos-dinamicos">
            @foreach($extras as $item)
                <div class="form-group">
                    {{ Form::label($item->slug, $item->titulo.':') }}

                    @if($item->tipo->slug == 'texto')
                        <input type="text" name="{{ $item->slug }}" class="form-control" placeholder="..." value="{{ $item->valor }}">

                    @elseif($item->tipo->slug == 'img')
                        <p><strong>media manager!!</strong></p>

                    @elseif($item->tipo->slug == 'texto-mediano')
                        <textarea name="{{ $item->slug }}" rows="3" class="form-control">{{ $item->valor }}</textarea>

                    @elseif($item->tipo->slug == 'texto-largo')
                        <textarea name="{{ $item->slug }}" rows="6" class="form-control">{{ $item->valor }}</textarea>

                    @elseif($item->tipo->slug == 'editor')
                        <textarea name="{{ $item->slug }}" rows="15" class="form-control summernote">{!! $item->valor !!}</textarea>

                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>

<hr>

<div class="btn-cont">
    {{--@if($parent !== null)
        {{ Form::hidden('parent_id', $parent->id) }}
    @endif--}}
    {{ Form::hidden('seccion_id', $seccion->id) }}
    {{ Form::submit('Guardar cambios', ['class' => 'btn btn-primary btn-lg']) }}
</div>

