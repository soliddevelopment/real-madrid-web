@extends('admin.main')
@section('title', 'Lista de contactos')
@section('content')

    <section id="" class="page">
        <div class="heading">
            @include('admin.partials._crumbs')
            <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
            {{--<div class="ops">
                <a href="{{ route('admin.contactos.create') }}" class="btn btn-primary">+ Nuevo</a>
            </div>--}}
        </div>

        <div class="contenido">
            <div class="row">
                <div class="col-md-12">

                    <div class="lista-box card-box">

                        @if(count($contactos)>0)
                            <table class="table table1">
                                <thead>
                                <tr>
                                    <th class="td-id">id</th>
                                    <th>nombre</th>
                                    <th>email</th>
                                    {{--<th>interés en</th>--}}
                                    <th>fecha</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contactos as $item)
                                    <tr>
                                        <td class="td-id">{{ $item->id }}</td>
                                        <td>{{ $item->nombre.' '.$item->apellido }}</td>
                                        <td>{{ $item->email }}</td>
                                        {{--<td>{{ $item->campo1 }}</td>--}}
                                        <td>
                                            {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                        </td>
                                        <td class="td-ops">
                                            {{--<input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.contactos.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#00AEEF" data-size="small" />--}}
                                            <a title="Ver contacto" href="{{ route('admin.contactos.detalle', [ 'id' => $item->id  ]) }} " class="">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            {{--<a href="{{ route('admin.contactos.edit', [ 'id' => $item->id  ]) }} " class="">
                                                <i class="fa fa-pencil"></i>
                                            </a>--}}
                                            &nbsp;
                                            {!! Form::open(['route'=>['admin.contactos.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}

                                            <a href="#"
                                               data-method="delete"
                                               data-title="¡Precaución!"
                                               data-text="¿Estás seguro que quieres borrar esto?"
                                               data-type="warning"
                                               data-id-form="form_{{ $item->id }}"
                                               class="hey-confirm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="no-hay">
                                <p>No hay items disponibles</p>
                            </div>
                        @endif

                        {{-- {!! $proyectos->links() !!} --}}
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
