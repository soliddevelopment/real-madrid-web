@extends('admin.main')
@section('title', $title)
@section('content')

<section id="contactosPage" class="page edit-page">

    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form">
                        @include('admin.partials._messages')
                        @if($contacto != null)
                            {!! Form::model($contacto, ['route'=>['admin.contactos.update', $contacto->id], 'method'=>'put', 'data-edit'=> 'true' ]) !!}
                                @include('admin.contactos._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>['admin.contactos.store', null], 'method'=>'post', 'data-edit'=> 'false']) !!}
                                @include('admin.contactos._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
