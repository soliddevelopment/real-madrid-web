@extends('admin.main')
@section('title', 'Detalle de contacto')
@section('content')

<section id="contactosPage" class="page edit-page">

    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="detalle-contacto">
                    <div class="card-box">
                        @if(strlen($contacto->nombre) > 0)
                            <div class="elem">
                                <span class="dato">Nombre:</span>
                                <span class="valor">
                                    {{$contacto->nombre}}
                                    @if(strlen($contacto->apellido)>0)
                                        {{$contacto->apellido}}
                                    @endif
                                </span>
                            </div>
                        @endif

                        @if(strlen($contacto->empresa) > 0)
                            <div class="elem">
                                <span class="dato">Empresa:</span>
                                <span class="valor">
                                    {{$contacto->empresa}}
                                </span>
                            </div>
                        @endif

                        @if(strlen($contacto->email) > 0)
                            <div class="elem">
                                <span class="dato">Email:</span>
                                <span class="valor">
                            {{$contacto->email}}
                        </span>
                            </div>
                        @endif

                        @if(strlen($contacto->telefono) > 0)
                            <div class="elem">
                                <span class="dato">Tel:</span>
                                <span class="valor">
                            {{$contacto->telefono}}
                        </span>
                            </div>
                        @endif

                        @if(strlen($contacto->mensaje) > 0)
                            <div class="elem">
                                <span class="dato">Mensaje:</span>
                                <span class="valor">
                                    {{$contacto->mensaje}}
                                </span>
                            </div>
                        @endif

                        @if(strlen($contacto->pais) > 0)
                            <div class="elem">
                                <span class="dato">País:</span>
                                <span class="valor">
                                    {{$contacto->pais->nombre}}
                                </span>
                            </div>
                        @endif

                        @if(strlen($contacto->campo1) > 0)
                            <div class="elem">
                                <span class="dato">Interés en:</span>
                                <span class="valor">
                                    {{ $contacto->campo1 }}
                                </span>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
