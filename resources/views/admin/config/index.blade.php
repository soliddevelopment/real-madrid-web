@extends('admin.main')
@section('title', 'Configuración')
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<section id="configsPage" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            @if($usuario->hasRole('superadmin'))
                <a href="{{ route('admin.config.create') }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
            @endif
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">

                <div class="card-box">

                    @if(count($configuraciones)>0)
                        <table class="table table1">
                            <thead>
                            <tr>
                                @if($usuario->hasRole('superadmin'))
                                    <th>id</th>
                                    <th>ONLY GLOBAL</th>
                                    <th>slug</th>
                                @endif
                                <th>título</th>
                                <th>descripción</th>
                                <th>tipo</th>
                                <th class="align-center">valor</th>

                                @if($usuario->hasRole('superadmin'))
                                    <th>creado</th>
                                @endif
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($configuraciones as $item)
                                <tr>
                                    @if($usuario->hasRole('superadmin'))
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->only_global }}</td>
                                        <th>{{ $item->slug }}</th>
                                    @endif
                                    <td>{{ $item->titulo }}</td>
                                    <td>{{ $item->descripcion }}</td>
                                    <td>{{ $item->tipo }}</td>
                                    <td class="align-center">
                                        @if($item->tipo == 'IMG')
                                            <a class="fancybox" href="{{ mediaimg($item->valor_img, 800, 600) }}"><img src="{{ mediaimg($item->valor_img, 44, 44) }}"></a>
                                        @elseif($item->tipo == 'STR')
                                            <strong>{{$item->valor_str}}</strong>
                                        @elseif($item->tipo == 'INT')
                                            <strong>{{$item->valor_int}}</strong>
                                        @elseif($item->tipo == 'BOOL')
                                            <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.config.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->valor_bool == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                        @endif
                                    </td>

                                    @if($usuario->hasRole('superadmin'))
                                        <td>
                                            {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                        </td>
                                    @endif
                                    <td class="td-ops">
                                        <a href="{{ route('admin.config.edit', [ 'id' => $item->id  ]) }} " class="">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        &nbsp;
                                        {!! Form::open(['route'=>['admin.config.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                        {!! Form::close() !!}

                                        @if($usuario->hasRole('superadmin'))
                                            <a href="#"
                                               data-method="delete"
                                               data-title="¡Precaución!"
                                               data-text="¿Estás seguro que quieres borrar esto?"
                                               data-type="warning"
                                               data-id-form="form_{{ $item->id }}"
                                               class="hey-confirm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div style="padding:20px 0;">
                            <h2>No hay configuraciones disponibles</h2>
                        </div>
                    @endif

                    {{-- {!! $proyectos->links() !!} --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
