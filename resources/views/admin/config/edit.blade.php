@extends('admin.main')
@section('title', $title)
@section('content')

<section id="usuariosPage" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $title }}</h1>
    </div>
    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form">
                        @include('admin.partials._messages')
                        @if($config != null)
                            {!! Form::model($config, ['route'=>['admin.config.update', $config->id], 'method'=>'PUT', 'files'=>true ]) !!}
                                @include('admin.config._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>['admin.config.store', null], 'method'=>'post', 'files'=>true ]) !!}
                                @include('admin.config._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('view_scripts')
    <script>
        $(document).ready(function(){
            function mostrarControlSegunTipo(){
                var tipo = $('#tipo').val();
                $('#control_'+tipo).show().siblings().hide();
            }
            mostrarControlSegunTipo();
            $('#tipo').change(function(){
                mostrarControlSegunTipo();
            });
        });
    </script>
@endsection

@endsection
