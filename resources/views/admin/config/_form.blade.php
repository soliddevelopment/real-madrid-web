

@if($usuario->hasRole('superadmin'))
    <div class="form-group">
        {{ Form::label('only_superadmin', 'Solo para global:') }}
        <select name="only_superadmin" class="form-control">
            <option value="1"{{ $config != null && $config->only_superadmin == 1 ? ' selected':'' }}>SI, solo superadmin</option>
            <option value="0"{{ $config != null && $config->only_superadmin == 0 ? ' selected':'' }}>No, admins tambien</option>
        </select>
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>
<div class="form-group">
    {{ Form::label('slug', 'Slug:') }}
    {{ Form::text('slug', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
</div>

<div class="form-group">
    {{ Form::label('tipo', 'Tipo:') }}
    <select name="tipo" id="tipo" class="form-control">
        <option value="IMG"{{ $config != null && $config->tipo == 'IMG' ? ' selected':'' }}>Imagen</option>
        <option value="STR"{{ $config != null && $config->tipo == 'STR' ? ' selected':'' }}>Texto</option>
        <option value="INT"{{ $config != null && $config->tipo == 'INT' ? ' selected':'' }}>Número</option>
        <option value="BOOL"{{ $config != null && $config->tipo == 'BOOL' ? ' selected':'' }}>Sí/no</option>
    </select>
</div>
<div class="valores">
    <div id="control_IMG" class="form-group">
        {{ Form::label('valor_img', 'Imagen:') }}
        <media-manager-btn
            v-bind:select-mode="true"
            custom-class="btn btn-primary btn-sm" input-name="valor_img" extensions="jpg,png"
            :preview="true" :img="true" default-img="{{ $config !== null && $config->valor_img !== null ? mediaimg($config->valor_img, 100, 100, false) : '' }}"
            title="Selecionar imagen" txt='Selecionar imagen'
        ></media-manager-btn>

        {{--:preview="true" :img="true" default-img="{{ $config !== null && $config->valor_img !== null && strlen($config->valor_img)>0 ? thumb($config->valor_img, 100, 100) : '' }}"--}}

        {{--<div class="img-preview clearfix">
            {{ Form::label('valor_img', 'Imagen:') }}
            <div class="cont">
                @if($config!=null)
                    <div class="img" style="{{ bgImgStyle($config->valor_img, 100, 100) }}"></div>
                @endif
                @if($config==null)
                    <div class="img" style="background-image:url(/images/upload.png)"></div>
                @endif
                {{ Form::file('valor_img') }}
            </div>
        </div>--}}
    </div>
    <div id="control_STR" class="form-group">
        {{ Form::label('valor_str', 'Texto:') }}
        {{ Form::text('valor_str', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
    </div>
    <div id="control_INT" class="form-group">
        {{ Form::label('valor_int', 'Número:') }}
        {{ Form::number('valor_int', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
    </div>
    <div id="control_BOOL" class="form-group">
        {{ Form::label('valor_bool', 'Habilitado:') }}
        <select name="valor_bool" class="form-control">
            <option value="1"{{ $config != null && $config->valor_habilitado == 1 ? ' selected':'' }}>Habilitado</option>
            <option value="0"{{ $config != null && $config->valor_habilitado == 0 ? ' selected':'' }}>Deshabilitado</option>
        </select>
    </div>


</div>

<div class="form-group">
    {{ Form::label('descripcion', 'Descripción:') }}
    {{ Form::textarea('descripcion', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows'=>'3']) }}
</div>

<div class="btn-cont">
    {{-- {{ Form::hidden('grupo_id', $grupo->id) }} --}}
    {{-- {{ Form::hidden('metas_nuevos', $post->grupo_id, ['id'=>'metas_nuevos']) }} --}}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
</div>

