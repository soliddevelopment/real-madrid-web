@extends('main')

@section('title', 'Detalle post')

@section('content')
	
	<h1>{{ $post->titulo }}</h1>
	<p>{{ $post->body }}</p>
	
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	{{-- Otra forma de generar links con routes --}}
	{!! Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class'=>'asdf')) !!}
	{{-- <a href=""></a> --}}

@endsection