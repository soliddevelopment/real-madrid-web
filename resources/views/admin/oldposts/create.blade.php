@extends('main')

@section('title', 'POSTS | Create post')

{{-- Estos son los stylesheets individuales de esta view --}}
@section('stylesheets')
	{!! Html::style('css/create-post.css') !!}
@endsection

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<h1>CREATE POST</h1>
<p>
	Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit deserunt incidunt repellat neque pariatur fuga quisquam possimus nisi impedit odit at ex repudiandae dolorum assumenda, optio, vel nulla aut, praesentium.
</p>

{!! Form::open(['action'=>'PostController@store', 'method'=>'post']) !!}
	
	<div class="form-group">
		{{ Form::label('titulo', 'Titulo:') }}
		{{ Form::text('titulo', '', ['class' => 'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('body', 'Body:') }}
		{{ Form::textarea('body', '', ['class' => 'form-control']) }}
	</div>

	<div class="btn-cont">
		{{ Form::submit('Create Post'), ['class' => 'btn btn-primary btn-lg'] }}
	</div>

{!! Form::close() !!}

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection