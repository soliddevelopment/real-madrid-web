@extends('main')

@section('title', 'Lista de posts')

@section('content')
	
	<h1>Lista de posts</h1>
	<p>
		<a href="{{ route('posts.create') }}">Crear post</a>
	</p>
	
	<ul>
		@foreach($posts->all() as $item)
		
			<li>
				<p>{{ $item->titulo }}</p>
				<p>{{ substr($item->body, 0, 8) }}{{ strlen($item->body)>8 ? "..." : "" }}</p>
				<p>Created: {{ date('M j, Y', strtotime($item->created_at)) }}</p>
				{{-- <a href="/posts/{{ $item->id }}">Leer m&aacute;s</a> --}}
				<a href="{{ route('posts.show', ['id'=> $item->id]) }}">Leer m&aacute;s</a>
				<a href="{{ route('posts.edit', ['id'=> $item->id]) }}">Editar</a>
			</li>

		@endforeach
	</ul>

@endsection