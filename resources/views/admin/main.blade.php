<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="solid.com.sv">

    <link rel="shortcut icon" href="{{ asset('p/img/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{ asset('p/img/favicon.ico')}}" type="image/x-icon">

    {{-- Esto me sirve para ajax calls --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>Administrador | @yield('title')</title>

    @yield('stylesheets')
    @yield('header_scripts')

    {{-- {!! Html::style('css/app.css') !!} --}}

    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">--}}

    <link rel="stylesheet" href="{{mix('a/css/lib/all.css')}}" type="text/css">
    <link rel="stylesheet" href="{{mix('a/css/sass/app.css')}}" type="text/css">
{{--    <link rel="stylesheet" href="{{mix('a/css/minton/all.css')}}" type="text/css">--}}
{{--    <link rel="stylesheet" href="{{mix('a/css/custom/custom-admin.css')}}" type="text/css">--}}

    <script type="text/javascript" src="{{mix('a/js/header.js')}}"></script>

</head>
{{-- @include('partials._head') --}}

<body class="fixed-left boxscroll do-nicescroll4">


<div id="wrapper">

    @include('admin.partials._top')
    @include('admin.partials._nav')

    <div class="content-page">
        <div class="content">
            @yield('content')
        </div>

        {{--<footer class="footer text-right">
        </footer>--}}
    </div>

</div>

<div id="vue-app">
    <example-component></example-component>
</div>

@yield('view_modals')

@include('admin.partials._vueloader')
@include('admin.partials._notifs')
@include('admin.media._manager')
@include('admin.media._cropper')

<input type="hidden" id="urlUploadImageSummernote" value="{{ route('admin.media.summernotePost') }}">

<script>
    // Esto es necesario para que funcione la navegacion del cms
    var resizefunc = [];
</script>


{{--<script type="text/javascript" src="{{asset('a/js/lib/exif.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('a/js/datatables.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('a/js/lib/axios.js') }}"></script>--}}

<script type="text/javascript" src="{{ config('app.env') === 'production' ? asset('a/js/vue/vue.min.js') : asset('a/js/vue/vue.js') }}"></script>
<script type="text/javascript" src="{{mix('a/js/vue/all.js')}}"></script>
<script type="text/javascript" src="{{mix('a/js/footer.js')}}"></script>
{{--<script type="text/javascript" src="{{mix('a/js/custom.js')}}"></script>--}}


@yield('view_scripts')

</body>
</html>
