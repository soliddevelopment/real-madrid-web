@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')

<section id="seccionesPage" class="page edit-page">

    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form">
                        @include('admin.partials._messages')
                        @if($seccion != null)
                            {!! Form::model($seccion, ['route'=>['admin.secciones.update', $seccion->id], 'method'=>'PUT', 'data-edit'=> 'true' ]) !!}
                            @include('admin.secciones._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>['admin.secciones.store', null], 'method'=>'post', 'data-edit'=> 'false']) !!}
                            @include('admin.secciones._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@section('view_scripts')
    <script>
        $(document).ready(function(){
            $('#has_children').change(function(){
                if($(this).is(':checked')){
                    $('#children_grupo_cont').show();
                } else {
                    $('#children_grupo_cont').hide();
                }
            });
        });
    </script>
@endsection

@endsection
