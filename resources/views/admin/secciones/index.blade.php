@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

    <section id="seccionesPage" class="page">
        <div class="heading">
            @include('admin.partials._crumbs')
            <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
            <div class="ops">
                @if($usuario->hasRole('superadmin'))
                    <a href="{{ route('admin.secciones.create') }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
                @endif
            </div>
        </div>

        <div class="contenido">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table1">
                                    <thead>
                                    <tr>
                                        {{-- <th>id</th> --}}
                                        {{-- <th>tipo</th> --}}
                                        <th>título</th>
                                        <th>tipo</th>
                                        <th>sección padre</th>
                                        <th>ops</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($secciones->all() as $item)
                                        <tr>
                                            {{-- <td>{{ $item->id }}</td> --}}
                                            {{-- <td><span class="label label-default">{{ $item->tipo_id }}</span></td> --}}
                                            <td><strong>{{ $item->titulo }}</strong> <small>({{ $item->slug }})</small></td>
                                            <td><strong><small>{{ $item->tipo }}</small></strong></td>
                                            <td>
                                                @if($item->parent !== null)
                                                    <small><strong>{{$item->parent->titulo}}</strong></small>
                                                @endif
                                            </td>
                                            <td>
                                                <small>
                                                    <ul>
                                                        @if($item->has_children == 1)
                                                            <li>Has children ({{ $item->secciones ? count($item->secciones) : '0' }})</li>
                                                        @endif
                                                        @if($item->has_imgs == 1)
                                                            <li>Imgs</li>
                                                        @endif
                                                        @if($item->has_categorias == 1)
                                                            <li>
                                                                Categorías
                                                                @if($item->categorias_administrables == 1)
                                                                    <small>(administrables)</small>
                                                                @endif
                                                            </li>
                                                        @endif
                                                        {{--@if($item->has_subcategorias == 1)
                                                            <li>Subcategorías</li>
                                                        @endif--}}
                                                        @if($item->sortable == 1)
                                                            <li>Sortable</li>
                                                        @endif
                                                        @if($item->adjuntos == 1)
                                                            <li>Adjuntos</li>
                                                        @endif
                                                    </ul>
                                                </small>
                                            </td>
                                            <td>
                                                {{--<a href="{{ route('admin.posts.index', $item->slug) }}">
                                                    <small>Ver posts</small>
                                                </a>--}}
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.secciones.extras.index', $item->slug) }}">Campos adicionales ({{ count($item->extras) }})</a>
                                            </td>
                                            <td>
                                                @if($item->has_categorias == 1)
                                                    <a href="{{ route('admin.secciones.categorias.index', $item->slug) }}">Categorías ({{ count($item->categorias) }})</a>
                                                @else
                                                    <small class="text-muted">N/A</small>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="#">Contenidos</a>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.secciones.posts.index', $item->slug) }}">Posts</a>
                                            </td>
                                            <td class="td-ops">
                                                <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.secciones.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                                <a href="{{ route('admin.secciones.edit', ['id' => $item->id]) }} " class="">
                                                    <i class="fa fa-pencil"></i>
                                                </a>

                                                {!! Form::open(['route'=>['admin.secciones.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                                {!! Form::close() !!}

                                                <a href="#"
                                                   data-method="delete"
                                                   data-title="¡Precaución!"
                                                   data-text="¿Estás seguro que quieres borrar esto?"
                                                   data-type="warning"
                                                   data-id-form="form_{{ $item->id }}"
                                                   class="hey-confirm">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
