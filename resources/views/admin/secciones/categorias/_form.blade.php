<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('slug', 'Slug:') }}
    {{ Form::text('slug', null, ['class' => 'form-control']) }}
</div>
{{-- <div class="form-group">
    <div class="img-preview clearfix">
        {{ Form::label('img', 'Imagen principal:') }}
        <small class="text-muted">Peso máximo 3 mb, Formatos: jpg, png</small>
        <div class="cont">
            @if($categoria!=null)
                <div class="img" style="background-image:url({{ thumb($categoria->img, 100, 100) }})"></div>
            @endif
            @if($categoria==null)
                <div class="img" style="background-image:url(/images/upload.png)"></div>
            @endif
            {{ Form::file('img') }}
        </div>
    </div>
</div> --}}

<div class="btn-cont">
    {{ Form::hidden('seccion_id', $seccion->id) }}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>
