@extends('admin.main')
@section('title', 'Categorías de '.$seccion->titulo)
@section('content')

<section id="categoriasPage" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            {{--@if($usuario->hasRole('superadmin'))
                <a href="{{ route('admin.secciones.create') }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
            @endif--}}
            <a href="{{ route('admin.secciones.categorias.create', [ 'seccion_id'=> $seccion->id, 'id'=>null ]) }}" class="btn btn-primary">+ Nueva</a>
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table1">
                                <thead>
                                <tr>
                                    {{-- <th></th> --}}
                                    @if($usuario->hasRole('superadmin'))
                                        <th>id</th>
                                    @endif
                                    <th>titulo</th>
                                    {{-- <th>slug</th> --}}
                                    {{-- <th>creado</th> --}}
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categorias as $item)
                                    <tr>
                                        {{-- <td><a class="fancybox" href="{{ thumb($item->img, 800, 600) }}"><img src="{{ thumb($item->img, 50, 50) }}"></a></td> --}}
                                        @if($usuario->hasRole('superadmin'))
                                            <td>{{ $item->id }}</td>
                                        @endif
                                        <td>{{ $item->titulo }}</td>
                                        {{-- <td>{{ $item->slug }}</td> --}}
                                        {{-- <td>{{ $item->created_at }}</td> --}}
                                        <td class="align-center">
                                            <?php /*<input class="st-switch" data-id="1" data-url="st" type="checkbox" <?php if($elem->st == 1) echo "checked";  data-plugin="switchery" data-color="#3bafda" data-size="small" /> */ ?>
                                        </td>
                                        <td class="td-ops" style="text-align:right;">
                                            <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.secciones.categorias.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />
                                            {{-- <a href="{{ route('admin.subcategorias.index', ['categoria'=> $item->id]) }}" class="btn btn-primary btn-sm waves-effect waves-light">Subcategorías</a> --}}

                                            {{--<a href="{{ route('admin.secciones.categorias.edit', ['seccion_id'=>$seccion->id, 'id' => $item->id, 'parent_id' => $parent->id]) }} " class="">--}}
                                            <a href="{{ route('admin.secciones.categorias.edit', ['seccion_id'=>$seccion->id, 'id' => $item->id]) }} " class="">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            {!! Form::open(['route'=>['admin.secciones.categorias.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}

                                            <a href="#"
                                               data-method="delete"
                                               data-title="¡Precaución!"
                                               data-text="¿Estás seguro que quieres borrar esto?"
                                               data-type="warning"
                                               data-id-form="form_{{ $item->id }}"
                                               class="hey-confirm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
