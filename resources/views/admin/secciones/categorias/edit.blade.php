@extends('admin.main')
@section('title', 'Editar categoría')
@section('content')


<section id="categoriasPage" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        {{--<div class="ops">
        </div>--}}
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form">
                        @include('admin.partials._messages')
                        @if($categoria != null)
                            {!! Form::model($categoria, ['route'=>['admin.secciones.categorias.update', $categoria->id], 'method'=>'put', 'data-edit'=> 'true' ]) !!}
                                @include('admin.secciones.categorias._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>['admin.secciones.categorias.store', null], 'method'=>'post', 'data-edit'=> 'false']) !!}
                                @include('admin.secciones.categorias._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@section('scripts')
	{{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
@endsection

@endsection
