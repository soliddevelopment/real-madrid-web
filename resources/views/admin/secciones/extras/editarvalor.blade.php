@extends('admin.main')
@section('title', $datosView['seccion_titulo'] )
@section('content')


    <section id="extrasPage" class="page">
        <div class="heading">
            @include('admin.partials._crumbs')
            <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
            {{--<div class="ops">
            </div>--}}
        </div>

        <div class="contenido">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box2">
                        <div class="editar-form" style="max-width: 960px">

                            @include('admin.partials._messages')

                            {!! Form::model($extra, ['route'=>['admin.secciones.contenidos.storevalor', $extra->id], 'method'=>'post', 'data-edit'=> 'true' ]) !!}

                                {{ Form::hidden('extra_id', $extra->id) }}

                                @include('admin.secciones._camposDinamicos')

                                <div class="btn-cont">
                                    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
                                </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@section('view_scripts')
    {{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
@endsection

@endsection
