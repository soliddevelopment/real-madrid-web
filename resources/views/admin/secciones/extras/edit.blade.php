@extends('admin.main')
@section('title', 'Editar extra')
@section('content')

<section id="extrasPage" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        {{--<div class="ops">
        </div>--}}
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form">
                        @include('admin.partials._messages')
                        @if($extra != null)
                            {!! Form::model($extra, ['route'=>[$edit_route_name, $extra->id], 'method'=>'put', 'data-edit'=> 'true' ]) !!}
                                @include('admin.secciones.extras._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>[$new_route_name, null], 'method'=>'post', 'data-edit'=> 'false']) !!}
                                @include('admin.secciones.extras._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection

@section('view_scripts')
	{{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
    <script>
        $(document).ready(function(){

            function checkTipo(){
                var val = $('#tipo_slug').val();
                if(val === 'img'){
                    $('#ancho_alto').show();
                } else {
                    $('#ancho_alto input').val('');
                    $('#ancho_alto').hide();
                }
            }
            checkTipo();

            $('#tipo_slug').unbind('change').change(function(){
                checkTipo();
            });

        });
    </script>
@endsection

