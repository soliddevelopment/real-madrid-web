<div class="form-group">
    {{ Form::label('tipo_id', 'Tipo:') }}
    <select id="tipo_slug" name="tipo_slug" class="form-control">
        @foreach($tipos as $item)
            <option value="{{ $item->slug }}" {{ $extra != null && $extra->tipo_id == $item->id ? 'selected="selected"':'' }}>{{ $item->titulo }}</option>
        @endforeach
    </select>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('titulo', 'Título:') }}
            {{ Form::text('titulo', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('slug', 'Slug:') }}
            {{ Form::text('slug', null, ['class' => 'form-control no-guiones']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="checkboxes-list">
            <ul>
                <li>
                    <label>
                        <input type="checkbox" name="obligatorio" value="1" {{ $extra && $extra->required == 1 ? 'checked':'' }}>
                        <span>Campo obligatorio</span>
                    </label>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="ancho_alto" class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('width', 'Ancho:') }}
            {{ Form::number('width', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('height', 'Alto:') }}
            {{ Form::number('height', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>

@if($seccion != null)
    {{ Form::hidden('seccion_id', $seccion->id) }}
@else
    <div class="form-group">
        {{ Form::label('seccion_id', 'Sección:') }}
        <select id="seccion_id" name="seccion_id" class="form-control">
            <option value="">SIN SECCIÓN</option>
            @foreach($secciones as $item)
                <option value="{{ $item->id }}" {{ $extra != null && $extra->seccion_id == $item->id ? 'selected="selected"':'' }}>{{ $item->titulo }}</option>
            @endforeach
        </select>
    </div>
@endif

<div class="btn-cont">

    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>
