@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')

    <section id="extrasPage" class="page">
        <div class="heading">
            @include('admin.partials._crumbs')
            <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
            <div class="ops">
                {{--@if($usuario->hasRole('superadmin'))
                    <a href="{{ route('admin.secciones.create') }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
                @endif--}}

                @if($seccion != null)
                    @if($modo == 'CONTENIDO')
                        <a href="{{ route('admin.secciones.contenidos.create', [ 'seccion_id'=> $seccion->id ]) }}" class="btn btn-primary">+ Nueva</a>
                    @else
                        <a href="{{ route('admin.secciones.extras.create', [ 'seccion_id'=> $seccion->id ]) }}" class="btn btn-primary">+ Nuevo</a>
                    @endif
                @else
                    @if($modo == 'CONTENIDO')
                        <a href="{{ route('admin.secciones.contenidos.create', [ 'seccion_id'=> null ]) }}" class="btn btn-primary">+ Nueva</a>
                    @else
                        <a href="{{ route('admin.secciones.extras.create', [ 'seccion_id'=> null ]) }}" class="btn btn-primary">+ Nuevo</a>
                    @endif
                @endif

            </div>
        </div>

        <div class="contenido">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-12">
                                @if(count($extras) > 0)
                                    <table class="table table1 {{ $usuario->hasRole('superadmin') ? 'heySort':'' }}" data-url="{{ route('admin.secciones.extras.sort') }}">
                                    <thead>
                                        <tr>
                                            @if($usuario->hasRole('superadmin'))
                                                <th>id</th>
                                            @endif
                                            @if($modo == 'CONTENIDO')
                                                <th>sección</th>
                                            @endif
                                            <th>titulo</th>
                                            @if($usuario->hasRole('superadmin'))
                                                <th>slug</th>
                                            @endif
                                            <th>tipo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($extras as $item)
                                            <tr id="elem_{{ $item->id }}">
                                                @if($usuario->hasRole('superadmin'))
                                                    <td class="td-id">{{ $item->id }}</td>
                                                @endif
                                                @if($modo == 'CONTENIDO')
                                                    <td>
                                                        @if($item->seccion != null)
                                                            <span class="label label-default">
                                                                {{ $item->seccion->titulo }}
                                                            </span>
                                                        @else
                                                            <small>- SIN SECCION -</small>
                                                        @endif
                                                    </td>
                                                @endif
                                                <td>{{ $item->titulo }}</td>
                                                @if($usuario->hasRole('superadmin'))
                                                    <td>
                                                        <small>
                                                            <strong>{{ $item->slug }}</strong> &nbsp;
                                                            <a href="#" class="copy-to-clipboard" data-txt="{{ $item->slug }}"><i class="fa fa-clipboard"></i></a>
                                                        </small>
                                                    </td>
                                                @endif
                                                <td>
                                                    @if($item->tipo !== null)
                                                        {{ $item->tipo->titulo }}
                                                    @endif
                                                </td>
                                                <td class="td-ops" style="text-align:right;">
                                                    {{--<input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.secciones.extras.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#3bafda" data-size="small" />--}}

                                                    @if($modo == 'CONTENIDO')
                                                        <a href="{{ route('admin.secciones.contenidos.editarvalor', $item->id) }}" title="Editar valor" class="">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                    @endif

                                                    @if($usuario->hasRole('superadmin'))
                                                        @if($modo == 'CONTENIDO')
                                                            <a href="{{ route($edit_route_name, ['id' => $item->id]) }} " class="">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        @else
                                                            <a href="{{ route($edit_route_name, ['seccion_id'=> $seccion != null ? $seccion->id : null, 'id' => $item->id]) }} " class="">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        @endif

                                                        {!! Form::open(['route'=>[$destroy_route_name, $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                            {{ Form::hidden('id', $item->id) }}
                                                        {!! Form::close() !!}
                                                        <a href="#"
                                                           data-method="delete"
                                                           data-title="¡Precaución!"
                                                           data-text="¿Estás seguro que quieres borrar esto?"
                                                           data-type="warning"
                                                           data-id-form="form_{{ $item->id }}"
                                                           class="hey-confirm">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @else
                                    <p class="no-hay">No se encontraron ítems</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

