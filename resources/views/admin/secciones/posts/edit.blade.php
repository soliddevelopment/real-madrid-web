@extends('admin.main')
@section('title', $title)
@section('content')

<section id="seccionesPage" class="page edit-page">

    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            @if($post != null)
                @foreach($galleriesExtras as $gal)
                    @if($gal->valor != null && $gal->valor != 'null')
                        <a href="{{ route('admin.galleries.detail', $gal->valor) }}" class="btn btn-primary btn-sm">{{ $gal->titulo }} &nbsp; <i class="fa fa-edit"></i></a>
                    @else
                        {!! Form::model($post, ['route'=>['admin.galleries.store', null], 'method'=>'post', 'data-edit'=> 'true' ]) !!}
                            {{ Form::hidden('post_id', $post->id) }}
                            {{ Form::hidden('extra_id', $gal->id) }}
                            {{ Form::hidden('extra_tipo_id', $gal->tipo_id) }}
                            {{ Form::hidden('titulo', $post->titulo.' galería') }}
                            {{ Form::hidden('slug', $post->slug.'-gallery') }}
                            {{ Form::submit($gal->titulo, ['class' => 'btn btn-primary btn-sm']) }}
                        {!! Form::close() !!}
                    @endif
                @endforeach
            @endif
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form extended">
                        @include('admin.partials._messages')
                        @if($post != null)
                            {!! Form::model($post, ['route'=>['admin.secciones.posts.update', $post->id], 'method'=>'post', 'class'=>'solid-validate', 'novalidate', 'data-edit'=> 'true' ]) !!}
                                @include('admin.secciones.posts._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>['admin.secciones.posts.store', null], 'method'=>'post', 'class'=>'solid-validate', 'novalidate', 'data-edit'=> 'false']) !!}
                                @include('admin.secciones.posts._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('view_scripts')

@endsection
