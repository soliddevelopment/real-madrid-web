@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')

    <section id="postsPage" class="page">
        <div class="heading">
            @include('admin.partials._crumbs')
            <h1 class="section-title">
                {{ $datosView['seccion_titulo'] }}
                {{ $parentPost ? '('.$parentPost->titulo.')':'' }}
            </h1>
            <div class="ops">
                <div class="search-list">
                    <input type="text" class="form-control" name="search" id="searchPostsInput" data-table-id="postsTable" placeholder="Buscar...">
                </div>
                @if($seccion->has_categorias == 1)
                    <form action="" method="GET">
                        <select id="filtro_categorias" name="cat" class="form-control filtro-categorias">
                            <option {{ $selected_cat === null ? 'selected':'' }} value="">[ Todas las categorías ]</option>
                            @foreach($categorias as $item)
                                <option value="{{$item->slug}}" {{ $selected_cat !== null && $selected_cat->id === $item->id ? 'selected':'' }}>{{$item->titulo}}</option>
                            @endforeach
                        </select>
                    </form>
                @endif

                @if($seccion->tipo == 'BLOG')
                    @if($seccion->categorias_administrables == 1 || $usuario->hasRole('superadmin'))
                        <a href="{{ route('admin.secciones.categorias.index', ['seccionSlug'=> $seccion->slug, 'parent_id'=>null]) }}" class="btn btn-primary">
                            <i class="fa fa-pencil"></i> &nbsp; Categorías
                            @if($seccion->categorias_administrables === 0)
                                (hidden)
                            @endif
                        </a>
                    @endif
                    @if($parentPost)
                        <a href="{{ route('admin.secciones.posts.create', ['slug'=>$seccion->slug, 'parentPostId'=>$parentPost->id]) }}" class="btn btn-primary">+ Nuevo item</a>
                    @else
                        <a href="{{ route('admin.secciones.posts.create', $seccion->slug) }}" class="btn btn-primary">+ Nuevo item</a>
                    @endif
                @endif

                {{--@if($seccion->children && count($seccion->children)>0)
                    <a href="{{ route('admin.secciones.posts.create', $seccion->slug) }}" class="btn btn-primary">+ Nuevo item</a>
                @else
                    <a href="{{ route('admin.secciones.posts.create', ['slug'=>$seccion->slug, 'parentPostId'=>$parentPost->id]) }}" class="btn btn-primary">+ Nuevo item</a>
                @endif--}}

            </div>
        </div>

        <div class="contenido">
            <div class="row">
                <div class="col-md-12">
                    <div class="lista-box card-box">

                        @if(count($posts)>0)

                            {{--<table class="table mb-0 posts-table table1 {{ !empty($seccion) && $seccion->sortable === 1 ? 'heySort':'' }}"
                                   data-url="{{ !empty($seccion) && $seccion->sortable === 1 ? route('admin.posts.sort'):'' }}">--}}

                            <table id="postsTable" class="table table1 {{ !empty($seccion) && $seccion->sortable === 1 ? 'heySort':'' }}"
                                   data-url="{{ !empty($seccion) && $seccion->sortable === 1 ? route('admin.secciones.posts.sort'):'' }}">
                                <thead>
                                    <tr>
                                        @if($thumb)
                                            <th></th>
                                        @endif
                                        @if($usuario->hasRole('superadmin'))
                                            <th class="td-id">id</th>
                                        @endif
                                        <th>título</th>
                                        @if($seccion->has_categorias == 1)
                                            <th>categoría</th>
                                        @endif
                                        <th>fecha creación</th>
                                        @if($seccion->children && count($seccion->children)>0)
                                            <th>contenidos</th>
                                        @endif
                                        @if($seccion->has_related == 1)
                                            <th class="text-left">relacionados</th>
                                        @endif
                                        @if($seccion->destacados == 1)
                                            <th class="td-destacado">destacado</th>
                                        @endif
                                        @if($galeria)
                                            <th class="td-galeria">galería</th>
                                        @endif
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $item)
                                    <tr id="elem_{{$item->id}}" data-post-id="{{ $item->id }}">
                                        {{--<td class="td-img"><img src="{{ mediaimg($item->img_id, 35, 35) }}"></td>--}}
                                        @if($thumb)
                                            <td class="td-img">
                                                <a href="{{ mediaimg($item->imagen, 1920) }}" class="fancybox"><img src="{{ $item->finalImg }}"></a>
                                            </td>
                                        @endif
                                        @if($usuario->hasRole('superadmin'))
                                            <td class="td-id">{{ $item->id }}</td>
                                        @endif
                                        <td class="td-titulo"><span>{{ $item->titulo }}</span></td>
                                        @if($seccion->has_categorias == 1)
                                            <td class="td-categoria">
                                                @if($item->categoria !== null)
                                                    <span>{{ $item->categoria->titulo }}</span>
                                                @else
                                                    <small>- Sin especificar -</small>
                                                @endif
                                            </td>
                                        @endif
                                        <td>
                                            {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                        </td>
                                        @if($item->seccion->children && count($item->seccion->children)>0)
                                            <td>
                                                <ul>
                                                    @foreach($item->seccion->children as $sec)
                                                        <li><a href="{{ route('admin.secciones.posts.index', ['slug'=>$sec->slug, 'parentPostId'=>$item->id]) }}">{{ $sec->titulo }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        @endif
                                        @if($seccion->has_related == 1)
                                            <td class="text-left">
                                                @if($item->relatedPosts && count($item->relatedPosts)>0)
                                                    <ul>
                                                        @foreach($item->relatedPosts as $related)
                                                            <li><small>{{ $related->titulo }}</small></li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    <small>---</small>
                                                @endif
                                            </td>
                                        @endif
                                        @if($seccion->destacados == 1)
                                            <td class="td-destacado">
                                                <input class="st-switch" data-id="{{ $item->destacado }}" data-url="{{ route('admin.secciones.posts.destacado', ['id' => $item->id]) }}" type="checkbox" {{ $item->destacado == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#2ac544" data-size="small" />
                                            </td>
                                        @endif
                                        @if($galeria)
                                            <td class="td-galeria">
                                                <a href="{{ route('admin.galleries.detail', $item->galeria) }}">Galería</a>
                                            </td>
                                        @endif
                                        <td class="td-ops">

                                            @if($seccion->tipo == 'BLOG')
                                                <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.secciones.posts.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#00AEEF" data-size="small" />
                                                <a href="{{ route('admin.secciones.posts.edit', ['seccionSlug' => $seccion->slug, 'id' => $item->id  ]) }} " class="">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            @else
                                                <a href="{{ route('admin.secciones.posts.detalle', ['seccionSlug' => $seccion->slug, 'id' => $item->id  ]) }} " class="">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            @endif
                                            &nbsp;
                                            {!! Form::open(['route'=>['admin.secciones.posts.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                            {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}

                                            <a href="#"
                                               data-method="delete"
                                               data-title="¡Precaución!"
                                               data-text="¿Estás seguro que quieres borrar esto?"
                                               data-type="warning"
                                               data-id-form="form_{{ $item->id }}"
                                               class="hey-confirm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="no-hay">
                                <p>No hay items disponibles</p>
                            </div>
                        @endif

                        {{-- {!! $proyectos->links() !!} --}}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
