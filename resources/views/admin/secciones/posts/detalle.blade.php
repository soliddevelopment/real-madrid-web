@extends('admin.main')
@section('title', $title)
@section('content')

<section id="seccionesPage" class="page detail-page">

    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            @if($post != null)
            @endif
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div id="detallePost">
                    <div class="card-box">
                        <div class="campos-dinamicos">
                            @foreach($extras as $campoDinamico)
                                <div class="elem">
                                    <p class="nombre">{{ $campoDinamico->titulo }}</p>
                                    <p class="valor">{{ $campoDinamico->valor }}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('view_scripts')

@endsection
