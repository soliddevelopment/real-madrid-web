
<div class="row">
    <div class="col-md-12">
        <div class="campos-normales">
            <div class="row">
                <div class="col-md-{{ $seccion->slug_manual == 1 ? '6':'12' }}">
                    <div class="form-group">
                        {{ Form::label('titulo', 'Título:') }}
                        {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'...', 'required']) }}
                    </div>
                </div>
                @if($seccion->slug_manual == 1)
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('slug', 'Slug:') }}
                            {{ Form::text('slug', null, ['class'=>'form-control', 'placeholder'=>'...', 'required']) }}
                        </div>
                    </div>
                @endif
            </div>

            <div class="row">
                @if($seccion->has_categorias == 1)
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('categoria_id', 'Categoría:') }}
                            <select name="categoria_id" id="categoria_id" class="form-control">
                                <option value="0">- Ninguna -</option>
                                @foreach($categorias as $item)
                                    <option value="{{ $item->id }}" {{ $post != null && $post->categoria_id == $item->id ? 'selected':'' }} >
                                        {{ $item->titulo }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif
            </div>

            @if($seccion->has_tags == 1)
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            {{ Form::label('tags', 'Tags:') }} <small class="text-muted">Palabras o frases separadas por comas</small>
                            {{ Form::textarea('tags', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows'=>2]) }}
                        </div>
                    </div>
                </div>
            @endif

        </div>

        <div class="campos-dinamicos">
            @foreach($extras as $campoDinamico)
                @include('admin.secciones._camposDinamicos')
            @endforeach
        </div>

        @if($seccion->has_related && $otrosPosts && count($otrosPosts) > 0)
            <div class="related">
                <div class="checkboxes-list">
                    <ul>
                        @foreach($otrosPosts as $otro)
                            <li>
                                <label>
                                    <input type="checkbox" name="related[]" value="{{ $otro->id }}" {{ $otro->checked ? 'checked':'' }}>
                                    <span>{{ $otro->titulo }}</span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <hr>

        <div class="btn-cont">
            @if($parentPost)
                {{ Form::hidden('parent_id', $parentPost->id) }}
            @endif
            {{ Form::hidden('seccion_id', $seccion->id) }}
            {{ Form::submit('Guardar cambios', ['class' => 'btn btn-primary btn-lg']) }}
        </div>

    </div>
</div>
