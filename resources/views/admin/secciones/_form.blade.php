<div class="form-group">
    {{ Form::label('seccion_id', 'Sección padre:') }}
    <select name="seccion_id" id="seccion_id" class="form-control">
        <option {{ $seccion === null || $seccion->seccion_id === null ? 'selected':'' }}>[ Sin sección padre ]</option>
        @foreach($secciones as $item)
            <option value="{{ $item->id }}" {{ $seccion != null && $seccion->seccion_id !== null && $item->id == $seccion->seccion_id ? 'selected':'' }}>{{ $item->titulo }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    {{ Form::label('titulo', 'Título:') }}
    {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'Ej. Posts normales']) }}
</div>
<div class="form-group">
    {{ Form::label('slug', 'Slug:') }}
    {{ Form::text('slug', null, ['class'=>'form-control', 'placeholder'=>'Ej. normales']) }}
</div>
<hr>

<div class="form-group checkb">
    <label>
        <input type="checkbox" id="has_children" name="has_children" value="1" {{ $seccion!==null && $seccion->has_children == 1 ? 'checked':'' }} >
        <span>Puede contener posts hijos</span>
    </label>
</div>
<div class="form-group checkb">
    <label>
        <input type="checkbox" name="has_imgs" value="1" {{ $seccion!==null && $seccion->has_imgs == 1 ? 'checked':'' }}>
        <span>Imágenes</span>
    </label>
</div>
<div class="form-group checkb">
    <label>
        <input type="checkbox" name="has_categorias" value="1" {{ $seccion!==null && $seccion->has_categorias===1 ? 'checked':'' }}>
        <span>Categorías</span>
    </label>
</div>
{{--<div class="form-group checkb">
    <label>
        <input type="checkbox" name="has_subcategorias" value="1" {{ $seccion!==null && $seccion->has_subcategorias===1 ? 'checked':'' }}>
        <span>Subcategorías</span>
    </label>
</div>--}}
<div class="form-group checkb">
    <label>
        <input type="checkbox" name="categorias_administrables" value="1" {{ $seccion!==null && $seccion->categorias_administrables===1 ? 'checked':'' }}>
        <span>Categorías administrables</span>
    </label>
</div>
<div class="form-group checkb">
    <label>
        <input type="checkbox" name="sortable" value="1" {{ $seccion!==null && $seccion->sortable===1 ? 'checked':'' }}>
        <span>Sortable (order)</span>
    </label>
</div>
<div class="form-group checkb">
    <label>
        <input type="checkbox" name="adjuntos" value="1" {{ $seccion!==null && $seccion->adjuntos===1 ? 'checked':'' }}>
        <span>Adjuntos</span>
    </label>
</div>
<div class="form-group checkb">
    <label>
        <input type="checkbox" name="has_related" value="1" {{ $seccion!==null && $seccion->has_related===1 ? 'checked':'' }}>
        <span>Posts relacionables</span>
    </label>
</div>

<hr>
<div class="btn-cont">
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
</div>
