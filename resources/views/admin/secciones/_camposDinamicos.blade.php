<div class="form-group">

    @if($campoDinamico->tipo_slug != 'gallery')
        {{ Form::label($campoDinamico->slug, $campoDinamico->titulo.':') }}
    @endif

    @if($campoDinamico->tipo_slug == 'texto')
        <input type="text" name="{{ $campoDinamico->slug }}" class="form-control" placeholder="..." value="{{ $campoDinamico->valor }}" {{ $campoDinamico->required ? 'required':'' }}>

    @elseif($campoDinamico->tipo_slug == 'img')
        <media-manager-btn
                v-bind:select-mode="true"
                custom-class="btn btn-primary btn-sm" input-name="{{ $campoDinamico->slug }}"
                :preview="true" extensions="jpg,png,gif"
                :img="true" default-img="{{ mediaimg($campoDinamico->valor, 100, 100) }}"
                width="{{ $campoDinamico->width }}" height="{{ $campoDinamico->height }}"
                title="Seleccionar imagen" txt='Seleccionar imagen'
        ></media-manager-btn>

    @elseif($campoDinamico->tipo_slug == 'texto-mediano')
        <textarea name="{{ $campoDinamico->slug }}" rows="3" class="form-control" {{ $campoDinamico->required ? 'required':'' }}>{{ $campoDinamico->valor }}</textarea>

    @elseif($campoDinamico->tipo_slug == 'texto-largo')
        <textarea name="{{ $campoDinamico->slug }}" rows="6" class="form-control" {{ $campoDinamico->required ? 'required':'' }}>{{ $campoDinamico->valor }}</textarea>

    @elseif($campoDinamico->tipo_slug == 'editor')
        <textarea name="{{ $campoDinamico->slug }}" rows="25" class="form-control summernote" {{ $campoDinamico->required ? 'required':'' }}>{!! $campoDinamico->valor !!}</textarea>

    @elseif($campoDinamico->tipo_slug == 'html')
        <textarea name="{{ $campoDinamico->slug }}" rows="25" class="form-control" {{ $campoDinamico->required ? 'required':'' }}>{!! $campoDinamico->valor !!}</textarea>

    @elseif($campoDinamico->tipo_slug == 'email')
        <input type="email" name="{{ $campoDinamico->slug }}" class="form-control" placeholder="..." value="{{ $campoDinamico->valor }}" {{ $campoDinamico->required ? 'required':'' }}>

    @elseif($campoDinamico->tipo_slug == 'fecha')
        <input type="text" name="{{ $campoDinamico->slug }}" class="form-control datepicker" placeholder="..." value="{{ $campoDinamico->valor }}" {{ $campoDinamico->required ? 'required':'' }}>

    @elseif($campoDinamico->tipo_slug == 'archivo')
        {{--<textarea name="{{ $campoDinamico->slug }}" rows="25" class="form-control summernote">{!! $campoDinamico->valor !!}</textarea>--}}
        <p>PROXIMAMENTE...</p>

    @elseif($campoDinamico->tipo_slug == 'lista')
        <p>PROXIMAMENTE...</p>

    @elseif($campoDinamico->tipo_slug == 'checkbox')
        <select name="{{ $campoDinamico->slug }}" class="form-control">
            <option value="1" {{ $campoDinamico->valor == 1 ? 'selected="selected"':'' }}>SI</option>
            <option value="0" {{ $campoDinamico->valor == 0 ? 'selected="selected"':'' }}>NO</option>
        </select>

    @endif
</div>
