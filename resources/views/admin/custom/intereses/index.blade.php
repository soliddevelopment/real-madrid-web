@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<section id="" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        {{--<div class="ops">
            @if($usuario->hasRole('superadmin'))
                <a href="{{ route('admin.custom.accommodations.create') }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
            @endif
        </div>--}}
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">

                <div class="lista-box card-box">

                    @if(count($contactos)>0)

                        <table class="table table1">
                            <thead>
                                <tr>
                                    @if($usuario->hasRole('superadmin'))
                                        <th class="td-id">id</th>
                                    @endif
                                    <th>nombre</th>
                                    <th>email</th>
                                    <th>tel</th>
                                    <th>fecha nac.</th>
                                    <th>servicio</th>
                                    {{--<th></th>--}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contactos as $item)
                                    <tr id="elem_{{$item->id}}">
                                        @if($usuario->hasRole('superadmin'))
                                            <td class="td-id">{{ $item->id }}</td>
                                        @endif
                                        <td>{{ $item->nombre.' '.$item->apellido }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->telefono }}</td>
                                        <td>
                                            {{ Carbon\Carbon::parse($item->fecha_nacimiento)->format('d/m/Y') }}
                                        </td>
                                        <td>
                                            {{ $item->servicio ? $item->servicio->titulo : 'N/A' }}
                                        </td>

                                        {{--<td class="td-ops">
                                            <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.custom.accommodations.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#00AEEF" data-size="small" />
                                            <a href="{{ route('admin.custom.accommodations.edit', [ 'id' => $item->id  ]) }} " class="">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            &nbsp;
                                            {!! Form::open(['route'=>['admin.custom.accommodations.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}

                                            @if($usuario->hasRole('superadmin'))
                                                <a href="#"
                                                   data-method="delete"
                                                   data-title="¡Precaución!"
                                                   data-text="¿Estás seguro que quieres borrar esto?"
                                                   data-type="warning"
                                                   data-id-form="form_{{ $item->id }}"
                                                   class="hey-confirm">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>--}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="no-hay">
                            <p>No hay items disponibles</p>
                        </div>
                    @endif

                    {{-- {!! $proyectos->links() !!} --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
