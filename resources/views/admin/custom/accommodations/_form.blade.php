
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {{ Form::label('nombre', 'Nombre:') }}
            {{ Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'...', 'readonly']) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {{ Form::label('slug', 'Slug:') }}
            {{ Form::text('slug', null, ['class'=>'form-control no-guiones', 'placeholder'=>'...', 'readonly']) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {{ Form::label('bajada', 'Bajada:') }}
            {{ Form::text('bajada', null, ['class'=>'form-control', 'placeholder'=>'...', 'readonly']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('resumen', 'Resumen:') }}
            {{ Form::textarea('resumen', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows' => '4']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('direccion', 'Dirección:') }}
            {{ Form::textarea('direccion', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows' => '3']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('icono', 'Imagen ícono:') }}
            <media-manager-btn
                v-bind:select-mode="true"
                custom-class="btn btn-primary btn-sm" input-name="icono" extensions="jpg,png"
                :preview="true" :img="true" default-img="{{ $elem !== null && $elem->icono !== null ? mediaimg($elem->icono, 100, 100, false) : '' }}"
                title="Seleccionar imagen" txt='Seleccionar imagen'
            ></media-manager-btn>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('body', 'Descripción:') }}
            {{ Form::textarea('body', null, ['class'=>'summernote', 'placeholder'=>'...']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('latitude', 'Latitude:') }}
            {{ Form::text('latitude', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('longitude', 'Longitude:') }}
            {{ Form::text('longitude', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('price1', 'Single room price:') }}
            {{ Form::number('price1', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('price2', 'Double room price:') }}
            {{ Form::number('price2', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('price3', 'Triple room price:') }}
            {{ Form::number('price3', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('price4', 'Quadruple room price:') }}
            {{ Form::number('price4', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('gallery_id', 'Galería asociada:') }}
            <select name="gallery_id" class="form-control">
                <option>[ Seleccione galería ]</option>
                @foreach($galleries as $gal)
                    <option value="{{ $gal->id }}" {{ $elem != null && $gal->id == $elem->gallery_id ? 'selected="selected"':'' }}>
                        {{ $gal->titulo }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="form-group amenities-list-cont">
            {{ Form::label('amenities', 'Amenities:') }}
            <?php $a=0; ?>
            <div class="amenities-list">
                @foreach($amenities as $item)
                    <?php $a++; ?>
                    <div id="amenity_{{ $a }}" class="elem">
                        <span class="ordenar"><i class="fa fa-align-justify"></i></span>
                        <input type="text" class="form-control" name="amenity_{{ $a }}" placeholder="Type a description of the amenity..." value="{{ $item->nombre }}">
                        <a href="#" class="cerrar"><i class="fa fa-trash"></i></a>
                    </div>
                @endforeach
            </div>
            <div class="amenities-btn-cont">
                <a href="#" class="btn btn-sm btn-primary agregar-amenities">+ Agregar amenities</a>
            </div>
            {{ Form::hidden('amenities_count', $a, ['id'=> 'amenities_count']) }}
        </div>
    </div>
</div>

<hr>

<div class="btn-cont">
    {{-- {{ Form::hidden('grupo_id', $grupo->id) }} --}}
    {{-- {{ Form::hidden('metas_nuevos', $post->grupo_id, ['id'=>'metas_nuevos']) }} --}}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
</div>

