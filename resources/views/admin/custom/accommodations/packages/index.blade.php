@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<section id="" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            @if($usuario->hasRole('superadmin'))
                <a href="{{ route('admin.custom.accommodations.packages.create', $accommodation->id) }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
            @endif
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">

                <div class="lista-box card-box">

                    @if(count($elementos)>0)

                        <table class="table table1 heySort" data-url="{{ route('admin.custom.accommodations.packages.sort', $accommodation->id) }}">
                            <thead>
                                <tr>
                                    <th></th>
                                    @if($usuario->hasRole('superadmin'))
                                        <th class="td-id">id</th>
                                    @endif
                                    <th>nombre</th>
                                    <th>slug</th>
                                    <th>galería</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($elementos as $item)
                                    <tr id="elem_{{$item->id}}">
                                        <td class="td-img"><img src="{{ mediaimg($item->img, 35, 35) }}"></td>
                                        @if($usuario->hasRole('superadmin'))
                                            <td class="td-id">{{ $item->id }}</td>
                                        @endif
                                        <td>{{ $item->nombre }}</td>
                                        <td>{{ $item->slug }}</td>
                                        <td>
                                            @if($item->galeria)
                                                <strong>{{ $item->galeria->titulo }}</strong>
                                            @else
                                                <strong>N/A</strong>
                                            @endif
                                        </td>
                                        {{--<td>
                                            {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                        </td>--}}

                                        <td class="td-ops">
                                            <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.custom.accommodations.packages.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#00AEEF" data-size="small" />
                                            <a href="{{ route('admin.custom.accommodations.packages.edit', ['accommodation_id'=> $item->accommodation_id, 'id'=> $item->id]) }} " class="">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            &nbsp;
                                            {!! Form::open(['route'=>['admin.custom.accommodations.packages.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}

                                            @if($usuario->hasRole('superadmin'))
                                                <a href="#"
                                                   data-method="delete"
                                                   data-title="¡Precaución!"
                                                   data-text="¿Estás seguro que quieres borrar esto?"
                                                   data-type="warning"
                                                   data-id-form="form_{{ $item->id }}"
                                                   class="hey-confirm">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="no-hay">
                            <p>No hay items disponibles</p>
                        </div>
                    @endif

                    {{-- {!! $proyectos->links() !!} --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
