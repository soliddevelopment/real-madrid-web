
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('nombre', 'Nombre:') }}
            {{ Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('slug', 'Slug:') }}
            {{ Form::text('slug', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('bajada', 'Bajada:') }}
            {{ Form::text('bajada', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
</div>

{{--
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('resumen', 'Resumen:') }}
            {{ Form::textarea('resumen', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows' => '4']) }}
        </div>
    </div>
</div>
--}}

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="form-group amenities-list-cont">
            {{ Form::label('amenities', 'Amenities:') }}
            <?php $a=0; ?>
            <div class="amenities-list">
                @foreach($amenities as $item)
                    <?php $a++; ?>
                    <div id="amenity_{{ $a }}" class="elem">
                        <span class="ordenar"><i class="fa fa-align-justify"></i></span>
                        <input type="text" class="form-control" name="amenity_{{ $a }}" placeholder="Type a description of the amenity..." value="{{ $item->nombre }}">
                        <a href="#" class="cerrar"><i class="fa fa-trash"></i></a>
                    </div>
                @endforeach
            </div>
            <div class="amenities-btn-cont">
                <a href="#" class="btn btn-sm btn-primary agregar-amenities">+ Agregar amenities</a>
            </div>
            {{ Form::hidden('amenities_count', $a, ['id'=> 'amenities_count']) }}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <div class="form-group prices-list-cont">
            {{ Form::label('prices', 'Prices:') }}
            <?php $a=0; ?>
            <div class="prices-list">
                @foreach($prices as $item)
                    <?php $a++; ?>
                    <div id="price_{{ $a }}" class="elem">
                        <span class="ordenar"><i class="fa fa-align-justify"></i></span>
                        <input type="text" class="form-control control-nights" name="price_nights_{{ $a }}" placeholder="Ex. 5 nights" value="{{ $item->nights_txt }}">
                        <input type="text" class="form-control control-value" name="price_value_{{ $a }}" placeholder="Ex. 150.00" value="{{ $item->value }}">
                        <a href="#" class="cerrar"><i class="fa fa-trash"></i></a>
                    </div>
                @endforeach
            </div>
            <div class="amenities-btn-cont">
                <a href="#" class="btn btn-sm btn-primary agregar-price">+ Add price</a>
            </div>
            {{ Form::hidden('prices_count', $a, ['id'=> 'prices_count']) }}
        </div>
    </div>
</div>

<hr>

<div class="btn-cont">
     {{ Form::hidden('accommodation_id', $accommodation->id) }}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
</div>

