@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')

<section id="" class="page edit-page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
    </div>
    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form">
                        @include('admin.partials._messages')
                        @if($elem != null)
                            {!! Form::model($elem, ['route'=>['admin.custom.accommodations.packages.update', $elem->id], 'method'=>'PUT', 'files'=>true, 'data-edit'=> 'true' ]) !!}
                                @include('admin.custom.accommodations.packages._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>['admin.custom.accommodations.packages.store', null], 'method'=>'post', 'files'=>true ]) !!}
                                @include('admin.custom.accommodations.packages._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('view_scripts')
    <script>

        function nuevaAmenity(){
            var $amenitiesCountInput = $('#amenities_count');
            var countActual = parseInt($amenitiesCountInput.val());
            if(countActual === null || countActual === undefined){ countActual = 0; }
            countActual++;
            var html = '<div id="amenity_'+countActual+'" class="elem"><span class="ordenar"><i class="fa fa-align-justify"></i></span><input type="text" class="form-control" name="amenity_'+countActual+'" placeholder="Type a description of the amenity..."><a href="#" class="cerrar"><i class="fa fa-trash"></i></a></div>';
            $('.amenities-list').append(html).promise().done(function(){
                setAmenitiesListCerrar();
            });
            $amenitiesCountInput.val(countActual);
        }

        function setAmenitiesListCerrar(){
            $('.amenities-list .elem .cerrar').unbind('click').click(function(e){
                e.preventDefault();
                $(this).closest('.elem').remove();
                // var $amenitiesCountInput = $('#amenities_count');
                // var countActual = parseInt($amenitiesCountInput.val());
                // var countNuevo = countActual - 1;
                // if(countNuevo < 0) countNuevo = 0;
                // $amenitiesCountInput.val(countNuevo);
            });
        }

        function actualizarOrdenGaleria(){
            var x = 0;
            $('.amenities-list .elem').each(function(){
                x++;
                var $formControl = $(this).find('.form-control');
                $(this).attr('id', 'amenity_'+x);
                $formControl.attr('name', 'amenity_'+x);
            });
            $('#amenities_count').val(x);
        }

        function nuevoPrecio(){
            var $pricesCountInput = $('#prices_count');
            var countActual = parseInt($pricesCountInput.val());
            if(countActual === null || countActual === undefined){ countActual = 0; }
            countActual++;
            var html = '<div id="price_'+countActual+'" class="elem"><span class="ordenar"><i class="fa fa-align-justify"></i></span><input type="text" class="form-control" name="price_nights_'+countActual+'" placeholder="Ex. 5 nights"><input type="text" class="form-control" name="price_value_'+countActual+'" placeholder="Ex. 150.00"><a href="#" class="cerrar"><i class="fa fa-trash"></i></a></div>';
            $('.prices-list').append(html).promise().done(function(){
                setPricesListCerrar();
            });
            $pricesCountInput.val(countActual);
        }

        function setPricesListCerrar(){
            $('.prices-list .elem .cerrar').unbind('click').click(function(e){
                e.preventDefault();
                $(this).closest('.elem').remove();
            });
        }

        function actualizarOrdenPrecios(){
            var x = 0;
            $('.prices-list .elem').each(function(){
                x++;
                var $formControl1 = $(this).find('.form-control.control-nights');
                var $formControl2 = $(this).find('.form-control.control-value');
                $(this).attr('id', 'amenity_'+x);
                $formControl1.attr('name', 'price_nights_'+x);
                $formControl2.attr('name', 'price_value_'+x);
            });
            $('#amenities_count').val(x);
        }

        $(document).ready(function(){
            $('.agregar-amenities').unbind('click').click(function(e){
                e.preventDefault();
                nuevaAmenity();
            });
            $('.agregar-price').unbind('click').click(function(e){
                e.preventDefault();
                nuevoPrecio();
            });
            setPricesListCerrar();
            setAmenitiesListCerrar();

        });

        var $elemToSort = $('.prices-list');
        $elemToSort.sortable({
            update: function (event, ui){
                actualizarOrdenPrecios();
            }
        });
    </script>
@endsection

