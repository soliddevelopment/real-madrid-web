@extends('admin.main')

@section('title', $title)

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
                <li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
                <li><a href="{{ route('admin.servicios.index') }}">Servicios</a></li>
                <li class="active">{{ $title }}</li>
            </ol>
            <h4 class="page-title">{{ $title }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
            {{-- <div class="pull-right">
                <a href="" class="btn btn-primary">+ Nuevo</a>
            </div> --}}
            {{-- <h4 class="m-t-0 header-title">
                <b>Lista de usuarios</b>
			</h4> --}}
			
			@if($servicio != null)
				{!! Form::model($servicio, ['route'=>['admin.servicios.update', $servicio->id], 'method'=>'PUT', 'files'=>true ]) !!}
					@include('admin.servicios._form')
				{!! Form::close() !!}
			@endif

			@if($servicio == null)
				{!! Form::open(['route'=>['admin.servicios.store', null], 'method'=>'post', 'files'=>true ]) !!}
					@include('admin.servicios._form')
				{!! Form::close() !!}
			@endif

		</div>
	</div>
</div>

@section('scripts')
	{{-- <script type="text/javascript" src="{{ mix('a/js/custom/editarPost.js') }}"></script> --}}
@endsection

@endsection