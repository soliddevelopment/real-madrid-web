@extends('admin.main')

@section('title', 'Importar usuarios')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <ol class="breadcrumb pull-right">
				<li><a href="{{ route('admin.home.home') }}">Administrador</a></li>
				@if(!empty($cuentaSeleccionada) && $cuentaSeleccionada !== null)
					<li><a href="{{ route('admin.cuentas.usuarios.index', $usuario->cuentas[0]->id) }}">Usuarios</a></li>
					<li class="active">Importar</li>
				@else
					<li><a href="{{ route('admin.usuarios.index') }}">Usuarios</a></li>
					<li class="active">Importar</li>
				@endif
            </ol>
            <h4 class="page-title">Usuarios</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

    	@include('admin.partials._messages')

        <div class="card-box">
			{!! Form::open(['route'=>['admin.usuarios.importarPost', null], 'method'=>'post', 'files'=>true ]) !!}

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('file', 'Archivo excel:') }}
							{{ Form::file('file') }}
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('role', 'Nivel de permisos de usuarios:') }}
							<select name="role" class="form-control">
								<option value="0">Sin roles (usuario común)</option>
								@foreach($roles as $role)
									@if($role->nombre != 'superadmin')
										<option value="{{$role->id}}" {{ $role->nombre === 'Basic' ? 'selected="selected"':'' }}>{{$role->nombre}} ({{$role->descripcion}})</option>
									@endif
								@endforeach
							</select>

						</div>
					</div>
				</div>

				<hr>

				<div class="btn-cont">
					{{ Form::submit('Importar', ['class' => 'btn btn-primary btn-lg']) }}
				</div>

			{!! Form::close() !!}

			{{-- {!! Form::open(['route'=>['usuarios.destroy', $usuario->id], 'method'=>'DELETE' ]) !!}
				{{ Form::submit('Borrar', ['class' => 'btn btn-primary btn-lg']) }}
			{!! Form::close() !!} --}}

		</div>
	</div>
</div>

@endsection
