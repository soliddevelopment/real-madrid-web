@extends('admin.main')

@section('title', 'Lista de usuarios')

@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<section id="configsPage" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            <a href="{{ route('admin.users.create') }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">

                <div class="card-box">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="usuariosTable" class="table table1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>usuario</th>
                                        <th>nivel permisos</th>
                                        <th>nombre</th>
                                        <th>apellido</th>
                                        <th>email</th>
                                        <th>tel</th>
                                        <th>fecha registro</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@section('view_scripts')
    <script>
        $(document).ready(function(){
            $('#usuariosTable').DataTable({
                serverSide: true,
                processing: true,
                pageLength: 20,
                paging: true,
                order: [[ 1, "asc" ]],

                dom: 'Bfrtip',
                buttons: [
                    // 'copy', 'csv', 'excel', 'pdf', 'print'
                ],

                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                ajax: {
                    "url": "{{ route('admin.users.listaAjax') }}",
                    "type": "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'usuario', name: 'usuario' },
                    { data: 'role', name: 'role', 'searchable': false },
                    { data: 'nombre', name: 'nombre' },
                    { data: 'apellido', name: 'apellido' },
                    { data: 'email', name: 'email' },
                    { data: 'telefono', name: 'telefono' },
                    { data: 'created_at', name: 'created_at', 'orderable': false, 'searchable': false },
                    {
                        orderable: false,
                        searchable: false,
                        data: null,
                        className: 'td-ops',
                        render: function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<a class="" href="/admin/usuarios/'+data.id+'/edit"><i class="fa fa-pencil"></i></a>'
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    });
                }
            });
        });
    </script>
@endsection

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
