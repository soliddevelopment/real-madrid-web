<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('nombre', 'Nombre:') }}
            {{ Form::text('nombre', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('apellido', 'Apellido:') }}
            {{ Form::text('apellido', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('email', 'Email:') }}
            {{ Form::email('email', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('usuario', 'Usuario:') }}
            {{ Form::text('usuario', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('telefono', 'Teléfono:') }}
            {{ Form::text('telefono', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {{ Form::label('sexo', 'Sexo:') }}
            {{ Form::text('sexo', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="row" style="">
    <div class="col-sm-6">
        <media-manager-btn
            v-bind:select-mode="true"
            custom-class="btn btn-primary btn-sm" input-name="img_id"
            :preview="true" extensions="jpg,png,gif"
            :img="true" default-img="{{ $user && $usr->img_id ? mediaimg($usr->img_id) : null }}"
            title="Seleccionar imagen de perfil" txt='Seleccionar imagen'
        ></media-manager-btn>
    </div>
    <div class="col-sm-6"></div>
</div>

@if($user == null)
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('password', 'Password:') }}
                {{ Form::password('password', ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('password_confirmation', 'Confirmar password:') }}
                {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
            </div>
        </div>
    </div>
@endif

@if($usuario->hasRole('superadmin') || $usuario->hasRole('admin'))
    <div class="form-group">
        {{ Form::label('role', 'Tipo usuario:') }}
        <select name="role" class="form-control">
            @foreach($roles as $role)
                @if($role->nombre != 'superadmin')
                    <option value="{{$role->id}}" {{ ($user === null && $role->slug === 'basic') || ($user !== null && $role->id === $user->role_id) ? 'selected="selected"':'' }}>{{$role->nombre}} - {{$role->descripcion}}</option>
                @endif
            @endforeach
        </select>
    </div>

@endif

<div class="btn-cont">
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
</div>

{{-- {!! Form::open(['route'=>['usuarios.destroy', $usuario->id], 'method'=>'DELETE' ]) !!}
    {{ Form::submit('Borrar', ['class' => 'btn btn-primary btn-lg']) }}
{!! Form::close() !!} --}}
