@extends('admin.main')
@section('title', $title)
@section('content')

<section id="configsPage" class="page">
	<div class="heading">
		@include('admin.partials._crumbs')
		<h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
		<div class="ops">
			@if($canMakeChanges)
				<div class="form-group">
					<a href="#" data-toggle="modal" data-target="#cambiarContrasenaModal" class="btn btn-sm btn-primary">Cambiar contraseña</a>
				</div>
			@endif
		</div>
	</div>

	<div class="contenido">
		<div class="row">
			<div class="col-md-12">

				@include('admin.partials._messages')

				<div class="card-box2">
					<div class="editar-form">
						@if($user != null)
							{!! Form::model($user, ['route'=>['admin.users.update', $user->id], 'method'=>'PUT' ]) !!}
							@include('admin.users._form')
							{!! Form::close() !!}
						@else
							{!! Form::open(['route'=>['admin.users.store', null], 'method'=>'post', 'files'=>true ]) !!}
							@include('admin.users._form')
							{!! Form::close() !!}
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('view_modals')

	@if($user != null)
		<div id="cambiarContrasenaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="cambiarContrasenaModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="cambiarContrasenaModalLabel">Cambiar contraseña</h4>
					</div>
					<div class="modal-body">
						<div class="modal-form">
							{!! Form::model($user, ['route'=>['admin.users.updatepassword', $user->id], 'method'=>'PUT' ]) !!}
							<div class="form-group">
								{{ Form::label('password', 'Contraseña:') }}
								{{ Form::password('password', ['class' => 'form-control']) }}
							</div>
							<div class="form-group">
								{{ Form::label('password_confirmation', 'Confirmar contraseña:') }}
								{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
							</div>
							<div class="btn-cont">
								{{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-lg']) }}
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	@endif
@endsection
