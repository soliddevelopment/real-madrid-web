
<div id="imageCropperModal" ref="imageCropperModal" class="heymodal">
    <div id="imageCropperVue">
        <div class="heymodal-bg"></div>
        <div class="heymodal-cuadro">
            <div class="heymodal-header">
                <div class="ops-left">
                    {{--<h1 class="title">@{{ title }}</h1>--}}

                    <div class="btn-group">
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('setDragMode', 'move')" title="Move">
                            <span class="fa fa-arrows"></span>
                        </button>
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('setDragMode', 'crop')" title="Crop">
                            <span class="fa fa-crop"></span>
                        </button>
                    </div>

                    <div class="btn-group">
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('rotate', -45)" title="Rotate left">
                            <span class="fa fa-rotate-left"></span>
                        </button>
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('rotate', 45)" title="Rotate right">
                            <span class="fa fa-rotate-right"></span>
                        </button>
                    </div>

                    <template v-if="w !== null && h !== null">
                        <div class="btn-group txt">
                            La medida obligatoria es <strong>@{{ w+' x '+h+' px' }}</strong>
                        </div>
                    </template>

                    {{--<div class="btn-group">
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('move', -10, 0)" title="Move left">
                            <span class="fa fa-arrow-left"></span>
                        </button>
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('move', 10, 0)" title="Move right">
                            <span class="fa fa-arrow-right"></span>
                        </button>
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('move', 0, -10)" title="Move up">
                            <span class="fa fa-arrow-up"></span>
                        </button>
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('move', 0, 10)" title="Move down">
                            <span class="fa fa-arrow-down"></span>
                        </button>
                    </div>

                    <div class="btn-group">
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('zoom', 0.1)" title="Zoom In">
                            <span class="fa fa-search-plus"></span>
                        </button>
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('zoom', -0.1)" title="Zoom Out">
                            <span class="fa fa-search-minus"></span>
                        </button>
                    </div>--}}

                </div>
                <div class="ops-right">

                    <div class="btn-group">
                        <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('reset')" title="Reset">
                            <span class="fa fa-refresh"></span>
                        </button>
                    </div>
                    <div class="btn-group">
                        <template v-if="w !== null && h !== null">
                            <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('confirmar2')" title="Confirmar">
                                Guardar y confirmar &nbsp; <span class="fa fa-check"></span>
                            </button>
                        </template>
                        <template v-if="w === null || h === null">
                            <button class="btn btn-primary cropper-btn" v-on:click="clickCropperBtn('confirmar')" title="Confirmar">
                                Guardar &nbsp; <span class="fa fa-check"></span>
                            </button>
                        </template>
                    </div>
                    <a href="#" class="heymodal-cerrar" v-on:click="clickCerrarModal()"><i class="fa fa-remove"></i></a>
                </div>
            </div>

            <div class="heymodal-content">
                {{--<img src="" alt="" id="imgEditarImagen">--}}
                {{--<img v-if="img !== null" v-bind:src="img.route" alt="" id="imgEditarImagen">--}}
                {{--<img src="http://localhost:8000/storage/mediamanager/media-1544326090-CIMG0331.jpg" id="imgEditarImagen">--}}
                {{--<img src="" id="imgEditarImagen">--}}
                {{--<p id="imgEditarImagen2"></p>--}}

                <template v-if="img !== null">
                    <vue-cropper
                        ref="cropper"
                        {{--:zoom="determineZoom"--}}
                        :aspect-ratio="getAspectRatio()"
                        :initial-aspect-ratio="getAspectRatio()"
                        :src="imgSrc"
                        alt="Source Image"
                        :cropmove="cropImage"
                    >
                    </vue-cropper>
                </template>

            </div>

            {{--<div class="heymodal-footer">
                <p>footer</p>
            </div>--}}

        </div>
        <input type="hidden" ref="cropperUrl" value="{{ route('admin.media.edit') }}">
        {{--<input type="hidden" ref="listaArchivosUrl" value="{{ route('admin.media.lista') }}">--}}
        {{--<div id="enviandoArchivosLoading" v-bind:class="{ open: enviandoArchivos }">
            <div class="cuadro-enviando">
                <h2>Enviando...</h2>
            </div>
        </div>--}}
    </div>
</div>
