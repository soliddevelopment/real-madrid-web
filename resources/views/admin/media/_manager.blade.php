
<div id="mediaManagerModal" ref="mediaManagerModal" class="heymodal">
    <div id="mediaManagerVue">
        <div class="heymodal-bg"></div>
        <div class="heymodal-cuadro">
            <div class="heymodal-header">
                <div class="ops-left">
                    <h1 class="title">@{{ title }}</h1>
                    <div class="filtros">
                        <div class="fil" id="filtroBuscar">
                            {{--<label>Buscar:</label>--}}
                            <input v-model="filtros.q" v-on:keyup="changedBuscar()" type="text" class="form-control" placeholder="Buscar...">
                        </div>
                        <div class="fil" id="filtroFormatos">
                            <div class="btn-group dropdown keep-open">
                                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" aria-expanded="false">
                                    Formatos
                                </button>
                                <div class="dropdown-menu dropdown-menu-left">
                                    <div class="checkbox-list">
                                        <ul>
                                            <template v-for="item in mimetypes">
                                                <li v-if="item.visible">
                                                    <label>
                                                        <input type="checkbox" @change="changedFormatos()" v-model="filtros.mimetypes" :value="item.val">
                                                        <span>@{{ item.nombre }}</span>
                                                    </label>
                                                </li>
                                            </template>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fil" id="filtroOrdenar">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
                                    Ordenar por
                                </button>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu">
                                    <li>
                                        <a v-on:click="changedOrder('fecha-desc')">Más reciente <i v-if="filtros.order === 'fecha-desc'" class="fa fa-check"></i></a>
                                    </li>
                                    <li>
                                        <a v-on:click="changedOrder('fecha-asc')">Menos reciente <i v-if="filtros.order === 'fecha-asc'" class="fa fa-check"></i></a>
                                    </li>
                                    <li>
                                        <a v-on:click="changedOrder('nombre')">Nombre <i v-if="filtros.order === 'nombre'" class="fa fa-check"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="fil" id="resetFiltros">
                            <button class="btn btn-default waves-effect waves-light" v-on:click="resetFiltros()">
                                <i class="fa fa-refresh"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="ops-right">

                    <template v-if="selectMode && !gallerySelection">
                        <template v-if="width != null && height != null">
                            <button v-if="cantidadSeleccionados === 1" class="btn btn-primary" v-on:click="clickSeleccionarYAjustar()">
                                Seleccionar y ajustar &nbsp;
                                <i class="fa fa-edit"></i>
                            </button>
                        </template>
                        <template v-if="width == null || height == null">
                            <button v-if="cantidadSeleccionados === 1" class="btn btn-primary" v-on:click="clickSelectSeleccionado()">
                                Seleccionar
                                <i class="fa fa-check"></i>
                            </button>
                        </template>
                    </template>

                    <template v-if="gallerySelection">
                        <button v-if="cantidadSeleccionados > 0" class="btn btn-primary" v-on:click="clickAgregarAGaleria()">
                            Agregar a galería
                            <i class="fa fa-plus"></i>
                        </button>
                    </template>

                    <button v-if="cantidadSeleccionados === 1" title="Zoom" class="btn btn-default" v-on:click="clickVerSeleccionado()">
                        <i class="fa fa-search-plus"></i>
                    </button>
                    <button v-if="cantidadSeleccionados === 1" title="Editar" class="btn btn-default" v-on:click="clickEditarSeleccionado()">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button v-if="cantidadSeleccionados > 0" title="Borrar" class="btn btn-default" v-on:click="clickBorrarSeleccionados()">
                        <i class="fa fa-trash"></i>
                    </button>

                    {{--<button v-if="cantidadSeleccionados > 0" class="btn btn-default" v-on:click="clickMover()">
                        Mover (@{{ cantidadSeleccionados }}) &nbsp;
                        <i class="fa fa-folder"></i>
                    </button>--}}

                    <div class="dropdown-cont">
                        {{--<div class="btn-group dropdown">
                            <button v-if="cantidadSeleccionados > 0" class="btn btn-default" v-on:click="clickMover()" aria-expanded="false">
                                Mover--}}{{-- (@{{ cantidadSeleccionados }}) &nbsp;--}}{{--
                                <i class="fa fa-arrow-right"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left" v-bind:class="{ active: moverBtnOpen }">
                                <template v-for="item of folders">
                                    <a href="#" v-on:click="clickMoverOpcion(item)" class="dropdown-item">@{{ item.nombre }}</a>
                                </template>
                                <ul>
                                    <li>
                                        <a href="#" v-on:click="clickMoverOpcion(item)">
                                            @{{ item.nombre }}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>--}}

                        <div class="btn-group"  v-if="cantidadSeleccionados > 0">
                            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
                                Mover{{-- (@{{ cantidadSeleccionados }}) &nbsp;--}}
                                <i class="fa fa-arrow-right"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-left" role="menu" v-bind:class="{ active: moverBtnOpen }">
                                <li>
                                    <a v-on:click="clickMoverOpcion(null)">HOME FOLDER</a>
                                </li>
                                <li v-for="item of folders">
                                    <a v-on:click="clickMoverOpcion(item)">@{{ item.nombre }}</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                    <button class="btn btn-default" v-on:click="clickCrearFolder()" title="Crear nuevo folder">
                        <strong>+</strong>
                        <i class="fa fa-folder"></i>
                    </button>

                    <button class="btn btn-default" v-on:click="clickSubirArchivos()">
                        Subir archivos &nbsp;
                        <i class="fa fa-upload"></i>
                    </button>

                    {{--<div v-if="seleccionadosOpciones.length > 0" class="btn-group">
                        <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
                            Seleccionados
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li v-for="item in seleccionadosOpciones">
                                <a v-on:click="clickOpcion(item)">@{{ item.txt }}</a>
                            </li>
                        </ul>
                    </div>--}}

                    {{--<a href="#" class="heymodal-cerrar" @click.stop><i class="fa fa-remove"></i></a>--}}
                    <a href="#" class="heymodal-cerrar" v-on:click="clickCerrarModal()"><i class="fa fa-remove"></i></a>

                </div>
            </div>

            <div class="heymodal-content">
                <template v-if="folder_actual !== null">
                    <div class="navigation-bar">
                        <div class="nav-bar-right">
                            <a href="#" title="Cambiar nombre folder actual" v-on:click="editarFolderActual()">Cambiar nombre &nbsp; <i class="fa fa-pencil"></i></a>
                            <a href="#" title="Borrar folder actual" v-on:click="clickBorrarFolderActual()">Borrar folder &nbsp; <i class="fa fa-trash"></i></a>
                        </div>

                        <a href="#" title="Ir a home folder" v-on:click="navigateFolder(null)" class="subir-folder">
                            <i class="fa fa-arrow-left"></i>
                            Home folder
                        </a>
                        <span class="actual"><i class="fa fa-folder"></i> @{{ folder_actual_nombre }}</span>
                    </div>
                </template>
                <div v-if="(archivos.length > 0 || folders.length > 0) && !loadingArchivos" class="lista-media" id="lista-media">

                    {{--<draggable v-model="archivos" group="people" @start="drag=true" @end="drag=false">
                        <div v-for="item in archivos" :key="element.id">@{{item.nombre}}</div>
                    </draggable>--}}

                    <div class="elem folder" v-for="item in folders">
                        <div class="cuadro" v-on:click="clickFolder(item)" v-bind:class="{ sel: item.selected }">
                            <div class="img folder"></div>
                            <div class="sel-overlay">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="info">
                                <p class="nombre">@{{ item.nombre }}</p>
                            </div>
                        </div>
                    </div>

                    <div class="elem file" v-for="item in archivos">
                        <div class="cuadro" v-on:click="clickElemento(item)" v-bind:class="{ sel: item.selected }">
                            <div class="img" v-bind:style="{ 'background-image': 'url(' + item.route + ')', 'background-size': (item.mime_type === 'image/png' ? 'contain': 'cover') }">
                            </div>
                            <div class="sel-overlay">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="info">
                                <p class="nombre">@{{ item.nombre }}</p>
                                <p class="tipo">@{{ item.mime_type }}</p>
                            </div>
                        </div>
                    </div>

                </div>
                <div v-if="archivos.length === 0 && folders.length === 0 && !loadingArchivos" class="no-hay">
                    <p>No se encontraron archivos en el media manager. Para agregar nuevos haz click en el botón o arrástralos aquí.</p>
                    <div>
                        <button class="btn btn-primary" v-on:click="clickSubirArchivos()">Subir archivos &nbsp; <i class="fa fa-upload"></i></button>
                    </div>
                </div>
                <div v-if="loadingArchivos" class="loading-archivos">
                    <img src="{{ asset('a/img/ajax-loader-fondo-gris.gif') }}" alt="Cargando...">
                </div>
            </div>

            {{--<div class="heymodal-footer">
                <p>footer</p>
            </div>--}}


        </div>

        <input type="hidden" ref="borrarArchivosUrl" value="{{ route('admin.media.borrar') }}">
        <input type="hidden" ref="listaArchivosUrl" value="{{ route('admin.media.lista') }}">
        <input type="hidden" ref="galeriaPostUrl" value="{{ route('admin.media.galeriaPost') }}">
        <input type="hidden" ref="listaFoldersURL" value="{{ route('admin.media.listaFolders') }}">
        <input type="hidden" ref="crearFolderURL" value="{{ route('admin.media.crearFolder') }}">
        <input type="hidden" ref="borrarFolderURL" value="{{ route('admin.media.borrarFolder') }}">
        <input type="hidden" ref="moverMediaURL" value="{{ route('admin.media.mover') }}">

        <form action="{{ route('admin.media.upload') }}" ref="mediaDropzoneRef" id="mediaDropzone" class="dropzone">
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </form>
        {{--<div id="enviandoArchivosLoading" v-bind:style="{ opacity: 1, visibility: 'visible' }">--}}
        <div id="enviandoArchivosLoading" v-bind:class="{ open: enviandoArchivos }">
            <div class="cuadro-enviando">
                <h2>Enviando...</h2>
            </div>
        </div>
    </div>
</div>
