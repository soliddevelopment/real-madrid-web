@extends('admin.main')
@section('title', 'Dashboard')
@section('content')

<section id="dashboard" class="page">
    <div id="bienvenida">
        <h1>Administrador de contenidos de <strong>{{ config('app.name') }}</strong></h1>
        <p>
            Bienvenido(a) al administrador de contenidos. Aquí podrá realizar distintas acciones para editar información de su <a href="{{ route('publico.home.home') }}" target="_blank">sitio web</a>.
            Por motivos de seguridad, recomendamos cerrar sesión cuando no se esté usando.
        </p>
        <div class="btns">
            <a href="{{ route('publico.home.home') }}" class="btn btn-primary" target="_blank">Abrir sitio web</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <div class="card-box">
                <h4>Mensajes de contacto</h4>
                <p>Puedes revisar los mensajes de contacto que has recibido a través de tu sitio web aquí.</p>
                <div class="links">
                    <ul>
                        <li>
                            <a href="{{ route('admin.config.edit', $configCorreo->id) }}">Cambiar correo donde recibo avisos</a>
                        </li>
                    </ul>
                </div>
                <div class="btns">
                    <a href="{{ route('admin.contactos.index') }}" class="btn btn-primary btn-sm">Ir a Contactos</a>
                </div>
            </div>

            {{--<div class="card-box">
                <h4>Configuración</h4>
                <p>Aquí podrás cambiar algunos parámetros de configuración del sitio</p>
                <div class="btns">
                    <a href="{{ route('admin.config.index') }}" class="btn btn-primary btn-sm">Ir a Configuración</a>
                </div>
            </div>

            <div class="card-box">
                <h4>Galerías de imágenes</h4>
                <p>Puede haber una o más galerías de imágenes en tu sitio, para ver la lista general puedes ingresar aquí.</p>
                <div class="btns">
                    <a href="{{ route('admin.galleries.index') }}" class="btn btn-primary btn-sm">Ir a Galerías</a>
                </div>
            </div>--}}

            <div class="card-box">
                <h4>Media manager</h4>
                <p>El Media Manager es donde se guardan todas las imágenes dinámicas del sitio. Puedes agregar todas las imágenes que quieras y luego vincularlas a otros items.</p>
                <div class="btns">
                    <media-manager-btn
                        v-bind:select-mode="false"
                        custom-class="btn btn-primary btn-sm"
                        title="Media manager"
                        txt='Abrir Media manager'
                    ></media-manager-btn>
                </div>
            </div>

            <div class="card-box">
                <h4>Otros links</h4>
                {{--<p>Cualquier duda que tengas puedes contactarnos a <a href="mailto:hola@solid.com.sv">hola@solid.com.sv</a></p>--}}
                <p>Cualquier duda que tengas puedes contactarnos a <a href="mailto:{{ config('misc.BUSINESS_EMAIL') }}">{{ config('misc.BUSINESS_EMAIL') }}</a></p>
                <div class="btns">
                    <a href="https://analytics.google.com" class="btn btn-primary btn-sm" target="_blank">Ir a Google Analytics</a>
                </div>
            </div>

        </div>
        {{--<div class="col-md-6">
            <div class="card-box">
                <h4>Carros</h4>
                <p>
                    Aquí podrás encontrar los carros para poder editar su información.
                </p>
                <div class="links">
                    <p><strong>PUNTA ROCA</strong></p>
                    <ul>
                        <li><a href="{{ route('admin.custom.accommodations.edit', $puntaroca->id) }}">Editar info </a></li>
                        <li><a href="{{ route('admin.custom.accommodations.packages.index', $puntaroca->id) }}">Editar paquetes </a></li>
                        <li><a href="{{ route('admin.galleries.detail', $puntaroca->galeria->id) }}">Editar galería </a></li>
                    </ul>
                    <p><strong>LAS FLORES</strong></p>
                    <ul>
                        <li><a href="{{ route('admin.custom.accommodations.edit', $lasflores->id) }}">Editar info </a></li>
                        <li><a href="{{ route('admin.custom.accommodations.packages.index', $lasflores->id) }}">Editar paquetes </a></li>
                        <li><a href="{{ route('admin.galleries.detail', $lasflores->galeria->id) }}">Editar galería </a></li>
                    </ul>
                </div>
                <div class="btns">
                    <a href="{{ route('admin.secciones.posts.index', 'carros') }}" class="btn btn-primary btn-sm">Ir a Carros</a>
                </div>
            </div>
        </div>--}}
    </div>
</section>

@endsection
