@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')

<section id="" class="page edit-page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
    </div>
    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box2">
                    <div class="editar-form">
                        @include('admin.partials._messages')
                        @if($elem != null)
                            {!! Form::model($elem, ['route'=>['admin.galleries.update', $elem->id], 'method'=>'PUT', 'files'=>true, 'data-edit'=> 'true' ]) !!}
                                @include('admin.galleries._form')
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['route'=>['admin.galleries.store', null], 'method'=>'post', 'files'=>true ]) !!}
                                @include('admin.galleries._form')
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('view_scripts')
@endsection

