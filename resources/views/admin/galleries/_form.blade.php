
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('titulo', 'Nombre:') }}
            {{ Form::text('titulo', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('slug', 'Slug:') }}
            {{ Form::text('slug', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('resumen', 'Resumen:') }}
            {{ Form::textarea('resumen', null, ['class'=>'form-control', 'placeholder'=>'...', 'rows' => '4']) }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('cover', 'Imagen cover:') }}
            <media-manager-btn
                v-bind:select-mode="true"
                custom-class="btn btn-primary btn-sm" input-name="cover" extensions="jpg,png"
                :preview="true" :img="true" default-img="{{ $elem !== null && $elem->cover !== null ? mediaimg($elem->cover, 100, 100, false) : '' }}"
                title="Seleccionar imagen" txt='Seleccionar imagen'
            ></media-manager-btn>
        </div>
    </div>
</div>

<hr>

<div class="btn-cont">
    {{--@if($post != null)
        {{ Form::hidden('post_id', $post->id) }}
    @endif--}}
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
</div>

