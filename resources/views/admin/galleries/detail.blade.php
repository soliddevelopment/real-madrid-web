@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')

<section id="" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            <media-manager-btn
                v-bind:select-mode="true"
                custom-class="btn btn-primary btn-sm"
                :preview="false" :img="true" extensions="jpg,png"
                :gallery-selection="true" :gallery-id="{{ $gallery->id }}"
                title="Agregar imágenes" txt='Agregar imágenes'
            ></media-manager-btn>
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="lista-imgs" data-sort-url="{{ route('admin.galleries.sort', $gallery->id) }}" data-delete-url="{{ route('admin.galleries.deleteElem', $gallery->id) }}">
                        @foreach($imgs as $item)
                            {{-- <div class="img" style="background-image:url('{{ $item->route }}')"></div>--}}
                            <div id="elem_{{ $item->id }}" class="elem" data-id="{{ $item->id }}">
                                <a href="{{ $item->route }}" class="img fancybox"><img src="{{ mediaimg($item->id, 100, 100) }}"></a>
                                <a href="#" class="borrar"><i class="fa fa-trash"></i></a>
                            </div>
                        @endforeach
                    </div>
                    @if(count($imgs)==0)
                        <div class="no-hay align-center">
                            <p>No se encontraron items</p>
                            <media-manager-btn
                                v-bind:select-mode="true"
                                custom-class="btn btn-primary btn-sm"
                                :preview="false" :img="true" extensions="jpg,png"
                                :gallery-selection="true" :gallery-id="{{ $gallery->id }}"
                                title="Agregar imágenes" txt='Agregar imágenes'
                            ></media-manager-btn>
                        </div>
                    @endif
                </div>
                @if($gallery->post != null)
                    <a href="{{ route('admin.secciones.posts.edit', ['id'=>$gallery->post->id, 'seccionSlug'=>$gallery->post->seccion->slug]) }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> &nbsp; Regresar a {{ $gallery->post->titulo }}</a>
                @endif
            </div>
        </div>
    </div>

</section>

@section('view_scripts')
    <script>
        function borrarElem(id){
            var url = $('.lista-imgs').first().data("delete-url");
            $('#elem_'+id).hide();
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: { img_id: id },
                success: function(data){
                    // console.log('success actualizando orden', data);
                    $('#elem_'+id).remove();
                }
            });
        }

        function actualizarOrdenGaleria(elemToSort, data, url){
            elemToSort.sortable("disable");
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: data,
                success: function(data){
                    // console.log('success actualizando orden', data);
                    elemToSort.sortable("enable");
                }
            });
        }

        $(document).ready(function(){
            var elemToSort = $('.lista-imgs').first();
            var url = elemToSort.data("sort-url");
            elemToSort.sortable({
                update: function (event, ui){
                    var data = elemToSort.sortable('serialize');
                    console.log("Sorted", data);
                    actualizarOrdenGaleria(elemToSort, data, url);
                }
            });

            $('.lista-imgs .elem .borrar').unbind('click').click(function(e){
                e.preventDefault();
                var $elem = $(this).closest('.elem');
                var id = parseInt($elem.data('id'));
                borrarElem(id);
            });
        });
    </script>
@endsection

@endsection
