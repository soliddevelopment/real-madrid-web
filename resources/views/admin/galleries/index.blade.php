@extends('admin.main')
@section('title', $datosView['seccion_titulo'])
@section('content')
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

<section id="" class="page">
    <div class="heading">
        @include('admin.partials._crumbs')
        <h1 class="section-title">{{ $datosView['seccion_titulo'] }}</h1>
        <div class="ops">
            @if($usuario->hasRole('superadmin'))
                <a href="{{ route('admin.galleries.create') }}" class="btn btn-primary">+ {{ $datosView['txt_nuevo'] }}</a>
            @endif
        </div>
    </div>

    <div class="contenido">
        <div class="row">
            <div class="col-md-12">

                <div class="lista-box card-box">

                    @if(count($elementos)>0)

                        <table class="table table1">
                            <thead>
                                <tr>
                                    <th></th>
                                    @if($usuario->hasRole('superadmin'))
                                        <th class="td-id">id</th>
                                    @endif
                                    <th>nombre</th>
                                    <th>slug</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($elementos as $item)
                                    <tr id="elem_{{$item->id}}">
                                        <td class="td-img"><img src="{{ mediaimg($item->cover, 35, 35) }}"></td>
                                        @if($usuario->hasRole('superadmin'))
                                            <td class="td-id">{{ $item->id }}</td>
                                        @endif
                                        <td>{{ $item->titulo }}</td>
                                        <td>{{ $item->slug }}</td>
                                        <td>
                                            <a href="{{ route('admin.galleries.detail', $item->id) }}">Administrar imágenes</a>
                                        </td>
                                        {{--<td>
                                            {{ Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i A') }}
                                        </td>--}}

                                        <td class="td-ops">
                                            <input class="st-switch" data-id="{{ $item->id }}" data-url="{{ route('admin.galleries.st', ['id' => $item->id]) }}" type="checkbox" {{ $item->active == 1 ? 'checked':'' }} data-plugin="switchery" data-color="#00AEEF" data-size="small" />
                                            <a href="{{ route('admin.galleries.edit', [ 'id' => $item->id  ]) }} " class="">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            &nbsp;
                                            {!! Form::open(['route'=>['admin.galleries.destroy', $item->id], 'method'=>'DELETE', 'id'=>'form_'.$item->id, 'class'=>'form-inline' ]) !!}
                                                {{ Form::hidden('id', $item->id) }}
                                            {!! Form::close() !!}

                                            @if($usuario->hasRole('superadmin'))
                                                <a href="#"
                                                   data-method="delete"
                                                   data-title="¡Precaución!"
                                                   data-text="¿Estás seguro que quieres borrar esto?"
                                                   data-type="warning"
                                                   data-id-form="form_{{ $item->id }}"
                                                   class="hey-confirm">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="no-hay">
                            <p>No hay items disponibles</p>
                        </div>
                    @endif

                    {{-- {!! $proyectos->links() !!} --}}
                </div>
            </div>
        </div>
    </div>
</section>

<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
@endsection
