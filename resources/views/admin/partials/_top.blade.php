<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="{{ route('publico.home.home') }}" target="_blank" class="logo" style="background-image:url('{{ mediaimg(configVal($configs, 'admin-logo'), null, null, false) }}')">
            CMS
        </a>
    </div>

    <!-- Navbar -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button class="button-menu-mobile open-left waves-effect">
                        <i class="fa fa-bars"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>
                <ul class="nav navbar-nav navbar-right pull-right">
                    <li class="elem dropdown hidden-xs cuenta-dropdown">
                        <button type="button" class="btn-elem dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">
                            <span>{{ $usuario->usuario ? $usuario->usuario : $usuario->email }}</span> <i class="fa fa-user"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li class="text-center" role="menuitem">
                                <a href="{{ route('admin.auth.logout') }}" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> Cerrar sesión</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>

</div>

<form id="logout-form" action="{{ route('admin.auth.logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>

<!-- Top Bar End -->
