<div id="loadingVue" v-bind:class="{ on: on }">
    <div class="loader">
        <img src="{{ asset('a/img/ajax-loader-fondo-gris.gif') }}">
        <template v-if="msg !== null">
            <p>@{{ msg }}</p>
        </template>
    </div>
</div>
