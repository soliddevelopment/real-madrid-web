<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">

            <ul>

                {{--<li class="menu-title">GENERAL</li>--}}
                <li>
                    <a href="{{ route('admin.home.home') }}" class="waves-effect waves-primary">
                        <i class="fa fa-home"></i><span> Home </span>
                    </a>
                </li>

                @if($usuario->hasRole('superadmin'))
                    <li>
                        <a href="{{ route('admin.logs') }}" class="waves-effect waves-primary" target="_blank">
                            <i class="fa fa-warning"></i><span> Log viewer </span>
                        </a>
                    </li>
                @endif

                @if($usuario->hasRole('superadmin') || $usuario->hasRole('admin'))
                    <li>
                        <a href="{{ route('admin.config.index') }}" class="waves-effect waves-primary {{ $controller=='ConfigController' ? "active" : "" }}">
                            <i class="fa fa-cogs"></i><span> Configuración </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('admin.users.index') }}" class="waves-effect waves-primary {{ $controller=='UsersController' ? "active" : "" }}">
                            <i class="fa fa-user-o"></i><span> Usuarios </span>
                        </a>
                    </li>
                @endif

            </ul>

            <hr>

        </div>

        <div class="clearfix"></div>
    </div>

</div>
<!-- Left Sidebar End -->
