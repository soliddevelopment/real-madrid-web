@if($datosView['crumbs'] !== null && count($datosView['crumbs'])>0)
    <ul class="crumbs">
        @foreach($datosView['crumbs'] as $item)
            <li>
                @if($item['route'] === null)
                    {{ $item['title'] }}
                @else
                    <a href="{{ $item['route'] }}">{{ $item['title'] }}</a>
                @endif
            </li>
        @endforeach
    </ul>
@endif
