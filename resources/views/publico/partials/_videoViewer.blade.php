{{--
<div class="video-viewer">
    <a href="" class="video-viewer-close">
        <i class="fa fa-remove"></i>
    </a>
    <div class="video-viewer-cont">
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/U_6dgaW4Vhk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
--}}

<div id="videoModal" class="video-modal">
    <a href="#" class="cerrar"><i class="fa fa-remove"></i></a>
    <div class="bg"></div>
    <div class="cuadro">
        <div class="cont">
            <div id="player"></div>
        </div>
    </div>
</div>
