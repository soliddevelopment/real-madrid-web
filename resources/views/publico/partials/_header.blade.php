<div id="header">
    <a href="{{ route('publico.home.home') }}" class="logo" data-aos="fade-down">
        <img src="{{ asset('/p/img/logo.png') }}">
    </a>

    <div id="menu" data-aos="fade-down">
        @include('publico.partials._nav')
        {{--@include('publico.partials._redes')--}}
    </div>

    <div id="sidemenu_btn" class="heywow-once">
        <span></span><span></span><span></span><span></span>
    </div>

    <div class="powered">
        <p>powered by</p>
        <img src="{{ asset('/p/img/lin-group-logo.png') }}" alt="LIN GROUP">
    </div>

</div>
