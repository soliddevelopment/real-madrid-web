@if(Session::has('success'))
    <script>
        $(document).ready(function(){
            //$.Notification.notify('success','top center', '{{ Session::get('success') }}');
            toastr.success('{{ Session::get('success') }}');
        });
    </script>
@endif

@if(Session::has('error'))
    <script>
        $(document).ready(function(){
            {{--$.Notification.notify('error','top center', '{{ Session::get('error') }}');--}}
            toastr.error('{{ Session::get('error') }}');
        });
    </script>
@endif
{{--
@if(count($errors) > 0)
    <script>
        $(document).ready(function(){
            var strErrores = '';
            @if(count($errors->all())>0)
                    @foreach($errors->all() as $error)
                strErrores+= '{{ $error }} <br>';
            @endforeach
            @endif
            // $.Notification.notify('error','top center', strErrores);
            toastr['error'](strErrores);
        });
    </script>
    <div class="mensaje mensaje-error" style="display:none;">
        <p>
            <strong>Errors:</strong>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </p>
    </div>

@endif--}}

