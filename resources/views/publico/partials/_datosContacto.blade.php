<div class="datos-contacto" data-aos="flip-left" data-aos-delay="400">
    <div class="elem">
        <div class="icon"><i class="fa fa-phone"></i></div>
        <a href="tel:50322639088">(503) 2263-9088</a>
    </div>
    <div class="elem">
        <div class="icon">
            <i class="fa fa-envelope"></i>
        </div>
        <a href="mailto:comercializacion@laboratoriodb.com">comercializacion@laboratoriodb.com</a>
    </div>
    <div class="elem">
        <div class="icon"><i class="fa fa-map-marker"></i></div>
        <a href="" data-latitude="13.7077672" data-longitude="-89.2400299" class="open-map-viewer">Carretera El Boquéron Km. 16 1/2 San Salvador, El Salvador. <span>Ver mapa</span></a>
    </div>
</div>
