<div id="contactoForm">
    <div class="form">
        {!! Form::open(['route'=>['publico.contacto.nuevo'], 'method'=>'post', 'class'=>'solid-validate', 'novalidate']) !!}
            @if ($errors->any())
                <div class="validation-summary-errors">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group" data-aos="fade-right" data-aos-delay="100">
                <p class="label">Nombre completo *</p>
                {{ Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Nombre completo *', 'required'=>true]) }}
            </div>
            <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                <p class="label">Email *</p>
                {{ Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Email *', 'required'=>true]) }}
            </div>

            {{--<div class="form-group">
                <p class="label">Teléfono</p>
                {{ Form::text('telefono', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
            </div>
            <div class="form-group">
                <p class="label">Empresa</p>
                {{ Form::text('empresa', null, ['class'=>'form-control', 'placeholder'=>'...']) }}
            </div>--}}

            <div class="form-group" data-aos="fade-right" data-aos-delay="300">
                <p class="label">Mensaje</p>
                {{ Form::textarea('mensaje', null, ['class'=>'form-control', 'rows'=>'4', 'placeholder'=>'Mensaje']) }}
            </div>
            <div class="form-group btn-cont align-right" data-aos="fade-right" data-aos-delay="400">
                {{--<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>--}}
                <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="{{ config('misc.GOOGLE_RECAPTCHA_KEY') }}"></div>
                {{ Form::submit('Enviar', ['class' => 'btn', 'id'=>'submitBtn', 'disabled']) }}
            </div>

            {{ Form::hidden('source', ( Request::get('source') && strlen(Request::get('source'))>0 ) ? Request::get('source') : '' ) }}

        {!! Form::close() !!}
    </div>
</div>

<script>
    function recaptchaCallback() {
        $('#submitBtn').removeAttr('disabled');
    };
</script>

{{--

<div id="contactoForm">
    <div class="form">
        <form action="{{ route('publico.contacto.nuevo') }}" method="post" class="solid-validate g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}" novalidate>

            <div class="form-group">
                <p class="label">Nombre completo *</p>
                <input type="text" name="nombre" class="form-control" placeholder="..." required>
            </div>
            <div class="form-group">
                <p class="label">Email *</p>
                <input type="email" name="email" class="form-control" placeholder="..." required>
            </div>
            <div class="form-group">
                <p class="label">Teléfono</p>
                <input type="text" name="telefono" class="form-control" placeholder="..." required>
            </div>
            <div class="form-group">
                <p class="label">Empresa</p>
                <input type="text" name="empresa" class="form-control" placeholder="..." required>
            </div>
            <div class="form-group">
                <p class="label">Mensaje</p>
                <textarea name="mensaje" rows="3" class="form-control" placeholder="..."></textarea>
            </div>
            <div class="form-group btn-cont align-right wow fadeInUp">
                <input type="submit" class="btn btn-color-1" value="Enviar">
            </div>
        </form>
    </div>
</div>
--}}
