<div id="loadingVue" v-bind:class="{ on: on }">
    <div class="loader">
        <img src="{{ asset('p/img/ajax-loader.gif') }}">
        <template v-if="msg !== null">
            <p>@{{ msg }}</p>
        </template>
    </div>
</div>
