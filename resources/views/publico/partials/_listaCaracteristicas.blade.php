<div class="lista-caracteristicas">
    <div class="carac" data-aos="fade-down">
        <img src="{{ asset('/p/img/iconos-caracteristicas/juegos.png') }}" alt="Juegos para niños">
        <p>Juegos para niños</p>
    </div>
    <div class="carac" data-aos="fade-down">
        <img src="{{ asset('/p/img/iconos-caracteristicas/seguridad.png') }}" alt="Seguridad 24/7">
        <p>Seguridad 24/7</p>
    </div>
    <div class="carac" data-aos="fade-down">
        <img src="{{ asset('/p/img/iconos-caracteristicas/parqueo.png') }}" alt="Amplio parqueo">
        <p>Amplio parqueo</p>
    </div>
    <div class="carac" data-aos="fade-down">
        <img src="{{ asset('/p/img/iconos-caracteristicas/10min.png') }}" alt="A 10 min de la ciudad">
        <p>A 10 min de la ciudad</p>
    </div>
    <div class="carac" data-aos="fade-down">
        <img src="{{ asset('/p/img/iconos-caracteristicas/paisajes.png') }}" alt="Hermosos paisajes">
        <p>Hermosos paisajes</p>
    </div>
</div>
