<div class="galeria-slider flechas-negras">
    @foreach($galeria->mediafiles as $img)
        <div class="slide">
            <a href="{{ mediaimg($img->id, 1800) }}" class="fancybox" data-fancybox="images" rel="images">
                <img src="{{ mediaimg($img->id, 350, 350) }}" alt="Laboratorio DB">
            </a>
        </div>
    @endforeach
</div>
