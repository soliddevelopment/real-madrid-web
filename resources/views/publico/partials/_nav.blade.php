<nav class="nav">
    <ul class="ul-nav">
        <li class="{{ setActive(['/']) }} ">
            <a href="{{ route('publico.home.home') }}">Inicio</a>
        </li>
        <li class="{{ setActive(['clinics']) }} ">
            <a  href="{{ route('publico.home.clinics') }}">Clinics</a>
        </li>
        <li class="{{ setActive(['challenge']) }} ">
            <a href="{{ route('publico.home.challenge') }}">Clinic World Challenge</a>
        </li>
        <li class="{{ setActive(['realspirit']) }} ">
            <a href="{{ route('publico.home.realspirit') }}">#RealSpirit</a>
        </li>
        <li class="">
            <a href="https://rmfclinicsworldchallenge.com/" target="_blank">WORLDWIDE</a>
        </li>
        {{--<li>
            <a class="scroll-to close-menu" href="#footer">contacto</a>
        </li>--}}
    </ul>
</nav>
