<div class="map-viewer">
    <div class="map-viewer-overlay"></div>
    <div class="map-viewer-content" v-if="center !== null">
        <a href="#" class="map-viewer-cerrar"><i class="fa fa-remove"></i></a>
        <div id="google-map"></div>
    </div>
</div>