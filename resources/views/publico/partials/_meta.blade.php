<meta property="og:title" content="@yield('title', '')">
<meta property="og:description" content="@yield('description', '')">
<meta property="og:url" content="{{ Request::fullUrl() }}">
<meta property="fb:app_id" content="" />
<meta property="og:type" content="website" />

<meta property="og:image" content="@yield('share_img', asset('/p/img/share.jpg'))">
<meta property="og:image:secure_url" content="@yield('share_img', asset('/p/img/share.jpg'))" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="630" />
<meta property="og:image:alt" content="@yield('title', '')" />
