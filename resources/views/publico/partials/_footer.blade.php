<footer id="footer">
    <div class="cols">
        <div class="col col-redes">
            @include('publico.partials._redes')
            <p>Fundación Real Madrid Costa Rica</p>
            <p>Derechos reservados | {{ date('Y') }}</p>
        </div>
        <div class="col col-info">
            <p class="amarillo">Contacto:</p>
            {{--<p class="blanco">12345-67890</p>--}}
            <p class="amarillo">clinic@frmclinicscostarica.com</p>
        </div>
    </div>
    <div class="sponsors">
        <div class="set main-set">
            <p class="titulo">Main sponsor:</p>
            <div class="lista">
                <img src="{{ asset('/p/img/sponsors/bac.png') }}" alt="BAC">
            </div>
        </div>
        <div class="set">
            <p class="titulo">Off sponsors:</p>
            <div class="lista">
                <img src="{{ asset('/p/img/sponsors/adidas.png') }}" alt="Adidas">
                <img src="{{ asset('/p/img/sponsors/barcelo.png') }}" alt="Barceló">
                <img src="{{ asset('/p/img/sponsors/colegio.png') }}" alt="Colegio Humboldt">
                <img src="{{ asset('/p/img/sponsors/rahso.png') }}" alt="Rahso">
                <img src="{{ asset('/p/img/sponsors/dos-pinos.png') }}" alt="Dos Pinos">
            </div>
        </div>
    </div>
</footer>
