
@if(count($errors) > 0)

    <div class="alert alert-danger">
        {{-- <strong>Errores:</strong> --}}
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>

@endif

@if(!empty($custom_errors))
    @if(count($custom_errors) > 0)

        <div class="alert alert-danger">
            {{-- <strong>Errores:</strong> --}}
            <ul>
                @foreach($custom_errors as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>

    @endif
@endif

