@extends('publico.main')
@section('title', 'Quiero Trabajar | Iniciar sesión')
@section('content')

<div id="login_page" class="page bg-gris-claro">
    <div class="generic-content">
        <div class="form wow fadeInUp" data-wow-delay="0.2s">

            {!! Form::open(['route'=>'publico.auth.loginPost', 'method'=>'post']) !!}

                <div class="form-group">
                    {{ Form::label('email', 'Usuario o email:') }}
                    {{ Form::text('email', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'Contraseña:') }}
                    {{ Form::password('password', ['class' => 'form-control']) }}
                </div>
                <div class="checkbox checkbox-primary">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                    </label>
                </div>

                @if(count($errors) > 0)
                    <div class="form-group errores-login">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @include('publico.partials._formMessages')

                <div class="form-group btn-cont clearfix">
                    <div class="pull-left">
                        <a href="{{ route('password.reset') }}">Olvidé mi contraseña</a>
                    </div>
                    {{ Form::submit('Iniciar sesión', ['class' => 'btn btn-color-1']) }}
                </div>

            {!! Form::close() !!}

            <div class="ops-abajo">
                <a href="{{ route('publico.auth.register') }}">Crear una cuenta</a>
            </div>

        </div>
    </div>
</div>

@endsection
