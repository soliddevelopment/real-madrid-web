@extends('publico.main')
@section('title', 'Quiero Trabajar | Registrarme')
@section('description', '')
@section('content')

<div id="registro_page" class="page bg-gris-claro">
    <div class="generic-content">
        <div id="form" class="form wow fadeInUp" data-wow-delay="0.2s">

            {!! Form::open(['action'=>'Publico\Auth\RegisterController@register', 'method'=>'post']) !!}

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {{ Form::label('tipo', 'Tipo *') }}
                            <select name="tipo" ref="tipoControl" @change="changedTipoControl()" class="form-control">
                                <option value="CANDIDATO" {{ $tipo == 'CANDIDATO' ? 'selected':'' }}>Candidato</option>
                                <option value="EMPRESA" {{ $tipo == 'EMPRESA' ? 'selected':'' }}>Empresa</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            {{ Form::label('email', 'Email *') }}
                            {{ Form::email('email', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="nombre">
                                <template v-cloak>@{{ tipo === 'CANDIDATO' ? 'Nombre *':'Nombre contacto *' }}</template>
                            </label>
                            {{ Form::text('nombre', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="col-sm-6">
                            <label for="apellido">
                                <template v-cloak>@{{ tipo === 'CANDIDATO' ? 'Apellido *':'Apellido contacto *' }}</template>
                            </label>
                            {{ Form::text('apellido', null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            {{ Form::label('password', 'Contraseña *') }}
                            {{ Form::password('password', ['class' => 'form-control']) }}
                        </div>
                        <div class="col-sm-6">
                            {{ Form::label('password_confirmation', 'Confirmar contraseña *') }}
                            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                {{--<template v-if="tipo == 'EMPRESA'">
                    <div class="form-group">
                        {{ Form::label('empresa_nombre', 'Nombre empresa:') }}
                        {{ Form::text('empresa_nombre', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('empresa_nombre', 'Nombre empresa:') }}
                        {{ Form::text('empresa_nombre', null, ['class' => 'form-control']) }}
                    </div>
                </template>--}}

                <div class="checkbox checkbox-primary">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                    </label>
                </div>

                @include('publico.partials._formMessages')

                <div class="form-group btn-cont">
                    {{ Form::submit('Ingresar', ['class' => 'btn btn-color-1']) }}
                </div>

            {!! Form::close() !!}

            <div class="ops-abajo">
                <a href="{{ route('publico.auth.login') }}">Ya tengo una cuenta, iniciar sesión</a>
            </div>

        </div>
    </div>
</div>

@endsection
@section('view_scripts')
    <script type="text/javascript" src="{{ asset('p/js/vue/custom/registro.js') }}"></script>
    <script>
        /*$(document).ready(function(){
            var data = {
                tipoControl: 'EMPRESA'
            };
            $('#form').my({ui:{
                '#tipoControl': 'tipoControl'
            }}, data);

        });*/


    </script>
@endsection
