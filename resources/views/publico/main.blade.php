<!doctype html>
<html>
<head>

    @include('publico.partials._analytics')

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{--<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />--}}
    <meta name="viewport" content= "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="csrf-token" content="{{ csrf_token() }}"> {{-- Esto sirve para ajax calls --}}

    <meta name="description" content="@yield('description', 'Desarrollamos software a la medida, sitios web y apps de calidad que ayudan a nuestros clientes resolver problemas, volverse más eficientes y cumplir sus metas.')">
    <title>@yield('title', 'Solid | Diseño y desarrollo de software, páginas web y apps')</title>

    @include('publico.partials._meta')

    <link rel="shortcut icon" href="{{ asset('p/img/favicon2.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{ asset('p/img/favicon2.ico')}}" type="image/x-icon">

    <link rel="stylesheet" href="{{ mix('p/css/lib/lib.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ mix('p/css/app.css') }}" type="text/css">

    <script type="text/javascript" src="{{ mix('p/js/header.js') }}"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <script>
        // esto se usa para preload de imagenes
        var imgArray = [];
    </script>

    @yield('stylesheets')
{{--    @yield('header_scripts')--}}

    @include('publico.partials._pixel')

</head>

<body>

    @include('publico.partials._loading')
    @include('publico.partials._header')

    <div id="ajax-content">
        @yield('content')
    </div>

    @include('publico.partials._footer')
    @include('publico.partials._mapa')
    @include('publico.partials._videoViewer')

    {{--<div class="whatsapp-btn">
        <a href="https://wa.me/50378617213" target="_blank" class="link">
            <img src="{{ asset('/p/img/lib/whatsapp.png') }}" alt="Whatsapp">
        </a>
    </div>--}}

    {{--@include('publico.partials._vueloader')
    <script type="text/javascript" src="{{ config('app.env') === 'production' ? asset('a/js/vue/vue.min.js') : asset('a/js/vue/vue.js') }}"></script>
    <script type="text/javascript" src="{{mix('p/js/vue/all.js')}}"></script>--}}

    <script type="text/javascript" src="{{ mix('p/js/footer.js') }}"></script>

    @yield('view_scripts')

    @include('publico.partials._notifs')

    @if(env('APP_ENV') != 'local')
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkQja-d-YbsYkkWviaFsw5bHvkfabzdSw&callback=initMapGeneral&" type="text/javascript"></script>
    @endif

</body>
</html>

