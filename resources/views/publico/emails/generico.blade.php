@component('mail::layout')

{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    <div class="header">
        @if($logo != null && strlen($logo) > 0)
            <img style="width:144px;" width="144" src="{{ $logo }}">
        @endif
    </div>
@endcomponent
@endslot

{{--  ---------------------------------------------------------------------------------  --}}
{{-- Body --}}

# {{ $titulo }}
{!! $body !!}

@if($btn_route != null)
@component('mail::button', ['url' => $btn_route ])
{{ $btn_txt }}
@endcomponent
@endif

Gracias,<br>
{{ config('app.name') }}

{{--  ---------------------------------------------------------------------------------  --}}

{{-- Subcopy --}}
@slot('subcopy')
@component('mail::subcopy')
    <!-- subcopy here -->
@endcomponent
@endslot


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    <!-- footer here -->
@endcomponent
@endslot


@endcomponent
