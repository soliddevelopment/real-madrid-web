@extends('publico.main')
@section('title', $meta['title'])
@section('description', $meta['description'])
@section('content')

    <script>
        imgArray = [
            '{{ asset('/p/img/bg-solid-2.jpg') }}',
        ];
    </script>

    <div id="landingPage" class="page">

        <div class="page-intro parallax-window" data-img="{{ asset('/p/img/bg-solid-2.jpg') }}">

            <div class="intro-landing" data-aos="fade-up" data-aos-delay="200">
                <article>
                    <p class="txt2" data-aos="fade-up">Somos una agencia digital ubicada en El Salvador, conformada por un equipo de expertos y nativos digitales. Desarrollamos software a la medida, sitios web y apps que ayudan a nuestros clientes resolver problemas, volverse más eficientes y cumplir sus metas.</p>
                </article>
            </div>

            {{--<h1 class="tit1" data-aos="fade-up">¡Gracias por contactarnos!</h1>
            <p class="bajada" data-aos="fade-up" data-aos-delay="200">Juntos podemos hacer grandes cosas. <br> Nos pondremos en contacto contigo a la brevedad.</p>
            <div class="btn-cont" data-aos="fade-up" data-aos-delay="400">
                <a href="{{ route('publico.home.home') }}" class="btn btn-color-1"><i class="fa fa-home"></i> &nbsp; Volver al Inicio</a>
            </div>--}}
            {{--<div class="ops" data-aos="fade" data-aos-delay="400">
                <a href="{{ route('publico.home.home') }}" title="Ir al inicio"><i class="fa fa-chevron-left"></i> &nbsp; Ir al a Inicio</a>
            </div>--}}
        </div>

        @include('publico.servicios._queHacemos', [ 'disableCTA' => true ])

        <section id="landingForm" class="sec bg-color-dark-1">
            <div class="m">
                <div class="heading">
                    <h1 class="tit1">¿Empezamos a trabajar?</h1>
                    <p class="bajada">Déjanos tus datos y te corresponderemos en seguida.</p>
                </div>
                @include('publico.partials._contactoForm')
            </div>
        </section>

        <section id="trabajosSlider" class="sec">
            <div class="m">
                <div class="heading">
                    <h1 class="tit1">Algunos de nuestros trabajos</h1>
                </div>
                @include('publico.trabajos._slider')
                <div class="sec-btn-cont">
                    <a href="{{ route('publico.trabajos.index') }}" class="btn btn-color-1">Ir a trabajos</a>
                </div>
            </div>
        </section>

    </div>

@endsection

@section('view_scripts')
@endsection

