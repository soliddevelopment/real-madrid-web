@extends('publico.main')
@section('title', $meta['title'])
@section('description', $meta['description'])
@section('content')

<script>
    imgArray = [
        {{--'{{ asset('/p/img/home-slider/slide1-2.png') }}',--}}
    ];
</script>

<div id="graciasPage" class="page">

    <section class="page-intro parallax-window" data-img="{{ asset('/p/img/intro-bgs/3.jpg') }}">
        <div class="overlay"></div>
        <div class="cont">
            <h1 class="title-huge" data-aos="fade-up">¡Gracias!</h1>
            <p class="bajada" data-aos="fade-up" data-aos-delay="400">Gracias por preferirnos. Nos pondremos en contacto en seguida.</p>
            <div class="btn-cont" data-aos="fade-up" data-aos-delay="650">
                <a href="{{ route('publico.home.home') }}" class="btn btn-linea blanco"><i class="fa fa-home"></i> &nbsp; Ir al inicio</a>
            </div>
        </div>
        <div class="ops">
            <a href="{{ route('publico.home.home') }}" title="Ir al inicio"><i class="fa fa-chevron-left"></i> &nbsp; Ir al a Inicio</a>
        </div>
    </section>

</div>

@endsection

@section('view_scripts')

@endsection

