@extends('publico.main')
@section('title', $meta['title'])
@section('description', $meta['description'])
@section('content')

<script>
    imgArray = [
    ];
</script>

<section id="clinicsPage" class="page">

    <div class="m">
        <div class="clinicas">
            <h1>CLINIC DE FÚTBOL DE LA FUNDACIÓN REAL MADRID EN COSTA RICA</h1>
            <h2>¡Entrená como los más grandes bajo la metodología de
                entrenamiento del Real Madrid CF!
            </h2>
            <div class="texto">
                <p>
                    Durante el programa podés disfrutar de sesiones impartidas por entrenadores avalados por la Cantera del Real Madrid C.F,
                    dirigidas a niños, niñas y jóvenes de 8 y 13 años. Los entrenamientos se llevarán a
                    cabo durante 5 días consecutivos, de lunes a viernes, con 3 sesiones diarias que se impartirán por
                    la tarde, iniciando el primer turno a las 16:00 hrs. El horario de cada sesión se asignará una vez se
                    cierre el periodo de inscripción y en función de las edades de los participantes.
                </p>
                <p>
                    ¡Animate! Viví el fútbol como tus estrellas favoritas y sé parte del auténtico #REALSPIRIT . ¡Es tu
                    oportunidad de tener una experiencia Real!
                </p>
                <p>
                    Los entrenadores que dirigirán los Clinics en costa Rica son los mismos que trabajan con las fuerzas básicas del Real Madrid CF en la ciudad deportiva del club, la mundialmente famosa Ciudad
                    Real Madrid. La metodología y filosofía de los entrenamientos están basadas en las utilizadas para
                    preparar a los jugadores del club.
                </p>
                <p>
                    El programa se enfoca en el entrenamiento personalizado de técnica y táctica, solución de problemas y toma de decisiones en el instante, además de promover los valores asociados con Fundación Real Madrid, como: liderazgo, trabajo en equipo, respeto hacia otros, igualdad, solidaridad y
                    esfuerzo. Los entrenamientos se desarrollarán en el Colegio Humboldt (San José, Costa Rica) del
                    27 al 31 de enero de 2020 en la 1ª semana y del 3 al 7 de febrero de 2020 en la 2ª semana.
                </p>
            </div>

            <h1 style="margin-top: 44px !important;">¿QUÉ INCLUYE EL PROGRAMA?</h1>
            <ul>
                <li>Participación en ceremonias de Apertura y Clausura para participantes y acompañantes.</li>
                <li>Entrenamientos con entrenadores y materiales oficiales de Fundación Real Madrid y
                    asistentes certificados durante 1 Programa completo (5 sesiones de lunes a viernes de una
                    duración aproximada de 1 hora y 40 minutos cada una).</li>
                <li>KIT de Bienvenida – Indumentaria Oficial FRM Clinic Costa Rica (camiseta + pantaloneta).</li>
                <li>Diploma de participación en el programa.</li>
                <li>Hidratación + snacks.</li>
                <li>Posibilidad de acceder a la selección para participar en CLINIC WORLD CHALLENGE
                    MADRID 2020.</li>
                <li>Para la Selección, además del criterio técnico-táctico, se tendrá en cuenta el Fair play y educación en valores demostrada a lo largo del clinic. </li>
                <li>Sorpresas adicionales.</li>
            </ul>

            <div class="btn-cont">
                <a href="{{ route('publico.home.challenge') }}" class="btn">Inscripción</a>
            </div>

        </div>
    </div>

</section>

@endsection

@section('view_scripts')
    <script>
        $(document).ready(function(){
            /*$(".fixed-on-scroll").sticky({
                topSpacing: 70
            });*/
        });
    </script>
@endsection

