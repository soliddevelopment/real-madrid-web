@extends('publico.main')
@section('title', $meta['title'])
@section('description', $meta['description'])
@section('content')

<script>
    imgArray = [
    ];
</script>

<section id="realspiritPage" class="page">

    <div class="m">
        <div class="lista-spirit">
            @foreach($fotos as $foto)
                <div class="elem">
                    <img src="{{ mediaimg($foto->imagen, 400) }}" alt="{{ $foto->titulo }}" class="img">
                    <div class="hover-content">
                        <h2>{{ $foto->titulo }}</h2>
                        <p class="foot">#RealSpirit</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

</section>

@endsection

@section('view_scripts')
@endsection

