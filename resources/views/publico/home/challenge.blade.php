@extends('publico.main')
@section('title', $meta['title'])
@section('description', $meta['description'])
@section('content')

<script>
    imgArray = [
    ];
</script>

<section id="challengePage" class="page">

    <div class="m">
        <p class="txt-intro">
            Podés formalizar tu inscripción rellenando el formulario que aparece a continuación.
            Hacé click en el botón inscripción y completá los datos. El precio por participante y
            por semana es de $250 USD impuestos Incluidos.
        </p>
        <p class="date-prisa">
            Date prisa, ¡Hay plazas limitadas!
        </p>

        <div class="formulario-inscripcion">
            <div class="form">
                {!! Form::open(['route'=>['publico.inscripcion'], 'method'=>'post', 'class'=>'solid-validate', 'novalidate']) !!}
                @if ($errors->any())
                    <div class="validation-summary-errors">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h2>Datos niño/a participante</h2>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="100">
                            <p class="label">Nombre *</p>
                            {{ Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="100">
                            <p class="label">Apellido *</p>
                            {{ Form::text('apellido', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                            <p class="label">Email *</p>
                            {{ Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                            <p class="label">Fecha nacimiento *</p>
                            {{ Form::text('fechanacimiento', null, ['class'=>'form-control datepicker', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                            <p class="label">Sexo *</p>
                            <select name="sexo" id="" class="form-control" required>
                                <option value="">Seleccionar...</option>
                                <option value="Mujer">Mujer</option>
                                <option value="Hombre">Hombre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                            <p class="label">Talla de camisa *</p>
                            {{ Form::text('talla', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                </div>

                <div class="form-group" data-aos="fade-right" data-aos-delay="300">
                    <p class="label">Observaciones</p>
                    {{ Form::textarea('observaciones', null, ['class'=>'form-control', 'rows'=>'4', 'placeholder'=>'...']) }}
                </div>

                <div class="form-group" data-aos="fade-right" data-aos-delay="300">
                    <label>
                        <input type="checkbox" name="">
                        <span>Consentimiento de utilización de imagen</span>
                    </label>
                </div>

                <hr>

                <h2>Datos adulto responsable</h2>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="100">
                            <p class="label">Nombre adulto responsable *</p>
                            {{ Form::text('responsable_nombre', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="100">
                            <p class="label">Apellido adulto responsable *</p>
                            {{ Form::text('responsable_apellido', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                            <p class="label">Email *</p>
                            {{ Form::email('responsable_email', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                            <p class="label">Teléfono *</p>
                            {{ Form::text('responsable_telefono', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" data-aos="fade-right" data-aos-delay="200">
                            <p class="label">Cédula *</p>
                            {{ Form::text('responsable_cedula', null, ['class'=>'form-control', 'placeholder'=>'...', 'required'=>true]) }}
                        </div>
                    </div>
                </div>

                <div class="form-group" data-aos="fade-right" data-aos-delay="300">
                    {{ Form::checkbox('terminos', 1, null, ['required' => true]) }}
                    <span>Acepto los <a href="#" target="_blank">términos y condiciones</a></span>
                    <div class="exp-terminos">
                        <p>Seleccionando esta opción declaro que la información aquí proporcionada es verídica y, en caso de ser necesario, comprobare la misma con los documentos que me sean solicitados.</p>
                        <p>Seleccionando esta opción autorizo la grabación de videos e imágenes y la cesión de las mismas a la Fundación Real Madrid (obligatorio)</p>
                    </div>
                </div>

                <div class="form-group btn-cont align-right" data-aos="fade-right" data-aos-delay="400">
                    <div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="{{ config('misc.GOOGLE_RECAPTCHA_KEY') }}"></div>
                    {{--{{ Form::submit('Enviar', ['class' => 'btn', 'id'=>'submitBtn', 'disabled']) }}--}}
                    {{ Form::submit('Enviar', ['class' => 'btn', 'id'=>'submitBtn', 'disabled'=>false]) }}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

</section>

@endsection

@section('view_scripts')
    <script>
        function recaptchaCallback() {
            $('#submitBtn').removeAttr('disabled');
        }
    </script>
@endsection

