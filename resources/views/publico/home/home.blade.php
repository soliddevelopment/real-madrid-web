@extends('publico.main')
@section('title', $meta['title'])
@section('description', $meta['description'])
@section('content')

<script>
    imgArray = [
        '{{ asset('/p/img/logo-white.png') }}',
    ];
</script>

<div id="homePage" class="page">

    <div class="home-fondo">
        <div class="cuadrito">
            <img src="{{ asset('/p/img/logo-blanco.png') }}" alt="RM">
            <div class="mensaje">
                Vive el sueño mundial
            </div>
        </div>

        <div class="cuadro">
            <div class="cont">
                <h1>Clinics Fundación Real Madrid Costa Rica</h1>
                <h3>¡Inscribite ya, hay plazas limitadas!</h3>
                <div class="botones">
                    <a href="{{ route('publico.home.clinics') }}" class="btn">Más información</a>
                    <a href="{{ route('publico.home.challenge') }}" class="btn">Inscripción</a>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('view_scripts')

@endsection

