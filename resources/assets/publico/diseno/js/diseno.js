$(function(){

	$('.descargar-link').click(function(e){
		e.preventDefault();
		var os = getMobileOperatingSystem();
		var url;
		if(os === 'ios'){
			url = 'https://www.apple.com/itunes/';
		} else {
			url = 'https://play.google.com/store?hl=en';
		}
		window.open(url, '_blank');
	});

	function getMobileOperatingSystem() {
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;
		if (/windows phone/i.test(userAgent)) {
			return "windows";
		}
		if (/android/i.test(userAgent)) {
			return "android";
		}
		if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
			return "ios";
		}
		return "unknown";
	}
	
	//set active class to current menu
	var mnActual = "mn-inicio";
	if($("[data-active-menu]").length){
		mnActual = $("[data-active-menu]").first().data("active-menu");
	}
	$("."+mnActual).addClass("active");
	
	//setear clase al header si hay un data-header-class
	if($("[data-header-class]").length){
		var hdrClass = $("[data-header-class]").first().data("header-class");
		$("#hdr").addClass(hdrClass);
	}

	//Fixed menu al hacer scroll
	if($("#hdr").length){
		var theMenu = $('#hdr'),
			scrollClass = 'fixed-hdr',
			//activateAtY = $("#hdr").outerHeight() - theMenu.outerHeight();
			activateAtY = 1;

		$(window).on("load scroll resize",function() {
			if($(window).scrollTop() > activateAtY) {
				theMenu.addClass(scrollClass);
			} else {
				theMenu.removeClass(scrollClass);
			}
		});
	}
	
	$(".toggle-menu").click(function(e){
		e.preventDefault();
		$("body").toggleClass("mostrar-menu-ppal");
	});
	
	if($(".slider-about-textos .slider").length){
		$(".slider-about-textos .slider").slick({
			vertical: true,
			dots: false,
			arrows: false,
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			touchMove: false,
			swipe: false,
			asNavFor: ".slider-about-images .slider",
			responsive: [
				{
					breakpoint: 992,
					settings: {
						vertical: false,
						adaptiveHeight: true,
						touchMove: true,
						swipe: true,
					}
				},
			]
		});
	}
	
	if($(".slider-about-images .slider").length){
		$(".slider-about-images .slider").slick({
			dots: false,
			arrows: true,
			infinite: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			prevArrow: $("#btnAboutPrev"),
			nextArrow: $("#btnAboutNext"),
			asNavFor: ".slider-about-textos .slider"
		});
	}
	
	if($(".slider-servicios .slider").length){
		$(".slider-servicios .slider").slick({
			dots: false,
			arrows: false,
			infinite: true,
			autoplay: true,
			autoplaSpeed: 2000,
			slidesToShow: 1,
			slidesToScroll: 1,
			//centerMode: true,
			variableWidth: true,
			touchMove: false,
			swipe: false,
			pauseOnFocus: false,
			pauseOnHover: false,
		});
	}
	
	if($("textarea.autosize").length){
		$("textarea.autosize").textareaAutoSize();
	}
	
	$('select.nice-select').niceSelect();
	
});

$(window).on("load", function () {
	//Inicializar el masonry de servicios del home
    if ($(".servicios-home").length) {
		var $grid = $('.servicios-home');
		$grid.on( 'layoutComplete', function() {
			$(".servicios-home").addClass("completado");
			$(".box-service-home").addClass("animated");
		}).masonry({
            horizontalOrder: true,
			percentPosition: true
        });
    }
})




/*function scrollToElement(element){
	var position = element.offset().top - $("#menu").outerHeight();
	$(window).scrollTop(position);
}*/