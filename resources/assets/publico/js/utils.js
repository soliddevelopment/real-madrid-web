
// Utilidades Solid ////////////////////////////////////////////////////////////////////////////////
var Solid = {

    imgArray: [],
    medidas: {
        w: null,
        h: null
    },

    init: function(ops){

        var opciones = $.extend({

            // PRELOADER Y LOADING DE IMAGENES
            loading: true,
            loadingTimeout: 0,

            // HEADER Y MENU
            headerChivo: true,
            headerChivoScrollOffset: 0,
            menuAnimado: true,
            menuAnimadoSide: 'right', // right, left

            // mierdas varias
            fancybox: true,
            parallax: true,

            solidValidate: true,
            solidValidateAuto: true,

            mapaLinks: true,

            videoViewer: true,

            ajaxNav: true,
            ajaxNavCallback: function(){},

        }, ops);

        // PRIMERO VAN LAS COSAS QUE SE SETEAN SOLO UNA VEZ
        Solid.setOnce(opciones);

        // ACA VAN LAS COSAS QUE SE SETEAN SIEMPRE QUE CARGA UNA PAGINA
        Solid.setTodo(opciones);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////



    }, // fin de Init //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // LAS COSAS QUE SE SETEAN SOLO UNA VEZ
    setOnce: function(opciones){

        // Se setea evento que va a etsar registarndo las medidas de la pagina
        $(window).resize(function(){
            Solid.setMedidas();
        });

        // Seteo el loading inicial
        function loadWebsiteInit(timeout, ops){
            if(timeout > 0){
                setTimeout(function(){
                    Solid.loadWebsite(ops);
                }, timeout);
            } else {
                Solid.loadWebsite(ops);
            }
        }
        if(opciones.loading){
            if( typeof imgArray !== 'undefined' && imgArray instanceof Array && imgArray.length>0 ){
                Solid.preload(imgArray, function(){
                    loadWebsiteInit(opciones.loadingTimeout, opciones);
                });
            } else { loadWebsiteInit(opciones.loadingTimeout, opciones); }
        }

        if(opciones.headerChivo){
            Solid.setHeaderChivo(opciones.headerChivoScrollOffset);
        }

        Solid.setMedidas();

        // Menú tipo mobile animado (siempre mobile hasta en web)
        if(opciones.menuAnimado){
            Solid.setMenuAnimado(opciones.menuAnimadoSide);
        }

        // AJAX NAV
        if(opciones.ajaxNav){
            /*$("a.ajax-link").ajaxnav({
                claseLink: "ajax-link",
                idContainer: "ajax-content",
                delay: 0,
                beforeLoad: function(){ // siempre antes de cambiar page
                    Solid.showLoading();
                },
                success: function(){ // each page change
                    Solid.setTodo(opciones);
                    setGeneral();

                    Solid.preload(imgArray, function(){
                        $("html, body").scrollTop(0);
                        // Solid.HideLoading();
                        Solid.loadWebsite(opciones);
                    });
                }
            });*/
            // Esto esta por gusto?
            /*History = window.History;
            History.Adapter.bind(window,'statechange',function(){
                var State = History.getState();
                //History.log(State.data, State.title, State.url);
                //console.log(State.data, State.title, State.url);
                console.log("State.data", State.data);
            });*/
        }

    },

    // LAS COSAS QUE SE SETEAN SIEMPRE QUE CARGA UNA PAGINA
    setTodo: function(opciones){

        // FANCYBOX PARA IMAGENES DINAMICAS
        if(opciones.fancybox){
            $(".fancybox").fancybox({
                'type' : 'image'
            });
        }

        // VIDEO VIEWER
        if(opciones.videoViewer){

            // ASYNC IMPORT DE YOUTUBE API
            window.YT || function(){
                var a=document.createElement("script");
                a.setAttribute("type","text/javascript");
                a.setAttribute("src","http://www.youtube.com/player_api");
                a.onload=function(){};
                a.onreadystatechange=function(){
                    if (this.readyState=="complete"||this.readyState=="loaded") doYT()
                };
                (document.getElementsByTagName("head")[0]||document.documentElement).appendChild(a)
            }();

            // SETEO EL CERRAR
            $('.video-modal .cerrar, .video-modal .bg').unbind('click').click(function(e){
                // cerrarVideoModals();
                $('#videoModal').fadeOut(444, function(){
                    if(videoViewerPlayer !== undefined && typeof(videoViewerPlayer.stopVideo)==='function'){
                        videoViewerPlayer.stopVideo();
                        $('#videoModal iframe').remove();
                        $('#videoModal .cuadro .cont').append($('<div id="player"></div>'));
                    } else {
                        $('#videoModal iframe').remove();
                    }
                });
            });

            $('.abrir-video-modal').unbind('click').click(function(e){
                e.preventDefault();
                //var id = $(this).data('id');
                var youtubeUrl = $(this).data('video-url');
                var youtubeId = Solid.getYoutubeId(youtubeUrl);
                // abrirVideoModal(id);
                Solid.openVideoModal(youtubeId);
            });
        }

        // Agregar clase .scroll-to a link para aimate scroll a elemento con x id
        $(".scroll-to").click(function(e){
            e.preventDefault();
            var ms = $(this).data("ms") || null;
            var selector = $(this).data("href") || $(this).attr("href") || null;
            var easing = $(this).data("easing") || null;
            var offset = $(this).data("offset") || null;
            if($(this).hasClass('close-menu')){
                Solid.closeMenu();
            }
            Solid.scrollTo(selector, ms, easing, offset);
        });
        //<a class="scroll-to" data-ms="1100" data-easing="easeOutCirc" href="#contacto">CONTACTO</a>

        if(Solid.isTouch()){
            $("body").addClass("hey-touch");
            $(".hvr-grow").removeClass("hvr-grow");
            $(".hvr-float").removeClass("hvr-float");
        } else {
            $("body").addClass("hey-no-touch");
        }

        if(opciones.solidValidate){
            $("form.solid-validate").each(function(){
                var $form = $(this);
                $form.unbind("validate").validate({
                    /*submitHandler: function (form) {
                        // console.log("form submit", form);
                        // Solid.submitFormSolidValidate(form, opciones);
                        var response = grecaptcha.getResponse();
                        if(response.length === 0){ //reCaptcha not verified
                            toastr.error('¡Te faltó validar que no eres un robot!');
                        } else {
                            //reCaptch verified
                            form.submit();
                        }
                    }*/
                });
            });
        }

        if(opciones.parallax){
            // $('.parallax').parallax();
            $('.parallax-window').each(function(){
                var imgSrc = $(this).data('img');
                $(this).parallax({imageSrc: imgSrc});
            });
        }

        if(opciones.ajaxNav){

        }

        // A un link que abre mapa ponerle data-latitude y data-longitude y clase css .open-map-viewer
        if(opciones.mapaLinks){
            $('.open-map-viewer').click(function(e){
                e.preventDefault();
                var lat = parseFloat($(this).data('latitude'));
                var lng = parseFloat($(this).data('longitude'));
                var locs = [{ lat: lat, lng: lng }];
                Solid.initGoogleMap('google-map', locs);
                $('.map-viewer').addClass('open');
            });
            $('.map-viewer .map-viewer-cerrar, .map-viewer .map-viewer-overlay').click(function(e){
                e.preventDefault();
                $('.map-viewer').removeClass('open');
            });
        }

        // tabs
        $('.solid-tabs').each(function(){
            $tabs = $(this);
            $tabs.find('.solid-tabs-btn').unbind('click').click(function(e){
                e.preventDefault();
                var id = $(this).data('id');
                $(this).addClass('active').siblings().removeClass('active');
                $('#'+id).addClass('active').siblings().removeClass('active');
            });
        });

    },

    openVideoModal: function(youtubeId){
        console.log('opening video', youtubeId);
        $('#videoModal').fadeIn(300, function(){
            videoViewerPlayer = new YT.Player('player', {
                height: '1205',
                width: '678',
                videoId: youtubeId,
                playerVars: { rel: 0, showinfo: 0, ecver: 2 },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });

            // 4. The API will call this function when the video player is ready.
            function onPlayerReady(event) {
                event.target.playVideo();
            }

            var done = false;
            function onPlayerStateChange(event) {
                /*if (event.data == YT.PlayerState.PLAYING && !done) {
                    setTimeout(stopVideo, 6000);
                    done = true;
                }*/
            }
            function stopVideo() {
                videoViewerPlayer.stopVideo();
            }

        });
    },

    // OBTIENE MEDIDAS DE LA VENTANA Y LAS GUARDA
    setMedidas: function(){
        this.medidas.W = $(window).width();
        this.medidas.H = $(window).height();
    },

    showHiddenEmail: function(selector, username, dominio, dom){
        var r = 'mailto:' + username + '@' + dominio + dom;
        if($(selector).length) $(selector).attr('href',r).html(username + '@' + dominio + dom);
    },

    preload: function(imgArray, callback_final, callback_siempre){
        // console.log('preloading', imgArray);
        if(imgArray && typeof imgArray !== 'undefined'){
            if(imgArray && imgArray.length>0){
                var loader = new PxLoader();
                for(i in imgArray){
                    var pxImage = new PxLoaderImage(imgArray[i]); pxImage.imageNumber = i + 1; loader.add(pxImage);
                }
                loader.addProgressListener(function(e) { // callback that runs every time an image loads
                    if(typeof callback_siempre === "function") callback_siempre();
                    //$log.val($log.val() + 'Image ' + e.resource.imageNumber + ' Loaded\r'); // log which image completed
                    //$progress.text(e.completedCount + ' / ' + e.totalCount); // the event provides stats on the number of completed items
                });
                loader.addCompletionListener(function() {
                    if(typeof callback_final === "function") callback_final();
                });
                loader.start();
            } else {
                if(typeof callback_final === "function") callback_final();
            }
        } else {
            if(typeof callback_final === "function") callback_final();
        }
    },

    loadWebsite: function(opciones){
        AOS.init();
        Solid.hideLoading();
    },

    showLoading: function(){
        $("#loading").fadeIn(144, function(){
            //$(this).remove();
            // $("#loading").addClass("ajax");
        });
    },

    hideLoading: function(){
        $("#loading").fadeOut(144, function(){
            //$(this).remove();
            $("#loading").addClass("ajax");
        });
    },

    // SETEA CLASES CSS DE HEADER QUE SE COMPACTA
    setHeaderChivo: function(headerChivoScrollOffset){
        if(headerChivoScrollOffset === undefined || headerChivoScrollOffset === null){
            var headerChivoScrollOffset = 0;
        }
        function scrollCheck(headerChivoScrollOffset){
            if($(document).scrollTop() > headerChivoScrollOffset){
                $("#header").addClass("compact");
                $("#menu").addClass("compact");
                $("#sidemenu_btn").addClass("compact");
                $(".page").addClass("compact");
            } else {
                $("#header").removeClass("compact");
                $("#menu").removeClass("compact");
                $("#sidemenu_btn").removeClass("compact");
                $(".page").removeClass("compact");
            }
        }
        scrollCheck(headerChivoScrollOffset);
        $(window).scroll(function(){
            scrollCheck(headerChivoScrollOffset);
        });
    },

    // SETEA MENU ANIMADO
    setMenuAnimado: function(menuAnimadoSide){
        $(".menu-btn").unbind("click").click(function(e){
            e.preventDefault();
            if(Solid.menuOpened){
                Solid.closeMenu();
            } else {
                Solid.openMenu();
            }
        });

        $("#menu-overlay").unbind("click").click(function(e){
            Solid.closeMenu();
        });

        $('#sidemenu_btn, #menu').addClass(menuAnimadoSide);
        $('#sidemenu_btn, #sidemenu_overlay').unbind("click").click(function(e){
            e.preventDefault();
            toggleOpen();
        });
        /*$("#menu li a").not('.void').click(function(){
            toggleOpen();
            var li = $(this).closest("li");
            $('#menu li').removeClass('active');
            li.addClass("active");
        });*/
        $('#sidemenu .void').click(function(e){
            e.preventDefault();
        });
        function toggleOpen(){
            $("#sidemenu_overlay").toggleClass("open");
            $("#sidemenu_btn").toggleClass("open");
            $("#menu").toggleClass("open");
        }
    },

    openMenu: function(){
        $("#menu, #menu-overlay").addClass("opened");
        $("#menu, #menu-overlay").addClass("open");
        $('body').css({ 'overflow-y': 'hidden' });
        Solid.menuOpened = true;
    },

    closeMenu: function(){
        $("#menu, #menu-overlay").removeClass("opened");
        $("#menu, #menu-overlay").removeClass("open");
        $('body').css({ 'overflow-y': 'auto' });
        Solid.menuOpened = false;
    },

    // HACER UN SCROLL ANIMADO A UN ELEMENTO DEL DOM
    scrollTo: function(selector, ms, easing, offset, callback){
        if(selector.length > 0){
            if(offset===null) var offset = 20;
            if(ms===null) var ms = 2000;
            if(easing===null) var easing = "easeOutQuart";

            if($(selector).length) {
                var scrolltop = $(selector).offset().top;
                if (scrolltop !== undefined) scrolltop += offset;

                $('html, body').animate({
                    //width: [ "toggle", "swing" ],
                    //height: [ "toggle", "swing" ]
                    //scrollTop: [scrolltop, "easeOutBounce"]
                    scrollTop: scrolltop
                }, ms, easing, function () {
                    if (typeof (callback) === "function") {
                        callback();
                    }
                });
            }
        }
    },

    isTouch: function () {
        if(Browser.browser==="Firefox"){
            if(Modernizr.touchevents===true){
                //console.log("Dispositivo touch (Firefox)");
                return true;
            } else {
                //console.log("Dispositivo no touch (Firefox)");
                return false;
            }

        } else {
            if(Modernizr.touch===true){
                //console.log("Dispositivo touch");
                return true;
            } else {
                //console.log("Dispositivo no touch");
                return false;
            }
        }
    },

    validateEmail: function(email){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    initGoogleMap: function(mapaId, markers, zoom, styles){
        console.log('iniciando mapa', mapaId, markers, zoom);
        if(markers.constructor !== Array){
            markers = [markers];
        }
        if(!zoom || zoom === undefined || zoom === null){
            zoom = 16;
        }

        if(styles && styles != undefined && styles != null){

        } else {
            styles = [];
        }

        var first = markers[0];
        var uluru = { lat: first.lat, lng: first.lng };

        var map = new google.maps.Map(document.getElementById(mapaId), {
            zoom: zoom,
            center: uluru,
            styles: styles
        });

        var bounds = new google.maps.LatLngBounds();

        for(var v in markers){
            var actual = markers[v];
            var ubic = { lat: actual.lat, lng: actual.lng };
            var marker = new google.maps.Marker({
                position: ubic,
                map: map,
                title: actual.title
            });

            if(markers.length > 1){
                bounds.extend(ubic);
                map.fitBounds(bounds);
            }
        }

        if(markers.length === 1){
            map.setZoom(zoom);
            map.panTo(uluru);
        }
    },

    getYoutubeId: function(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    },

};

// **************************************************************************************************************************

// PARA DETERMINAR BROWSER ////////////////////////////////////////////////////////////////////////////////

var Browser = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;
            if (dataString.indexOf(data[i].subString) !== -1) return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) return;
        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },
    dataBrowser: [
        {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
        {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
        {string: navigator.userAgent, subString: "Safari", identity: "Safari"},
        {string: navigator.userAgent, subString: "Opera", identity: "Opera"}
    ]
};

var videoViewerPlayer;

// **************************************************************************************************************************

// Mierdas custom de JQUERY

$.fn.random = function() {
    return this.eq(Math.floor(Math.random() * this.length));
};

