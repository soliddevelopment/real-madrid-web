const editarPerfilForm = new Vue({
    el: '#form',
    data: {
        tipo: 'CANDIDATO',
        members: [],
        mostrarCamposCheckbox: false,
    },
    methods: {
        setLlenarCamposLegales(){
            this.mostrarCamposCheckbox = this.$refs.mostrarCamposLegales.checked;
        },
        changedMostrarCamposLegales(){
            this.setLlenarCamposLegales();
        }
    },
    mounted(){
        const _self = this;
        console.log('editarPerfil mounted');
        _self.setLlenarCamposLegales();

        /*_self.$vueEventBus.$on('open_media_manager', function(resp){
            console.log('EVENTO open_media_manager ESCUCHADO', resp);
        });*/

        /*_self.$vueEventBus.$emit('loading', {
            msg: 'Enviando...',
            on: true
        });*/

    }
});
