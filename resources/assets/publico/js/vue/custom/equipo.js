import axios from 'axios';

const equipoContent = new Vue({
    el: '#nuestroEquipo',
    data: {
        members: [],
        departamentos: [],
        loading: true,
        departamento: 0,
        loadingDepartamentos: true,
        empresa: true,
        openedMembers: false
    },
    methods: {

        clickOpcion(op){
            console.log('Se hizo click en una opcion', op);
        },

        openModal(){
            jQuery('#mediaManagerModal').fadeIn(200);
        },

        closeModal(){
            jQuery('#mediaManagerModal').fadeOut(100);
        },

        clickDepartamento(id){
            this.departamento = id;
            this.openedMembers = false;
            this.fetchMembers();
        },

        clickVerMas(){
            if(this.openedMembers){
                this.openedMembers = false;
            } else {
                this.openedMembers = true;
            }
        },

        fetchMembers(){
            let _self = this;
            let url = this.$refs.membersUrl.value;
            _self.loading = true;
            _self.members = [];

            /*if(_self.departamento > 0){
                url += '?departamento='+_self.departamento;
            }*/

            axios.get(url, {
                params: {
                    departamento: _self.departamento
                }
            }).then(function(response){
                _self.members = [];
                for(let item of response.data){
                    _self.members.push(item);
                }
                console.log('members', _self.members);
                _self.loading = false;
            });

        },

        fetchDepartamentos(){
            let _self = this;
            if(_self.$refs.departamentosUrl !== undefined){
                let url = this.$refs.departamentosUrl.value;
                _self.loadingDepartamentos = true;
                axios.get(url, {
                    params: {
                        // departamento: _self.departamento
                    }
                }).then(function(response){
                    console.log('RESPONSE FETCH', response);
                    _self.departamentos = response.data;
                    _self.loadingDepartamentos = false;
                });
            }
        },

    },
    mounted(){
        const _self = this;
        _self.fetchDepartamentos();
        _self.fetchMembers();

        /*_self.$vueEventBus.$on('open_media_manager', function(resp){
            console.log('EVENTO open_media_manager ESCUCHADO', resp);
        });*/

        /*_self.$vueEventBus.$on('image_edited', function(resp){
            console.log('EVENTO image_edited ESCUCHADO', resp);
            _self.reloadArchivos();
        });*/

        /*_self.$vueEventBus.$emit('loading', {
            msg: 'Enviando...',
            on: true
        });*/

    }
});

