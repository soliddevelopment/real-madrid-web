
const solicitarFormVue = new Vue({
    el: '#solicitarFormVue',
    data: {
        valid: false,
        dirtyForm: false,
        enviando: false,
        empleados: [],
        empleadosFiltrados: [],
        form: {
            empresa_id: 0,
            empleado_id: 0,
            celular: null,
            cedula: null,
            terminos: 0,
            prestamo: null
        },
        errores: []
    },
    methods: {
        changedEmpresa(){
            this.empleadosFiltrados = [];
            this.form.empleado_id = 0;
            for (const item of this.empleados){
                if(item.empresa_id === this.form.empresa_id){
                    this.empleadosFiltrados.push(item);
                }
            }
        },

        getEmpleadoSeleccionado(){
            let selec = null;
            if(this.form.empleado_id){
                for (const item of this.empleados){
                    if(item.id === this.form.empleado_id){
                        selec = item; break;
                    }
                }
            }
            return selec;
        },

        changedEmpleado(){

        },

        clickSubmit(){
            if(this.validate()){
                this.submit();
            } else {

            }
        },

        cleanCedula(cedula){
            cedula = cedula.toString().replace(/[\W_]+/g,'');
            cedula = cedula.toUpperCase();
            return cedula;
        },

        validate(){
            this.errores = [];
            this.dirtyForm = true;

            if(!this.form.empresa_id || this.form.empresa_id === 0) {
                this.errores.push('Debe elegir la empresa.');
            }
            if(!this.form.empleado_id || this.form.empleado_id === 0) {
                this.errores.push('Debe seleccionar su nombre.');
            }
            if(!this.form.cedula) {
                this.errores.push('Debe ingresar su cédula.');
            } else {
                const empleadoSeleccionado = this.getEmpleadoSeleccionado();
                console.log('cedulas', this.cleanCedula(this.form.cedula), this.cleanCedula(empleadoSeleccionado.cedula));
                if(this.cleanCedula(this.form.cedula) !== this.cleanCedula(empleadoSeleccionado.cedula)){
                    this.errores.push('La cédula ingresada no coincide con el nombre seleccionado.');
                }
            }
            if(!this.form.celular) {
                this.errores.push('Debe ingresar su celular.');
            }
            if(!this.form.prestamo) {
                this.errores.push('Debe seleccionar el préstamo que desea.');
            }
            if(!this.form.terminos) {
                this.errores.push('Debe aceptar los términos y condiciones.');
            }

            if(this.errores.length === 0){
                this.valid = true;
            } else {
                this.valid = false;
            }

            return this.valid;

        },

        submit(){
            const _self = this;
            _self.enviando = true;
            if(_self.valid) {
                const url = _self.$refs.formElem.action;
                const data = _self.form;
                axios.post(url, data).then(function (response) {
                    console.log('SUBMIT SUCCESS', response);
                    _self.enviando = false;
                    _self.resetForm();
                    vex.dialog.alert('¡Gracias! Nos pondremos en contacto con usted pronto.');
                }).catch((err) => {
                    console.log('SUBMIT ERROR', err);
                    let msg = 'Ocurrió un error';
                    switch(err.error_code){
                        case 'VAL': msg = 'Datos enviados incorrectos'; break;
                        case 'CED': msg = 'Cédula incorrecta'; break;
                        case 'LIMIT': msg = 'Lo sentimos, usted ya tiene una solicitud pendiente.'; break;
                    }
                    vex.dialog.alert(msg);
                });
            }
        },

        clickReset(){
            this.resetForm();
        },

        resetForm(){
            this.valid = false;
            this.dirtyForm = false;
            this.enviando = false;
        },

        fetchEmpleados(){
            const _self = this;
            const url = this.$refs.urlGetEmpleados.value;
            const data = {};
            axios.get(url, data).then(function(response){
                // console.log('GET empleados', response);
                _self.empleados = response.data;
            });
        }

    },
    mounted(){
        const _self = this;
        _self.fetchEmpleados();

    }
});
