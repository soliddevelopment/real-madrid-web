// import axios from 'axios';

const editarDatosForm = new Vue({
    el: '#form',
    data: {
        tipo: 'CANDIDATO',
        members: [],

    },
    methods: {
        setTipo(){
            const _self = this;
            const tipoSelected = _self.$refs.tipoControl.value;
            if(tipoSelected === 'CANDIDATO' || tipoSelected === 'EMPRESA'){
                _self.tipo = tipoSelected;
            } else {
                _self.tipo = 'CANDIDATO';
            }
        },
        changedTipoControl(){
            this.setTipo();
        }
    },
    mounted(){
        const _self = this;
        console.log('registroForm mounted');
        _self.setTipo();

        /*_self.$vueEventBus.$on('open_media_manager', function(resp){
            console.log('EVENTO open_media_manager ESCUCHADO', resp);
        });*/

        /*_self.$vueEventBus.$emit('loading', {
            msg: 'Enviando...',
            on: true
        });*/

    }
});

