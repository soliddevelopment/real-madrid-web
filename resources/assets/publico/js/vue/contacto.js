import axios from 'axios';

const contactoFormVue = new Vue({
    el: '#contactoFormVue',
    data: {
        valid: false,
        dirtyForm: false,
        enviando: false,
        form: {
            nombre: null,
            email: null,
            empresa: null,
            pais_iso: 'CR',
            solucion: null,
            mensaje: null,
            telefono: null,
        },
        errores: []
    },

    methods: {

        clickSubmit(){
            if(this.validate()){
                this.submit();
            } else {

            }
        },

        validate(){
            this.errores = [];
            this.dirtyForm = true;

            if(!this.form.nombre) {
                this.errores.push('Debe escribir su nombre.');
            }
            if(!this.form.email) {
                this.errores.push('Debe escribir su email.');
            }
            if(!this.form.mensaje) {
                this.errores.push('Por favor describa su consulta brevemente.');
            }
            /*if(!this.form.solucion || this.form.solucion === 0) {
                this.errores.push('Debe seleccionar el interés de la consulta.');
            }*/

            if(this.errores.length === 0){
                this.valid = true;
            } else {
                this.valid = false;
            }

            return this.valid;

        },

        submit(){
            const _self = this;
            _self.enviando = true;
            if(_self.valid) {
                const url = _self.$refs.formElem.action;
                const data = _self.form;
                axios.post(url, data).then(function (response) {
                    console.log('SUBMIT SUCCESS', response);
                    _self.enviando = false;
                    _self.resetForm();
                    vex.dialog.alert('¡Gracias! Nos pondremos en contacto con usted pronto.');
                }).catch((err) => {
                    console.log('SUBMIT ERROR', err);
                    let msg = 'Ocurrió un error';
                    switch(err.error_code){
                        case 'VALIDATOR': msg = 'Datos enviados incorrectos'; break;
                        default: msg = 'Lo sentimos, ocurrió un error'; break;
                    }
                    vex.dialog.alert(msg);
                });
            }
        },

        clickReset(){
            this.resetForm();
        },

        resetForm(){
            this.valid = false;
            this.dirtyForm = false;
            this.enviando = false;
            this.form = {
                nombre: null,
                email: null,
                empresa: null,
                pais_iso: 'CR',
                solucion: null,
                mensaje: null,
                telefono: null
            };
        },

    },
    mounted(){
        const _self = this;
    }
});
