// **********************************************************************************************************************************
$(document).ready(function(){
    setGeneral();
    setGeneralOnce();
});

$(window).scroll(function(){
    // windowScrolled();
});
// **********************************************************************************************************************************
// COSAS QUE SOLO DEBEN LLAMARSE UNA VEZ AUNQUE SE CAMBIE CON AJAX NAV
function setGeneralOnce(){

    setSolidUtilities();

    // $("body").overlayScrollbars({ });

}
function setSolidUtilities(){
    Solid.init({
        loading: true,
        loadingTimeout: 0,
        ajaxNav: false,
        ajaxNavCallback: function(){
            initMap();
        },
        menuAnimado: false,
        // menuAnimadoSide: 'right',
        setHeaderChivo: true,
        pdfViewer: false,

        // Validate y eso
        solidValidate: true,
        solidValidateAuto: false, // TRUE si quiero que se manejen el hideloading y showloading form solos, si no especificar aca abajo:
        showLoadingForm: function(form){
            var $form = $(form);
            $form.find('*[type=submit]').attr("disabled", "disabled").addClass("loading").val("Sending..");
        },
        hideLoadingForm: function(form){
            var $form = $(form);
            $form.find('*[type=submit]').removeAttr("disabled").removeClass("loading").val("Send");
        },
        resetForm: function(form){
            var $form = $(form);
            $form.find('input[type=text], input[type=email], input[type=tel], select, textarea').val('');
        }
    });
    Browser.init();
}
// **********************************************************************************************************************************
// COSAS QUE DEBEN LLAMARSE CADA VEZ QUE HAY AJAX PAGE CHANGE
function setGeneral(){

    // Llamar funciones custom aca
    // Solid.showHiddenEmail('.hidden-email-1', 'atencion', 'asdf', '.net');

}

// **********************************************************************************************************************************
// FUNCIONES CUSTOM
