
$(document).ready(function(){
    setSolid();
    setMisc();
    setSliders();
});

function setSolid(){
    Solid.init({
        // PRELOADER Y LOADING DE IMAGENES
        loading: true,
        loadingTimeout: 0,

        // HEADER Y MENU
        headerChivo: true,
        headerChivoScrollOffset: 0,
        menuAnimado: true,
        menuAnimadoSide: 'right', // right, left
        ajaxNav: false,
        videoViewer: true,
    });
    Browser.init();
}

// **********************************************************************************************************************************

function initMapGeneral(){
    /*var styles = [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.business",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dadada"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c9c9c9"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        }
    ];
    var locs = [{ lat: 13.715963, lng: -89.278027 }];
    Solid.initGoogleMap('footer-map', locs, 14, styles);*/
}

function setMisc(){

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        //startDate: '-3d'
    });

    /*Solid.showHiddenEmail('.hidden-email-1', 'hola', 'solid', '.com.sv');

    var d = '.com.sv';
    var c = 'solid';
    var a = 'hola';
    var b = '@';
    var r = 'mailto:' + a + b + c + d;

    $('#linkContactoEmailFooter').attr('href',r).html('<i class="fa fa-envelope" /><span>'+a + b + c + d+'</span>');*/
    // $('#linkContactoTelFooter').attr('href',r).html('<i class="fa fa-envelope" /><span>'+a + b + c + d+'</span>');

    //$('.servicios-nav').niceScroll();

    /*$('.close-menu').click(function(){
        Solid.closeMenu();
    });*/

}

function setSliders(){

    $(".home-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        autoplay: true,
        pauseOnHover: false,
        speed: 1150,
        fade: true,
        autoplaySpeed: 5000,
    });

    $(".prod-imgs-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true,
        fade: true,
        pauseOnHover: true,
        speed: 1150,
        autoplaySpeed: 5500,
    });

    $(".productos-slider").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        autoplay: true,
        pauseOnHover: false,
        speed: 444,
        infinite: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

}
