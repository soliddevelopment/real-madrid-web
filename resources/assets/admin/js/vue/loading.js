
const loadingVue = new Vue({
    el: '#loadingVue',
    data: {
        on: false,
        msg: null,
        selectMode: false,
    },
    methods: {

    },
    mounted(){
        const _self = this;
        _self.$vueEventBus.$on('loading', function(resp){
            _self.on = resp.on;
            if(resp.msg !== null && resp.msg !== undefined && resp.msg.length > 0){
                _self.msg = resp.msg;
            }
        });

        /*_self.$vueEventBus.$on('image_edited', function(resp){
            console.log('EVENTO image_edited ESCUCHADO', resp);
            _self.reloadArchivos();
        });*/

    }
});
