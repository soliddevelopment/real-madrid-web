import axios from 'axios';
//import draggable from 'vuedraggable';

const mediaManagerVue = new Vue({
    el: '#mediaManagerVue',
    data: {
        componentId: null,
        selectMode: false,
        title: 'Media manager',
        loadingArchivos: true,
        archivos: [],
        cancelArchivos: null,
        enviandoArchivos: false,
        cantidadSeleccionados: 0,
        dropzone: null,
        //extensions: [],
        mimetypes: [
            { val: 'image/jpeg', nombre: 'jpg/jpeg', slug: 'jpg', checked: true, visible: true },
            { val: 'image/png', nombre: 'png', slug: 'png', checked: true, visible: true },
            { val: 'image/gif', nombre: 'gif', slug: 'gif', checked: true, visible: true },
            { val: 'text/html', nombre: 'html', slug: 'html', checked: true, visible: true },
            { val: 'application/msword', nombre: 'doc (word)', slug: 'doc', checked: true, visible: true },
            { val: 'application/pdf', nombre: 'pdf', slug: 'pdf', checked: true, visible: true },
            { val: 'application/vnd.ms-powerpoint', nombre: 'ppt (powerpoint)', slug: 'ppt', checked: true, visible: true },
            { val: 'application/zip', nombre: 'zip', slug: 'zip', checked: true, visible: true },
            { val: 'application/x-rar-compressed', nombre: 'rar', slug: 'rar', checked: true, visible: true }
        ],
        filtros: {
            q: null,
            mimetypes: [],
            order: 'fecha-desc' // fecha-desc, fecha-asc, nombre
        },
        width: null,
        height: null,
        gallerySelection: false,
        galleryId: null,

        moverBtnOpen: false,
        folders: [],
        folder_actual: null,
        folder_actual_nombre: 'HOME FOLDER',
    },
    methods: {

        clickOpcion(op){
            console.log('Se hizo click en una opcion', op);
        },

        clickElemento(elemento){
            // console.log('seleccionar archivo', elemento);
            for(const item of this.archivos){
                if(item.id === elemento.id){
                    if(item.selected){
                        item.selected = false;
                        this.cantidadSeleccionados--;
                    } else {
                        item.selected = true;
                        this.cantidadSeleccionados++;
                    }
                    break;
                }
            }
        },

        clickFolder(item){
            //alert('click folder ' + item.nombre);
            this.navigateFolder(item);
        },

        navigateFolder(item){
            if(item === null){
                this.folder_actual = null;
                this.folder_actual_nombre = 'HOME FOLDER';
            } else {
                this.folder_actual = item.id;
                for(let item of this.folders){
                    if(item.id === this.folder_actual){
                        this.folder_actual_nombre = item.nombre;
                        break;
                    }
                }
            }
            this.reloadArchivos();
        },

        clickSubirArchivos(){
            $('#mediaDropzone').click();
        },

        clickCerrarModal(){
            this.cantidadSeleccionados = 0;
            this.archivos = [];
            this.dropzone.destroy();
            this.closeModal();
        },

        getSeleccionado(){
            let seleccionado;
            for(const item of this.archivos){
                if(item.selected){
                    seleccionado = item;
                    break;
                }
            }
            return seleccionado;
        },

        getFileById(id){
            let seleccionado;
            for(const item of this.archivos){
                if(item.id === id){
                    seleccionado = item;
                    break;
                }
            }
            return seleccionado;
        },

        getArchivosForFancybox(){
            const _self = this;
            let archivosArr = [];
            const seleccionado = _self.getSeleccionado();
            for(let item of _self.archivos){
                if(item.id !== seleccionado.id){
                    archivosArr.push({
                        title: item.nombre,
                        href: item.route
                    });
                }
            }
            archivosArr.unshift({
                title: seleccionado.nombre,
                href: seleccionado.route
            });
            return archivosArr;
        },

        changedFormatos(){
            this.reloadArchivos();
        },
        changedBuscar(){
            this.reloadArchivos();
        },
        changedOrder(str){
            this.filtros.order = str;
            this.reloadArchivos();
        },

        resetFiltros(){
            this.filtros = {
                q: null,
                mimetypes: [],
                order: 'fecha-desc' // fecha-desc, fecha-asc, nombre
            };
            this.reloadArchivos();
            this.deselectAll();
        },

        clickVerSeleccionado(){
            const _self = this;
            let archivosFancybox = _self.getArchivosForFancybox();
            console.log('archivosFancybox', archivosFancybox);
            $.fancybox.open(archivosFancybox, {
                padding : 0
            });
            return false;
        },

        clickEditarSeleccionado(){
            const _self = this;
            const seleccionado = _self.getSeleccionado();
            _self.$vueEventBus.$emit('open_image_cropper', {
                img: seleccionado,
                w: null, h: null
            });
        },

        clickSeleccionarYAjustar(){
            const _self = this;
            const seleccionado = _self.getSeleccionado();
            _self.$vueEventBus.$emit('open_image_cropper', {
                img: seleccionado,
                w: _self.width, h: _self.height
            });
        },

        clickAgregarAGaleria(){
            const _self = this;
            let seleccionadosStr = '';
            let x = 0;
            for(const item of this.archivos){
                if(item.selected){
                    if(x > 0) seleccionadosStr += ',';
                    seleccionadosStr += item.id;
                    x++;
                }
            }
            _self.confirmarSeleccionGaleria(seleccionadosStr);
        },

        confirmarSeleccionGaleria(seleccionadosStr){
            const _self = this;
            const url = this.$refs.galeriaPostUrl.value;
            _self.$vueEventBus.$emit('loading', {
                msg: 'Enviando...',
                on: true
            });
            axios.post(url, { 'gallery_str': seleccionadosStr, 'gallery_id': _self.galleryId }).then(function(response){
                console.log('success confirmarSeleccionGaleria', response);
                if(response.data.success){
                    window.location.reload();
                } else {
                    _self.$vueEventBus.$emit('loading', {
                        msg: null, on: false
                    });
                }
            }).catch((err)=>{
                _self.$vueEventBus.$emit('loading', {
                    msg: null, on: false
                });
            });
        },

        clickSelectSeleccionado(){
            const _self = this;
            const seleccionado = _self.getSeleccionado();
            _self.confirmarSeleccion(seleccionado);
        },

        confirmarSeleccion(seleccionado){
            const _self = this;
            document.querySelector('#mediaManagerSelectInput_'+_self.componentId).value = seleccionado.id;
            const defaultImg = document.querySelector('#mediaManagerDefaultImg_' + _self.componentId);
            if(defaultImg !== null) defaultImg.style.display = 'none';
            const previewImg = document.querySelector('#mediaManagerPreviewImg_' + _self.componentId);
            if(previewImg !== null) {
                previewImg.style.backgroundImage = 'url("'+seleccionado.route+'")';
                // jQuery().css({ 'background-image' });
                previewImg.style.display = 'block';
            }
            // TODO: aca tengo que hacer la parte pendiente de los files genericos para media manager

            _self.closeModal();
        },

        clickBorrarSeleccionados(){
            let seleccionadosStr = '';
            let x = 0;
            for(const item of this.archivos){
                if(item.selected){
                    if(x > 0) seleccionadosStr += ',';
                    seleccionadosStr += item.id;
                    x++;
                }
            }
            this.borrarArchivos(seleccionadosStr);
        },

        clickMover(){
            if(this.moverBtnOpen){
                this.moverBtnOpen = false;
            } else {
                this.moverBtnOpen = true;
            }
        },

        clickMoverOpcion(folder){
            const _self = this;
            const url = this.$refs.moverMediaURL.value;
            _self.$vueEventBus.$emit('loading', {
                msg: 'Enviando...',
                on: true
            });

            let seleccionadosStr = '';
            let x = 0;
            for(const item of this.archivos){
                if(item.selected){
                    if(x > 0) seleccionadosStr += ',';
                    seleccionadosStr += item.id;
                    x++;
                }
            }

            let folder_id = null;
            if(folder){
                folder_id = folder.id;
            }

            return new Promise(function(resolve, reject){
                axios.post(url, { archivos: seleccionadosStr, folder_id: folder_id }).then(function(response){
                    console.log('RESPONSE clickMoverOpcion', response);
                    _self.$vueEventBus.$emit('loading', {
                        msg: null, on: false
                    });
                    if(response.data.success){
                        _self.reloadArchivos();
                        resolve(response);
                    } else {
                        reject(response);
                    }
                }).catch((err)=>{
                    _self.$vueEventBus.$emit('loading', {
                        msg: null, on: false
                    });
                });
            });
        },

        clickCrearFolder(){
            const _self = this;
            swal("Escribir nombre del nuevo folder:", {
                content: "input",
            }).then((value) => {
                if(value.length > 0) {
                    _self.crearFolder(value).then((resp) => {
                        _self.reloadArchivos();
                    });
                }
            });
        },

        editarFolderActual(){
            const _self = this;
            swal("Escribir nuevo nombre del folder:", {
                content: "input",
            }).then((value) => {
                if(value.length > 0) {
                    _self.crearFolder(value, _self.folder_actual).then((resp) => {
                        _self.navigateFolder(null);
                        _self.reloadArchivos();
                    });
                }
            });
        },

        clickBorrarFolderActual(){
            const _self = this;
            swal({
                title: 'Un momento...',
                text: '¿Estás seguro que quieres borrar el folder '+_self.folder_actual_nombre+'?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#09c78e",
                confirmButtonText: "aceptar",
                cancelButtonText: "cancelar",
                closeOnConfirm: true
            })
            .then((willDelete) => {
                if (willDelete) {
                    _self.confirmarBorrarFolderActual();
                } else {
                    //swal("Your imaginary file is safe!");
                }
            });
        },

        confirmarBorrarFolderActual(){
            const _self = this;
            const url = this.$refs.borrarFolderURL.value;
            return new Promise(function(resolve, reject){
                axios.post(url, { id: _self.folder_actual }).then(function(response){
                    console.log('RESPONSE borrarFolder', response);
                    if(response.data.success){
                        _self.navigateFolder(null);
                        _self.reloadArchivos();
                        resolve(response);
                    } else {
                        reject(response);
                    }
                });
            });
        },

        crearFolder(nombreFolder, editId = null){
            const _self = this;
            const url = this.$refs.crearFolderURL.value;
            return new Promise(function(resolve, reject){
                axios.post(url, { nombre: nombreFolder, folder_actual: _self.folder_actual, id: editId }).then(function(response){
                    console.log('RESPONSE crearFolder', response);
                    if(response.data.success){
                        resolve(response);
                    } else {
                        reject(response);
                    }
                });
            });
        },

        borrarArchivos(str){
            console.log('borrarArchivos', str);
            const _self = this;
            swal({
                title: 'Un momento...',
                text: '¿Estás seguro que quieres borrar esto?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#09c78e",
                confirmButtonText: "aceptar",
                cancelButtonText: "cancelar",
                closeOnConfirm: true
            })
            .then((willDelete) => {
                if (willDelete) {
                    _self.confirmarBorrarArchivos(str);
                } else {
                    //swal("Your imaginary file is safe!");
                }
            });
        },

        confirmarBorrarArchivos(str){
            const _self = this;
            const url = this.$refs.borrarArchivosUrl.value;
            axios.post(url, { archivos: str }).then(function(response){
                console.log('RESPONSE BORRAR ARCHIVOS', response);
                if(response.data.success){
                    $.Notification.notify('success','top center', 'Los archivos fueron borrados exitosamente');
                    //_self.reloadArchivos();
                    _self.removerBorrados(str);
                    _self.deselectAll();
                }
            });
        },

        deselectAll(){
            this.cantidadSeleccionados = 0;
            for(const item of this.archivos){
                item.selected = false;
            }
        },

        removerBorrados(str){
            const _self = this;
            const strArray = str.split(',');
            console.log('borrados strArray', strArray);
            let archivos = [];
            for(const item of _self.archivos){
                if(strArray.indexOf(item.id+'') > -1){} else {
                    archivos.push(item);
                }
            }
            _self.archivos = archivos;
        },

        reloadArchivos(){
            this.fetchFolders();
            this.fetchArchivos();
        },

        openModal(){
            jQuery('#mediaManagerModal').fadeIn(200);
            jQuery('body, html').css({
                'overflow-y': 'hidden'
            });
        },

        closeModal(){
            jQuery('#mediaManagerModal').fadeOut(100);
            jQuery('body, html').css({
                'overflow-y': 'auto'
            });
        },

        fetchFolders(){
            let _self = this;
            return new Promise(function(resolve, reject){
                const url = _self.$refs.listaFoldersURL.value;

                axios.get(url, {
                    params: {
                        folder_actual: _self.folder_actual
                    }
                }).then(function(response){
                    console.log('RESPONSE FETCH FOLDERS', response);
                    _self.folders = response.data;
                    resolve(response);
                });

            });
        },

        fetchArchivos(){
            let _self = this;

            return new Promise(function(resolve, reject){
                const url = _self.$refs.listaArchivosUrl.value;
                _self.loadingArchivos = true;
                _self.cantidadSeleccionados = 0;
                let mimetypesStr = '';

                let x=0; for(let item of _self.filtros.mimetypes){
                    x++;
                    mimetypesStr += item;
                    if(x < _self.filtros.mimetypes.length) mimetypesStr += ',';
                }

                if(_self.cancelArchivos !== null){
                    _self.cancelArchivos.cancel('canceladaa!!');
                }

                let CancelToken = axios.CancelToken;
                _self.cancelArchivos = CancelToken.source();

                axios.get(url, {
                    params: {
                        mimetypes: mimetypesStr,
                        q: _self.filtros.q,
                        order: _self.filtros.order,
                        items: 5000,
                        folder_actual: _self.folder_actual
                    },
                    cancelToken: _self.cancelArchivos.token

                }).then(function(response){
                    console.log('RESPONSE FETCH ARCHIVOS', response);
                    _self.loadingArchivos = false;
                    _self.archivos = response.data.data;
                    //jQuery('#lista-media').draggable();
                    resolve(response);
                });

            });
        },

        setDropzone(){
            const _self = this;
            const url = this.$refs.mediaDropzoneRef.getAttribute('action');

            if(_self.dropzone !== null) _self.dropzone.destroy();
            _self.dropzone = new Dropzone("#mediaDropzone", {
                url: url,
                // acceptedFiles: ".png, .jpg, .jpeg",
                maxFilesize: 3000,
                method: "post",
                addRemoveLinks: true,
                parallelUploads: 20,
                autoProcessQueue: true,
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            _self.dropzone.on("addedfile", function(file, message){
                console.log("addedfile", file);
            });

            _self.dropzone.on("totaluploadprogress", function(progress){
                console.log("totaluploadprogress", progress);
                // updateProgressBar(progress, galeriaProgress);
            });

            _self.dropzone.on("queuecomplete", function(resp){
                console.log("queuecomplete", resp);
                _self.enviandoArchivos = false;
                // _self.dropzone.removeAllFiles();
                // updateProgressBar(0, galeriaProgress);d
                // Se termino de subir todo
                // resetDropzone(myDropzone, galeriaProgress, cont);
            });

            _self.dropzone.on("complete", function(resp){
                console.log("complete", resp);
                // _self.enviandoArchivos = false;
                _self.dropzone.removeAllFiles();
            });

            _self.dropzone.on("sending", function(file, xhr, formData){
                console.log("sending", file);
                formData.append('folder_id', _self.folder_actual);
                _self.enviandoArchivos = true;
                // formData.append("id", id);
            });

            _self.dropzone.on("success", function(file, response){
                console.log("success response", response);
                if(response.success){
                    _self.archivos.unshift(response.file);
                } else {
                    $.Notification.notify('error','top center', 'No se pudo subir un archivo.');
                }

            });

            _self.dropzone.on("error", function(error){
                console.log("Dropzone upload error", error);
                $.Notification.notify('error','top center', 'No se pudo subir un archivo.');

            });

        },

        setExtensionesPermitidas(extensions){
            const _self = this;
            if(extensions){
                let extensionsArray = extensions.split(',');
                console.log('extensionsArray', extensionsArray);
                for(const item of _self.mimetypes){
                    if(extensionsArray.includes(item.slug)){
                        item.visible = true;
                    } else {
                        item.visible = false;
                    }
                }
            } else {
                for(const item of _self.mimetypes){
                    item.visible = true;
                }
            }
        },
    },

    mounted(){
        const _self = this;
        _self.$vueEventBus.$on('open_media_manager', function(resp){
            console.log('EVENTO open_media_manager ESCUCHADO', resp);

            _self.componentId = resp.componentId;
            if(resp.selectMode === true || resp.selectMode === false){
                _self.selectMode = resp.selectMode;
            } else {
                _self.selectMode = false;
            }

            if(resp.gallerySelection === true || resp.gallerySelection === false){
                _self.gallerySelection = resp.gallerySelection;
                if(resp.galleryId !== null && resp.galleryId !== undefined){
                    _self.galleryId = resp.galleryId;
                }
            } else {
                _self.gallerySelection = false;
            }

            if(resp.title && resp.title.length > 0){
                _self.title = resp.title;
            } else {
                if(_self.selectMode){
                    _self.title = 'Seleccionar un archivo';
                } else {
                    _self.title = 'Media manager';
                }
            }

            if(resp.width && resp.width !== null && resp.width > 0
            && resp.height && resp.height !== null && resp.height > 0){
                _self.width = resp.width;
                _self.height = resp.height;
            }

            _self.setExtensionesPermitidas(resp.extensions);

            for(let item of _self.mimetypes){
                if(item.checked && item.visible){
                    _self.filtros.mimetypes.push(item.val);
                }
            }

            _self.reloadArchivos();
            _self.setDropzone();
            _self.openModal();
        });

        _self.$vueEventBus.$on('image_edited', function(resp){
            console.log('EVENTO image_edited ESCUCHADO', resp);
            _self.reloadArchivos().then(function(respReload){
                if(resp.seleccionarAuto){
                    let seleccionado = _self.getFileById(resp.file.id);
                    _self.confirmarSeleccion(seleccionado);
                }
            });
        });

        /*_self.$vueEventBus.$emit('loading', {
            msg: 'Enviando...',
            on: true
        });*/

    }
});


var dropZone = document.getElementById('mediaDropzone');
function showDropZone() {
    dropZone.style.visibility = "visible";
    dropZone.style.opacity = '1';
}
function hideDropZone() {
    dropZone.style.visibility = "hidden";
    dropZone.style.opacity = '0';
}
function allowDrag(e) {
    if (true) {  // Test that the item being dragged is a valid one
        e.dataTransfer.dropEffect = 'copy';
        e.preventDefault();
    }
}

function handleDrop(e) {
    e.preventDefault();
    hideDropZone();
    // alert('Drop!');
}
window.addEventListener('dragenter', function(e) {
    showDropZone();
});
dropZone.addEventListener('dragenter', allowDrag);
dropZone.addEventListener('dragover', allowDrag);
dropZone.addEventListener('dragleave', function(e) {
    hideDropZone();
});
dropZone.addEventListener('drop', handleDrop);





/*

var dropzone = new Dropzone('#demo-upload', {
    previewTemplate: document.querySelector('#preview-template').innerHTML,
    parallelUploads: 2,
    thumbnailHeight: 120,
    thumbnailWidth: 120,
    maxFilesize: 3,
    filesizeBase: 1000,
    thumbnail: function(file, dataUrl) {
        if (file.previewElement) {
            file.previewElement.classList.remove("dz-file-preview");
            var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
            for (var i = 0; i < images.length; i++) {
                var thumbnailElement = images[i];
                thumbnailElement.alt = file.name;
                thumbnailElement.src = dataUrl;
            }
            setTimeout(function() { file.previewElement.classList.add("dz-image-preview"); }, 1);
        }
    }

});


// Now fake the file upload, since GitHub does not handle file uploads
// and returns a 404

var minSteps = 6,
    maxSteps = 60,
    timeBetweenSteps = 100,
    bytesPerStep = 100000;

dropzone.uploadFiles = function(files) {
    var self = this;

    for (var i = 0; i < files.length; i++) {

        var file = files[i];
        totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

        for (var step = 0; step < totalSteps; step++) {
            var duration = timeBetweenSteps * (step + 1);
            setTimeout(function(file, totalSteps, step) {
                return function() {
                    file.upload = {
                        progress: 100 * (step + 1) / totalSteps,
                        total: file.size,
                        bytesSent: (step + 1) * file.size / totalSteps
                    };

                    self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
                    if (file.upload.progress == 100) {
                        file.status = Dropzone.SUCCESS;
                        self.emit("success", file, 'success', null);
                        self.emit("complete", file);
                        self.processQueue();
                        //document.getElementsByClassName("dz-success-mark").style.opacity = "1";
                    }
                };
            }(file, totalSteps, step), duration);
        }
    }
}
*/
