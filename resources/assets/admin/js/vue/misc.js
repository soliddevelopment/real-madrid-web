//
// import Vue from 'vue';
// import VueCropper from 'vue-cropperjs';
// Vue.component(VueCropper);

Vue.component('media-manager-btn', {
    template: `
        <div :class="{ preview: preview, 'media-manager-btn': true }" :id="'mediaManagerBtn_'+componentId">
            <template v-if="preview">
                <!--<img v-if="defaultImg === null" :src="defaultImg">                -->
                <!--<img v-if="preview && defaultImg !== null" :id="'mediaManagerPreviewImg_'+componentId" :src="defaultImg" class="preview-img">                -->
                
                <template v-if="img">
                     <img v-on:click="clickOpen()" :id="'mediaManagerDefaultImg_'+componentId" v-if="defaultImg !== null && defaultImg.length > 0" :src="defaultImg" class="preview-img default-img">
                     <div v-on:click="clickOpen()" :id="'mediaManagerPreviewImg_'+componentId" class="preview-img selected-img"></div>
                </template>
                <template v-if="!img">
                     <p>Aqui ira preview de archivo generico</p>
                </template>
                <!--<div v-if="defaultImg !== null && defaultImg !== undefined && defaultImg.length > 0" class="preview-img"
                    :id="'mediaManagerPreviewImg_'+componentId" 
                    :src="defaultImg" >
                </div>       -->
                
            </template>
            <a v-on:click="clickOpen()" :class="[ customClass, 'the-btn' ]" v-html="txt"></a>
            <input v-if="inputName !== null && inputName.length > 0" :id="'mediaManagerSelectInput_'+componentId" ref="mediaManagerSelectInput" :name="inputName" type="hidden" value="">
            <div class="clearfix"></div>
            <!--<input v-if="gallerySelection && galleryId !== null" :id="'mediaManagerGalleryInput_'+componentId" ref="mediaManagerGalleryInput" type="hidden" value="">-->
        </div>
    `,
    props: {
        title: {
            type: String,
            default: 'Media Manager'
        },
        txt: {
            type: String,
            default: 'Media Manager'
        },
        defaultImg: {
            type: String,
            default: null
        },
        inputName: {
            type: String,
            default: null
        },
        selectMode: {
            type: Boolean,
            default: false
        },
        img: {
            type: Boolean,
            default: false
        },
        preview: {
            type: Boolean,
            default: false
        },
        gallerySelection: {
            type: Boolean,
            default: false
        },
        galleryId: {
            default: null
        },
        customClass: {
            type: String,
            default: ''
        },
        width: null,
        height: null,

        extensions:{
            type: String,
            default: null
        },

    },

    data(){
        return {
            componentId: null,
            changedImg: false,
            /*mimetypes: [
                { val: 'image/jpeg', nombre: 'jpg/jpeg', slug: 'jpg' },
                { val: 'image/png', nombre: 'png', slug: 'png' },
                { val: 'image/gif', nombre: 'gif', slug: 'gif' },
                { val: 'text/html', nombre: 'html', slug: 'html' },
                { val: 'application/msword', nombre: 'doc (word)', slug: 'doc' },
                { val: 'application/pdf', nombre: 'pdf', slug: 'pdf' },
                { val: 'application/vnd.ms-powerpoint', nombre: 'ppt (powerpoint)', slug: 'ppt' },
                { val: 'application/zip', nombre: 'zip', slug: 'zip' },
                { val: 'application/x-rar-compressed', nombre: 'rar', slug: 'rar' }
            ],*/
        };
    },
    methods: {
        clickOpen: function () {
            const _self = this;
            _self.$vueEventBus.$emit('open_media_manager', {
                selectMode: _self.selectMode,
                title: _self.title,
                componentId: _self.componentId,
                width: _self.width,
                height: _self.height,
                gallerySelection: _self.gallerySelection,
                galleryId: _self.galleryId,
                extensions: _self.extensions
            });
        }
    },
    model: {
        event: 'open'
    },
    mounted() {
        this.componentId = this._uid
    }
});
