import axios from 'axios';

const nuestroEquipo = new Vue({
    el: '#nuestroEquipo',
    data: {
        members: []
    },
    methods: {

        clickOpcion(op){
            console.log('Se hizo click en una opcion', op);
        },

        openModal(){
            jQuery('#mediaManagerModal').fadeIn(200);
        },

        closeModal(){
            jQuery('#mediaManagerModal').fadeOut(100);
        },

        fetchArchivos(){
            let _self = this;
            const url = this.$refs.listaArchivosUrl.value;

            axios.get(url, {
                params: {
                    q: _self.filtros.q,
                    order: _self.filtros.order,
                    items: 5000
                }
            }).then(function(response){
                console.log('RESPONSE FETCH ARCHIVOS', response);
                _self.loadingArchivos = false;
                _self.archivos = response.data.data;
            });

        },

    },
    mounted(){
        const _self = this;
        /*_self.$vueEventBus.$on('open_media_manager', function(resp){
            console.log('EVENTO open_media_manager ESCUCHADO', resp);
        });*/

        /*_self.$vueEventBus.$on('image_edited', function(resp){
            console.log('EVENTO image_edited ESCUCHADO', resp);
            _self.reloadArchivos();
        });*/

        /*_self.$vueEventBus.$emit('loading', {
            msg: 'Enviando...',
            on: true
        });*/

    }
});

