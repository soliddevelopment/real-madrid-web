import VueCropper from 'vue-cropperjs';

let imageCropperVue = new Vue({
    el: '#imageCropperVue',
    components: { VueCropper },
    data() {
        return {
            title: '',
            cropper: null,
            w: null,
            h: null,
            img: null,
            imgSrc: null
        }
    },
    computed: {

    },
    methods: {
        cropImage(ev){
            // console.log('cropImage', ev);
        },
        gcd(a, b) {
            return (b === 0) ? a : this.gcd (b, a%b);
        },
        initCropper(resp){
            const _self = this;
            if(resp.w !== null && resp.w > 0) _self.w = resp.w;
            if(resp.h !== null && resp.h > 0) _self.h = resp.h;

            if(resp.img !== null) {
                _self.img = resp.img;
                _self.imgSrc = _self.img.route;
                if(_self.$refs.cropper !== undefined) {
                    _self.$refs.cropper.replace(_self.imgSrc);
                }
            }

        },

        getAspectRatio(){
            const _self = this;
            let aspectRatio = null;
            if(!isNaN(_self.w) && !isNaN(_self.h) && _self.w > 0 && _self.h > 0){
                const r = _self.gcd(_self.w, _self.h);
                aspectRatio = (_self.w/r) / (_self.h/r);
            }
            console.log('aspectRatio', aspectRatio);
            return aspectRatio;
        },

        openModal(){
            jQuery('#imageCropperModal').fadeIn(200);
        },

        closeModal(){
            jQuery('#imageCropperModal').fadeOut(100);
        },

        hidingModal(){

        },

        clickCerrarModal(){
            this.closeModal();
        },

        clickCropperBtn(method=null, option=null, option2=null){
            // this.$refs.cropper.rotate(90);
            // this.$refs.cropper.zoom('0.2'); // no funciona
            if(method === 'confirmar'){
                this.confirmar();
            } else if(method === 'confirmar2') {
                this.confirmar(true);
            } else {
                this.$refs.cropper[method](option);
            }
        },

        confirmar(seleccionarAutomaticamente = false){
            const _self = this;

            _self.$vueEventBus.$emit('loading', {
                msg: 'Enviando...',
                on: true
            });

            let objCroppedCanvas = {};
            if(_self.w !== null && _self.h !== null){
                console.log('_self.w y _self.h', _self.w, _self.h);
                objCroppedCanvas = {
                    width: _self.w,
                    // maxWidth: _self.w,
                    height: _self.h,
                    // maxHeight: _self.h
                };
            } else {

                const cropboxData = _self.$refs.cropper.getCropBoxData();
                const canvasData = _self.$refs.cropper.getContainerData();
                // console.log('canvasData', canvasData);

                const imgData = _self.$refs.cropper.getData();
                console.log('imgData', imgData);

                objCroppedCanvas = {
                    width: cropboxData.width,
                    // maxWidth: canvasData.width,
                    height: cropboxData.height,
                    // maxHeight: canvasData.height
                };
            }

            console.log('objCroppedCanvas !!!!', objCroppedCanvas);

            const config = { headers: { 'Content-Type': 'multipart/form-data' } };
            const cropperUrl = _self.$refs.cropperUrl.value;
            const croppedCanvas = _self.$refs.cropper.getCroppedCanvas(objCroppedCanvas);
            croppedCanvas.toBlob(function (blob) {
                var formData = new FormData();
                formData.append('img', blob);
                formData.append('id', _self.img.id);

                console.log('sending formdata', formData, config);

                jQuery.ajax(cropperUrl, {
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function () {
                        var xhr = new XMLHttpRequest();
                        xhr.upload.onprogress = function (e) {
                            /*var percent = '0';
                            var percentage = '0%';
                            if (e.lengthComputable) {
                                percent = Math.round((e.loaded / e.total) * 100);
                                percentage = percent + '%';
                                $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                            }*/
                        };
                        return xhr;
                    },
                    success: function (response) {
                        console.log('SUCCESSSS!!', response);
                        if(response.success){
                            $.Notification.notify('success','top center', 'El archivo fue editado y copiado.');
                            _self.$vueEventBus.$emit('image_edited', {
                                file: response.file,
                                seleccionarAuto: seleccionarAutomaticamente
                            });
                            _self.closeModal();

                            // _self.reloadArchivos();
                        }
                    },
                    error: function () {
                        alert('upload error');
                        console.log('upload error');
                    },
                    complete: function () {
                        // $progress.hide();
                        _self.$vueEventBus.$emit('loading', {
                            on: false
                        });
                    },
                });
            }, _self.img.mime_type, 0.7);
        },

        determineZoom(){
            console.log('determineZoom Working');
            const currentWidth = this.$refs.cropper.getCanvasData().width;
            console.log(currentWidth);
        }

    },
    mounted(){
        const _self = this;
        _self.$vueEventBus.$on('open_image_cropper', function(resp){
            console.log('EVENTO open_image_cropper ESCUCHADO', resp);
            _self.initCropper(resp);
            _self.openModal();
        });

        jQuery('#imageCropperModal').on('hidden.bs.modal', function () {
            _self.hidingModal();
        })

    }
});
