$(document).ready(function(){

    var cropperObj = null;
    var cropperId = null;
    var cropperW = 100;
    var cropperH = 100;
    var cropperUrl = null;
    var cropperTipo = 0; // 0 post, 1 contenido, 2 otro
    var cropperCampo = null;
    var $boton = null;
    var uploadedImageURL = null;

    setCropper();

    function resetEditarImg(){
        $('#imgEditarImagen').cropper('destroy').attr('src', null);
        cropperObj = null;
        cropperId = null;
        cropperUrl = null;
        cropperTipo = 0;
        cropperCampo = null;
    }

    function setCropper(){
        $('.img-preview .img.edit').unbind('click').click(function(e) {
            e.preventDefault();
            $(this).closest('.img-preview').find('.editar-img').trigger('click');
        });
        $('.editar-img').unbind('click').click(function(e){
            e.preventDefault();
            var id = $(this).data('id');
            var img = $(this).data('img');
            cropperId = id;
            cropperW = parseInt($(this).data('w'));
            cropperH = parseInt($(this).data('h'));
            cropperTipo = parseInt($(this).data('tipo'));
            cropperCampo = $(this).data('campo');
            cropperUrl = $(this).data('url');
            $boton = $(this);

            populateCropper(img);
            openHeymodal('modalEditarImagen');
        });
        $('#modalEditarToolbar .cropper-btn').unbind('click').click(function(){
            var method = $(this).data('method');
            var option = $(this).data('option');
            var option2 = $(this).data('second-option');

            var $progress = $('#cropperProgress');
            var $progressBar = $progress.find('.progress-bar');

            console.log('method', method);

            if(method === 'confirmar'){
                var objCroppedCanvas = {};
                if(cropperW !== null && cropperH !== null && cropperW !== undefined && cropperH !== undefined && !isNaN(cropperW) && !isNaN(cropperH)){
                    objCroppedCanvas = {
                        width: cropperW,
                        maxWidth: cropperW,
                        height: cropperH,
                        maxHeight: cropperH
                    };
                } else {
                    var canvasData = $('#imgEditarImagen').cropper('getContainerData');
                    console.log('canvasData', canvasData);
                    objCroppedCanvas = {
                        width: canvasData.width,
                        maxWidth: canvasData.width,
                        height: canvasData.height,
                        maxHeight: canvasData.height
                    };
                }
                var croppedCanvas = $('#imgEditarImagen').cropper('getCroppedCanvas', objCroppedCanvas);
                croppedCanvas.toBlob(function (blob) {
                    var formData = new FormData();
                    formData.append('img', blob);
                    formData.append('id', cropperId);
                    formData.append('tipo', cropperTipo);
                    formData.append('campo', cropperCampo);

                    $.ajax(cropperUrl, {
                        method: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        xhr: function () {
                            var xhr = new XMLHttpRequest();
                            xhr.upload.onprogress = function (e) {
                                var percent = '0';
                                var percentage = '0%';
                                if (e.lengthComputable) {
                                    percent = Math.round((e.loaded / e.total) * 100);
                                    percentage = percent + '%';
                                    $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                                }
                            };
                            return xhr;
                        },
                        success: function (res) {
                            if(res.success) {
                                resetEditarImg();
                                cerrarHeymodals();
                                console.log('success upload');
                                $.Notification.notify('success', 'top right', 'La imagen fue editada exitosamente');
                                var $imgPrev = $boton.closest('.img-preview').find('.img');
                                var base64_string = croppedCanvas.toDataURL('image/jpeg', 0.5);
                                var newBackgroundCssProp = 'url(' + base64_string + ')';
                                // var newBackgroundCssProp = "url('"+canvasDataUrl+"')";
                                $imgPrev.css({'background-image': newBackgroundCssProp});
                                $boton.data('img', res.img);
                            } else {
                                $.Notification.notify('error', 'top right', 'Ocurrió un error, intente subir la imagen nuevamente');
                            }

                        },
                        error: function () {
                            alert('upload error');
                            console.log('upload error');
                        },
                        complete: function () {
                            $progress.hide();
                        },
                    });
                });
            } else {
                $('#imgEditarImagen').cropper(method, option, option2);
            }

        });
        // Import image
        var URL = window.URL || window.webkitURL;
        var $inputImage = $('#cropperInputImage');
        var $image = $('#imgEditarImagen');

        if (URL) {
            $inputImage.change(function () {
                var files = this.files;
                var file;

                if (!$image.data('cropper')) return;

                if (files && files.length) {
                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        if (uploadedImageURL) {
                            URL.revokeObjectURL(uploadedImageURL);
                        }
                        uploadedImageURL = URL.createObjectURL(file);
                        $image.cropper('destroy');
                        populateCropper(uploadedImageURL);
                    } else {
                        window.alert('Please choose an image file.');
                    }
                }
            });
        } else {
            $inputImage.prop('disabled', true).parent().addClass('disabled');
        }
    }

    function populateCropper(img){
        $('#imgEditarImagen').cropper('destroy').attr('src', null);
        cropperObj = null;
        $('#imgEditarImagen').attr('src', img);
        initCropper();
    }

    function gcd(a, b) {
        return (b == 0) ? a : gcd (b, a%b);
    }

    function initCropper(){
        var $image = $('#imgEditarImagen');

        var aspectRatio = null;

        if(!isNaN(cropperW) && !isNaN(cropperH) && cropperW > 0 && cropperH > 0){
            var r = gcd(cropperW, cropperH);
            // aspectRatio = cropperW/r + ":" + cropperH/r;
            aspectRatio = (cropperW/r) / (cropperH/r);
        }
        console.log('aspectRatio', aspectRatio);
        $image.cropper({
            aspectRatio: aspectRatio,
            dragMode: 'move',
            crop: function(event) {
            }
        });
        // Get the Cropper.js instance after initialized
        cropperObj = $image.data('cropper');
    }
});