$(document).ready(function(){
	
	$('#categoria_id').change(function(){
		var $cat = $(this);
		var idCat = parseInt($cat.val());

		if(idCat > 0){
			$("#subcategoria_id option").each(function(){
        		var $subcat = $(this);
				if($subcat.val() === 0){
					$subcat.attr("selected", "selected");
				}
				if(parseInt($subcat.data("cat")) === idCat){
					$subcat.show();
				} else {
					$subcat.hide();
				}
			});
			$("#subcategoria_id").val(0);
			$("#subcategoria_id option[data-cat=0]").show();
		} else {
			$("#subcategoria_id").val(0);
			$("#subcategoria_id option").hide();
			$("#subcategoria_id option[data-cat=0]").show();
		}
	});

});