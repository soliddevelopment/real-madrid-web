$(document).ready(function () {

	jQuery.extend(jQuery.validator.messages, {
		required: "Este campo es requerido.",
		// required: "This field is required.",
		remote: "Please fix this field.",
		email: "Este email no es válido.",
		// email: "This is not a valid email.",
		url: "Este no es un URL válido.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Muy corto.")
	});

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

    setImgPreview();
    setHeyConfirm2();
    setFancybox();
    setSwitchery();
    setSortableOrder();
    setDropzone();
	setSummernote();
	setSlugsAutomaticos();
	setTablesMisc();
    setHeymodals();
    setDropdownKeepOpen();
    setCopyToClipboard();
    // setYoutubeControl();
	setForms();
	setSearchList();

});
/*
function setYoutubeControl(){
	$('.youtube-control').each(function(){
		var $control = $(this);
		var name = $(this).attr('name');
		var val = $(this).val();
		var $newHiddenControl = $('<input type="hidden" name="'+name+'" value="'+val+'">');
		$control.after($newHiddenControl);
		$control.attr('name','');

		$control.blur(function(){
			var val = $control.val();
			var cleanId = getYoutubeId(val);
			if(cleanId){
				$newHiddenControl.val(cleanId);
			} else {
				$newHiddenControl.val('');
			}

		});
	});
}*/

function setSearchList(){
	var $input = $('#searchPostsInput');
	var $tableList = $('#' + $input.data('table-id'));

	if($input.length && $tableList.length){
		$input.keyup(function(){
			var str = $input.val();
			var filter = str.toUpperCase();
			if(str.length > 0){
				$tableList.find('tbody tr').each(function(){
					var $tr = $(this);
					var titulo = $(this).find('td.td-titulo span').html();
					var categoria = $(this).find('td.td-categoria span').html();
					if(titulo === undefined || titulo === null) titulo = '';
					if(categoria === undefined || categoria === null) categoria = '';

					if (titulo.toUpperCase().indexOf(filter) > -1 || categoria.toUpperCase().indexOf(filter) > -1) {
						$tr.show();
					} else {
						$tr.hide();
					}
				});
			} else {
				$tableList.find('tbody tr').show();
			}
		});
	}
}

function showLoading(){
	$('#loading').fadeIn(144);
}

function hideLoading(){
	$('#loading').fadeOut(144);
}

function setForms(){
	var selector = 'form';
	$(document).on('submit',selector, function(){
		var $form = $(this);
		$form.find('button[type=submit], input[type=submit]').attr('disabled', 'disabled');
		showLoading();
	});

	$("form.solid-validate").each(function(){
		var $form = $(this);
		$form.validate({
			/*submitHandler: function (form) {
                // console.log("form submit", form);
                // Solid.submitFormSolidValidate(form, opciones);
                var response = grecaptcha.getResponse();
                if(response.length === 0){ //reCaptcha not verified
                    toastr.error('¡Te faltó validar que no eres un robot!');
                } else {
                    //reCaptch verified
                    form.submit();
                }
            }*/
		});
	});
}

function setCopyToClipboard(){
	function copyToClipboard(txt) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(txt).select();
		document.execCommand("copy");
		$temp.remove();
	}
	$('.copy-to-clipboard').unbind('click').click(function(e){
		e.preventDefault();
		var txt = $(this).data('txt');
		copyToClipboard(txt);
	});
}

function getYoutubeId(url){
	var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
	var match = url.match(regExp);
	return (match&&match[7].length==11)? match[7] : false;
}

function setDropdownKeepOpen(){
	$('.dropdown.keep-open .dropdown-toggle').on('click', function (event) {
		$(this).parent().toggleClass('open');
	});
	$('body').on('click', function (e) {
		if (!$('.dropdown.keep-open').is(e.target)
			&& $('.dropdown.keep-open').has(e.target).length === 0
			&& $('.open').has(e.target).length === 0
		) {
			$('.dropdown.keep-open').removeClass('open');
		}
	});
}

function setHeymodals(){
    /*$('.heymodal .heymodal-cerrar, .heymodal .heymodal-bg').unbind('click').click(function(e){
        e.preventDefault();
        var $heymodal = $(this).closest('.heymodal');
        cerrarHeymodal($heymodal);
    });*/
}
function cerrarHeymodal($heymodal){
	$heymodal.fadeOut(144);
}
function cerrarHeymodals(){
    $('.heymodal').fadeOut(100);
    // if($('#imgEditarImagen').length) {
    //     $('#imgEditarImagen').cropper('destroy').attr('src', null);
    //     cropperObj = null;
    // }
}
function openHeymodal(id){
    $('.heymodal').fadeOut(100);
    $('#'+id).fadeIn(100);
}

function setTablesMisc(){
	$('#filtro_categorias').change(function(){
		$(this).closest('form').submit();
	});
}

function setSlugsAutomaticos(){
	function removeAccents(str) {
        var accents    = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
        var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
        str = str.split('');
        var strLen = str.length;
        var i, x;
        for (i = 0; i < strLen; i++) {
          if ((x = accents.indexOf(str[i])) != -1) {
            str[i] = accentsOut[x];
          }
        }
        return str.join('');
    }
    $('form input[name=slug]').each(function(){
        var $slug = $(this);
        var $form = $slug.closest('form');
        var $titulo = $form.find('input[name=titulo], input[name=nombre]');
        var editMode = $form.data('edit');

        $titulo.keyup(function(){
			console.log('key');
            if(editMode === false || editMode === 'false' || editMode === undefined || editMode === null){
                var tituloVal = $titulo.val();
                tituloVal = removeAccents(tituloVal);
                tituloVal = tituloVal.toLowerCase();

                var replaceTo = '-';
				if($slug.hasClass('no-guiones')){
					replaceTo = '';
				}
                var nuevoSlug = tituloVal.replace(/[^A-Z0-9]+/ig, replaceTo);
                $slug.val(nuevoSlug);
            }
        });
    });
}

function showLoading() {
    $("#loading-generico").fadeIn(200);
}
function hideLoading() {
    $("#loading-generico").fadeOut(200);
}

function setSummernote(){
	function sendFile(file, editor, welEditable) {
		var data = new FormData();
		console.log('sendFile', file);
		data.append("file", file);
		var url = $('#urlUploadImageSummernote').val();
		$.ajax({
			data: data,
			type: 'POST',
			url: url,
			cache: false,
			contentType: false,
			processData: false,
			success: function(url) {
				//editor.insertImage(welEditable, url);
				editor.summernote('insertImage', url, function ($image) {
					console.log('image inserted', url);
				});
			}
		});
	}

	$('.summernote').summernote({
		height: 200,
		minHeight: null,
		maxHeight: null,
		focus: true,
		tooltip: false,
		maximumImageFileSize: 524288, // 512KB
		popover: {
			image: [
				['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
				['float', ['floatLeft', 'floatRight', 'floatNone']],
				['remove', ['removeMedia']]
			],
			link: [
				['link', ['linkDialogShow', 'unlink']]
			],
			air: [
				['color', ['color']],
				['font', ['bold', 'underline', 'clear']],
				['para', ['ul', 'paragraph']],
				['table', ['table']],
				['insert', ['link', 'picture']]
			]
		},
		callbacks: {
			onInit: function() {
				// console.log('Summernote is launched');
				// note-btn btn btn-light btn-sm
				/*var noteBtn = '<button id="summernoteBtnMediaManager" type="button" class="btn btn-light btn-sm btn-small" title="Insertar imagen" data-event="something" tabindex="-1"><i class="fa fa-image"></i></button>';
				var fileGroup = '<div class="note-file btn-group">' + noteBtn + '</div>';
				$(fileGroup).appendTo($('.note-toolbar'));
				// Button tooltips
				$('#summernoteBtnMediaManager').tooltip({
					container: 'body',
					placement: 'bottom'
				});
				// Button events
				$('#summernoteBtnMediaManager').click(function(event) {
					alert(1);
				});*/
			},
			onPaste: function (e) {
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
				e.preventDefault();
				document.execCommand('insertText', false, bufferText);
			},
			onImageUpload: function(files, editor, welEditable) {
				//var $files = $(files);
				var $this = $(this);
				sendFile(files[0], $this, welEditable);
			}
		},
		toolbar: [
			['style', ['bold', 'italic', 'underline']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['insert', ['picture', 'link', 'video', 'table', 'hr']]
		],
	});
}

function setDropzone(){

	Dropzone.autoDiscover = false;

	// SETEO TODO LO DEL DROPZONE
	$(".galeria-dropzone").each(function(){

		var arrayArchivosSubidos = [];

		var cont = $(this).closest(".galeria-cont");
		var id = $(this).data("id");
		var url = $(this).attr("action");
		var url_borrar = $(this).data("url-borrar");
		var galeriaProgress = cont.find(".galeria-progress");
		var this_dropzone = $(this);

		var myDropzone = new Dropzone(this, {
			url: url,
			acceptedFiles: ".png, .jpg, .jpeg",
			maxFilesize: 3000,
			method: "post",
			addRemoveLinks: true,
			parallelUploads: 20,
			autoProcessQueue: false,
			headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		myDropzone.on("addedfile", function(file, message){
			// console.log("addedfile", file);
			cont.find(".btn.guardar").removeAttr("disabled");
		});

		myDropzone.on("totaluploadprogress", function(progress){
			// console.log("totaluploadprogress", progress);
		    updateProgressBar(progress, galeriaProgress);
		});

		myDropzone.on("queuecomplete", function(progress){
			// console.log("queuecomplete", progress);
			updateProgressBar(0, galeriaProgress);
			cont.find(".btn.guardar").removeAttr("disabled");
			//window.location.href = window.location.href;
			//agregarArchivosALista(cont, this_dropzone, arrayArchivosSubidos, galeriaProgress);

			// console.log('ARCHIVOS SUBIDOS', arrayArchivosSubidos);
			for(v in arrayArchivosSubidos){
				var actual = arrayArchivosSubidos[v];

				var urlBorrar = this_dropzone.data("url-borrar");
				var img = $('<div class="img" data-id="'+actual.id+'" id="img_'+actual.id+'"></div>');
				img.append('<a href="'+actual.imgpath_big+'" class="fancybox"><img src="'+actual.imgpath_small+'"></a> <a href="#" class="borrar" data-url="'+urlBorrar+'">Borrar</a>');

				cont.find(".galeria-actual").prepend(img).promise().done(function(){
					// Seteo links de borrar de nuevo
					setBorrarClick(cont, urlBorrar);
				});
			}

			// Se termino de subir todo
			resetDropzone(myDropzone, galeriaProgress, cont);

		});

		myDropzone.on("sending", function(file, xhr, formData){
			// console.log("sending", file);
			formData.append("id", id);

			cont.find(".btn.guardar").attr("disabled", "disabled");
		});

		myDropzone.on("success", function(file, response){
			//console.log("success response", response);
			// response = JSON.parse(response);
			// console.log("success response", response);
			if(response.success){
				arrayArchivosSubidos.push(response.img_obj);
				$.Notification.notify('success','top right', 'Se subieron los archivos.');
			} else {
				$.Notification.notify('error','top right', 'No se pudo subir todos los archivos.');
			}

		});

		cont.find(".btn.guardar").unbind("click").click(function(e){
			e.preventDefault();
			myDropzone.processQueue();
		});

		cont.find(".btn.reset").unbind("click").click(function(e){
			e.preventDefault();
			resetDropzone(myDropzone, galeriaProgress, cont);
		});

		// SETEO EL SORTABLE DE LA GALERIA
		var sortableDiv = cont.find(".galeria-actual");
		var url = this_dropzone.data("url-orden");

		sortableDiv.sortable({
			update: function (event, ui){
				var data = sortableDiv.sortable('serialize');
				console.log("Sorted", data);
				actualizarOrdenGaleria(sortableDiv, data, url);
			}
		});

		// SETEO BORRAR IMGS
		setBorrarClick(cont, url_borrar);

	});

	function agregarArchivosALista(cont, dropzone, arraySubidos, galeriaProgress){



	}

	function resetDropzone(myDropzone, galeriaProgress, cont){
		myDropzone.removeAllFiles(true);
		updateProgressBar(0, galeriaProgress);
		cont.find(".btn.guardar").attr("disabled", "disabled");
	}

	function setBorrarClick(cont, url_borrar){
		cont.find(".galeria-actual .img .borrar").unbind("click").click(function(e){
			e.preventDefault();
			var img = $(this).closest(".img");
			var id = img.data("id");
			var url = url_borrar;
			$.ajax({
				type: "POST",
				url: url,
				dataType: 'json',
				data: { id: id },
				success: function(data){
					console.log("borrar img", data);
					img.remove();
				}
			});
		});
	}
}

function actualizarOrdenGaleria(sortableDiv, data, url){
	sortableDiv.sortable("disable");
	$.ajax({
		type: "POST",
		url: url,
		dataType: 'json',
		data: data,
		success: function(data){
			console.log("actualizarOrdenGaleria", data);
			sortableDiv.sortable("enable");
		}
	});
}

function updateProgressBar(porcentaje, galeriaProgress){
    if(!isNaN(porcentaje)){
    	if(porcentaje > 0){
	    	$(".galeria-progress").css({
	    		opacity: 1
	    	});
    	} else {
    		$(".galeria-progress").css({
	    		opacity: 0
	    	});
    	}
        $(".galeria-progress .progress-bar").attr("aria-valuenow", porcentaje).css({
            width: porcentaje + "%"
        });
    }
}


function setSortableOrder(){
	/*var url = $(".sortable-table").data("url");
	$(".sortable-guardar").click(function(){
		var data = $('table.sortable tbody').sortable('serialize');
		console.log(data);
	});*/
	// table.heySort o ul.heySort o div.heySort
	$('.heySort').each(function(){
		var parent = $(this);
		var url = parent.data("url");

		var elemToSort;

		if(parent.is('table')){
			elemToSort = parent.find('tbody');
		} else {
			elemToSort = parent;
		}

		elemToSort.sortable({
			update: function (event, ui){
				var data = elemToSort.sortable('serialize');
				console.log("Sorted", data);
				actualizarOrden(parent, elemToSort, data, url);
			}
		});
	});
}
function actualizarOrden(parent, elemToSort, data, url){
	// $('table.sortable body').sortable("disable");
	elemToSort.sortable("disable");

	$.ajax({
		type: "POST",
		url: url,
		dataType: 'json',
		data: data,
		success: function(data){
			// console.log('success actualizando orden', data);
			elemToSort.sortable("enable");
		}
	});
}

function setSwitchery(){
	$(".st-switch").change(function(){
		//console.log("Switch change");
		var url = $(this).data("url");
	    var type = $(this).data("type");
	    var id = $(this).data("id");

	    var val;
	    if($(this).is(":checked")){
	    	//console.log("checked");
			val = 1;
	    } else {
	    	//console.log("not checked");
	    	val = 0;
	    }

	    $.ajax({
			type: "POST",
			url: url,
			dataType: 'json',
			/*headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},*/
			data: {
				id: id,
            	val: val
			},
			success: function(data){
				if(data.success){
					console.log("success switched", data);
				} else {
					alert("error");
					console.log("error switched", data);
				}

			}
		});

	});
}
/* EJEMPLO:
<input type="checkbox"
	class="st-switch"
	data-id="<?=$pro->id;?>"
	data-url="st_proyecto"
	<?php if($pro->st == 1) echo "checked"; ?>
	data-plugin="switchery"
	data-color="#3bafda"
	data-size="small" />
*/
function setFancybox(){

	$(".fancybox").fancybox({
		'type' : 'image'
	});
}

function setHeyConfirm2() {
	$(".hey-confirm").unbind("click").click(function(e){
        e.preventDefault();
	    var title = $(this).attr("data-title");
	    var text = $(this).attr("data-text");
	    var type = $(this).data("type");
	    var id_form = $(this).data("id-form");

		swal({
			title: title,
			text: text,
			type: type,
			showCancelButton: true,
			//confirmButtonColor: "#DD6B55",
			confirmButtonText: "aceptar",
			cancelButtonText: "cancelar",
			closeOnConfirm: true
		}).then(function(willDelete){
			if (willDelete) {
				//window.location.href = window.location.href;
				if($('#'+id_form).length){
					document.getElementById(id_form).submit();
				}
			} else {
				//swal("Your imaginary file is safe!");
			}
		});

	});
}
function setHeyConfirm() {

    $(".hey-confirm").unbind("click").click(function(e){
        e.preventDefault();

	    var url = $(this).attr("href");
	    var title = $(this).attr("data-title");
	    var text = $(this).attr("data-text");
	    var method = $(this).data("method");
	    var type = $(this).data("type");
	    var id = $(this).data("id");
	    var tipo = $(this).data("tipo");

	    console.log("click1");

		swal({
			title: title,
			text: text,
			type: type,
			showCancelButton: true,
			//confirmButtonColor: "#DD6B55",
			confirmButtonText: "aceptar",
			cancelButtonText: "cancelar",
			closeOnConfirm: true
		})
		.then(function(willDelete){
			if (willDelete) {
				if (method == undefined || method == null || method.toLowerCase() === "get") {
					window.location.href = url;
				} else {
					//alert("post");
					//Entra aqui porque es un post
					$.ajax({
						type: "POST",
						url: url,
						dataType: 'json',
						data: {
							id: id,
							tipo: tipo
						},
						success: function(data){
							//alert(data);
							if(data.success){
								console.log("success", data);
								window.location.href = window.location.href;
								if(data.RedirectUrl !== null){
									window.location.href = data.RedirectUrl;
								}
							} else {
								alert("error");
								console.log("error", data);
							}

						},
						/*error: function(error){
                            console.log("error", error);
                        }*/
					});
				}
			} else {
				//swal("Your imaginary file is safe!");
			}
		});

	});
}

//"1<br /><font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Trying to get property of non-object in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Trying to get property of non-object in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-warning' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Warning: unlink(D:/PROYECTOS/urbanistas/sitio/php/img/home_slider/): Permission denied in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>25</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr><tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0295</td><td bgcolor='#eeeeec' align='right'>1248136</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.unlink' target='_new'>unlink</a>(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>25</td></tr></table></font><br /><font size='1'><table class='xdebug-error xe-fatal-error' dir='ltr' border='1' cellspacing='0' cellpadding='1'><tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Fatal error: Call to a member function delete() on boolean in D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php on line <i>27</i></th></tr><tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr><tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr><tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>252320</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\PROYECTOS\urbanistas\sitio\php\admin\_borrar_slide.php' bgcolor='#eeeeec'>...\_borrar_slide.php<b>:</b>0</td></tr></table></font>"

function setImgPreview(){
	$(".img-preview .img").unbind("click").click(function(){
		var div = $(this).closest(".img-preview");
		div.find("input[type=file]").click();
	});
	$(".img-preview input[type=file]").unbind("change").change(function(e){
		var div = $(this).closest(".img-preview");
		var img = div.find(".img");
		var input = this;

	    if (input.files && input.files[0]) {

	        var reader = new FileReader();
	        var file = input.files[0];

	        reader.readAsDataURL(file);
	        reader.onload = function (e) {

	            var thisresult = this.result;
	            var exif = EXIF.readFromBinaryFile(base64ToArrayBuffer(thisresult));
	            var orientation = exif.Orientation;
	            if (orientation >= 2 && orientation <= 8) { } else {
	                orientation = 0;
	            }

	            var src = e.target.result;
	            var hiddenObj = div.find(".hid-orientation");
	            img.css({ 'background-image': "url(" + src + ")" });
	            hiddenObj.val(orientation);

	            if (orientation === 3 || orientation === 8) {
	                img.addClass("rotar-izq-90");
	            } else if (orientation === 5 || orientation === 6 || orientation === 7) {
	                img.addClass("rotar-der-90");
	            }

	            // ********************************************************************

	        }
	    }
	});
}
function base64ToArrayBuffer(base64) {
    base64 = base64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}
/*
/* EJEMPLO:
<div class="form-group img-preview">
    <span class="img" style="<?=$thumb_img_bg;?>"></span>
    <div class="info">
        <label>Imagen principal (~ 1200 x 800 px)</label>
        <input type="file" name="img_thumb">
        <input type="hidden" class="hid-orientation" name="img_thumb_orientation" value="0">
    </div>
    <div class="clearfix"></div>
</div>
*/
