<?php

use Illuminate\Database\Seeder;
use App\Models\Fijos\ExtraTipo;

class TiposExtrasSeeder extends Seeder
{
    public function run()
    {
        $elem = new ExtraTipo();
        $elem->slug = 'texto';
        $elem->titulo = 'Texto corto';
        $elem->regex = null;
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'img';
        $elem->titulo = 'Imagen';
        $elem->regex = '';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'texto-mediano';
        $elem->titulo = 'Texto mediano';
        $elem->regex = '';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'texto-largo';
        $elem->titulo = 'Texto largo';
        $elem->regex = '';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'editor';
        $elem->titulo = 'Editor de texto';
        $elem->regex = '';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'numero';
        $elem->titulo = 'Número';
        $elem->regex = null;
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'email';
        $elem->titulo = 'Email';
        $elem->regex = '^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'fecha';
        $elem->titulo = 'Fecha';
        $elem->regex = '^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'archivo';
        $elem->titulo = 'Archivo';
        $elem->regex = '';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'lista';
        $elem->titulo = 'Lista';
        $elem->regex = '';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'checkbox';
        $elem->titulo = 'Checkbox';
        $elem->regex = '';
        $elem->save();

        $elem = new ExtraTipo();
        $elem->slug = 'gallery';
        $elem->titulo = 'Galería';
        $elem->regex = '';
        $elem->save();
    
        $elem = new ExtraTipo();
        $elem->slug = 'html';
        $elem->titulo = 'HTML';
        $elem->regex = null;
        $elem->save();

    }
}
