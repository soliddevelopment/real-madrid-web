<?php

use Illuminate\Database\Seeder;

use App\Models\Fijos\Seccion;

class SeccionesFormsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        // FORMULARIO DE INSCRIPCION
        $tips = Seccion::createNewForm('Formulario de inscripción', 'inscripcion', []);
        $tips->newCampo('Nombre', 'nombre', 'texto', true);
        $tips->newCampo('Apellido', 'apellido', 'texto', true);
        $tips->newCampo('Email', 'email', 'email', true);
        $tips->newCampo('Fecha de nacimiento', 'fechanacimiento', 'fecha', true);
        $tips->newCampo('Sexo', 'sexo', 'texto', true);
        $tips->newCampo('Talla de camisa', 'talla', 'texto', true);
        $tips->newCampo('Observaciones', 'observaciones', 'texto-mediano', false);
        //$tips->newCampo('Consentimiento', '', 'checkbox', true);
        
        $tips->newCampo('Nombre', 'responsable_nombre', 'texto', true);
        $tips->newCampo('Apellido', 'responsable_apellido', 'texto', true);
        $tips->newCampo('Email', 'responsable_email', 'texto', true);
        $tips->newCampo('Teléfono', 'responsable_telefono', 'texto', true);
        $tips->newCampo('Cédula', 'responsable_cedula', 'texto', true);
        //$tips->newCampo('Términos', '', 'checkbox', true);
        
    
        /*
        OPCIONES TIPO DE EXTRA
        'texto', 'img', 'texto-mediano', 'texto-largo', 'editor', 'numero', 'email', 'fecha', 'archivo', 'lista', 'checkbox', 'gallery', 'html'
        
        OPCIONES PARA CREATE NEW SECCION
        'has_children', 'sortable', 'slug_manual', 'destacados', 'has_categorias', 'has_related'
        */
    }
}
