<?php

use Illuminate\Database\Seeder;
use App\Models\Fijos\UserRole;

class UserRoleTableSeeder extends Seeder
{
    public function run()
    {
        $role_user = new UserRole();
        $role_user->slug = 'superadmin';
        $role_user->nombre = 'Super Admin';
        $role_user->descripcion = 'PERMISOS ABSOLUTOS';
        $role_user->save();

        $role_user = new UserRole();
        $role_user->slug = 'admin';
        $role_user->nombre = 'Admin';
        $role_user->descripcion = 'Permisos de administrador';
        $role_user->save();

        $role_user = new UserRole();
        $role_user->slug = 'supervisor';
        $role_user->nombre = 'Supervisor';
        $role_user->descripcion = 'Permisos de supervisor';
        $role_user->save();

        /*$role_user = new UserRole();
        $role_user->slug = 'basic';
        $role_user->nombre = 'Basic';
        $role_user->descripcion = 'Permisos básicos (no admin)';
        $role_user->save();*/

    }
}
