<?php

use Illuminate\Database\Seeder;
use App\Models\Fijos\Config;

class ConfigsTableSeeder extends Seeder
{
    public function run()
    {
        $config = new Config();
        $config->tipo = 'IMG';
        $config->slug = 'admin-logo';
        $config->titulo = 'Admin logo';
        $config->only_superadmin = 0;
        $config->save();

        $config = new Config();
        $config->tipo = 'IMG';
        $config->slug = 'contacto-logo';
        $config->titulo = 'Contacto logo';
        $config->only_superadmin = 0;
        $config->save();

        $config = new Config();
        $config->tipo = 'STR';
        $config->slug = 'email-contacto';
        $config->titulo = 'Email para recibir mensajes de contacto';
        $config->valor_str = 'heymans@brandy.la';
        $config->only_superadmin = 0;
        $config->save();
    }
}
