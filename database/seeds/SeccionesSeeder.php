<?php

use Illuminate\Database\Seeder;

use App\Models\Fijos\Seccion;
use App\Models\Fijos\Gallery;

class SeccionesSeeder extends Seeder
{
    public function run()
    {
    
        // fotos
        $tips = Seccion::createNew('Fotos', 'fotos', ['sortable']);
        $tips->newExtra('Imagen', 'imagen', 'ITEM', 'img');
        
        // GALERIA
        /*$galeriaPrincipal = new Gallery();
        $galeriaPrincipal->slug = 'galeria-principal';
        $galeriaPrincipal->titulo = 'Galería principal';
        $galeriaPrincipal->save();*/
    
        /*
        OPCIONES TIPO DE EXTRA
        'texto', 'img', 'texto-mediano', 'texto-largo', 'editor', 'numero', 'email', 'fecha', 'archivo', 'lista', 'checkbox', 'gallery', 'html'
        
        OPCIONES PARA CREATE NEW SECCION
        'has_children', 'sortable', 'slug_manual', 'destacados', 'has_categorias', 'has_related'
        */
    }
}
