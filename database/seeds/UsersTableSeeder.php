<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

use App\Models\Fijos\User;
use App\Models\Fijos\UserRole;


class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $role_superadmin = UserRole::where('slug', 'superadmin')->first();
        $role_admin = UserRole::where('slug', 'admin')->first();

        $now = Carbon::now();

        $user = new User();
        $user->role_id = $role_superadmin->id;
        $user->nombre = 'Jose Carlos';
        $user->apellido = 'Heymans';
        $user->usuario = 'heymans';
        $user->email = 'heymans@brandy.la';
        $user->password = bcrypt('123456789');
        $user->email_verified_at = $now;
        $user->save();
        // $user->roles()->attach($role_superadmin);

        $user = new User();
        $user->role_id = $role_admin->id;
        $user->nombre = 'Brandy';
        $user->apellido = 'Tech';
        $user->usuario = 'admin';
        $user->email = 'hola@brandy.la';
        $user->password = bcrypt('123456789');
        $user->email_verified_at = $now;
        $user->save();
        // $user->roles()->attach($role_admin);

    }
}
