<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtrasTable extends Migration
{
    public function up()
    {
        Schema::create('extras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('modo')->default('ITEM'); // ITEM, CONTENIDO, CAMPO (forms)
            $table->integer('seccion_id')->unsigned()->nullable()->default(null);
            $table->integer('tipo_id')->unsigned();
            $table->string('slug');
            $table->string('titulo');
            $table->tinyInteger('required')->default(0);
            $table->integer('width')->nullable()->default(null);
            $table->integer('height')->nullable()->default(null);
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('extras');
    }
}
