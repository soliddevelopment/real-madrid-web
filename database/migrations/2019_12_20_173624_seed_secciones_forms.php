<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedSeccionesForms extends Migration
{
    public function up()
    {
        Artisan::call('db:seed', [
            '--class' => SeccionesFormsSeeder::class,
            '--force' => true
        ]);
    }
    
    public function down()
    {
        //
    }
}
