<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->nullable()->default(null);
            $table->string('usuario')->nullable()->default(null);
            $table->string('email')->unique();
            $table->string('nombre');
            $table->string('apellido')->nullable()->default(null);
            $table->string('telefono')->nullable()->default(null);
            $table->boolean('sexo')->default(1); // 1 hombre, 0 mujer
            $table->integer('img_id')->nullable()->default(null);
            $table->string('password');

            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->boolean('active')->default(1);
            $table->dateTime('last_login')->nullable()->default(null);
            $table->dateTime('last_login_admin')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
