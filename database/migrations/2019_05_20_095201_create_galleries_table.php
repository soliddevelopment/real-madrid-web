<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned()->nullable()->default(null);
            $table->integer('ent_id')->unsigned()->nullable()->default(null);
            $table->string('slug')->nullable()->default(null);
            $table->string('titulo')->default('')->nullable();
            $table->integer('cover')->nullable()->default(null);
            $table->text('resumen')->nullable()->default(null);
            $table->boolean('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('galleries');
    }
}
