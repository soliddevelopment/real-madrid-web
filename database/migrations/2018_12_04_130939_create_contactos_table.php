<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactosTable extends Migration
{
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->string('tipo')->nullable();
            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->boolean('sexo')->default(0);
            $table->string('cedula')->nullable();
            $table->string('empresa')->nullable();
            $table->string('puesto')->nullable();
            $table->string('telefono')->nullable();
            $table->string('email')->nullable();
            $table->string('direccion')->nullable();
            $table->text('mensaje', 65535)->nullable();
            $table->string('campo1')->nullable()->default(null);
            $table->string('campo2')->nullable()->default(null);
            $table->string('campo3')->nullable()->default(null);
            $table->string('source')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}
