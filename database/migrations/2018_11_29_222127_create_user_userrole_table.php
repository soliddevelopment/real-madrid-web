<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserUserroleTable extends Migration
{
    public function up()
    {
        Schema::create('user_userrole', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('userrole_id');
        });
        
        Artisan::call('db:seed', [
            '--class' => UserRoleTableSeeder::class,
            '--force' => true
        ]);
    
        Artisan::call('db:seed', [
            '--class' => UsersTableSeeder::class,
            '--force' => true
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('user_userrole');
    }
}
