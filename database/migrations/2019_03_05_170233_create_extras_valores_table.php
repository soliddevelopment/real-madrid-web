<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtrasValoresTable extends Migration
{
    public function up()
    {
        Schema::create('extras_valores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned()->nullable()->default(null);
            $table->integer('extra_id')->unsigned();
            $table->integer('tipo_id')->unsigned();

            $table->string('valor_str')->nullable()->default(null);
            $table->boolean('valor_bool')->nullable()->default(null);
            $table->integer('valor_int')->nullable()->default(null);
            $table->text('valor_txt')->nullable()->default(null);
            $table->longText('valor_txt_large')->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('extras_valores');
    }
}
