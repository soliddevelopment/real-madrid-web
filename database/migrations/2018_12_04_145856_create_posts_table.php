<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seccion_id')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned()->nullable()->default(null);
            $table->integer('post_id')->unsigned()->nullable();
            $table->string('slug')->nullable()->default(null);
            $table->string('titulo')->default('')->nullable();
            $table->integer('img_id')->nullable()->default(null);
            $table->text('resumen')->nullable();
            $table->text('tags')->nullable();
            $table->boolean('destacado')->default(0);
            $table->boolean('active')->default(1);
            $table->integer('order')->default(0);
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('last_updated_by')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
