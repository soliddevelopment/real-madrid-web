<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsRelationsTable extends Migration
{
    public function up()
    {
        Schema::create('posts_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_post_id')->unsigned();
            $table->integer('related_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('posts_relations');
    }
}
