<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeccionesTable extends Migration
{
    public function up()
    {
        Schema::create('secciones', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('seccion_id')->nullable()->default(null);
            $table->string('slug', 191);
            $table->string('titulo', 191);
            $table->boolean('has_children')->default(0);
            $table->boolean('has_imgs')->default(0);
            $table->boolean('has_categorias')->default(0);
            $table->boolean('has_related')->default(0);
            $table->boolean('categorias_administrables')->default(0);
            $table->boolean('sortable')->default(0);
            $table->boolean('adjuntos')->default(0);
            $table->boolean('slug_manual')->default(0);
            $table->tinyInteger('destacados')->default(0);
            $table->boolean('active')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }
    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('secciones');
    }
}
