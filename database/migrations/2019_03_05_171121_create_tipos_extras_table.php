<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposExtrasTable extends Migration
{
    public function up()
    {
        Schema::create('extras_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titulo');
            $table->text('regex')->nullable()->default(null);
            $table->string('display_mode')->nullable()->default(null);
        });
    
        Artisan::call('db:seed', [
            '--class' => TiposExtrasSeeder::class,
            '--force' => true
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('extras_tipos');
    }
}
