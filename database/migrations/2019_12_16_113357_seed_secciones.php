<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedSecciones extends Migration
{
    public function up()
    {
        Artisan::call('db:seed', [
            '--class' => SeccionesSeeder::class,
            '--force' => true
        ]);
    }
    
    public function down()
    {
        //
    }
}
