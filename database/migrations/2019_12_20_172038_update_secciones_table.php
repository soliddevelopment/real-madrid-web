<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeccionesTable extends Migration
{
    public function up()
    {
        Schema::table('secciones', function (Blueprint $table) {
            $table->string('tipo')->default('BLOG')->after('id'); // tipo BLOG, FORM
        });
    }

    public function down()
    {
        Schema::table('secciones', function (Blueprint $table) {
            $table->dropColumn('tipo');
        });
    }
}
