<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo'); // IMG, STR, INT, BOOL
            $table->string('slug');
            $table->string('titulo')->nullable()->default(null);
            $table->text('descripcion')->nullable()->default(null);
            $table->integer('valor_img')->nullable()->default(null);
            $table->string('valor_str')->nullable()->default(null);
            $table->integer('valor_int')->nullable()->default(null);
            $table->tinyInteger('valor_bool')->default(0);
            $table->tinyInteger('only_superadmin')->default(1);
            $table->timestamps();
        });
    
        Artisan::call('db:seed', [
            '--class' => ConfigsTableSeeder::class,
            '--force' => true
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
