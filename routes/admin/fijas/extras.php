<?php

Route::prefix('admin')->group(function () {

    Route::get('/extras/seccion/{seccionSlug}', [
        'uses' => 'ExtrasController@index',
        'as' => 'admin.secciones.extras.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);

    Route::get('/extras/edit/{seccion_id}/{id}', [
        'uses' => 'ExtrasController@edit',
        'as' => 'admin.secciones.extras.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::get('/extras/create/{seccion_id}', [
        'uses' => 'ExtrasController@edit',
        'as' => 'admin.secciones.extras.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);

    Route::post('/extras/store', [
        'uses' => 'ExtrasController@store',
        'as' => 'admin.secciones.extras.store',
        'middleware' => 'admin_roles', 'roles' => ['superadmin']
    ]);
    Route::put('/extras/{id}/update', [
        'uses' => 'ExtrasController@store',
        'as' => 'admin.secciones.extras.update',
        'middleware' => 'admin_roles', 'roles' => ['superadmin']
    ]);

    Route::delete('/extras/{id}/destroy', [
        'uses' => 'ExtrasController@destroy',
        'as' => 'admin.secciones.extras.destroy',
        'middleware' => 'admin_roles', 'roles' => ['superadmin']
    ]);
    Route::post('/extras/{id}/st', [
        'uses' => 'ExtrasController@st',
        'as' => 'admin.secciones.extras.st',
        'middleware' => 'admin_roles', 'roles' => ['superadmin']
    ]);
    Route::post('/extras/sort', [
        'uses' => 'ExtrasController@sort',
        'as' => 'admin.secciones.extras.sort',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);

});
