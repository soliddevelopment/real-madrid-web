<?php

Route::prefix('admin')->group(function () {

    Route::get('/contenidos', [
        'uses' => 'ContenidosController@index',
        'as' => 'admin.secciones.contenidos.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);

    /*Route::get('/contenidos/seccion/{seccionSlug}', [
        'uses' => 'ContenidosController@index',
        'as' => 'admin.secciones.contenidos.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);*/

    Route::get('/contenidos/{id}/edit', [
        'uses' => 'ContenidosController@edit',
        'as' => 'admin.secciones.contenidos.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);

    Route::get('/contenidos/{id}/editarvalor', [
        'uses' => 'ContenidosController@editarvalor',
        'as' => 'admin.secciones.contenidos.editarvalor',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);

    Route::post('/contenidos/{id}/storevalor', [
        'uses' => 'ContenidosController@storevalor',
        'as' => 'admin.secciones.contenidos.storevalor',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);

    Route::get('/contenidos/create', [
        'uses' => 'ContenidosController@edit',
        'as' => 'admin.secciones.contenidos.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);

    Route::post('/contenidos/store', [
        'uses' => 'ContenidosController@store',
        'as' => 'admin.secciones.contenidos.store',
        'middleware' => 'admin_roles', 'roles' => ['superadmin']
    ]);

    Route::put('/contenidos/{id}/update', [
        'uses' => 'ContenidosController@store',
        'as' => 'admin.secciones.contenidos.update',
        'middleware' => 'admin_roles', 'roles' => ['superadmin']
    ]);

    Route::delete('/contenidos/{id}/destroy', [
        'uses' => 'ContenidosController@destroy',
        'as' => 'admin.secciones.contenidos.destroy',
        'middleware' => 'admin_roles', 'roles' => ['superadmin']
    ]);

    Route::post('/contenidos/sort', [
        'uses' => 'ContenidosController@sort',
        'as' => 'admin.secciones.contenidos.sort',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'admin']
    ]);

});
