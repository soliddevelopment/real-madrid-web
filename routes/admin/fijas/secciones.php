<?php

Route::prefix('admin')->group(function () {
    
    Route::get('/secciones', [
        'uses' => 'SeccionesController@index',
        'as' => 'admin.secciones.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::get('/secciones/{id}/edit', [
        'uses' => 'SeccionesController@edit',
        'as' => 'admin.secciones.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::get('/secciones/create', [
        'uses' => 'SeccionesController@edit',
        'as' => 'admin.secciones.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::post('/secciones/store', [
        'uses' => 'SeccionesController@store',
        'as' => 'admin.secciones.store',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::put('/secciones/{id}/update', [
        'uses' => 'SeccionesController@store',
        'as' => 'admin.secciones.update',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::delete('/secciones/{id}/destroy', [
        'uses' => 'SeccionesController@destroy',
        'as' => 'admin.secciones.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::post('/secciones/{id}/st', [
        'uses' => 'SeccionesController@st',
        'as' => 'admin.secciones.st',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
});
