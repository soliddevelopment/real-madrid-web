<?php

Route::prefix('admin')->group(function () {

    Route::get('/contactos', [
        'uses' => 'ContactosController@index',
        'as' => 'admin.contactos.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin' ]
    ]);

    /*Route::get('/contactos/create', [
        'uses' => 'ContactosController@edit',
        'as' => 'admin.contactos.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);

    Route::get('/contactos/edit/{id}', [
        'uses' => 'ContactosController@edit',
        'as' => 'admin.contactos.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin','admin']
    ]);*/

    Route::get('/contactos/detalle/{id}', [
        'uses' => 'ContactosController@detalle',
        'as' => 'admin.contactos.detalle',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin','admin']
    ]);

    Route::post('/contactos/store', [
        'uses' => 'ContactosController@store',
        'as' => 'admin.contactos.store',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin','admin']
    ]);

    Route::put('/contactos/{id}/update', [
        'uses' => 'ContactosController@store',
        'as' => 'admin.contactos.update',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin','admin']
    ]);

    Route::delete('/contactos/{id}/destroy', [
        'uses' => 'ContactosController@destroy',
        'as' => 'admin.contactos.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin','admin']
    ]);

});
