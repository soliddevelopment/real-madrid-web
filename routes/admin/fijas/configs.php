<?php

Route::prefix('admin')->group(function () {
    Route::get('/config', [
        'uses' => 'ConfigController@index',
        'as' => 'admin.config.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/config/edit/{id}', [
        'uses' => 'ConfigController@edit',
        'as' => 'admin.config.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/config/create', [
        'uses' => 'ConfigController@edit',
        'as' => 'admin.config.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::post('/config/store', [
        'uses' => 'ConfigController@store',
        'as' => 'admin.config.store',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::put('/config/{id}/update', [
        'uses' => 'ConfigController@store',
        'as' => 'admin.config.update',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::delete('/config/{id}/destroy', [
        'uses' => 'ConfigController@destroy',
        'as' => 'admin.config.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::post('/config/{id}/st', [
        'uses' => 'ConfigController@st',
        'as' => 'admin.config.st',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
});
