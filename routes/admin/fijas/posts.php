<?php

Route::prefix('admin')->group(function () {

    Route::get('/posts/list/{seccionSlug}', [
        'uses' => 'PostsController@index',
        'as' => 'admin.secciones.posts.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    /*Route::get('/posts/{id}/detalle', [
        'uses' => 'PostsController@detalle',
        'as' => 'admin.secciones.posts.detalle',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);*/
    Route::get('/posts/list/{seccionSlug}/edit/{id}', [
        'uses' => 'PostsController@edit',
        'as' => 'admin.secciones.posts.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/posts/list/{seccionSlug}/detalle/{id}', [
        'uses' => 'PostsController@detalle',
        'as' => 'admin.secciones.posts.detalle',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/posts/list/{seccionSlug}/create', [
        'uses' => 'PostsController@edit',
        'as' => 'admin.secciones.posts.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/posts/store', [
        'uses' => 'PostsController@store',
        'as' => 'admin.secciones.posts.store',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/posts/update/{id}', [
        'uses' => 'PostsController@store',
        'as' => 'admin.secciones.posts.update',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::delete('/posts/{id}/destroy', [
        'uses' => 'PostsController@destroy',
        'as' => 'admin.secciones.posts.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/posts/{id}/st', [
        'uses' => 'PostsController@st',
        'as' => 'admin.secciones.posts.st',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/posts/{id}/destacado', [
        'uses' => 'PostsController@destacado',
        'as' => 'admin.secciones.posts.destacado',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/posts/sort', [
        'uses' => 'PostsController@sort',
        'as' => 'admin.secciones.posts.sort',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
});
