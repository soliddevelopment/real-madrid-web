<?php

Route::prefix('admin')->group(function () {
    Route::get('/media', [
        'uses' => 'MediaController@lista',
        'as' => 'admin.media.lista',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
    Route::post('/media/upload', [
        'uses' => 'MediaController@postUpload',
        'as' => 'admin.media.upload',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
    Route::post('/media/summernotePost', [
        'uses' => 'MediaController@summernotePost',
        'as' => 'admin.media.summernotePost',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
    Route::post('/media/borrar', [
        'uses' => 'MediaController@borrar',
        'as' => 'admin.media.borrar',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
    Route::post('/media/edit', [
        'uses' => 'MediaController@edit',
        'as' => 'admin.media.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);

    Route::get('/media/folders', [
        'uses' => 'MediaController@listaFolders',
        'as' => 'admin.media.listaFolders',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
    Route::post('/media/crearFolder', [
        'uses' => 'MediaController@crearFolder',
        'as' => 'admin.media.crearFolder',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
    Route::post('/media/borrarFolder', [
        'uses' => 'MediaController@borrarFolder',
        'as' => 'admin.media.borrarFolder',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
    Route::post('/media/mover', [
        'uses' => 'MediaController@mover',
        'as' => 'admin.media.mover',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);

    Route::post('/media/galeriaPost', [
        'uses' => 'GalleriesController@galeriaPost',
        'as' => 'admin.media.galeriaPost',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin', 'supervisor']
    ]);
});
