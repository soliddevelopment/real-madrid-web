<?php

use Illuminate\Http\Request;

Route::prefix('admin')->group(function () {
    
    Route::get('/login', [
        'uses' => 'Auth\LoginController@showLoginForm',
        'as' => 'admin.auth.login'
    ]);
    Route::post('/login', [
        'uses' => 'Auth\LoginController@login',
        'as' => 'admin.auth.login'
    ]);
    Route::post('/logout', [
        'uses' => 'Auth\LoginController@logout',
        'as' => 'admin.auth.logout'
    ]);
    
    Route::get('/', [
        'uses' => 'HomeController@index',
        'as' => 'admin.home.home',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin' ]
    ]);
    Route::get('/logs', [
        'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
        'as' => 'admin.logs',
        'middleware' => 'admin_roles', 'roles' => ['superadmin', 'admin' ]
    ]);
    
});
