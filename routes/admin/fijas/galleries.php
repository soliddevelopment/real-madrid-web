<?php

Route::prefix('admin')->group(function () {

    Route::get('/galleries', [
        'uses' => 'GalleriesController@index',
        'as' => 'admin.galleries.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/galleries/detail/{id}', [
        'uses' => 'GalleriesController@detail',
        'as' => 'admin.galleries.detail',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/galleries/edit/{id}', [
        'uses' => 'GalleriesController@edit',
        'as' => 'admin.galleries.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/galleries/create', [
        'uses' => 'GalleriesController@edit',
        'as' => 'admin.galleries.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::post('/galleries/store', [
        'uses' => 'GalleriesController@store',
        'as' => 'admin.galleries.store',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::put('/galleries/{id}/update', [
        'uses' => 'GalleriesController@store',
        'as' => 'admin.galleries.update',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::delete('/galleries/{id}/destroy', [
        'uses' => 'GalleriesController@destroy',
        'as' => 'admin.galleries.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin']
    ]);
    Route::post('/galleries/{id}/st', [
        'uses' => 'GalleriesController@st',
        'as' => 'admin.galleries.st',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/galleries/{id}/sort', [
        'uses' => 'GalleriesController@sort',
        'as' => 'admin.galleries.sort',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/galleries/{id}/deleteElem', [
        'uses' => 'GalleriesController@deleteElem',
        'as' => 'admin.galleries.deleteElem',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);


});
