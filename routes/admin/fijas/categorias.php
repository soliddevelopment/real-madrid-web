<?php

Route::prefix('admin')->group(function () {

    Route::get('/categorias/seccion/{seccionSlug}', [
        'uses' => 'CategoriasController@index',
        'as' => 'admin.secciones.categorias.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin' ]
    ]);

    Route::get('/categorias/edit/{seccion_id}/{id}', [
        'uses' => 'CategoriasController@edit',
        'as' => 'admin.secciones.categorias.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin','admin']
    ]);
    Route::get('/categorias/create/{seccion_id}', [
        'uses' => 'CategoriasController@edit',
        'as' => 'admin.secciones.categorias.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin','admin']
    ]);

    Route::post('/categorias/store', [
        'uses' => 'CategoriasController@store',
        'as' => 'admin.secciones.categorias.store',
        'middleware' => 'admin_roles', 'roles' => ['superadmin','admin']
    ]);
    Route::put('/categorias/{id}/update', [
        'uses' => 'CategoriasController@store',
        'as' => 'admin.secciones.categorias.update',
        'middleware' => 'admin_roles', 'roles' => ['superadmin','admin']
    ]);

    Route::delete('/categorias/{id}/destroy', [
        'uses' => 'CategoriasController@destroy',
        'as' => 'admin.secciones.categorias.destroy',
        'middleware' => 'admin_roles', 'roles' => ['superadmin','admin']
    ]);
    Route::post('/categorias/{id}/st', [
        'uses' => 'CategoriasController@st',
        'as' => 'admin.secciones.categorias.st',
        'middleware' => 'admin_roles', 'roles' => ['superadmin','admin']
    ]);

});
