<?php

Route::prefix('admin')->group(function () {
    
    Route::get('/usuarios', [
        'uses' => 'UsersController@index',
        'as' => 'admin.users.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/usuarios/{id}/edit', [
        'uses' => 'UsersController@edit',
        'as' => 'admin.users.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/usuarios/create', [
        'uses' => 'UsersController@edit',
        'as' => 'admin.users.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/usuarios/store', [
        'uses' => 'UsersController@store',
        'as' => 'admin.users.store',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::put('/usuarios/{id}/update', [
        'uses' => 'UsersController@store',
        'as' => 'admin.users.update',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::put('/usuarios/{id}/updatepassword', [
        'uses' => 'UsersController@updatePassword',
        'as' => 'admin.users.updatepassword',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::delete('/usuarios/{id}/destroy', [
        'uses' => 'UsersController@destroy',
        'as' => 'admin.users.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/usuarios/{id}/st', [
        'uses' => 'UsersController@st',
        'as' => 'admin.users.st',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/usuarios/listaAjax', [
        'uses' => 'UsersController@listaAjax',
        'anyData'  => 'datatables.data',
        'as' => 'admin.users.listaAjax',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);

});
