<?php

use Illuminate\Http\Request;

Route::prefix('admin')->group(function () {

    Route::get('/unete', [
        'uses' => 'Custom\UneteController@index',
        'as' => 'admin.intereses.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin' ]
    ]);

    /*Route::delete('/accommodations/{id}/destroy', [
        'uses' => 'Custom\AccommodationsController@destroy',
        'as' => 'admin.custom.accommodations.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);*/

    /*Route::get('/accommodations', [
        'uses' => 'Custom\AccommodationsController@index',
        'as' => 'admin.custom.accommodations.index',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/accommodations/edit/{id}', [
        'uses' => 'Custom\AccommodationsController@edit',
        'as' => 'admin.custom.accommodations.edit',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::get('/accommodations/create', [
        'uses' => 'Custom\AccommodationsController@edit',
        'as' => 'admin.custom.accommodations.create',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/accommodations/store', [
        'uses' => 'Custom\AccommodationsController@store',
        'as' => 'admin.custom.accommodations.store',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::put('/accommodations/{id}/update', [
        'uses' => 'Custom\AccommodationsController@store',
        'as' => 'admin.custom.accommodations.update',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::delete('/accommodations/{id}/destroy', [
        'uses' => 'Custom\AccommodationsController@destroy',
        'as' => 'admin.custom.accommodations.destroy',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);
    Route::post('/accommodations/{id}/st', [
        'uses' => 'Custom\AccommodationsController@st',
        'as' => 'admin.custom.accommodations.st',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);

    Route::post('/accommodations/sort', [
        'uses' => 'Custom\AccommodationsController@sort',
        'as' => 'admin.custom.accommodations.sort',
        'middleware' => 'admin_roles',
        'roles' => ['superadmin', 'admin']
    ]);*/

});

