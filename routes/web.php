<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/media/img', [
    'as' => 'mediaimg',
    'uses' => 'ImageController@mediaImg'
]);

Route::get('/media/file/{id}', [
    'as' => 'mediafile',
    'uses' => 'FilesController@mediaFile'
]);

Route::get('/files/{file_name}', [
    'uses' => 'FilesController@getFile',
    'as' => 'descargar'
]);

Route::get('/pdfviewer/{filename}', [
    'uses' => 'PdfViewerController@index',
    'as' => 'pdfviewer'
]);


