<?php

// PRINCIPALES

Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'publico.home.home'
]);

Route::get('/clinics', [
    'uses' => 'HomeController@clinics',
    'as' => 'publico.home.clinics'
]);

Route::get('/challenge', [
    'uses' => 'HomeController@challenge',
    'as' => 'publico.home.challenge'
]);

Route::get('/realspirit', [
    'uses' => 'HomeController@realspirit',
    'as' => 'publico.home.realspirit'
]);

Route::get('/worldwide', [
    'uses' => 'HomeController@worldwide',
    'as' => 'publico.home.worldwide'
]);

/*Route::post('/contacto', [
    'uses' => 'ContactoController@contactoNuevo',
    'as' => 'publico.contacto.nuevo'
]);*/

Route::get('/gracias', [
    'uses' => 'HomeController@gracias',
    'as' => 'publico.home.gracias'
]);

Route::post('/inscripcion', [
    'uses' => 'ContactoController@nuevaInscripcion',
    'as' => 'publico.inscripcion'
]);
