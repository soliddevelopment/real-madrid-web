<?php

use Illuminate\Http\Request;

// RUTAS PUBLICAS
Route::middleware(['api'])->group(function () {

    Route::get('/datos', [
        'uses' => 'Custom\TotalController@getDatos',
    ]);

    /*Route::get('/accommodations', [
        'uses' => 'Custom\AccommodationsController@getLista',
    ]);
    Route::get('/accommodations/{slug}', [
        'uses' => 'Custom\AccommodationsController@getDetalle',
    ]);

    Route::get('/tours', [
        'uses' => 'Custom\ToursController@getLista',
    ]);
    Route::get('/tours/{slug}', [
        'uses' => 'Custom\ToursController@getDetalle',
    ]);*/

});

// RUTAS CON AUTH
Route::group(['middleware'=>'auth:api'], function () {

});
