const mix = require('laravel-mix');

// *************************************************************************************
// ADMIN

// CSS
mix.styles([
    // ahora los estoy importando desde scss
], 'public/a/css/lib/all.css');

// Tdo el sass admin
mix.sass('resources/assets/admin/css/sass/app.scss', 'public/a/css/sass');

// Header scripts
mix.scripts([
    'resources/assets/admin/js/lib/modernizr.min.js',
    'resources/assets/admin/js/lib/jquery.min.js'
], 'public/a/js/header.js');

// Libs que tienen que ir separadas por conflictos
mix.scripts('resources/assets/admin/js/lib/modernizr.min.js', 'public/a/js/lib/modernizr.min.js');

// Footer scripts
mix.scripts([
    'resources/assets/admin/js/lib/jquery-ui.min.js',
    'resources/assets/admin/js/lib/switchery.min.js',
    'resources/assets/admin/js/lib/skycons.min.js',
    'resources/assets/admin/js/lib/notify.min.js',
    'resources/assets/admin/js/lib/notify-metro.js',
    'resources/assets/admin/js/lib/sweet-alert.js',
    'resources/assets/admin/js/lib/bootstrap-timepicker.min.js',
    'resources/assets/admin/js/lib/bootstrap.min.js',
    'resources/assets/admin/js/lib/detect.js',
    'resources/assets/admin/js/lib/fastclick.js',
    'resources/assets/admin/js/lib/jquery.slimscroll.js',
    'resources/assets/admin/js/lib/jquery.blockUI.js',
    'resources/assets/admin/js/lib/waves.js',
    'resources/assets/admin/js/lib/wow.min.js',
    'resources/assets/admin/js/lib/jquery.nicescroll.js',
    'resources/assets/admin/js/lib/jquery.scrollTo.min.js',
    'resources/assets/admin/js/lib/jquery.fancybox.js',
    'resources/assets/admin/js/lib/toastr.min.js',
    'resources/assets/admin/js/lib/bootstrap-datepicker.js',
    'resources/assets/admin/js/lib/jquery-ui.min.js',
    // 'resources/assets/admin/js/lib/summernote.min.js',
    'resources/assets/admin/js/lib/summernote-bs4.min.js',
    'resources/assets/admin/js/lib/dropzone.js',
    'resources/assets/admin/js/lib/jquery.validate.js',
    'resources/assets/admin/js/lib/datatables.js',
    'resources/assets/admin/js/lib/cropper.js',
    // 'resources/assets/admin/js/lib/jquery-cropper.js',
    'resources/assets/admin/js/lib/axios.js',

    // Minton files
    'resources/assets/admin/js/pages/jquery.core.js',
    'resources/assets/admin/js/pages/jquery.app.js',
    'resources/assets/admin/js/custom/utils.js',
    'resources/assets/admin/js/custom/ajustes.js',
    'resources/assets/admin/js/custom/custom.js',

], 'public/a/js/footer.js');

// Vue lib la incluyo por separado ya q es distinta dependiendo del environment
mix.scripts('resources/assets/admin/js/vue/vue.js', 'public/a/js/vue/vue.js');
mix.scripts('resources/assets/admin/js/vue/vue.js', 'public/a/js/vue/vue.min.js');

// Componentes de vue
mix.js([
    'resources/assets/admin/js/vue/misc.js', // components varios
    'resources/assets/admin/js/vue/wrapper.js', // wrapper de vue para poder incluir components
    'resources/assets/admin/js/vue/loading.js', // wrapper de vue para loading
    'resources/assets/admin/js/vue/media-manager.js', // component separado para importar media
    'resources/assets/admin/js/vue/image-cropper.js', // component separado para editar imagen (se podra conectar con el anterior)
],'public/a/js/vue/all.js');

/*
/!*************************************************************************************
 ***************************************************************************************
PUBLIC
*/

/*mix.js('resources/vue/js/app.js', 'public/p/vue/vueapp.js');
mix.sass('resources/vue/css/app.scss', 'public/p/vue/vueapp.css');*/

mix.styles([
    // 'resources/assets/publico/css/lib/slick.css',
], 'public/p/css/lib/lib.css');

mix.sass('resources/assets/publico/css/sass/app.scss', 'public/p/css/app.css').version();

mix.scripts([
    'resources/assets/publico/js/lib/modernizr.min.js',
], 'public/p/js/header.js');

mix.scripts([
    // Importados de node_modules
    'node_modules/aos/dist/aos.js',
    'node_modules/selectivizr/selectivizr.js',
    'node_modules/jquery/dist/jquery.js',
    'node_modules/jquery.easing/jquery.easing.js',
    'node_modules/jquery-validation/dist/jquery.validate.js',
    'node_modules/jquery-validation/dist/localization/messages_es.js',
    'node_modules/popper.js/dist/umd/popper.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
    'node_modules/pxloader/dist/pxloader-images.js',
    'node_modules/slick-carousel/slick/slick.js',
    'node_modules/vex-js/dist/js/vex.combined.js',
    'node_modules/axios/dist/axios.js',
    'node_modules/toastr/toastr.js',
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
    //'node_modules/jquery.nicescroll/dist/jquery.nicescroll.js',
    //'node_modules/jquery-pjax/jquery.pjax.js',
    // 'node_modules/in-viewport/build/in-viewport.min.js',

    // Importados de lib
    'resources/assets/publico/js/lib/solid/ajaxnav.js',
    'resources/assets/publico/js/lib/tweenmax.min.js',
    'resources/assets/publico/js/lib/jquery-parallax.js',
    'resources/assets/publico/js/lib/parallax.js',
    'resources/assets/publico/js/lib/jquery.sticky.js',
    //'resources/assets/publico/js/lib/jquery.nicescroll.js',

    'resources/assets/publico/js/libconfig.js',
    'resources/assets/publico/js/utils.js',
    'resources/assets/publico/js/custom.js'

], 'public/p/js/footer.js').version();

// mix.scripts('resources/assets/publico/js/lib/history.js', 'public/p/js/lib/history.js');
// mix.scripts('resources/assets/publico/js/lib/history.adapter.jquery.js', 'public/p/js/lib/history.adapter.jquery.js');

// Componentes de vue
/*
mix.js([
    'resources/assets/publico/js/vue/misc.js', // components varios
    'resources/assets/publico/js/vue/wrapper.js', // wrapper de vue para poder incluir components
    'resources/assets/publico/js/vue/loading.js', // wrapper de vue para loading
],'public/p/js/vue/all.js');
*/

// mix.js('resources/assets/publico/js/vue/custom/registro.js', 'public/p/js/vue/custom/registro.js');


/*
/!*************************************************************************************
 ***************************************************************************************
PUBLIC ELIAS
*/
/*

mix.sass('resources/assets/publico/elias/sass/app.scss', 'public/p/css/app.css');

mix.styles([
    'resources/assets/publico/elias/css/jquery.fancybox.min.css',
    'resources/assets/publico/elias/css/slick.css',
    'resources/assets/publico/elias/css/bootstrap.custom.css',
    'resources/assets/publico/elias/css/animate.css',
    'resources/assets/publico/elias/css/nice-select.css',
], 'public/p/css/lib/lib.css');

mix.scripts([

    'resources/assets/publico/elias/js/jquery-3.4.1.min.js',
    'resources/assets/publico/elias/js/slick.min.js',
    'resources/assets/publico/elias/js/jquery.fancybox.min.js',
    'resources/assets/publico/elias/js/jquery.nice-select.min.js',
    'resources/assets/publico/elias/js/jquery.textarea_autosize.js',
	'resources/assets/publico/elias/js/masonry.pkgd.min.js',
    'resources/assets/publico/elias/js/wow.min.js',

    'node_modules/toastr/toastr.js',

    'resources/assets/publico/elias/js/diseno.js',
], 'public/p/js/footer.js');
*/
